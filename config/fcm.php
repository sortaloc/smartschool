<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => 'AAAASLyZkKM:APA91bEy2q080j_mc4PFekXsppM3blAAOWVIwTP7ogAE95i2PAJyxSCPxn8fN7ZoOp_nw10Z-esc4dq9b0GCAyJMd2dv5VtWYoJhYBWXiUdTUPas6qrZViJiwKd2B3tulAfz0f6qWsQ0',
        'sender_id' => '312401825955',
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
