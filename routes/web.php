<?php

//auth
Route::group(["namespace" => "Auth"], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login.store');
    Route::get('/register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('/register', 'RegisterController@register')->name('register.store');
});

Route::group(["middleware" => "isActive"], function () {

    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(["namespace" => "Web"], function () {
        // Route Home
        Route::resource('profile', 'User\ProfileController')->only('index', 'update');
        // Route Home
        Route::resource('home', 'Home\HomeController');
        Route::get('/', 'Home\HomeController@index');
        # User
        Route::resource('/data-user', 'User\UserController');
        Route::get('/data-user-paginate', 'User\UserController@paginate')->name('user.paginate');
        Route::get('/ajax-user', 'User\UserController@ajaxUser')->name('user.ajax');
        Route::get('/export-user', 'User\UserController@export')->name('user-export');

        // Route Configuration
        # Bab
        Route::group(["namespace" => "Konfigurasi"], function () {
            Route::resource('/config-admin', 'AdminController');
            Route::resource('/headline-news', 'HeadlineNewsController');
            Route::resource('/app-version', 'AppVersionController');
            Route::resource('config-wilayah', 'WilayahController')->only('index');
            Route::resource('config-province', 'ProvinceController');
            Route::get('/config-province-datatable', 'ProvinceController@datatable')->name('province.datatable');
            Route::resource('config-city', 'CityController');
            Route::get('/config-city-datatable', 'CityController@datatable')->name('city.datatable');
            Route::resource('config-district', 'DistrictController');
            Route::get('/config-district-datatable', 'DistrictController@datatable')->name('district.datatable');
            //smart school
            Route::resource('config-schools', 'SchoolController');
            Route::resource('config-book-categories', 'BookCategoryController');
            Route::resource('config-libraries', 'LibraryController');
        });

        Route::group(["namespace" => "SmartSchools", "middleware" => "isMySchoolActive"], function () {
            Route::resource('schools', 'SchoolController');
            Route::resource('school-years', 'SchoolYearController');
            Route::resource('school-generations', 'SchoolGenerationController');
            Route::resource('classes', 'ClassController');
            Route::resource('sub-classes', 'SubClassController');
            Route::resource('school-classes', 'SchoolClassController');
            Route::resource('school-students', 'StudentController');
            Route::resource('school-teachers', 'TeacherController');
            Route::resource('school-teacher-profiles', 'TeacherProfileController');
            Route::resource('school-subjects-categories', 'SubjectsCategoryController');
            Route::resource('school-subjects', 'SubjectsController');
            Route::resource('school-teacher-subjects', 'TeacherSubjectsController');
            Route::resource('school-class-subjects', 'ClassSubjectsController');
            Route::resource('school-schedules', 'ScheduleController');
            Route::resource('school-journals', 'JournalController');
            Route::resource('school-assessment-categories', 'AssessmentCategoryController');
            Route::resource('school-assessment-type-refs', 'AssessmentTypeRefController');
            Route::resource('school-assessment-types', 'AssessmentTypeController');
            Route::resource('school-academic-predicates', 'PredicateAcademicController');
            Route::resource('school-character-predicates', 'PredicateCharacterController');
            Route::resource('school-tasks', 'TaskController');
            Route::resource('school-task-questions', 'TaskQuestionController');
            Route::resource('school-task-corrections', 'TaskCorrectionController');
            Route::resource('school-task-results', 'TaskResultController');
            Route::resource('school-book-categories', 'BookCategoryController');
            Route::resource('school-libraries', 'LibraryController');
            Route::resource('school-teacher-files', 'TeacherFileController');
            Route::resource('school-rapors', 'RaporController');
            Route::resource('school-admins', 'AdminController');
            Route::resource('school-class-presences', 'ClassPresenceController');
            Route::resource('school-wall-magazines', 'WallMagazineController');
            Route::resource('school-announcements', 'AnnouncementController');
        });

        Route::group(['prefix' => 'laravel-filemanager'], function () {
            \UniSharp\LaravelFilemanager\Lfm::routes();
        });
    });

    Route::group(["namespace" => "Api"], function () {
        Route::post('/fcm-token', 'ApiFCMController@update')->name('fcm-token');
    });
});
Route::group(["namespace" => "Web\Konfigurasi"], function () {
    Route::get('/ajax-province', 'ProvinceController@ajaxProvince')->name('province.ajax');
    Route::get('/ajax-city', 'CityController@ajaxCity')->name('city.ajax');
    Route::get('/ajax-district', 'DistrictController@ajaxDistrict')->name('district.ajax');
});
