<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'middleware' => 'cors'], function () {
    Route::get('/app-version', 'ApiAppVersionController@index');
    Route::post('/login', 'ApiAuthController@login');
    Route::post('/register', 'ApiAuthController@register');
    Route::get('/check', 'ApiAuthController@check');
    Route::post('/request-reset-password', 'ApiAuthController@password');
    Route::post('/reset-password', 'ApiAuthController@resetPassword');
    Route::post('/midtrans-handler', 'BerlanggananController@midtransHandler');
    Route::get('/paket', 'ApiPaketController@index');
    Route::get('/promo', 'ApiPromoController@index');
    Route::get('/promo/{id}', 'ApiPromoController@show');
    Route::get('/province', 'ApiAdministratifController@province');
    Route::get('/city', 'ApiAdministratifController@city');
    Route::get('/district', 'ApiAdministratifController@district');
    Route::get('get_server_time', 'ApiApplicationController@serverTime');
    Route::get('/headline-news', 'ApiHeadlineNewsController@index');

    Route::group(['middleware' => 'jwt.verify'], function () {
        Route::get('/logout', 'ApiAuthController@logout');
        Route::get('/user', 'ApiAuthController@getAuthenticatedUser');
        Route::get('/activate-token', 'ApiAuthController@requestNewActivateToken');
        Route::post('/activate', 'ApiAuthController@activate');
        Route::post('/change-password', 'ApiAuthController@changePassword');
        Route::post('/change-profile', 'ApiUserController@changeProfile');
        Route::post('/change-avatar', 'ApiUserController@changeAvatar');
        Route::get('/active-paket', 'ApiUserController@activePaket');
        Route::get('/jenjang', 'ApiJenjangController@index');
        Route::get('/kelas', 'ApiKelasController@index');
        Route::get('/kurikulum', 'ApiKurikulumController@index');
        Route::get('/mata-pelajaran', 'ApiMataPelajaranController@index');
        Route::get('/bab', 'ApiBabController@index');
        Route::get('/materi', 'ApiMateriController@index');
        Route::get('/bank-soal', 'ApiBankSoalController@index');
        Route::get('/genius-mengaji', 'ApiGeniusMengajiController@index');
        Route::get('/motivasi-genius', 'ApiMotivasiGeniusController@index');
        Route::get('/pojok-literasi', 'ApiPojokLiterasiController@index');
        Route::get('/super-genius', 'ApiSuperGeniusController@index');
        Route::get('/tryout', 'ApiTryoutController@index');
        Route::get('/kelas/{id}', 'ApiKelasController@show');
        Route::get('/kurikulum/{id}', 'ApiKurikulumController@show');
        Route::get('/mata-pelajaran/{id}', 'ApiMataPelajaranController@show');
        Route::get('/bab/{id}', 'ApiBabController@show');
        Route::post('/berlangganan', 'BerlanggananController@store');
        Route::get('/smart-genius', 'ApiSmartGeniusController@index');
        Route::get('/mentor-genius', 'ApiMentorGeniusController@index');
        Route::post('/mentor-genius/rate', 'ApiMentorGeniusController@rate');
        Route::get('/mentor-genius/last-contact', 'ApiMentorGeniusController@lastContact');
        Route::get('/user-report', 'ApiReportController@index');
        Route::get('/my-report', 'ApiReportController@myReport');
        Route::post('/user-report', 'ApiReportController@store');

        //custom middleware
        Route::get('/materi/{id}', 'ApiMateriController@show');
        Route::get('/super-genius/{id}', 'ApiSuperGeniusController@show');
        Route::get('/rangkuman', 'ApiRangkumanController@index');
        Route::get('/tryout/{id}', 'ApiTryoutController@show');
        Route::get('/latihan-soal', 'ApiLatihanSoalController@index');
        Route::get('/quiz', 'ApiQuizController@index');
        Route::get('/bank-soal/{id}', 'ApiBankSoalController@show');

        Route::group(['middleware' => 'berlangganan'], function () {
            Route::get('/tryout', 'ApiTryoutController@index');
            Route::get('/genius-mengaji/{id}', 'ApiGeniusMengajiController@show');
            Route::get('/motivasi-genius/{id}', 'ApiMotivasiGeniusController@show');
            Route::get('/pojok-literasi/{id}', 'ApiPojokLiterasiController@show');

        });

        //marketing
        Route::get('marketing', 'ApiMarketingController@index');
        Route::post('marketing', 'ApiMarketingController@update');
        Route::post('marketing-withdraw', 'ApiMarketingController@withdraw');
        Route::get('marketing-withdraw-history', 'ApiMarketingController@withdrawHistory');
        Route::get('marketing', 'ApiMarketingController@index');
        Route::get('marketing-member', 'ApiMarketingController@member');

        //guru
        Route::get('sekolah', 'ApiGeniusSchoolController@index');
        Route::get('tugas', 'ApiTugasGuruController@index');
        Route::get('rekap-nilai', 'ApiRekapNilaiGuruController@index');
        Route::group(['prefix' => 'tugas'], function () {
            Route::get('tugas-saya', 'ApiTugasGuruController@myTugas');
            Route::post('kerjakan', 'ApiTugasGuruController@take');
            Route::post('simpan-jawaban', 'ApiTugasGuruController@save');
            Route::post('selesai', 'ApiTugasGuruController@done');
        });

        Route::get('events', 'ApiEventController@index');
        Route::post('take_event', 'ApiEventController@take');
        Route::get('event_transactions', 'ApiEventController@transaction');
        Route::post('confirm_event_transaction', 'ApiEventController@confirmTransaction');
        Route::get('event_questions', 'ApiEventController@question');
        Route::post('save_question_answer', 'ApiEventController@saveAnswer');
        Route::post('finish_event', 'ApiEventController@finish');
        Route::get('event_results', 'ApiEventController@result');
        Route::post('/fcm-token', 'ApiFCMController@update');

        Route::group(['namespace' => 'SmartSchools', 'middleware' => 'haveSchool'], function () {
            Route::get('smartschool', 'SchoolController@index');
            Route::group(['middleware' => 'haveClass'], function () {
                Route::get('smartschool/class', 'ClassController@index');
                Route::get('smartschool/class/students', 'ClassController@student');
                Route::get('smartschool/class/teacher/{id}', 'TeacherController@show');
                Route::post('smartschool/class/join-rooms', 'ClassController@joinRoom');
                Route::get('smartschool/class/teacher/{id}/materi', 'TeacherController@file');
                Route::get('smartschool/class/schedules', 'ScheduleController@index');
                Route::get('smartschool/get_teacher_files', 'TeacherController@file');
                Route::get('smartschool/get_teacher_tasks', 'TeacherController@task');
                Route::post('smartschool/take_teacher_tasks', 'TaskController@take');
                Route::post('smartschool/save_task_answers', 'TaskController@saveAnswer');
                Route::post('smartschool/finish_the_tasks', 'TaskController@done');
                Route::get('smartschool/get_my_tasks', 'TaskController@myTask');
                Route::get('smartschool/get_my_task_progres', 'TaskController@myTaskStatistik');
                Route::get('smartschool/library/categories', 'LibraryController@category');
                Route::get('smartschool/library/books', 'LibraryController@book');
                Route::get('smartschool/get_announcements', 'SchoolController@announcement');
                Route::get('smartschool/get_my_rapor', 'RaporController@rapor');
            });
        });
    });
});
