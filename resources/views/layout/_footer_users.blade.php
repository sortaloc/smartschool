
<script src="{{ asset('template/vendors/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/popper.js/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/metisMenu/metisMenu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('template/vendors/DataTables/datatables.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script src="{{ asset('template/vendors/jquery-knob/jquery.knob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/chart.js/Chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/jvectormap/jquery-jvectormap-us-aea-en.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('template/vendors/bootstrap-tagsinput-latest/js/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script> --}}
<script src="{{ asset('template/vendors/bs4-tags-input/tagsinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/vendors/autosize-master/dist/autosize.js') }}" type="text/javascript"></script>

<script src="{{ asset('template/js/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('template/js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script>

{{-- <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-messaging.js"></script>

<script src="firebase-custom.js"></script> --}}
