<!DOCTYPE html>
<html>
<head>
	<title>Pembayaran Event Genius</title>
</head>
<body>
  
<p>Hallo {{ $details['name'] }}</p>
<p>Silahkan melakukan pembayaran untuk menyelesaikan registrasi Event Genius <strong>{{ $details['event'] }}</strong>, Jika dalam waktu 1x24 jam <strong>({{ \Carbon\Carbon::parse($details['expired'])->isoFormat('DD MMMM Y, H:mm') }})</strong> Anda tidak melakukan pembayaran maka transaksi ini akan otomatis kadaluarsa.</p>
<p>Terimakasih.</p>
<strong>PT GENIUS INDONESIA JAYA</strong>
  
</body>
</html>