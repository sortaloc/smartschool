@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Latihan Soal
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Mata Pelajaran</th>
                    <th>Bab</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                @php
                $total_latihan = 0;
                @endphp
                @foreach ($latihan as $r)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $r->bab->mapel->name }}</td>
                    <td>{{ $r->bab->name }}</td>
                    <td>{{ $r->grade }}</td>
                    @php
                    $total_latihan = $total_latihan + $r->grade;
                    @endphp
                </tr>
                @endforeach
                @if($total_latihan)
                <tr>
                    <td colspan="3" class="text-center"><strong>Total Nilai Latihan</strong></td>
                    <td><strong>{{$total_latihan}}</strong></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Quiz
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Mata Pelajaran</th>
                    <th>Bab</th>
                    <th>Materi</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                @php
                $total_quiz = 0;
                @endphp
                @foreach ($quiz as $q)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $q->materi->bab->mapel->name }}</td>
                    <td>{{ $q->materi->bab->name }}</td>
                    <td>{{ $q->materi->name }}</td>
                    <td>{{ $q->grade }}</td>
                    @php
                    $total_quiz = $total_quiz + $q->grade;
                    @endphp
                </tr>
                @endforeach
                @if($total_quiz)
                <tr>
                    <td colspan="4" class="text-center"><strong>Total Nilai Quiz</strong></td>
                    <td><strong>{{$total_quiz}}</strong></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Tryout
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Tryout</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                @php
                $total_tryout = 0;
                @endphp
                @foreach ($tryout as $t)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $t->tryout_list ? $t->tryout_list->name : '' }}</td>
                    <td>{{ $t->grade }}</td>
                    @php
                    $total_tryout = $total_tryout + $t->grade;
                    @endphp
                </tr>
                @endforeach
                @if($total_tryout)
                <tr>
                    <td colspan="2" class="text-center"><strong>Total Nilai Tryout</strong></td>
                    <td><strong>{{$total_tryout}}</strong></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

