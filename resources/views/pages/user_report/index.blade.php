@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Pilih Kelas
        </div>
    </div>
    <div class="ibox-body">
        <div class="row">
            <div class="col-xl-2">
                <select class="form-control" id="kelas">
                    <option value="" selected>Pilih Kelas</option>
                    @foreach ($kelas as $b)
                    <option value="{{$b->id}}">{{$b->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xl-2">
                <select name="kurikulum_id" onChange="getUrl()" id="data_kurikulum" class="form-control" placeholder="-- Pilih Kurikulum --">
                </select>
            </div>
        </div>
    </div>
</div>
@if($data)
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama</th>
                    <th>No Hp</th>
                    <th>Kabupaten</th>
                    <th>Provinsi</th>
                    <th>Total Nilai</th>
                    <th>Avg. Durasi</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $r)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $r->user->profile->name }}</td>
                    <td>{{ $r->user->profile->phone_number }}</td>
                    <td>{{ $r->user->profile->city->name }}</td>
                    <td>{{ $r->user->profile->city->province->name }}</td>
                    <td>{{ $r->total_grade }}</td>
                    <td>{{ $r->average_duration }}</td>
                    <td class="text-center">
                        <a href="{{ route('user-report.show', $r->id) }}"><span class="badge badge-primary">Detail</span></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
<script>
    $("#kelas").change(function(){
            $.ajax({
                url: "/getKurikulumByKelas/"+$(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#data_kurikulum').html(data.kurikulum);
                }
            });
        });
</script>
<script>
    function getUrl(){
        var id = document.getElementById("data_kurikulum").value;
        if (id) {
            window.location = '/user-report?kurikulum_id='+id;
        }
        return false;
    }
</script>
@endsection

