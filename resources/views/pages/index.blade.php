@extends('pages.home')
@section('content')
@role('Super Admin')
<style>
.bg-gradient-x-primary {
    background-image: -webkit-linear-gradient(left,#008385 0,#00E7EB 100%);
    background-image: linear-gradient(to right,#008385 0,#00E7EB 100%);
    background-repeat: repeat-x;
}
.bg-gradient-x-danger {
    background-image: -webkit-linear-gradient(left,#FF425C 0,#FFA8B4 100%);
    background-image: linear-gradient(to right,#FF425C 0,#FFA8B4 100%);
    background-repeat: repeat-x;
}
.bg-gradient-x-warning {
    background-image: -webkit-linear-gradient(left,#FF864A 0,#FFCAB0 100%);
    background-image: linear-gradient(to right,#FF864A 0,#FFCAB0 100%);
    background-repeat: repeat-x;
}
.bg-gradient-x-success {
    background-image: -webkit-linear-gradient(left,#11A578 0,#32EAB2 100%);
    background-image: linear-gradient(to right,#11A578 0,#32EAB2 100%);
    background-repeat: repeat-x;
}
.text-bold-400 {
    font-weight: 400;
}
.mb-0, .my-0 {
    margin-bottom: 0!important;
}
.h5, h5 {
    font-size: 1.14rem;
}
.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
    margin-bottom: .5rem;
    font-weight: 400;
    line-height: 1.2;
    color: inherit;
}
.white {
    color: #FFF!important;
}
.p-2 {
    padding: 1.5rem!important;
}
.bg-primary.bg-darken-2, .btn-primary.btn-darken-2 {
    background-color: #008385!important;
}
.bg-danger.bg-darken-2, .btn-danger.btn-darken-2 {
    background-color: #FF425C!important;
}
.bg-success.bg-darken-2, .btn-success.btn-darken-2 {
    background-color: #11A578!important;
}
.bg-warning.bg-darken-2, .btn-warning.btn-darken-2 {
    background-color: #FF864A!important;
}
</style>
<div class="row">
    <div class="col-xl-3 col-lg-6 col-12">
        <div class="card">
            <div class="card-content">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-primary bg-darken-2">
                        <i class="fa fa-user-o fa-3x font-large-2 white"></i>
                    </div>
                    <div class="p-2 bg-gradient-x-primary white media-body">
                        <h5>TOTAL ADMIN</h5>
                        <h5 class="text-bold-400 mb-0"><i class="feather icon-plus"></i> {{ App\User::whereHas("roles", function($q){ $q->whereIn("name", ["Super Admin", "Admin"]); })->count() }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        <div class="card">
            <div class="card-content">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-success bg-darken-2">
                        <i class="fa fa-user-o fa-3x font-large-2 white"></i>
                    </div>
                    <div class="p-2 bg-gradient-x-success white media-body">
                        <h5>TOTAL SEKOLAH</h5>
                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-up"></i> {{ App\Models\SmartSchools\School::count() }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        <div class="card">
            <div class="card-content">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-danger bg-darken-2">
                        <i class="fa fa-user-o fa-3x font-large-2 white"></i>
                    </div>
                    <div class="p-2 bg-gradient-x-danger white media-body">
                        <h5>TOTAL GURU</h5>
                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-up"></i> {{ App\Models\SmartSchools\SchoolTeacher::count() }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        <div class="card">
            <div class="card-content">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-warning bg-darken-2">
                        <i class="fa fa-dollar fa-3x font-large-2 white"></i>
                    </div>
                    <div class="p-2 bg-gradient-x-warning white media-body">
                        <h5>TOTAL SISWA</h5>
                        <h5 class="text-bold-400 mb-0"><i class="feather icon-arrow-down"></i> {{ App\Models\SmartSchools\SchoolStudent::count() }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<canvas id="bar-chart" width="900" height="200"></canvas>
<script>
var dates = {!! json_encode($dates) !!}
var newUsers = {!! json_encode($new_users) !!}
new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: getDate(),
      datasets: [
        {
          label: "User Baru",
          backgroundColor: getColor(),
          data: getNewUser()
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Statistik Registrasi User (30 Hari Terakhir)'
      }
    }
});

function getDate(){
    var dateLabels = []
    for(var x in dates){
        dateLabels.push(dates[x])
    }
    return dateLabels
}
function getNewUser(){
    var newUsersData = []
    for(var x in newUsers){
        newUsersData.push(newUsers[x])
    }
    return newUsersData
}
function getColor(){
    var colors = []
    for(var x in dates){
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        colors.push("rgb(" + r + "," + g + "," + b + ")")
    }
    return colors
}
</script>
@endrole
@endsection
