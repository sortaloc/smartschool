@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-generations.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat Tahun Angkatan</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Tahun</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->year }}</td>
                    <td align="center">
                        <form id="{{$data->id}}" action="{{route('school-generations.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button onclick="return confirm('Perhatian! Menghapus Data Tahun Angkatan akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <!-- <a href="{{ route('school-generations.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a> -->
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
