@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Info Sekolah'))
<li>
    <a href="{{ route('schools.index') }}"><i class="sidebar-item-icon fa fa-university"></i>
        <span class="nav-label">Identitas Sekolah</span>
    </a>
</li>
@endif
@if(auth()->user()->level == 'Guru Sekolah')
<li>
    <a href="{{ route('school-teacher-profiles.index') }}"><i class="sidebar-item-icon fa fa-user"></i>
        <span class="nav-label">Profil Guru</span>
    </a>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Kelas'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
        <span class="nav-label">Manajemen Kelas</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('classes.index') }}">Referensi Kelas</a></li>
        <li><a href="{{ route('sub-classes.index') }}">Referensi Sub Kelas</a></li>
        <li><a href="{{ route('school-classes.index') }}">Data Kelas</a></li>
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Info Siswa') || auth()->user()->can('Lihat Info Guru'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
        <span class="nav-label">Personal Data</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Info Siswa'))
        <li><a href="{{ route('school-students.index') }}">Data Siswa</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Info Guru'))
        <li><a href="{{ route('school-teachers.index') }}">Data Guru</a></li>
        @endif
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Mata Pelajaran') || auth()->user()->can('Lihat Jadwal Kelas') || auth()->user()->can('Lihat Jurnal Kelas'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
        <span class="nav-label">Manajemen Kurikulum</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Mata Pelajaran'))
        <li><a href="{{ route('school-subjects-categories.index') }}">Kategori Mata Pelajaran</a></li>
        <li><a href="{{ route('school-subjects.index') }}">Mata Pelajaran</a></li>
        <li><a href="{{ route('school-class-subjects.index') }}">Mata Pelajaran Kelas</a></li>
        <li><a href="{{ route('school-teacher-subjects.index') }}">Guru Mata Pelajaran</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Jadwal Kelas'))
        <li><a href="{{ route('school-schedules.index') }}">Jadwal Kelas</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Jurnal Kelas'))
        <li><a href="{{ route('school-journals.index') }}">Jurnal Kelas</a></li>
        @endif
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Kategori Penilaian') || auth()->user()->can('Lihat Jenis Penilaian') || auth()->user()->can('Lihat Predikat'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
        <span class="nav-label">Bobot Penilaian</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Kategori Penilaian'))
        <li><a href="{{ route('school-assessment-categories.index') }}">Kategori Penilaian</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Jenis Penilaian'))
        <li><a href="{{ route('school-assessment-type-refs.index') }}">Referensi Jenis Penilaian</a></li>
        <li><a href="{{ route('school-assessment-types.index') }}">Jenis Penilaian</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Predikat'))
        <li><a href="{{ route('school-academic-predicates.index') }}">Predikat Akademik</a></li>
        <li><a href="{{ route('school-character-predicates.index') }}">Predikat Karakter</a></li>
        @endif
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Soal'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
        <span class="nav-label">Soal</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('school-tasks.index', ['status' => 'upcoming']) }}">Daftar Soal</a></li>
        <li><a href="{{ route('school-task-results.index',['type' => 'rekap']) }}">Rekap Nilai</a></li>
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Rapor'))
<li>
    <a href="{{ route('school-rapors.index') }}"><i class="sidebar-item-icon fa fa-bar-chart"></i>
        <span class="nav-label">Rapor Siswa</span>
    </a>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Buku'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-book"></i>
        <span class="nav-label">Perpustakaan</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('school-book-categories.index') }}">Kategori Buku</a></li>
    </ul>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('school-libraries.index') }}">Daftar Buku</a></li>
    </ul>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Materi Guru'))
<li>
    <a href="{{ route('school-teacher-files.index') }}"><i class="sidebar-item-icon fa fa-play"></i>
        <span class="nav-label">Daftar Materi Guru</span>
    </a>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Mading'))
<li>
    <a href="{{ route('school-wall-magazines.index') }}"><i class="sidebar-item-icon fa fa-newspaper-o"></i>
        <span class="nav-label">Mading</span>
    </a>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Pengumuman'))
<li>
    <a href="{{ route('school-announcements.index') }}"><i class="sidebar-item-icon fa fa-bullhorn"></i>
        <span class="nav-label">Pengumuman</span>
    </a>
</li>
@endif
@if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Admin') || auth()->user()->can('Lihat Tahun Ajaran') || auth()->user()->can('Lihat Tahun Angkatan'))
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-cog"></i>
        <span class="nav-label">Konfigurasi Lainnya</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Admin'))
        <li><a href="{{ route('school-admins.index') }}">Data Admin</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Tahun Ajaran'))
        <li><a href="{{ route('school-years.index') }}">Tahun Ajaran</a></li>
        @endif
        @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Lihat Tahun Angkatan'))
        <li><a href="{{ route('school-generations.index') }}">Tahun Angkatan</a></li>
        @endif
    </ul>
</li>
@endif
