<form id="{{$val->id}}" action="{{route('school-students.destroy',$val->id)}}" method="post" style="display:inline">
    @method('DELETE')
    @csrf
    <button onclick="return confirm('Perhatian! Menghapus Data Siswa akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
</form>
{{ Form::open(['id' => 'editStatus'.$val->id, 'route' => ['school-students.update', $val->id], 'method' => 'PATCH', 'class' => 'd-inline']) }}
    @if($val->status == 0)
    <input type="hidden" name="update_status" value="1">
    {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success', 'data-toggle' => 'tooltip', ' data-placement' => 'top', 'title' => 'Aktifkan Akun']) }}
    @else
    <input type="hidden" name="update_status" value="0">
    {{ Form::button('<i class="fa fa-ban"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'data-toggle' => 'tooltip', ' data-placement' => 'top', 'title' => 'Nonaktifkan Akun']) }}
    @endif
{{ Form::close() }}
<a href="{{ route('school-students.edit', $val->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
