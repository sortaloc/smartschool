@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-students.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah Siswa</a>
            <a href="/img/template_upload_siswa.xlsx" class="btn btn-sm btn-outline-primary"><i class="fa fa-download"></i> Template</a>
            {{ Form::open(['id' => 'bulk-import', 'route' => ['school-students.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'd-inline']) }}
            <a href="#" onclick="$('#bulk').click();" class="btn btn-sm btn-outline-primary"><i class="fa fa-upload"></i> Import Data</a>
            <input onchange="document.getElementById('bulk-import').submit();" type="file" name="bulk_import" id="bulk" class="d-none" accept=".xlsx">
            {{ Form::close() }}
        </div>
        <a href="{{ Request::fullUrl() }}?download=true" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>

    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @else
        @include('flash')
        @endif
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Lengkap</th>
                    <th>NISN</th>
                    <th>Email</th>
                    <th>Kelas</th>
                    <th>Angkatan</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#table2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('school-students.index') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'NISN', name: 'NISN' },
                    { data: 'user.email', name: 'user.email' },
                    { data: 'class[0].class.class_name', name: 'class[0].class.class_name', "defaultContent": "Belum ada kelas", orderable: false, searchable: false},
                    { data: 'generation.year', name: 'generation.year', "width": "80px", orderable: false, searchable: false },
                    { data: 'action', name: 'action', "width": "120px", orderable: false, searchable: false}
                    ]
        });
    });
</script>
@endsection
