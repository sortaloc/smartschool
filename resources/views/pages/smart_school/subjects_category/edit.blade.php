@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Update', 'route' => ['school-subjects-categories.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Kategori Mata Pelajaran', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('kategori_mata_pelajaran', $data->name, ['class' => 'form-control', 'placeholder' => 'Muatan Nasional, Muatan Lokal Dll ']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Edit', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
$("#datepicker1").datepicker( {
    format: " yyyy", // Notice the Extra space at the beginning
    viewMode: "years",
    minViewMode: "years",
    orientation: "bottom"
});
$("#datepicker2").datepicker( {
    format: " yyyy", // Notice the Extra space at the beginning
    viewMode: "years",
    minViewMode: "years",
    orientation: "bottom"
});
</script>
@endsection
