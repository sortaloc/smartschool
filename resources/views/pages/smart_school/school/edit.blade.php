@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'schoolEdit', 'route' => ['schools.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_sekolah', $data->name, ['class' => 'form-control', 'placeholder' => 'Nama Sekolah', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NPSN', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NPSN', $data->NPSN, ['class' => 'form-control', 'placeholder' => 'NPSN', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIS/NSS/NDS', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NIS_NSS_NDS', $data->NDS, ['class' => 'form-control', 'placeholder' => 'NIS/NSS/NDS', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Jenjang Sekolah (lengkap)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('jenjang', $data->jenjang, ['class' => 'form-control', 'placeholder' => 'contoh : SEKOLAH MENENGAH KEJURUAN']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Alamat', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('alamat', $data->address, ['class' => 'form-control', 'placeholder' => 'Alamat Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('provinsi', 'Provinsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="provinsi" class="province form-control">
                            @if($data->district_id)
                                <option value="{{ $data->district->city->province_id }}">{{ $data->district->city->province->name }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('kabupaten', 'Kota / Kabupaten', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="kabupaten" class="city form-control">
                            @if($data->district_id)
                                <option value="{{ $data->district->city_id }}">{{ $data->district->city->type }} {{ $data->district->city->name }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('kec', 'Kecamatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="kecamatan" class="district form-control">
                            @if($data->district_id)
                                <option value="{{ $data->district_id }}">{{ $data->district->name }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Desa / Kelurahan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('desa', $data->village, ['class' => 'form-control', 'placeholder' => 'Desa / Kelurahan Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kode Pos', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('kode_pos', $data->zip_code, ['class' => 'form-control', 'placeholder' => 'Kode Pos Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Fax', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('fax', $data->fax, ['class' => 'form-control', 'placeholder' => 'Fax Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'No. Telp', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('no_telp', $data->phone, ['class' => 'form-control', 'placeholder' => 'No. Telp Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Website', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('website', $data->website, ['class' => 'form-control', 'placeholder' => 'Alamat Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Email', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('email', $data->email, ['class' => 'form-control', 'placeholder' => 'Email Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Nama Kepala Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_kepala_sekolah', $data->headmaster, ['class' => 'form-control', 'placeholder' => 'Nama Kepala Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIP Kepala Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nip_kepala_sekolah', $data->headmaster_nip, ['class' => 'form-control', 'placeholder' => 'NIP Kepala Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'TTD Kepala Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload">
                            <div class="image-edit">
                                <input name="ttd_kepala_sekolah" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview" style="background-image: url('{{$data->headmaster_signature}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Logo', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload">
                            <div class="image-edit">
                                <input name="logo" type='file' id="imageUpload2" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload2"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview2" style="background-image: url('{{$data->logo}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Ubah Info Sekolah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
var $province_id = {{ $data->district_id ? $data->district->city->province_id : 'null' }};
var $city_id = {{ $data->district_id ? $data->district->city_id : 'null' }};
$('.province').select2({
    ajax: {
        url: '/ajax-province',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.province').on('change', function(){
    $province_id = $(this).val();
});
$('.city').select2({
    ajax: {
        url: '/ajax-city',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                province_id: $province_id, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.city').on('change', function(){
    $city_id = $(this).val();
});
$('.district').select2({
    ajax: {
        url: '/ajax-district',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                city_id: $city_id, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview2').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview2').hide();
            $('#imagePreview2').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload2").change(function() {
    readURL2(this);
});
</script>
@endsection
