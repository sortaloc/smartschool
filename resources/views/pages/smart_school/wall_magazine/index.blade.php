@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-wall-magazines.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Judul</th>
                        <th>Pengunggah</th>
                        <th width="200">Logo</th>
                        <th>File</th>
                        <th>status</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $data->title }}</td>
                        <td>{{ $data->user->name }}</td>
                        <td>
                            <img src="{{ $data->logo }}" alt="">
                        </td>
                        <td>
                            <a href="{{ $data->file }}" class="badge badge-primary" target="_blank">Buka</a>
                        </td>
                        <td>
                            @if($data->status == 0)
                                <span class="badge badge-danger">Arsip</span>
                            @elseif($data->status == 1)
                                <span class="badge badge-success">Publish</span>
                            @endif
                        </td>
                        <td>
                            {{ Form::open(['id' => 'destroy'.$data->id, 'route' => ['school-wall-magazines.destroy', $data->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            <a href="{{ route('school-wall-magazines.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                            {{ Form::open(['id' => 'edit'.$data->id, 'route' => ['school-wall-magazines.update', $data->id], 'method' => 'PATCH', 'class' => 'd-inline']) }}
                            @if($data->status == 0)
                            <input type="hidden" name="update_status" value="1">
                            {{ Form::button('<i class="fa fa-check"></i>', ['type' => 'submit', 'class' => 'btn btn-success']) }}
                            @else
                            <input type="hidden" name="update_status" value="0">
                            {{ Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            @endif
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
