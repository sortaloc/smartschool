@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Update', 'route' => ['school-wall-magazines.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Judul Mading', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('judul', old('judul') ? old('judul') : $data->title, ['class' => 'form-control', 'placeholder' => '']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Logo', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload">
                            <div class="image-edit">
                                <input name="logo" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview" style="background-image: url('{{$data->logo}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'File (hiraukan jika tidak diubah)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::file('file', ['class' => 'form-control', 'accept' => '.pdf']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Simpan', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
</script>
@endsection
