@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Update', 'route' => ['school-character-predicates.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Batas Bawah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('batas_bawah', old('batas_bawah') ? old('batas_bawah') : $data->lower_limit, ['class' => 'form-control', 'placeholder' => 'Gunakan titik "." untuk bilangan desimal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Batas Atas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('batas_atas', old('batas_atas') ? old('batas_atas') : $data->upper_limit, ['class' => 'form-control', 'placeholder' => 'Gunakan titik "." untuk bilangan desimal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Deskripsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('deskripsi', old('deskripsi') ? old('deskripsi') : $data->description, ['class' => 'form-control', 'placeholder' => 'contoh : A, Baik, B, Cukup Baik']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Edit', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endsection
