@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            @if(auth()->user()->can('Semua Hak Akses') || auth()->user()->can('Tambah Jadwal Kelas'))
            <a href="{{ route('school-schedules.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
            @endif
        </div>
        @php
            $label = '?';
            if(isset($_GET["class_id"])){
                $label = '&';
            }
            @endphp
            <a href="{{ Request::fullUrl() }}{{ $label }}download=true" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>
    <div class="ibox-body">
        @include('flash')
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getRekap()" class="form-control d-inline pull-left" id="class">
                    <option value="">- Semua Kelas -</option>
                    @foreach($classes->unique('school_class_id') as $class)
                    <option {{ request()->input('class_id') == $class->school_class_id ? 'selected' : '' }} value="{{ $class->school_class_id }}">{{ $class->class->class_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Hari</th>
                    <th>Waktu</th>
                    <th>Mapel</th>
                    <th>Pengampu</th>
                    <th>Kelas</th>
                    <th>No HP</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->day }}</td>
                    <td>{{ $data->start_at }} - {{ $data->end_at }}</td>
                    <td>{{ $data->teacher_subjects->subjects->name }}</td>
                    <td>{{ $data->teacher_subjects->teacher->name }}</td>
                    <td>{{ $data->teacher_subjects->class->class_name }}</td>
                    <td>{{ $data->teacher_subjects->teacher->phone }}</td>
                    <td align="center">
                        <form id="{{$data->id}}" action="{{route('school-schedules.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button onclick="return confirm('Perhatian! Menghapus Data Jadwal akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-schedules.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
function getRekap(){
    var $class_id = $('#class').val()
    window.location.href = '/school-schedules?class_id='+$class_id;
}
</script>
@endsection
