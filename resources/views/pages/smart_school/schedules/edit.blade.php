@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-schedules.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Kelas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text(null, $data->teacher_subjects->class->class_name, ['class' => 'form-control', 'id' => 'teacher', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Hari', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('hari', ['SENIN' => 'Senin', 'SELASA' => 'Selasa', 'RABU' => 'Rabu', 'KAMIS' => 'Kamis', 'JUMAT' => 'Jumat', 'SABTU' => 'Sabtu', 'MINGGU' => 'Minggu'], old('hari') ? old('hari') : $data->day, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Waktu', ['class' => 'font-bold']) }}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Mulai', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::time('waktu_mulai',old('waktu_mulai') ? old('waktu_mulai') : $data->start_at, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Selesai', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::time('waktu_selesai',old('waktu_selesai') ? old('waktu_selesai') : $data->end_at, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Mata Pelajaran', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text(null, $data->teacher_subjects->subjects->name, ['class' => 'form-control', 'id' => 'teacher', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Pengampu', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text(null, $data->teacher_subjects->teacher->name, ['class' => 'form-control', 'id' => 'teacher', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Ubah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
var $result = [];
$select2 = $('.subjects').select2({
    ajax: {
        url: '/school-teacher-subjects',
        delay: 500,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term,
                class_id: $('#class_id').val(),
                page_limit: 10
            }
        },
        'success': function(data) {
            $result = data.results;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.subjects').on('change', function(){
    var $key = $(this).val();
    var $teacher = $result.find(x => x.id == $key).teacher;
    $('#teacher').val($teacher)
});
</script>
@endsection
