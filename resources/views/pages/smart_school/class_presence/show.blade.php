@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-class-presences.show', [$journal->id, 'status' => 'H']) }}" class="btn badge {{ app('request')->input('status') == 'H' ? 'badge-success' : 'badge-secondary' }}">Hadir</a>
            <a href="{{ route('school-class-presences.show', [$journal->id, 'status' => 'T']) }}" class="btn badge {{ app('request')->input('status') == 'T' ? 'badge-warning' : 'badge-secondary' }}">Telat</a>
            <a href="{{ route('school-class-presences.show', [$journal->id, 'status' => 'I']) }}" class="btn badge {{ app('request')->input('status') == 'I' ? 'badge-primary' : 'badge-secondary' }}">Izin</a>
            <a href="{{ route('school-class-presences.show', [$journal->id, 'status' => 'S']) }}" class="btn badge {{ app('request')->input('status') == 'S' ? 'badge-info' : 'badge-secondary' }}">Sakit</a>
            <a href="{{ route('school-class-presences.show', [$journal->id, 'status' => 'A']) }}" class="btn badge {{ app('request')->input('status') == 'A' ? 'badge-danger' : 'badge-secondary' }}">Alfa</a>
            <a href="{{ route('school-class-presences.show', $journal->id) }}" class="btn badge {{ app('request')->input('status') == '' ? 'badge-success' : 'badge-secondary' }}">Semua</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Siswa</th>
                    <th>Waktu Masuk</th>
                    <th>Status</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($presences as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->student->name }}</td>
                    <td>{{ $data->joined_at }}</td>
                    <td>{{ $data->status }}
                        @if($data->presence == 'H')
                            <span class="badge badge-success">Hadir</span>
                        @elseif($data->presence == 'A')
                            <span class="badge badge-danger">Alfa</span>
                        @elseif($data->presence == 'T')
                            <span class="badge badge-warning">Telat</span>
                        @elseif($data->presence == 'S')
                            <a href="{{ $data->letter }}" target="_blank" class="badge badge-primary">Sakit</a>
                        @elseif($data->presence == 'I')
                            <span href="{{ $data->letter }}" target="_blank" class="badge badge-info">Izin</span>
                        @endif
                    </td>
                    <td align="center">
                        <!-- <form id="{{$data->id}}" action="{{route('school-class-presences.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button onclick="return confirm('Perhatian! Menghapus Data Jadwa akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form> -->
                        <a href="{{ route('school-class-presences.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
    $('#table2').dataTable( {
        "pageLength": 50
    } );
</script>
@endsection
