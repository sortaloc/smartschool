@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-years.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Tahun Ajaran</th>
                    <th>Semester</th>
                    <th>Status</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->start_year }} / {{ $data->end_year }}</td>
                    <td>{{ $data->semester }}</td>
                    <td>{{ $data->is_active }}</td>
                    <td align="center">
                        @if($data->getOriginal('is_active') == 0)
                        <form id="{{$data->id}}" action="{{route('school-years.update',$data->id)}}" method="post" style="display:inline">
                            @method('PATCH')
                            @csrf
                            <input type="hidden" name="update_status" value="1">
                            <button class="btn btn-success" type="submit" value="Delete"><i class="fa fa-check"></i></button>
                        </form>
                        <form id="del{{$data->id}}" action="{{route('school-years.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger" type="submit" value="Delete"><i class="fa fa-trash"></i></button>
                        </form>
                        @endif
                        <a href="{{ route('school-years.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
