@extends('pages.home')
@section('content')
<link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-admins.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama Guru', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('guru', $teachers, old('guru'), ['class' => 'form-control admin', 'placeholder' => '- Pilih Guru -', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Permission / Hak Akses', ['class' => 'font-bold']) }}
                    <div class="input-group">
                        <div>
                        @foreach($permissions as $key => $p)
                        <div>
                            <div class="pretty p-default">
                                <input type="checkbox" {{ in_array($p, old('permissions') ? old('permissions') : []) ? 'checked' : '' }} name="permissions[]" value="{{$p}}"/>
                                <div class="state p-primary">
                                    <label>{{ $p }}</label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
 $('.admin').select2()
</script>
@endsection
