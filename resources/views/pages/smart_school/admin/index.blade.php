@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-admins.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah Admin</a>

        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Nama Admin</th>
                        <th>Roles</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $data->user ? $data->user->name : $data->user->email }}</td>
                        <td>
                            @foreach($data->user->getRoleNames() as $r)
                            {{$r}},
                            @endforeach
                        </td>
                        <td>
                            @if($data->user->hasRole('Admin Sekolah'))
                            {{ Form::open(['id' => 'destroy'.$data->id, 'route' => ['school-admins.destroy', $data->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            <a href="{{ route('school-admins.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
