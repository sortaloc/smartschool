@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-tasks.index') }}" class="btn btn-warning">Kembali</a>
        </div>
            <a href="{{ route('school-task-results.create', ['class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getRekap()" class="form-control d-inline pull-left" id="class">
                    <option value="">- Pilih Kelas -</option>
                    @foreach($classes->unique('school_class_id') as $class)
                    <option {{ request()->input('class_id') == $class->school_class_id ? 'selected' : '' }} value="{{ $class->school_class_id }}">{{ $class->class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getRekap()" class="form-control d-inline pull-left" id="subjects">
                    <option value="">- Pilih Mata Pelajaran -</option>
                    @foreach($classes->unique('school_subjects_id') as $class)
                    <option {{ request()->input('subjects_id') == $class->school_subjects_id ? 'selected' : '' }} value="{{ $class->school_subjects_id }}">{{ $class->subjects->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Siswa</th>
                    @foreach($tasks->groupBy('assessment_type_ref_id') as $key => $task)
                    @foreach($task as $key => $value)
                    <th>{{$value->assessment_type->name}} {{ $key+1 }}</th>
                    @endforeach
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $val)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $val['name'] }}</td>

                    @foreach ($val['results'] as $val2)
                        <td>{{ $val2 }}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
function getRekap(){
    var $class_id = $('#class').val()
    var $subjects_id = $('#subjects').val()
    if($class_id && $subjects_id){
        window.location.href = '/school-task-results?type=rekap&class_id='+$class_id+'&subjects_id='+$subjects_id;
    }
}
</script>
@endsection

