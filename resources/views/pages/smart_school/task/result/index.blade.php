@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-tasks.index', ['class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-warning">Kembali</a>
        </div>
            <a href="#" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="mb-3">
            <strong>Nama Tugas : {{ $results->first() ? $results->first()->task->name : '' }}</strong>
        </div>
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="class">
                    <option value="">- Pilih Kelas -</option>
                    @foreach($classes as $class)
                    <option {{ request()->input('class_id') == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="subjects">
                    <option value="">- Pilih Mata Pelajaran -</option>
                    @foreach($subjects as $key => $value)
                    <option {{ request()->input('subjects_id') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Siswa</th>
                    <th>NISN</th>
                    <th>Kelas</th>
                    <th>Nilai</th>
                    <th width="40">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($results as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $d->student->name }}</td>
                    <td>{{ $d->student->NISN }}</td>
                    <td>{{ $d->student->class->first() ? $d->student->class->first()->class->class_name : 'Belum Ada Kelas' }}</td>
                    <td><b>{{ $d->point }}</b></td>
                    <td>
                        <a href="{{ route('school-task-results.edit', [$d->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Edit Nilai"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$('.select2').select2({
});

function getTask(){
    var $class_id = $('#class').val()
    var $subjects_id = $('#subjects').val()
    var $task_id = {{ request()->input('task_id') }}
    if($class_id && !$subjects_id){
        window.location.href = '/school-task-results?task_id='+$task_id+'&class_id='+$class_id;
    }else if($class_id && $subjects_id){
        window.location.href = '/school-task-results?task_id='+$task_id+'&class_id='+$class_id+'&subjects_id='+$subjects_id;
    }else{
        alert('Pilih kelas');
        window.location.href = '/school-task-results?task_id='+$task_id;
    }
}
</script>
@endsection

