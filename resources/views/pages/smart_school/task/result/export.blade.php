<table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th width="10">No</th>
            <th>Nama Siswa</th>
            @foreach($tasks->groupBy('assessment_type_ref_id') as $key => $task)
            @foreach($task as $key => $value)
            <th>{{$value->assessment_type->name}} {{ $key+1 }}</th>
            @endforeach
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($datas as $val)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $val['name'] }}</td>

            @foreach ($val['results'] as $val2)
                <td>{{ $val2 }}</td>
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>

