@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title" id="status">
            @php
                $status = app('request')->input('status');
            @endphp
            {{ $title }} |
            <a href="{{ route('school-tasks.create') }}" class="btn btn-sm btn-outline-primary"> <i class="fa fa-plus"></i> Buat Soal</a> |
            <button data-status="upcoming" onclick="setStatus('upcoming')" class="btn badge status {{ $status == 'upcoming' ? 'badge-success' : 'badge-secondary' }}">Soal Mendatang</button>
            <button data-status="all" onclick="setStatus('all')" class="btn badge status {{ $status == 'all' ? 'badge-success' : 'badge-secondary' }}">Semua Soal</button>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="class">
                    <option value="">- Pilih Kelas -</option>
                    @foreach($classes as $class)
                    <option {{ request()->input('class_id') == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="subjects">
                    <option value="">- Pilih Mata Pelajaran -</option>
                    @foreach($subjects as $key => $value)
                    <option {{ request()->input('subjects_id') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    <th>Jenis</th>
                    <th>Kelas</th>
                    <th width="50">Siswa Yg Mengerjakan</th>
                    <th width="140" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $d->name }}</td>
                    <td>{{ $d->description }}</td>
                    <td>{{ $d->question_type }}</td>
                    <td>
                        @foreach($d->task_classes as $class)
                        <div><strong>{{ $class->teacher_subjects->class->class_name }} - {{ $class->teacher_subjects->subjects->name }}</strong> - ({{ $class->teacher_subjects->teacher->name }}) ({{ $class->start_at }} - {{ $class->end_at }})</div>
                        @endforeach
                    </td>
                    <td>{{ $d->results->count() }}</td>
                    <td class="text-center">
                        <form id="{{$d->id}}" action="{{route('school-tasks.destroy',$d->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" question_type="submit" value="Delete" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-tasks.edit', $d->id) }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
                        <a href="{{ route('school-task-questions.create', ['task_id' => $d->id]) }}" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Daftar Soal"><i class="fa fa-list"></i></a>
                        <form id="edit{{$d->id}}" action="{{route('school-tasks.update',$d->id)}}" method="post" style="display:inline">
                            @method('PATCH')
                            @csrf
                            <input type="hidden" name="type" value="change_status">
                            @if($d->is_visible === 0)
                            <input type="hidden" name="is_visible" value="1">
                            <button onclick="return confirm('Yakin Ingin Menampilkan Soal Ini ?')"  class="btn  btn-danger" type="submit"><i class="fa fa-eye-slash"></i></button>
                            @elseif($d->is_visible === 1)
                            <input type="hidden" name="is_visible" value="0">
                            <button onclick="return confirm('Yakin Ingin Menyembunyikan Soal Ini ?')"  class="btn  btn-success" type="submit"><i class="fa fa-eye"></i></button>
                            @endif
                        </form>
                        <a href="{{ route('school-task-corrections.index', ['task_id' => $d->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}"><button class="btn btn-primary mt-1" data-toggle="tooltip" data-placement="top" title="Koreksi">Koreksi</button></a>
                        <a href="{{ route('school-task-results.index', ['task_id' => $d->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-success mt-1" data-toggle="tooltip" data-placement="top" title="Hasil">Hasil</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$('.select2').select2({
});

var status = $('#status .badge-success').data('status');

function getTask(){
    var $class_id = $('#class').val()
    var $subjects_id = $('#subjects').val()
    if($class_id && !$subjects_id){
        window.location.href = '/school-tasks?class_id='+$class_id+'&status='+status;
    }else if($class_id && $subjects_id){
        window.location.href = '/school-tasks?class_id='+$class_id+'&subjects_id='+$subjects_id+'&status='+status;
    }else{
        alert('Pilih kelas');
        window.location.href = '/school-tasks'+$class_id+'?status='+status;
    }
}
function setStatus($var){
    status = $var;
    getTask();
}
</script>
@endsection

