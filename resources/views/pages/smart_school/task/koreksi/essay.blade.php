@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'koreksiCreate', 'route' => ['school-task-corrections.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                    <input type="hidden" name="answer_id" value="{{ $data->id }}">
                    <div class="form-group">
                        {{ Form::label('soal', 'Soal', ['class' => 'font-bold']) }}
                        <div class="input-group-icon left">
                            <div class="card p-2 wrap-text">{!! $data->question->question !!}</div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('jawaban', 'Jawaban Text', ['class' => 'font-bold']) }}
                        <div class="input-group-icon left">
                            <div class="card p-2 wrap-text">{!! $data->text_answer !!}</div>
                        </div>
                    </div>
                    @if($data->file_answer)
                    <div class="form-group">
                        {{ Form::label('jawaban', 'Jawaban File', ['class' => 'font-bold']) }}
                        <div class="input-group-icon left">
                            <a target="_blank" href="{{ $data->file_answer }}" class="badge badge-primary">Buka</a>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        {{ Form::label('koreksi', 'Koreksi (boleh kosong)', ['class' => 'font-bold']) }}
                        <div class="input-group-icon left">
                            <div class="input-icon"></div>
                            <textarea class="form-control" id="summary-ckeditor" name="koreksi" row="40">{{ old('koreksi') ? old('koreksi') : $data->correction }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                    {{ Form::label('nilai', 'Nilai', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nilai', old('nilai') ? old('nilai') : $data->point, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Nilai']) }}
                    </div>
                </div>
                    <div class="form-group">
                        <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                        {{ Form::button('<i class="fa fa-save"></i> Koreksi', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('template/vendors/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
<script>
    CKEDITOR.replace( 'editor2', {
	filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
	filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
} );
</script>
@endsection
