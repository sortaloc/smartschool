@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-task-corrections.index', ['task_id' => $task->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-warning">Kembali</a>
            @if($previous_result)
            <a href="{{ route('school-task-corrections.create', ['result_id' => $previous_result->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-info">Siswa Sebelumnya</a>
            @endif
            @if($next_result)
            <a href="{{ route('school-task-corrections.create', ['result_id' => $next_result->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-primary">Siswa Selanjutnya</a>
            @endif
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5" class="text-center">No Soal</th>
                    <th width="400" class="text-center">Soal</th>
                    <th width="400" class="text-center">Jawaban</th>
                    @if($task->question_type == 'Pilihan Ganda')
                    <th width="400" class="text-center">Jawaban Benar</th>
                    @else
                    <th width="400" class="text-center">Nilai</th>
                    <th width="40" class="text-center">Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td class="soal">{!! substr($d->question->question, 0, 200) !!} {{ strlen($d->question->question) >= 200 ? '...' : '' }}</td>
                    @if($task->question_type == 'Pilihan Ganda')
                    <td class="text-center"><span class="badge {{ $d->option_answer == $d->question->correct_answer ? 'badge-success' : 'badge-danger' }}"><h3>{{ $d->option_answer }}</h3></span></td>
                    <td class="text-center"><h3>{{ $d->question->correct_answer }}</h3></td>
                    @else
                    <td class="soal">@if($d->file_answer)<div><a target="_blank" href="{{ $d->file_answer }}" class="badge badge-primary">Buka Jawaban File</a></div>@endif @if($d->text_answer){!! substr($d->text_answer, 0, 200) !!} {{ strlen($d->text_answer) >= 200 ? '...' : '' }} @endif </td>
                    <td class="text-center"><h3>{{ $d->point }}</h3></td>
                    <td class="text-center">
                        <a href="{{ route('school-task-corrections.edit', [$d->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-info">Koreksi</a>
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

