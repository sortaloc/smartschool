@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-tasks.index', ['class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}" class="btn btn-warning">Kembali</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="mb-3">
            <strong>Nama Tugas : {{ $task->name }}</strong>
        </div>
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="class">
                    <option value="">- Pilih Kelas -</option>
                    @foreach($classes as $class)
                    <option {{ request()->input('class_id') == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select onchange="getTask()" class="form-control d-inline pull-left select2" id="subjects">
                    <option value="">- Pilih Mata Pelajaran -</option>
                    @foreach($subjects as $key => $value)
                    <option {{ request()->input('subjects_id') == $key ? 'selected' : '' }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10" class="text-center">No</th>
                    <th class="text-center">Nama Siswa</th>
                    <th class="text-center">Kelas</th>
                    <th class="text-center">Mulai Mengerjakan</th>
                    <th class="text-center">Selesai Mengerjakan</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Status</th>
                    <th width="100" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $d->student->name }}</td>
                    <td>{{ $d->task_class ? $d->task_class->teacher_subjects->class->class_name : 'Tidak ada kelas' }}</td>
                    <td>{{ date('d-M-Y H:i', strtotime($d->created_at)) }}</td>
                    <td>
                        @if($d->finish)
                            {{date('d-M-Y H:i', strtotime($d->finish))}}
                        @else
                            <span class="badge badge-danger">belum selesai</span>
                        @endif
                    </td>
                    <td class="text-center"><b>{{ $d->point }}</b></td>
                    <td class="text-center">
                        @if($d->status == 'Proses')
                            <span class="badge badge-warning">sedang mengerjakan</span>
                        @elseif($d->status == 'Sedang Dikoreksi')
                            <span class="badge badge-danger">belum dikoreksi</span>
                        @elseif($d->status == 'Selesai')
                            <span class="badge badge-success">selesai</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ route('school-task-corrections.create', ['result_id' => $d->id, 'class_id' => request()->input('class_id'), 'subjects_id' => request()->input('subjects_id')]) }}"><button class="btn btn-info">Koreksi</button></a>
                        @if($d->status != 'Proses')
                        <form id="{{$d->id}}" action="{{route('school-task-results.update',$d->id)}}" method="post" style="display:inline">
                            @method('PATCH')
                            @csrf
                            <input type="hidden" name="kesempatan" value="1">
                            <button onclick="return confirm('Yakin Ingin Memberi Kesempatan Siswa Mengerjakan Lagi ?')"  class="btn  btn-warning" type="submit" value="Delete"><i class="fa fa-refresh"></i></button>
                        </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
$('.select2').select2({
});

function getTask(){
    var $class_id = $('#class').val()
    var $subjects_id = $('#subjects').val()
    var $task_id = {{ request()->input('task_id') }}
    if($class_id && !$subjects_id){
        window.location.href = '/school-task-corrections?task_id='+$task_id+'&class_id='+$class_id;
    }else if($class_id && $subjects_id){
        window.location.href = '/school-task-corrections?task_id='+$task_id+'&class_id='+$class_id+'&subjects_id='+$subjects_id;
    }else{
        alert('Pilih kelas');
        window.location.href = '/school-task-corrections?task_id='+$task_id;
    }
}
</script>
@endsection

