{{-- Option --}}
{{-- End Option --}}
<div class="modal fade bd-example-modal-lg" id="modal-option-{{$q->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pilihan</th>
                        <th>Jawaban</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>A</td>
                        <td class="pilihan_jawaban">{!! $q->option->A !!}</td>
                    </tr>
                    <tr>
                        <td>B</td>
                        <td class="pilihan_jawaban">{!! $q->option->B !!}</td>
                    </tr>
                    <tr>
                        <td>C</td>
                        <td class="pilihan_jawaban">{!! $q->option->C !!}</td>
                    </tr>
                    <tr>
                        <td>D</td>
                        <td class="pilihan_jawaban">{!! $q->option->D !!}</td>
                    </tr>
                    @if($q->option->E)
                    <tr>
                        <td>E</td>
                        <td class="pilihan_jawaban">{!! $q->option->E !!}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>