@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div><strong>Nama Tugas : {{ $data->task->name }}</strong></div>
    </div>
    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            @foreach ($errors->all() as $error)
                    <ul>
                        <li><strong>{{ $error }}</strong></li>
                    </ul>
            @endforeach
        </div>
        @else
        @include('flash')
        @endif
        {{ Form::open(['id' => 'QuizEdit', 'route' => ['school-task-questions.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
        <div class="row mt-3">
            <div class="col-xl-6 col-md-12">
                <div class="form-group">
                    {{ Form::label('no_soal', 'Nomor Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::number('nomor_soal', old('nomor_soal') ? old('nomor_soal') : $data->number, ['class' => 'form-control', 'id' => 'max_point', 'placeholder' => 'Nomor Soal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('soal', 'Pertanyaan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pertanyaan', $data->question, ['class' => 'form-control editor_field', 'id' => 'textarea-soal', 'placeholder' => 'Pertanyaan', 'rows' => 10, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pembahasan', 'Pembahasan (boleh kosong)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pembahasan', $data->justification, ['class' => 'form-control editor_field', 'id' => 'pembahasan', 'placeholder' => 'Pembahasan (boleh kosong)', 'readOnly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
            <div class="form-group">
                    {{ Form::label('pilihan_a', 'pilihan A', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea("pilihan_a", old('pilihan_a') ? old('pilihan_a') : $data->option->A, ['class' => 'form-control editor_field', 'id' => 'pilihan_a', 'placeholder' => 'pilihan A', 'rows' => 1, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pilihan_b', 'pilihan B', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pilihan_b', old('pilihan_b') ? old('pilihan_a') : $data->option->B, ['class' => 'form-control editor_field', 'id' => 'pilihan_b', 'placeholder' => 'pilihan B', 'rows' => 1, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pilihan_c', 'pilihan C', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pilihan_c', old('pilihan_c') ? old('pilihan_a') : $data->option->C, ['class' => 'form-control editor_field', 'id' => 'pilihan_c', 'placeholder' => 'pilihan C', 'rows' => 1, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pilihan_d', 'pilihan D', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pilihan_d', old('pilihan_d') ? old('pilihan_a') : $data->option->D, ['class' => 'form-control editor_field', 'id' => 'pilihan_d', 'placeholder' => 'pilihan D', 'rows' => 1, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pilihan_e', 'pilihan E', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pilihan_e', old('pilihan_e') ? old('pilihan_a') : $data->option->E, ['class' => 'form-control editor_field', 'id' => 'pilihan_e', 'placeholder' => 'pilihan E (boleh kosong)', 'rows' => 1, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('jawaban', 'Jawaban Benar', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('jawaban_benar', ['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E'], $data->correct_answer, ['class' => 'form-control', 'id' => 'bab_id', 'placeholder' => '- Pilih Jawaban Yang Benar -']) }}
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <div class="form-group text-center">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Edit Soal', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
<div class="modal fade bd-example-modal-lg editorModal" id="editorModal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
      <div class="modal-body">
        <textarea class="form-control editor" id="editor" name="editor" row="40"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script src="/template/vendors/tinymce/tinymce.min.js"></script>
<script src="/template/vendors/tinymce/config_modal.js"></script>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>
@endsection
