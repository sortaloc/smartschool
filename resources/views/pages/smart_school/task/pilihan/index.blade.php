@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div><strong>Nama Tugas : {{ $data->name }}</strong></div>
        <div class="d-inline">
        <a href="/img/template_upload_soal_pilihan.xlsx" class="btn btn-sm btn-outline-primary"><i class="fa fa-download"></i> Template</a>
        {{ Form::open(['id' => 'bulk-import', 'route' => ['school-task-questions.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'd-inline']) }}
            <a href="#" onclick="$('#bulk').click();" class="btn btn-sm btn-outline-primary"><i class="fa fa-upload"></i> Import Data</a>
            <input onchange="document.getElementById('bulk-import').submit();" type="file" name="bulk_import" id="bulk" class="d-none" accept=".xlsx">
            <input type="hidden" name="task_id" value="{{ request()->input('task_id') }}">
        {{ Form::close() }}
        </div>
    </div>
    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            @foreach ($errors->all() as $error)
                    <ul>
                        <li><strong>{{ $error }}</strong></li>
                    </ul>
            @endforeach
        </div>
        @else
        @include('flash')
        @endif
        {{ Form::open(['id' => 'quizCreate', 'route' => ['school-task-questions.store', 'task_id='.Request::input('task_id')], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        <div class="row mt-3">
            <div class="col-xl-6 col-md-12">
                <div class="form-group">
                    {{ Form::label('no_soal', 'Nomor Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::number('nomor_soal', old('nomor_soal'), ['class' => 'form-control', 'id' => 'max_point', 'placeholder' => 'Nomor Soal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('soal', 'Pertanyaan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pertanyaan', old('pertanyaan'), ['class' => 'form-control editor_field', 'id' => 'textarea-soal', 'placeholder' => 'Pertanyaan', 'rows' => 10, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('pembahasan', 'Pembahasan (boleh kosong)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pembahasan', old('pembahasan'), ['class' => 'form-control editor_field', 'id' => 'pembahasan', 'placeholder' => 'Pembahasan (boleh kosong)', 'readOnly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                @include('pages.smart_school.task.pilihan.option.option_a')
                @include('pages.smart_school.task.pilihan.option.option_b')
                @include('pages.smart_school.task.pilihan.option.option_c')
                @include('pages.smart_school.task.pilihan.option.option_d')
                @include('pages.smart_school.task.pilihan.option.option_e')
                <div class="form-group">
                    {{ Form::label('jawaban', 'Jawaban Benar', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('jawaban_benar', ['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D',  'E' => 'E'], null, ['class' => 'form-control', 'id' => 'bab_id', 'placeholder' => '- Pilih Jawaban Yang Benar -']) }}
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <div class="form-group text-center">
                    {{ Form::button('<i class="fa fa-save"></i> Tambah Soal', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5" class="text-center" style="vertical-align : middle">No</th>
                    <th width="350" class="text-center">Soal</th>
                    <th width="5" class="text-center" style="vertical-align : middle">Pilihan</th>
                    <th width="5" class="text-center" style="vertical-align : middle">Jawaban</th>
                    <th width="350" class="text-center">Pembahasan</th>
                    <th width="20" class="text-center" style="vertical-align : middle">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->questions as $q)
                <tr>
                    <td>{{ $q->number }}</td>
                    <td class="soal">{!! $q->question !!}</td>
                    <td>
                    <span data-toggle="modal" data-target="#modal-option-{{$q->id}}" class="badge btn badge-primary">Lihat Pilihan</span>
                    @if($q->option)
                    @include('pages.smart_school.task.pilihan.option')
                    @endif
                    </td>
                    <td>{{ $q->correct_answer }}</td>
                    <td class="soal">{!! $q->justification !!}</td>
                    <td class="text-center">
                        <form id="{{$q->id}}" action="{{route('school-task-questions.destroy',$q->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-task-questions.edit', $q->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade bd-example-modal-lg editorModal" id="editorModal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
      <div class="modal-body">
        <textarea class="form-control editor" id="editor" name="editor" row="50"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script src="/template/vendors/tinymce/tinymce.min.js"></script>
<script src="/template/vendors/tinymce/config_modal.js"></script>
<script>
    autosize(document.querySelector('textarea'));
</script>
@endsection
