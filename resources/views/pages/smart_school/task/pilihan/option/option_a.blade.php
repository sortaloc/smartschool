{{-- pilihan A --}}
<div class="form-group">
    {{ Form::label('pilihan_a', 'pilihan A', ['class' => 'font-bold']) }}
    <div class="input-group-icon left">
        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
        {{ Form::textarea('pilihan[a]', null, ['class' => 'form-control editor_field', 'id' => 'pilihan_a', 'placeholder' => 'pilihan A', 'rows' => 1, 'readOnly']) }}
    </div>
</div>
<script>
    autosize(document.querySelector('textarea'));
</script>
{{-- End pilihan A --}}
