{{-- pilihan D --}}
<div class="form-group">
    {{ Form::label('pilihan_d', 'pilihan D', ['class' => 'font-bold']) }}
    <div class="input-group-icon left">
        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
        {{ Form::textarea('pilihan[d]', null, ['class' => 'form-control editor_field', 'id' => 'pilihan_d', 'placeholder' => 'pilihan D', 'rows' => 1, 'readOnly']) }}
    </div>
</div>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>
{{-- End pilihan D --}}
