{{-- pilihan C --}}
<div class="form-group">
    {{ Form::label('pilihan_c', 'pilihan C', ['class' => 'font-bold']) }}
    <div class="input-group-icon left">
        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
        {{ Form::textarea('pilihan[c]', null, ['class' => 'form-control editor_field', 'id' => 'pilihan_c', 'placeholder' => 'pilihan C', 'rows' => 1, 'readOnly']) }}
    </div>
</div>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>
{{-- End pilihan C --}}
