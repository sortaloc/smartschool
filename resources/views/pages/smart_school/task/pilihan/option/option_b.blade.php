{{-- pilihan B --}}
<div class="form-group">
    {{ Form::label('pilihan_b', 'pilihan B', ['class' => 'font-bold']) }}
    <div class="input-group-icon left">
        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
        {{ Form::textarea('pilihan[b]', null, ['class' => 'form-control editor_field', 'id' => 'pilihan_b', 'placeholder' => 'pilihan B', 'rows' => 1, 'readOnly']) }}
    </div>
</div>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>

{{-- End pilihan B --}}
