{{-- pilihan E --}}
<div class="form-group">
    {{ Form::label('pilihan_e', 'pilihan E', ['class' => 'font-bold']) }}
    <div class="input-group-icon left">
        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
        {{ Form::textarea('pilihan[e]', null, ['class' => 'form-control editor_field', 'id' => 'pilihan_e', 'placeholder' => 'pilihan E (boleh kosong)', 'rows' => 1, 'readOnly']) }}
    </div>
</div>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>
{{-- End pilihan E --}}
