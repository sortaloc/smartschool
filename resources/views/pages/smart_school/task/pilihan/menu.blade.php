@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }}
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif
            <div class="ibox-body">
                @include('pages.quiz.filter')
            </div>
        </div>
    </div>
</div>
@endsection
