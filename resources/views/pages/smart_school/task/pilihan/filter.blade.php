<div class="row">
    <div class="col-xl-2">
        <select name="kelas_id" id="kelas_id_filter" class="form-control" placeholder="-- Pilih Kelas --">
            <option value="">-- Pilih Kelas --</option>
            @foreach ($kelas as $kel)
            <option value="{{$kel->id}}">{{$kel->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-xl-2">
        <select name="kurikulum_id" id="data_kurikulum" class="form-control" placeholder="-- Pilih Kurikulum --">
        </select>
    </div>
    <div class="col-xl-2">
        <select name="mapel_id" id="data_mapel" class="form-control" placeholder="-- Pilih Mata Pelajaran --">
        </select>
    </div>
    <div class="col-xl-2">
        <select name="bab_id" id="data_bab" class="form-control" placeholder="-- Pilih Bab --">
        </select>
    </div>
    <div class="col-xl-2">
        <select onChange="getUrl()" name="materi_id" id="data_materi" class="form-control" placeholder="-- Pilih Materi --">
        </select>
    </div>
</div>
<script>
    $("#kelas_id_filter").change(function(){
            $.ajax({
                url: "/getKurikulumByKelas/"+$(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#data_kurikulum').html(data.kurikulum);
                }
            });
        });
</script>
<script>
    $("#data_mapel").change(function(){
            $.ajax({
                url: "/getBabByMapel/"+$(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#data_bab').html(data.bab);
                }
            });
        });
</script>
<script>
    $("#data_bab").change(function(){
            $.ajax({
                url: "/getMateriByBab/"+$(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#data_materi').html(data.materi);
                }
            });
        });
</script>
<script>
    $("#data_kurikulum").change(function(){
            $.ajax({
                url: "/getMapelByKurikulum/"+$(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#data_mapel').html(data.mapel);
                }
            });
        });
</script>
<script>
    function getUrl(){ 
        var materi_id = document.getElementById("data_materi").value;    
        if (materi_id) {
            window.location = '/quiz/'+materi_id;
        }
        return false;
    }
</script>