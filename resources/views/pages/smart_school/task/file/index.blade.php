@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div><strong>Nama Tugas: {{ $data->name }}</strong></div>
    </div>
    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            @foreach ($errors->all() as $error)
                    <ul>
                        <li><strong>{{ $error }}</strong></li>
                    </ul>
            @endforeach
        </div>
        @else
        @include('flash')
        @endif
        {{ Form::open(['id' => 'quizCreate', 'route' => ['school-task-questions.store', 'task_id='.Request::input('task_id')], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        <div class="row mt-3">
            <div class="col-xl-6 col-md-12">
                <div class="form-group">
                    {{ Form::label('no_soal', 'Nomor Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::number('nomor_soal', old('nomor_soal'), ['class' => 'form-control', 'id' => 'max_point', 'placeholder' => 'Nomor Soal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('soal', 'Pertanyaan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pertanyaan', old('pertanyaan'), ['class' => 'form-control editor', 'id' => 'textarea-soal', 'placeholder' => 'Pertanyaan', 'rows' => 10, 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group text-center">
                    {{ Form::button('<i class="fa fa-save"></i> Tambah Soal', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="5" class="text-center" style="vertical-align : middle">No</th>
                    <th width="350" class="text-center">Pertanyaan</th>
                    <th width="20" class="text-center" style="vertical-align : middle">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->questions as $q)
                <tr>
                    <td class="text-center">{{ $q->number }}</td>
                    <td class="soal">{!! $q->question !!}</td>
                    <td class="text-center">
                        <form id="{{$q->id}}" action="{{route('school-task-questions.destroy',$q->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-task-questions.edit', $q->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script src="/template/vendors/tinymce/tinymce.min.js"></script>
<script src="/template/vendors/tinymce/config_modal.js"></script>
<script>
    autosize(document.querySelector('textarea'));
</script>
@endsection
