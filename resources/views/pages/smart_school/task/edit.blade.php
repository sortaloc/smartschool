@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    Formulir {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'tugasEdit', 'route' => ['school-tasks.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_soal', old('nama_soal') ? old('nama_soal') : $data->name, ['class' => 'form-control', 'placeholder' => 'Nama Soal']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Kategori Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::text('', $data->task_type, ['class' => 'form-control', 'placeholder' => '', 'readOnly']) }}
                    </div>
                </div>
                @if($data->task_type != 'Utama')
                <div class="form-group">
                    {{ Form::label('', 'Tugas Lama (Susulan/Perbaikan)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::text('', $data->old_task->name, ['class' => 'form-control', 'placeholder' => '', 'readOnly']) }}
                    </div>
                </div>
                @endif

                <div class="form-group">
                    {{ Form::label('', 'Jenis Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::text('', $data->question_type, ['class' => 'form-control', 'readOnly']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Jenis Penilaian', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        <select name="jenis_penilaian" class="form-control">
                            <option value="">- Pilih Jenis Penilaian -</option>
                            @foreach ($assessments as $item)
                                <option {{ $item->id == $data->assessment_type_ref_id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->category }} - {{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Deskripsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('deskripsi', old('deskripsi') ? old('deskripsi') : $data->description, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Deskripsi Soal']) }}
                    </div>
                </div>

                <div class="form-group" id="subjects_list">
                    {{ Form::label('', 'Soal untuk :', ['class' => 'font-bold']) }}
                    @foreach($data->task_classes as $key => $tc)
                    <div class="row" id="subjects_list{{$key}}">
                        <div class="col-4">
                            <div class="form-group">
                                {{ Form::label('', 'Kelas') }}
                                <div class="input-group">
                                    <select name="kelas[{{$key}}]" class="form-control">
                                        @foreach($subjects as $mapel)
                                        <option {{ $mapel->id == $tc->teacher_subjects->id ? 'selected' : ''}} value="{{ $mapel->id }}">{{ $mapel->class->class_name }} - {{ $mapel->subjects->name }} - {{ $mapel->teacher->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pl-0 pr-1">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Mulai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_mulai[{{$key}}]" value="{{ old('waktu_mulai[$key]') ? old('waktu_mulai[$key]') : date('Y-m-d\TH:i', $tc->start_time) }}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pl-0 pr-1">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Selesai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_selesai[{{$key}}]" value="{{ old('waktu_selesai[$key]') ? old('waktu_selesai[$key]') : date('Y-m-d\TH:i', $tc->end_time)}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-1 pl-1 pt-2">
                            <div class="form-group ml-0">
                                @if($loop->first)
                                <button id="addButton" onclick="event.preventDefault()" class="btn btn-primary mt-4"><i class="fa fa-plus"></i></button>
                                @else
                                <button onclick="event.preventDefault(); removeButton('{{$key}}')" class="btn btn-danger remove-button mt-4"><i class="fa fa-times"></i></button>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @if(!$data->task_classes()->first())
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                {{ Form::label('', 'Kelas') }}
                                <div class="input-group">
                                    <select name="kelas[1]" class="form-control">
                                        @foreach($subjects as $mapel)
                                        <option value="{{ $mapel->id }}">{{ $mapel->class->class_name }} - {{ $mapel->subjects->name }} - {{ $mapel->teacher->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pl-0 pr-1">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Mulai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_mulai[1]" value="{{ old('waktu_mulai[1]') }}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-3 pl-0 pr-1">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Selesai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_selesai[1]" value="{{ old('waktu_selesai[1]')}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-1 pl-1 pt-2">
                            <div class="form-group ml-0">
                                <button id="addButton" onclick="event.preventDefault()" class="btn btn-primary mt-4"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Simpan', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
var $subjects = {!! $subjects !!};
var $total_subjects = {!! $data->task_classes->count() !!};

function removeButton($id){
    $('#subjects_list'+$id).remove()
};

$('#addButton').on('click', function(){
    $total_subjects++;
    $('#subjects_list').append(
        '<div class="row" id="subjects_list'+$total_subjects+'">\
            <div class="col-4">\
                <div class="form-group">\
                    <label>Kelas</label>\
                    <div class="input-group">\
                        <select name="kelas['+$total_subjects+']" class="form-control">\
                            '+getMapel()+'\
                        </select>\
                    </div>\
                </div>\
            </div>\
            <div class="col-3 pl-0 pr-1">\
                <div class="form-group">\
                    <label>Waktu Mulai</label>\
                    <div class="input-group">\
                    <input type="datetime-local" name="waktu_mulai['+$total_subjects+']" id="" class="form-control">\
                    </div>\
                </div>\
            </div>\
            <div class="col-3 pl-0 pr-1">\
                <div class="form-group">\
                    <label>Waktu Selesai</label>\
                    <div class="input-group">\
                        <input type="datetime-local" name="waktu_selesai['+$total_subjects+']" id="" class="form-control">\
                    </div>\
                </div>\
            </div>\
            <div class="col-2 pl-1 pt-2">\
                <div class="form-group ml-0">\
                    <button data-xx="'+$total_subjects+'" onclick="event.preventDefault(); removeButton('+$total_subjects+')" class="btn btn-danger remove-button mt-4"><i class="fa fa-times"></i></button>\
                </div>\
            </div>\
        </div>'
    );
});

function getMapel(){
    var $mapel = '';
    for(var x in $subjects){
        $mapel += '<option value="'+$subjects[x].id+'">'+$subjects[x].class.class_name+' - '+$subjects[x].subjects.name+' - '+$subjects[x].teacher.name+'</option>';
    }
    return $mapel;
}
</script>
@endsection
