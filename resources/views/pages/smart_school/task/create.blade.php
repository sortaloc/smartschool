@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    Formulir {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'tugasCreate', 'route' => ['school-tasks.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_soal', old('nama_soal'), ['class' => 'form-control', 'placeholder' => 'Nama Soal']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Kategori Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        <select name="kategori_soal" class="form-control" id="task_type">
                            <option value="">- Pilih Kategori Soal -</option>
                            <option value="Utama">Utama</option>
                            <option value="Susulan">Susulan</option>
                            <option value="Perbaikan">Perbaikan</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="form_preference" style="display:none">
                    {{ Form::label('', 'Preferensi Nilai', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('preferensi_nilai', ['Terbaik' => 'Nilai Terbaik', 'Perbaikan' => 'Nilai Perbaikan / Remidial', 'Maksimal Nilai' => 'Maksimal Nilai'], old('preferensi_nilai'), ['class' => 'form-control', 'id' => 'point_preference', 'placeholder' => '- Pilih Preferensi Nilai -']) }}
                    </div>
                </div>

                <div class="form-group" id="form_point" style="display:none">
                    {{ Form::label('', 'Nilai Maksimal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('maksimal_nilai', old('maksimal_nilai'), ['class' => 'form-control', 'id' => 'max_point', 'placeholder' => 'Masukan Maksimal Nilai']) }}
                    </div>
                </div>

                <div class="form-group" id="form_old_task" style="display:none">
                    {{ Form::label('', 'Tugas Lama (Susulan/Perbaikan)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="tugas_lama" id="old_task" class="form-control">
                            <option value="">Pilih Tugas Yang Ingin Disusul / Diperbaiki</option>
                            @foreach($tasks as $task)
                            <option value="{{ $task->id }}">{{ $task->name }}
                            @foreach($task->task_classes as $ts)
                                {{ $ts->teacher_subjects->subjects->name }} - Kelas {{ $ts->teacher_subjects->class->class_name }},
                            @endforeach
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Jenis Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        <select name="jenis_soal" class="form-control">
                            <option value="">- Pilih Jenis Soal -</option>
                            <option value="Pilihan Ganda">Pilihan Ganda</option>
                            <option value="Essay">Essay</option>
                            <option value="File">Upload File (Laporan/Artikel/Jurnal dll)</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', 'Jenis Penilaian', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        <select name="jenis_penilaian" class="form-control">
                            <option value="">- Pilih Jenis Penilaian -</option>
                            @foreach ($assessments as $item)
                                <option value="{{ $item->id }}">{{ $item->category }} - {{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Deskripsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('deskripsi', old('deskripsi'), ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Deskripsi Soal']) }}
                    </div>
                </div>

                <div class="form-group" id="subjects_list">
                    {{ Form::label('', 'Soal untuk :', ['class' => 'font-bold']) }}
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                {{ Form::label('', 'Kelas') }}
                                <div class="input-group">
                                    <select name="kelas[1]" class="form-control">
                                        @foreach($subjects as $mapel)
                                        <option value="{{ $mapel->id }}">{{ $mapel->class->class_name }} - {{ $mapel->subjects->name }} - {{ $mapel->teacher->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Mulai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_mulai[1]" value="{{ old('waktu_mulai[1]') }}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Selesai') }}
                                <div class="input-group">
                                    <input type="datetime-local" name="waktu_selesai[1]" value="{{ old('waktu_selesai[1]')}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-1 pl-0 pt-2">
                            <div class="form-group ml-0">
                                <button id="addButton" onclick="event.preventDefault()" class="btn btn-primary mt-4"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Selanjutnya', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
var $subjects = {!! $subjects !!};
var $total_subjects = 1;

function removeButton($id){
    $('#subjects_list'+$id).remove()
};

$('#addButton').on('click', function(){
    $total_subjects++;
    $('#subjects_list').append(
        '<div class="row" id="subjects_list'+$total_subjects+'">\
            <div class="col-4">\
                <div class="form-group">\
                    <label>Kelas</label>\
                    <div class="input-group">\
                        <select name="kelas['+$total_subjects+']" class="form-control">\
                            '+getMapel()+'\
                        </select>\
                    </div>\
                </div>\
            </div>\
            <div class="col-3">\
                <div class="form-group">\
                    <label>Waktu Mulai</label>\
                    <div class="input-group">\
                    <input type="datetime-local" name="waktu_mulai['+$total_subjects+']" id="" class="form-control">\
                    </div>\
                </div>\
            </div>\
            <div class="col-3">\
                <div class="form-group">\
                    <label>Waktu Selesai</label>\
                    <div class="input-group">\
                        <input type="datetime-local" name="waktu_selesai['+$total_subjects+']" id="" class="form-control">\
                    </div>\
                </div>\
            </div>\
            <div class="col-2 pl-0 pt-2">\
                <div class="form-group ml-0">\
                    <button data-xx="'+$total_subjects+'" onclick="event.preventDefault(); removeButton('+$total_subjects+')" class="btn btn-danger remove-button mt-4"><i class="fa fa-times"></i></button>\
                </div>\
            </div>\
        </div>'
    );
});

function getMapel(){
    var $mapel = '';
    for(var x in $subjects){
        $mapel += '<option value="'+$subjects[x].id+'">'+$subjects[x].class.class_name+' - '+$subjects[x].subjects.name+' - '+$subjects[x].teacher.name+'</option>';
    }
    return $mapel;
}

$('#task_type').change(function(){
    if($(this).val() == 'Susulan'){
        $('#form_old_task').show();
        $('#form_point').hide();
        $('#form_preference').hide();
        $('#point_preference').val('');
        $('#max_point').val(null);
    }else if($(this).val() == 'Perbaikan'){
        $('#form_old_task').show();
        $('#form_preference').show();
        $('#form_point').hide();
        $('#max_point').val(null);
    }else{
        $('#form_old_task').hide();
        $('#form_preference').hide();
        $('#form_point').hide();
        $('#old_task').val('');
        $('#old_task').trigger('change');
        $('#point_preference').val('');
        $('#max_point').val(null);
    }
})
$('#point_preference').change(function(){
    if($(this).val() == 'Maksimal Nilai'){
        $('#form_point').show();
    }else{
        $('#form_point').hide();
    }
})
$('#old_task').select2({ width: '100%' });
</script>
@endsection
