<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <h1>DATA HASIL TUGAS SEKOLAH GENIUS</h1>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>NAMA GURU</td>
                        <td></td>
                        <td>{{ strtoupper($guru->profile->name) }}</td>
                    </tr>
                    <tr>
                        <td>ID TUGAS</td>
                        <td></td>
                        <td>{{ strtoupper($tugas->id) }}</td>
                    </tr>
                    <tr>
                        <td>NAMA TUGAS</td>
                        <td></td>
                        <td>{{ $tugas->name }}</td>
                    </tr>
                    <tr>
                        <td>MATA PELAJARAN</td>
                        <td></td>
                        <td>{{ strtoupper($tugas->mata_pelajaran) }}</td>
                    </tr>
                    <tr>
                        <td>SEMESTER</td>
                        <td></td>
                        <td>{{ strtoupper($tugas->semester) }}</td>
                    </tr>
                    <tr>
                        <td>KELAS</td>
                        <td></td>
                        <td>{{ strtoupper($tugas->kelas) }}</td>
                    </tr>
                    <tr>
                        <td>SEKOLAH</td>
                        <td></td>
                        <td>{{ strtoupper($guru->school_name) }}</td>
                    </tr>
                </thead>    
            </table>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td rowspan="2">NO</td>
                        <td rowspan="2">NAMA</td>
                        <td colspan="{{ $questions->count() }}">JAWABAN</td>
                        <td rowspan="2">NILAI</td>
                    </tr>
                    <tr>
                    @foreach ($questions as $d)
                        <td>{{ $d->number }}</td>
                    @endforeach
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>