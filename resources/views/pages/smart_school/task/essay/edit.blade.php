@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div><strong>Nama Tugas : {{ $data->task->name }}</strong></div>
    </div>
    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            @foreach ($errors->all() as $error)
                    <ul>
                        <li><strong>{{ $error }}</strong></li>
                    </ul>
            @endforeach
        </div>
        @else
        @include('flash')
        @endif
        {{ Form::open(['id' => 'QuizEdit', 'route' => ['school-task-questions.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
        <div class="row mt-3">
            <div class="col-xl-6 col-md-12">
                <div class="form-group">
                    {{ Form::label('no_soal', 'Nomor Soal', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-list text-info"></i></div>
                        {{ Form::number('nomor_soal', old('nomor_soal') ? old('nomor_soal') : $data->number, ['class' => 'form-control', 'id' => 'max_point', 'placeholder' => 'Nomor Soal']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('soal', 'Pertanyaan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pertanyaan', $data->question, ['class' => 'form-control editor', 'id' => 'textarea-soal', 'placeholder' => 'Pertanyaan', 'rows' => 10, 'readOnly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12">
                <br><br><br>
                <div class="form-group mt-3">
                    {{ Form::label('pembahasan', 'Pembahasan (boleh kosong)', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('pembahasan', $data->justification, ['class' => 'form-control editor', 'id' => 'pembahasan', 'placeholder' => 'Pembahasan (boleh kosong)', 'readOnly']) }}
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group text-center">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                            Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Edit Soal', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
<script src="/template/vendors/tinymce/tinymce.min.js"></script>
<script src="/template/vendors/tinymce/config_modal.js"></script>
<script>
    autosize(document.querySelectorAll('textarea'));
</script>
@endsection
