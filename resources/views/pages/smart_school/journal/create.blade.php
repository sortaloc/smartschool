@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-journals.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Kelas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="kelas" id="class_id" class="form-control">
                            <option value="">Pilih Kelas</option>
                            @foreach($classes as $class)
                            <option value="{{ $class->id }}">{{ $class->class_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Wali Kelas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text(null, null, ['class' => 'form-control', 'id' => 'homeroom', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Hari', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::date('hari', old('hari'), ['class' => 'form-control', 'id' => 'date']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Waktu', ['class' => 'font-bold']) }}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Mulai', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::time('waktu_mulai',old('waktu_mulai'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                {{ Form::label('', 'Waktu Selesai', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::time('waktu_selesai',old('waktu_selesai'), ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Mata Pelajaran', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>

                        <select name="mata_pelajaran" class="form-control subjects">
                        <option value="">Pilih Mata Pelajaran</option>
                        @if($classes[0])
                            @foreach($classes[0]->subjects as $subjects)
                            <option
                            @if(old('mata_pelajaran') == $subjects->id)
                                selected
                            @endif
                             value="{{ $subjects->id }}">{{ $subjects->class->class_name }} - Kelas {{ $subjects->subjects->name }}</option>
                            @endforeach
                        @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Pengampu', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text(null, null, ['class' => 'form-control', 'id' => 'teacher', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Materi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('materi', old('materi'), ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Catatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('catatan', old('catatan'), ['class' => 'form-control', 'placeholder' => 'Isi setelah sesi selesai', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
var $result = [];
var $date = null;
var $classes = {!! $classes !!}

$('#date').on('change', function(){
    var $date = $(this).val();
    $('.subjects').val("x").trigger("change")
});
$('.subjects').select2({
    ajax: {
        url: '/school-schedules',
        delay: 500,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term,
                class_id: $('#class_id').val(),
                date: $('#date').val() ? $('#date').val() : new Date().toISOString().slice(0, 19).replace('T', ' '),
                page_limit: 10
            }
        },
        'success': function(data) {
            $result = data.results;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.subjects').on('change', function(){
    var $key = $(this).val();
    var $teacher = $result.find(x => x.id == $key);
    $('#teacher').val($teacher ? $teacher.teacher : '')
});

$('#class_id').on('change', function(){
    var $key = $(this).val();
    var $teacher = $classes.find(x => x.id == $key);
    $('#homeroom').val($teacher.homeroom ? $teacher.homeroom.name : 'belum ada wali kelas')
});
</script>
@endsection
