<form id="{{$dt->id}}" action="{{route('school-journals.destroy',$dt->id)}}" method="post" style="display:inline">
    @method('DELETE')
    @csrf
    <button onclick="return confirm('Perhatian! Menghapus Data Jadwa akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
</form>
<a href="{{ route('school-journals.edit', $dt->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
<a href="{{ route('school-journals.show', $dt->id) }}" class="btn btn-secondary"><i class="fa fa-desktop"></i></a>