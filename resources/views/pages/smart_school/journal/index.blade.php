@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-journals.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
             | <a href="#" id="today" class="btn badge {{ app('request')->input('date') == 'today' ? 'badge-success' : 'badge-secondary' }}">Hari Ini</a>
        </div>
            @php
            $label = '?';
            if(isset($_GET["class_id"]) || isset($_GET["date"]) || isset($_GET["year"]) || isset($_GET["month"])){
                $label = '&';
            }
            @endphp
            <a href="{{ Request::fullUrl() }}{{ $label }}download=true" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="row mb-3">
            <div class="col-xs-6 col-md-3 ">
                <select onchange="goUrl()" class="form-control d-inline pull-left" id="class">
                    <option value="">- Semua Kelas -</option>
                    @foreach($classes as $class)
                    <option {{ request()->input('class_id') == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select onchange="goUrl()" class="form-control d-inline pull-left" id="year">
                    <option value="">- Semua Tahun -</option>
                    @for($x = 2020; $x <= date('Y'); $x++)
                    <option {{ request()->input('year') == $x ? 'selected' : '' }} value="{{ $x }}">{{ $x }}</option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-6 col-md-3 ">
                <select disabled="true" onchange="goUrl()" class="form-control d-inline pull-left" id="month">
                    <option value="">- Semua Bulan -</option>
                    @for($x = 1; $x <= 12; $x++)
                    <option {{ request()->input('month') == $x ? 'selected' : '' }} value="{{ $x }}">{{ $x }}</option>
                    @endfor
                </select>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Kelas</th>
                    <th>Hari</th>
                    <th>Waktu</th>
                    <th>Mapel</th>
                    <th>Pengampu</th>
                    <th>Presensi</th>
                    <th>Materi</th>
                    <th>Catatan</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
var $table = null;
var $today = ''
var $class_id = ''
var $month = ''
var $year = ''
$(document).ready( function () {
    $table = $('#table2').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('school-journals.index') }}",
            data: function(d){
                d.class_id = $class_id;
                d.month = $month;
                d.year = $year;
            },
        },
        
        columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'kelas', name: 'class_schedule.teacher_subjects.class.class.name', orderable: false, searchable: true },
                { data: 'hari', name: 'date', orderable: false, searchable: false },
                { data: 'waktu', name: 'waktu', orderable: false, searchable: false },
                { data: 'mapel', name: 'class_schedule.teacher_subjects.subjects.name', orderable: false, searchable: true},
                { data: 'pengampu', name: 'class_schedule.teacher_subjects.teacher.name', orderable: false, searchable: true },
                { data: 'presensi', name: 'presensi', orderable: false, searchable: false },
                { data: 'materi', name: 'materi', orderable: true, searchable: true },
                { data: 'note', name: 'note', orderable: false, searchable: false },
                { data: 'action', name: 'action', "width": "120px", orderable: false, searchable: false}
                ]
    });

    $('#today').click(function(){
        $today = '&date=today'
        this.goUrl()
    })
});

function goUrl(){

    if($('#class').val()){
        $class_id = $('#class').val()
    }else{
        $class_id = '';
    }

    if($('#month').val()){
        $month = $('#month').val()
    }else{
        $month = '';
    }

    if($('#year').val()){
        $year = $('#year').val()
    }else{
        $year = '';
    }

    if($year){
        $('#month').prop('disabled', false);
    }else{
        $('#month').prop('disabled', true);
    }

    $table.ajax.reload();
}
</script>

@endsection
