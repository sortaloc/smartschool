@extends('pages.home')
@section('content')
<script src='https://meet.jit.si/external_api.js'></script>
<div id="meet"></div>
<input type="hidden" id="email" value="{{ $teacher->user->email }}">
<input type="hidden" id="name" value="{{ $teacher->name }}">
<input type="hidden" id="avatar" value="{{ $teacher->photo }}">
<script>
$( document ).ready(function() {
var $email = $('#email').val()
var $name = $('#name').val()
var $avatar = $('#avatar').val()
const domain = 'meet.jit.si';
const options = {
    roomName: 'genius-smartschool-'+{{ $data->id }},
    height: 700,
    enableWelcomePage : false ,
    parentNode: document.querySelector('#meet'),
    configOverwrite: {disableKick: true},
};
const api = new JitsiMeetExternalAPI(domain, options);
api.executeCommand('avatarUrl', $avatar);
api.executeCommand('displayName', $name);
api.executeCommand('email', $email);
});
</script>
@endsection
