@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-teachers.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                {{ Form::hidden('user_id', $data->user_id, ['class' => 'form-control', 'readOnly']) }}
                <div class="form-group">
                    {{ Form::label('', 'Nama Guru', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_guru', old('nama_guru') ? old('nama_guru') : $data->name, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIP / NIK', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('NIP_NIK', old('NIP_NIK') ? old('NIP_NIK') : $data->NIP, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NUPTK', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('NUPTK', old('NUPTK') ? old('NUPTK') : $data->NUPTK, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                {{ Form::label('', 'Tempat Lahir', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::text('tempat_lahir', old('tempat_lahir') ? old('tempat_lahir') : $data->place_of_birth, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="form-group">
                                {{ Form::label('', 'Tanggal Lahir', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::date('tanggal_lahir', old('tanggal_lahir') ? old('tanggal_lahir') : $data->date_of_birth, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Jenis Kelamin', ['class' => 'font-bold']) }}
                    <div>
                        <div class="d-inline ml-3">
                            <input
                            @if(old('jenis_kelamin') == 'Laki-laki')
                                checked
                            @elseif($data->gender == 'Laki-laki')
                                checked
                            @endif
                            class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="Laki-laki">
                            <label class="" for="inlineRadio1">Laki-laki</label>
                        </div>
                        <div class="d-inline ml-5">
                            <input
                            @if(old('jenis_kelamin') == 'Perempuan')
                                checked
                            @elseif($data->gender == 'Perempuan')
                                checked
                            @endif
                            class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="Perempuan">
                            <label class="" for="inlineRadio2">Perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Agama', ['class' => 'font-bold']) }}
                    <div class="">
                        {{ Form::select('agama', array('ISLAM' => 'ISLAM',
                        'KRISTEN' => 'KRISTEN', 'KATHOLIK' => 'KATHOLIK', 'HINDU' =>
                        'HINDU', 'BUDHA' => 'BUDHA', 'KHONG HUCU' => 'KHONG HUCU'),
                        old('agama') ? old('agama') : $data->religion, ['class' => 'form-control select2']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Jabatan Struktural', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('jabatan_struktural', old('jabatan_struktural') ? old('jabatan_struktural') : $data->structural_position, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Tugas Tambahan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('tugas_tambahan', old('tugas_tambahan') ? old('tugas_tambahan') : $data->additional_position, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Email', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::email('email', old('email') ? old('email') : $data->user->email, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Golongan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('golongan', old('golongan') ? old('golongan') : $data->PNS_rank, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'No HP', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('no_hp', old('no_hp') ? old('no_hp') : $data->phone, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Foto', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload image-upload-p">
                            <div class="image-edit">
                                <input name="foto" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview" style="background-image: url('{{$data->photo}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Tanda Tangan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload">
                            <div class="image-edit">
                                <input name="tanda_tangan" type='file' id="imageUpload2" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload2"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview2" style="background-image: url('{{$data->signature}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Simpan', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview2').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview2').hide();
            $('#imagePreview2').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload2").change(function() {
    readURL2(this);
});

</script>
@endsection
