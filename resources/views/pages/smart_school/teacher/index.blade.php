@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-teachers.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah Guru</a>
            <a href="/img/template_upload_guru.xlsx" class="btn btn-sm btn-outline-primary"><i class="fa fa-download"></i> Template</a>
            {{ Form::open(['id' => 'bulk-import', 'route' => ['school-teachers.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'd-inline']) }}
            <a href="#" onclick="$('#bulk').click();" class="btn btn-sm btn-outline-primary"><i class="fa fa-upload"></i> Import Data</a>
            <input onchange="document.getElementById('bulk-import').submit();" type="file" name="bulk_import" id="bulk" class="d-none" accept=".xlsx">
            {{ Form::close() }}
        </div>
        <a href="{{ Request::fullUrl() }}?download=true" class="btn btn-sm pull-right btn-outline-primary"> <i class="fa fa-download"></i> Download</a>
    </div>

    <div class="ibox-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @else
        @include('flash')
        @endif
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Lengkap</th>
                    <th>NIP</th>
                    <th>Email</th>
                    <th>Mata Pelajaran</th>
                    <th>Golongan</th>
                    <th>No HP</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->NIP }}</td>
                    <td>{{ $data->user->email }}</td>
                    <td>
                        @foreach($data->subjects->groupBy('school_subjects_id') as $mapel)
                            @foreach($mapel as $value)
                                <div>- {{ $value->subjects->name}} - Kelas {{$value->class->class_name}}</div>
                            @endforeach
                        @endforeach
                    </td>
                    <td>{{ $data->PNS_rank }}</td>
                    <td>{{ $data->phone }}</td>
                    <td align="center">
                        <form id="{{$data->id}}" action="{{route('school-teachers.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button onclick="return confirm('Perhatian! Menghapus Data Guru akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-teachers.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
