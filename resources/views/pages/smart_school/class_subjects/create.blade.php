@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-class-subjects.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Kelas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('kelas', $classes, old('kelas'), ['class' => 'form-control select2', 'id' => 'class_id', 'placeholder' => '- Pilih Kelas -']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Mata Pelajaran', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('mata_pelajaran', [], null, ['class' => 'form-control', 'id' => 'subjects', 'placeholder' => '- Pilih Mata Pelajaran -']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Nilai KKM', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('kkm', old('kkm'), ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
$('.select2').select2();
$('#subjects').select2({
    ajax: {
        url: '/school-subjects?without=class_subjects',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                class_id: $('#class_id').val(), //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
</script>
@endsection
