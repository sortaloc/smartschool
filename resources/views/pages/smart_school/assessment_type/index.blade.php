<style>
    #table_length select {
    width: 120px;
    display: inline-block;
    margin-left : 20px;
    }
</style>
@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-assessment-types.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Kelas</th>
                    <th>Mata Pelajaran</th>
                    <th>Kategori Penilaian</th>
                    <th>Jenis Penilaian</th>
                    <th>Bobot</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->assessment_category->class->name }}</td>
                    <td>{{ $data->assessment_category->subjects->name }}</td>
                    <td>{{ $data->assessment_type_ref->category }}</td>
                    <td>{{ $data->assessment_type_ref->name }}</td>
                    <td>{{ $data->weight }}</td>
                    <td align="center">
                        <form id="{{$data->id}}" action="{{route('school-assessment-types.destroy',$data->id)}}" method="post" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <button onclick="return confirm('Perhatian! Menghapus Data ini akan menghapus data lainnya.')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('school-assessment-types.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    // Append all paragraphs
    $("#table_length").append('{{ Form::select('', $classes, app('request')->input('class_id'), ['class' => 'form-control', 'id' => 'class_id']) }}')
    .append('{{ Form::select('', $subjects, app('request')->input('subjects_id'), ['class' => 'form-control', 'id' => 'subjects_id']) }}')
});

var $route = '/school-assessment-types';

$( document ).ready(function() {
    $('#class_id').on('change', function(){
        $class_id = $(this).val()
        goUrl()
    })
    $('#subjects_id').on('change', function(){
        $subjects_id = $(this).val()
        goUrl()
    })

    var $class_id = $('#class_id').val() ? $('#class_id').val() : null;
    var $subjects_id = $('#subjects_id').val() ? $('#subjects_id').val() : null;

    function goUrl(){
        if($class_id && $subjects_id){
            window.location.href = $route+'?class_id='+$class_id+'&subjects_id='+$subjects_id
        }else if($class_id){
            window.location.href = $route+'?class_id='+$class_id
        }else if($subjects_id){
            window.location.href = $route+'?subjects_id='+$subjects_id
        }else{
            window.location.href = $route
        }

    }

})


</script>
@endsection
