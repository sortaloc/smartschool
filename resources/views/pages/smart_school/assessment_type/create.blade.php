@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-assessment-types.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Kategori Penilaian', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="kategori_penilaian" id="category_id" class="form-control">
                            <option value="">Pilih Kategori Penilaian</option>
                            @foreach($assessment_categories as $c)
                            <option {{ $c->id == old('kategori_penilaian') ? 'selected' : '' }} value="{{ $c->id }}">{{ $c->subjects->name }} - Kelas {{ $c->class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Nama Referensi Jenis Penilaian', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="jenis_penilaian" id="type_id" class="form-control">
                            <option value="">Pilih Jenis Penilaian</option>
                            @foreach($assessment_types as $at)
                            <option {{ $at->id == old('jenis_penilaian') ? 'selected' : '' }} value="{{ $at->id }}">{{ $at->name }} - {{ $at->category }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Bobot', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('bobot', old('bobot'), ['class' => 'form-control', 'placeholder' => 'Isi 1, 2, 3 dst']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
    $('#category_id').select2();
    $('#type_id').select2();
</script>
@endsection
