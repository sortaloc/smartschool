@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Form {{ $title }}
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'Create', 'route' => ['school-teacher-files.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama Materi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_materi', old('nama_materi'), ['class' => 'form-control', 'placeholder' => '']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Tipe Materi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('tipe_materi',['Video' => 'Vidio', 'Document' => 'Dokumen'], old('tipe_materi'), ['class' => 'form-control', 'placeholder' => '- Pilih Tipe Materi -']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'File', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::file('file', ['class' => 'form-control', 'placeholder' => '']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endsection
