@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-teacher-files.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Nama Materi</th>
                        <th>Nama Guru</th>
                        <th>Jenis Materi</th>
                        <th width="200">File</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $data->name}}</td>
                        <td>{{ $data->teacher->name }}</td>
                        <td>{{ $data->type }}</td>
                        <td>
                            <a href="{{ $data->file }}" class="badge badge-primary" target="_blank">Buka</a>
                        </td>
                        <td>
                            {{ Form::open(['id' => 'destroy'.$data->id, 'route' => ['school-teacher-files.destroy', $data->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            <a href="{{ route('school-teacher-files.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
