@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-assessment-categories.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Kelas</th>
                        <th>Mata Pelajaran</th>
                        <th width="300">Pengetahuan</th>
                        <th width="300">Keterampilan</th>
                        <th width="300">Sikap</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $data->class->name }}</td>
                        <td>{{ $data->subjects->name }}</td>
                        <td>
                            {{ $data->pengetahuan }}
                        </td>
                        <td>
                            {{ $data->keterampilan }}
                        </td>
                        <td>
                            {{ $data->sikap }}
                        </td>
                        <td>
                            {{ Form::open(['id' => 'destroy'.$data->id, 'route' => ['school-assessment-categories.destroy', $data->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            <a href="{{ route('school-assessment-categories.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
