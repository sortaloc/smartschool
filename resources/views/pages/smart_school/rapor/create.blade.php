@extends('pages.home')
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@else
@include('flash')
@endif
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Data Siswa
                </div>
            </div>
            <div class="ibox-body">
                <div class="form-group">
                    {{ Form::label('', 'Foto', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url('{{$rapor->student->photo}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Nama Lengkap Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_siswa', $rapor->student->name, ['class' => 'form-control', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIPD', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NIPD',  $rapor->student->NIPD, ['class' => 'form-control', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NISN', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NISN',  $rapor->student->NISN, ['class' => 'form-control', 'readOnly']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="row">
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">
                        Data Kehadiran
                        </div>
                    </div>
                    <div class="ibox-body">
                        <div class="form-group">
                            {{ Form::label('', 'Sakit', ['class' => 'font-bold']) }}
                            <div class="input-group-icon left">
                                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                {{ Form::text('sakit', $rapor->student->raporThisYear()->first()->sakit, ['class' => 'form-control', 'readOnly']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('', 'Izin', ['class' => 'font-bold']) }}
                            <div class="input-group-icon left">
                                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                {{ Form::text('izin', $rapor->student->raporThisYear()->first()->izin, ['class' => 'form-control', 'readOnly']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('', 'Tanpa Keterangan', ['class' => 'font-bold']) }}
                            <div class="input-group-icon left">
                                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                {{ Form::text('tanpa_keterangan', $rapor->student->raporThisYear()->first()->tanpa_keterangan, ['class' => 'form-control', 'readOnly']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">
                        Kenaikan Kelas
                        </div>
                    </div>
                    <div class="ibox-body">
                    {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                        <div class="form-group">
                            {{ Form::label('', 'Kenaikan Kelas', ['class' => 'font-bold']) }}
                            <div class="input-group-icon left">
                                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                {{ Form::select('kenaikan_kelas', ['Naik Kelas' => 'Naik Kelas', 'Tidak Naik Kelas' => 'Tidak Naik Kelas'], $rapor->kenaikan_kelas, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('', 'Ke Kelas', ['class' => 'font-bold']) }}
                            <div class="input-group-icon left">
                                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                <select name="ke_kelas" class="form-control">
                                    <option value="">Pilih Kelas</option>
                                    @foreach($classes as $class)
                                    <option {{ $class->id == $rapor->ke_kelas ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary mt-3 pull-right">Simpan</button>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Nilai Akademik
                </div><a href="{{ route('school-rapors.edit', $rapor->id) }}" class="btn btn-primary pull-right"><i class="fa fa-refresh"></i> Hitung Ulang</a>
            </div>
            <div class="ibox-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Mata Pelajaran</th>
                            <th>Pengetahuan</th>
                            <th>Keterampilan</th>
                            <th>Nilai Akhir</th>
                            <th>Predikat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rapor->academics as $academic)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $academic->subjects->name }}</td>
                            <td>{{ $academic->pengetahuan }}</td>
                            <td>{{ $academic->keterampilan }}</td>
                            <td>{{ $academic->final_point }}</td>
                            <td>{{ $academic->predicate }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Catatan Akademik
                </div>
            </div>
            <div class="ibox-body">
            {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                <textarea name="catatan_akademik" cols="30" class="form-control" rows="7">{{ $rapor->catatan_akademik }}</textarea>
                <button class="btn btn-primary mt-3 pull-right">Simpan</button>
            {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Praktik Kerja Lapangan
                </div>
                <button data-toggle="modal" data-target="#modalPKL" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></button>
            </div>
            <div class="ibox-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Mitra DU/DI</th>
                            <th>Lokasi</th>
                            <th>Lamanya (bulan)</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rapor->pkls as $pkl)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $pkl->mitra }}</td>
                            <td>{{ $pkl->location }}</td>
                            <td>{{ $pkl->duration }}</td>
                            <td>{{ $pkl->description }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Ekstrakurikuler
                </div>
                <button data-toggle="modal" data-target="#modalExtracurricular" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></button>
            </div>
            <div class="ibox-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kegiatan Ekstrakurikuler</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rapor->extracurriculars as $extracurricular)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $extracurricular->activity }}</td>
                            <td>{{ $extracurricular->description }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Deskripsi Perkembangan Karakter
                </div>
                <button data-toggle="modal" data-target="#modalKarakter" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></button>
            </div>
            <div class="ibox-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Karakter Yang DIbangun</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rapor->characters as $character)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $character->assessment_type_ref->name }}</td>
                            <td>{{ $character->description }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Catatan Perkembangan Karakter
                </div>
            </div>
            <div class="ibox-body">
            {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                <textarea name="catatan_karakter" cols="30" class="form-control" rows="7">{{ $rapor->catatan_perkembangan_karakter }}</textarea>
                <button class="btn btn-primary mt-3 pull-right">Simpan</button>
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row card">
    <div class="text-center">
    {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @if(!$rapor->rapor)
            <button class="btn btn-danger mt-3 mb-3 text-center">Buat PDF</button>
        @else
            <button class="btn btn-primary mt-3 mb-3 text-center">Perbarui PDF</button>
        @endif
        <input type="hidden" name="student_id" value="{{ $rapor->student_id }}">
    {{ Form::close() }}
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalExtracurricular" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ekstrakurikuler</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
      <div class="modal-body">
        <div class="form-group">
            {{ Form::label('', 'Kegiatan', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::text('kegiatan_ekstrakurikuler', old('kegiatan_ekstrakurikuler'), ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Keterangan', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::text('keterangan', old('keterangan'), ['class' => 'form-control']) }}
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalPKL" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Praktik Kerja Lapangan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
      <div class="modal-body">
        <div class="form-group">
            {{ Form::label('', 'Mitra', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::text('mitra', old('mitra'), ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Lokasi', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::text('lokasi', old('lokasi'), ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Lamanya (bulan)', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::number('lamanya', old('lamanya'), ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Keterangan', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::text('keterangan', old('keterangan'), ['class' => 'form-control']) }}
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
<!-- Modal Karakter -->
<div class="modal fade" id="modalKarakter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Perkembangan Karakter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {{ Form::open(['id' => 'Create', 'route' => ['school-rapors.update', $rapor->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
      <div class="modal-body">
        <div class="form-group">
            {{ Form::label('', 'Karakter', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::select('karakter', $characters, old('karakter'), ['class' => 'form-control', 'id' => 'assessment_ref', 'placeholder' => 'Pilih Karakter']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Keterangan', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::select('', [], old('keterangan'), ['class' => 'form-control', 'id' => 'character', 'placeholder' => 'Pilih Keterangan', 'style' => 'width=100%']) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('', 'Keterangan (bisa disesuaikan)', ['class' => 'font-bold']) }}
            <div class="input-group-icon left">
                <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                {{ Form::textarea('keterangan', old('keterangan'), ['class' => 'form-control', 'id' => 'characterDescription']) }}
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
<script>
var $character
$('#character').select2({
    ajax: {
        url: '/school-character-predicates',
        delay: 500,
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term,
                assessment_type_ref_id: $('#assessment_ref').val(),
                page_limit: 10
            }
        },
        'success': function(data) {
            $character = data.results;
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    },
    width: '100%'
});
$('#character').change(function(){
    var $key = $(this).val();
    var $description = $character.find(x => x.id == $key);
    $('#characterDescription').val($description.description)
})
</script>
@endsection
