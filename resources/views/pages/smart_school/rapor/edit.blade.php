@extends('pages.home')
@section('content')
{{ Form::open(['id' => 'Create', 'route' => ['school-students.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-4">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Data Siswa
                </div>
            </div>
            <div class="ibox-body">
                <div class="form-group">
                    {{ Form::label('', 'Nama Lengkap Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_siswa', old('nama_siswa') ? old('nama_siswa') : $data->name, ['class' => 'form-control']) }}
                        {{ Form::hidden('id', $data->id, ['class' => 'form-control', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIPD', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NIPD', old('NIPD') ? old('NIPD') : $data->NIPD, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NISN', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NISN', old('NISN') ? old('NISN') : $data->NISN, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                {{ Form::label('', 'Tempat Lahir', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::text('tempat_lahir', old('tempat_lahir') ? old('tempat_lahir') : $data->place_of_birth, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="form-group">
                                {{ Form::label('', 'Tanggal Lahir', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::date('tanggal_lahir', old('tanggal_lahir') ? old('tanggal_lahir') : $data->date_of_birth, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Jenis Kelamin', ['class' => 'font-bold']) }}
                    <div>
                        <div class="d-inline ml-3">
                            <input
                            @if(old('jenis_kelamin') == 'Laki-laki' || $data->gender == 'Laki-laki')
                                checked
                            @endif
                            class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio1" value="Laki-laki">
                            <label class="" for="inlineRadio1">Laki-laki</label>
                        </div>
                        <div class="d-inline ml-5">
                            <input
                            @if(old('jenis_kelamin') == 'Perempuan' || $data->gender == 'Perempuan')
                                checked
                            @endif
                            class="form-check-input" type="radio" name="jenis_kelamin" id="inlineRadio2" value="Perempuan">
                            <label class="" for="inlineRadio2">Perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Agama', ['class' => 'font-bold']) }}
                    <div class="">
                        {{ Form::select('agama', array('ISLAM' => 'ISLAM',
                        'KRISTEN' => 'KRISTEN', 'KATHOLIK' => 'KATHOLIK', 'HINDU' =>
                        'HINDU', 'BUDHA' => 'BUDHA', 'KHONG HUCU' => 'KHONG HUCU'),
                        old('agama') ? old('agama') : $data->religion, ['class' => 'form-control select2']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Status Dalam Keluarga', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('status_dalam_keluarga', old('status_dalam_keluarga') ? old('status_dalam_keluarga') : $data->status_in_family, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Anak Ke', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('anak_ke', old('anak_ke') ? old('anak_ke') : $data->child_number, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Alamat Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('alamat_siswa', old('alamat_siswa') ? old('alamat_siswa') : $data->address, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Provinsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                        @endphp
                        {{ Form::select('provinsi', [$data->district->city->province->id => $data->district->city->province->name], old('provinsi'), ['class' => 'form-control province', 'id' => 'dqe4wqe']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kota / Kabupaten', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('kota', [$data->district->city->id => $data->district->city->name], old('kota'), ['class' => 'form-control city']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kecamatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('kecamatan_siswa', [$data->district->id => $data->district->name], old('kecamatan_siswa'), ['class' => 'form-control district']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Desa / Kelurahan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('desa_siswa', old('desa_siswa') ? old('desa_siswa') : $data->village, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kode Pos', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('kode_pos_siswa', old('kode_pos_siswa') ? old('kode_pos_siswa') : $data->zip_code, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'No. Telp/HP', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('no_telp_siswa', old('no_telp_siswa') ? old('no_telp_siswa') : $data->phone, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Asal Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('asal_sekolah', old('asal_sekolah') ? old('asal_sekolah') : $data->origin_school, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Diterima di Sekolah ini', ['class' => 'font-bold']) }}
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                {{ Form::label('', 'Di Kelas', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    <select name="kelas_diterima" id="" class="form-control select2">
                                        @foreach($classes as $class)
                                        <option
                                        @if(old('kelas_diterima') == $class->id || $data->start_class_id == $class->id)
                                            selected
                                        @endif
                                        value="{{ $class->id }}">{{ $class->class->name }} {{ $class->sub_class->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="form-group">
                                {{ Form::label('', 'Tanggal', ['class' => 'font-bold']) }}
                                <div class="input-group-icon left">
                                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                                    {{ Form::date('tanggal_diterima', old('tanggal_diterima') ? old('tanggal_diterima') : $data->register_date, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Email Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::email('email', old('email') ? old('email') : $data->user->email, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kelas', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="kelas" class="form-control select2">
                            @foreach($classes as $class)
                            <option
                            @if(old('kelas') == $class->id)
                                selected
                            @elseif($data->class->first())
                                @if($data->class->first()->class_id == $class->id)
                                selected
                                @endif
                            @endif
                             value="{{ $class->id }}">{{ $class->class->name }} {{ $class->sub_class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Angkatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select name="angkatan" class="form-control select2">
                            @foreach($generations as $generation)
                            <option
                            @if(old('angkatan') == $generation->id || $data->year_generation_id == $generation->id)
                                selected
                            @endif
                            value="{{ $generation->id }}">{{ $generation->year }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Foto', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input name="foto_siswa" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url('{{$data->photo}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Data Orang Tua Siswa
                </div>
            </div>
            <div class="ibox-body">
                <div class="form-group">
                    {{ Form::label('', 'Nama Orang Tua', ['class' => 'font-bold']) }}
                    <div>
                        {{ Form::label('', 'Ayah', ['class' => 'font-bold']) }}
                    </div>
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_ayah', old('nama_ayah') ? old('nama_ayah') : $data->parent->father_name, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Ibu', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_ibu', old('nama_ibu') ? old('nama_ibu') : $data->parent->mother_name, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Alamat Orang Tua Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('alamat_orang_tua', old('alamat_orang_tua') ? old('alamat_orang_tua') : $data->parent->address, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Provinsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $pp_id = $data->parent->district_id ? $data->parent->district->city->province_id : null;
                            $pp_name = $data->parent->district_id ? $data->parent->district->city->province->name : 'Pilih Provinsi';
                            $pp = [$pp_id => $pp_name];
                        @endphp
                        {{ Form::select('provinsi_ot', [$pp], old('provinsi_ot'), ['class' => 'form-control province_ot']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kota / Kabupaten', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $cp_id = $data->parent->district_id ? $data->parent->district->city_id : null;
                            $cp_name = $data->parent->district_id ? $data->parent->district->city->name : 'Pilih Kota';
                            $cp = [$cp_id => $cp_name];
                        @endphp
                        {{ Form::select('kota_ot', [$cp], old('kota_ot'), ['class' => 'form-control city_ot']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kecamatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $dp_id = $data->parent->district_id ? $data->parent->district_id : null;
                            $dp_name = $data->parent->district_id ? $data->parent->district->name : 'Pilih Kecamatan';
                            $dp = [$dp_id => $dp_name];
                        @endphp
                        {{ Form::select('kecamatan_orang_tua', [$dp], old('kecamatan_orang_tua'), ['class' => 'form-control district_ot']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Desa / Kelurahan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('desa_orang_tua', old('desa_orang_tua') ? old('desa_orang_tua') : $data->parent->village, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kode Pos', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('kode_pos_orang_tua', old('kode_pos_orang_tua') ? old('kode_pos_orang_tua') : $data->parent->zip_code, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'No. Telp/HP Orang Tua Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('no_telp_orang_tua', old('no_telp_orang_tua') ? old('no_telp_orang_tua') : $data->parent->phone, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Pekerjaan Orang Tua Siswa', ['class' => 'font-bold']) }}
                    <div>
                        {{ Form::label('', 'Ayah', ['class' => 'font-bold']) }}
                    </div>
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('pekerjaan_ayah', old('pekerjaan_ayah') ? old('pekerjaan_ayah') : $data->parent->father_job, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Ibu', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('pekerjaan_ibu', old('pekerjaan_ibu') ? old('pekerjaan_ibu') : $data->parent->mother_job, ['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                Data Wali Siswa
                </div>
            </div>
            <div class="ibox-body">
                <div class="form-group">
                    {{ Form::label('', 'Nama Wali Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_wali', old('nama_wali') ? old('nama_wali') : $data->guardian->name, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Alamat Wali Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('alamat_wali', old('alamat_wali') ? old('alamat_wali') : $data->guardian->address, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Provinsi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $pg_id = $data->guardian->district_id ? $data->guardian->district->city->province_id : null;
                            $pg_name = $data->guardian->district_id ? $data->guardian->district->city->province->name : 'Pilih Provinsi';
                            $pg = [$pg_id => $pg_name];
                        @endphp
                        {{ Form::select('provinsi_w', [$pg], old('provinsi_w'), ['class' => 'form-control province_w']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kota / Kabupaten', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $cg_id = $data->guardian->district_id ? $data->guardian->district->city_id : null;
                            $cg_name = $data->guardian->district_id ? $data->guardian->district->city->name : 'Pilih Kota';
                            $cg = [$cg_id => $cg_name];
                        @endphp
                        {{ Form::select('kota_w', [$cg], old('kota_w'), ['class' => 'form-control city_w']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kecamatan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        @php
                            $dg_id = $data->guardian->district_id ? $data->guardian->district_id : null;
                            $dg_name = $data->guardian->district_id ? $data->guardian->district->name : 'Pilih Kecamatan';
                            $dg = [$dg_id => $dg_name];
                        @endphp
                        {{ Form::select('kecamatan_wali', [$dg], old('kecamatan_wali'), ['class' => 'form-control district_w']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Desa / Kelurahan', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('desa_wali', old('desa_wali') ? old('desa_wali') : $data->guardian->village, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kode Pos', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('kode_pos_wali', old('kode_pos_wali') ? old('kode_pos_wali') : $data->guardian->zip_code, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'No. Telp/HP Wali Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('no_telp_wali', old('no_telp_wali') ? old('no_telp_wali') : $data->guardian->phone, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Pekerjaan Wali Siswa', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('pekerjaan_wali', old('pekerjaan_wali') ? old('pekerjaan_wali') : $data->guardian->job, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Ubah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
<script>
var $province_id = {{ $data->district->city->province_id }};
var $city_id = {{ $data->district->city_id }};
var $province_id_ot = {{ $data->parent->district_id ? $data->parent->district->city->province_id : -1}};
var $city_id_ot = {{ $data->parent->district_id ? $data->parent->district->city_id : -1}};
var $province_id_w = {{ $data->guardian->district_id ? $data->guardian->district->city->province_id : -1}};
var $city_id_w = {{ $data->guardian->district_id ? $data->guardian->district->city_id : -1}};
$('.select2').select2();
//siswa
$('.province').select2({
    ajax: {
        url: '/ajax-province',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.province').on('change', function(){
    $province_id = $(this).val();
    $city_id = 'zzz';
});
$('.city').select2({
    ajax: {
        url: '/ajax-city',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                province_id: $province_id, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.city').on('change', function(){
    $city_id = $(this).val();
});
$('.district').select2({
    ajax: {
        url: '/ajax-district',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                city_id: $city_id, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
//ot
$('.province_ot').select2({
    ajax: {
        url: '/ajax-province',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.province_ot').on('change', function(){
    $province_id_ot = $(this).val();
    $city_id_ot = 'zzz';
});
$('.city_ot').select2({
    ajax: {
        url: '/ajax-city',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                province_id: $province_id_ot, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.city_ot').on('change', function(){
    $city_id_ot = $(this).val();
});
$('.district_ot').select2({
    ajax: {
        url: '/ajax-district',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                city_id: $city_id_ot, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
//wali
$('.province_w').select2({
    ajax: {
        url: '/ajax-province',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.province_w').on('change', function(){
    $province_id_w = $(this).val();
    $city_id_w = 'zzz';
});
$('.city_w').select2({
    ajax: {
        url: '/ajax-city',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                province_id: $province_id_w, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
$('.city_w').on('change', function(){
    $city_id_w = $(this).val();
});
$('.district_w').select2({
    ajax: {
        url: '/ajax-district',
        dataType: 'json',
        data: function (params) {
            return {
                q: params.term, // search term
                city_id: $city_id_w, //Get your value from other elements using Query, for example.
                page_limit: 10
            }
        }
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
</script>
@endsection
