@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            {{ Form::open(['id' => 'Create', 'route' => ['schools.update', $school_id], 'method' => 'PATCH', 'class' => 'd-inline', 'enctype' => 'multipart/form-data']) }}
                @if(!$rapor_distribution)
                    <input type="hidden" name="distribution" value="1">
                    <button class="btn btn-success">Bagikan Rapor</button>
                @else
                    <input type="hidden" name="distribution" value="0">
                    <button class="btn btn-danger">Tarik Rapor</button>
                @endif
            {{ Form::close() }}
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <div class="row mb-3">
            <div class="col-xs-6 col-md-2 ">
                <select class="form-control d-inline pull-left" id="class">
                    <option value="">- Semua Kelas -</option>
                    @foreach($classes as $class)
                    <option {{ request()->input('class_id') == $class->id ? 'selected' : '' }} value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <!-- <div class="col-xs-6 col-md-2 ">
                <select class="form-control d-inline pull-left" id="year">
                    @foreach($years as $year)
                    <option {{ request()->input('year_id') == $year->id ? 'selected' : '' }} value="{{ $year->id }}">{{ $year->start_year }} - {{ $year->end_year }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-6 col-md-2 ">
                <select class="form-control d-inline pull-left" id="semester">
                    <option {{ request()->input('semester') == 'Ganjil' ? 'selected' : '' }} value="Ganjil">Ganjil</option>
                    <option {{ request()->input('semester') == 'Genap' ? 'selected' : '' }} value="Genap">Ganjil</option>
                </select>
            </div> -->
        </div>
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Lengkap</th>
                    <th>NISN</th>
                    <th>Email</th>
                    <th>Kelas</th>
                    <th>Angkatan</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $data->name }}</td>
                    <td>{{ $data->NISN }}</td>
                    <td>{{ $data->user->email }}</td>
                    <td>{{ $data->class->first() ? $data->class->first()->class->class_name : 'belum ada kelas' }}</td>
                    <td>{{ $data->generation ? $data->generation->year : 'Belum ada angkatan' }}</td>
                    <td align="center">
                        @if($data->raporThisYear()->first())
                        <a href="{{ route('school-rapors.create', ['student_id' => $data->id]) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                        @if($data->raporThisYear()->first()->rapor)
                        <a href="{{ $data->raporThisYear()->first()->rapor }}" target="_blank" class="btn btn-success"><i class="fa fa-download"></i></a>
                        @endif
                        @else
                        <a href="{{ route('school-rapors.create', ['student_id' => $data->id]) }}" class="btn btn-danger"><i class="fa fa-plus"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
var $class_id = null;
/* var $year_id = null
var $semester = null */
function go(){
    $class_id = $('#class').val()
    /* $year_id = $('#year').val()
    $semester = $('#semester').val() */
    window.location.href = '/school-rapors?class_id='+$class_id;
}
$('#class').change(function(){
    go()
})
/* $('#year').change(function(){
    go()
})
$('#semester').change(function(){
    go()
}) */
</script>
@endsection
