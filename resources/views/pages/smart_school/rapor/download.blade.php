<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $name }}</title>

    <style>
        .page-break {
            page-break-after: always;
        }
        .page-break-avoid {
            page-break-inside: avoid;
        }
        .header {
            display: block;
            font-size: 1.17em;
            margin-top: 1em;
            margin-bottom: 1em;
            margin-left: 0;
            margin-right: 0;
            font-weight: bold;
            text-align : center;
        }
        .header > div{
            margin-bottom: 0.3em!important;
        }

        .table-info-sekolah >td:nth-child(1){
            width:200px;
        }
        .table-info-sekolah >td:nth-child(2){
            width:10px;
        }
        .table-info-sekolah >td:nth-child(3){
            width:250px;
            border-bottom:dotted;
        }
        .table-info-sekolah >td{
            padding-top: 10px;
        }
        .table-info-siswa >td:nth-child(1){
            width:25px;
        }
        .table-info-siswa >td:nth-child(2){
            width:250px;
        }
        .table-info-siswa >td:nth-child(3){
            width:10px;
        }
        .table-info-siswa >td:nth-child(4){
            width:250px;
            border-bottom:dotted;
        }
        .table-info-siswa >td{
            padding-top: 7px;
        }
        .table-bordered{
            width: 100%;
            border:1px solid black;
            border-collapse:collapse;
        }
        .table-bordered tr td{
            padding-top : 3px;
            padding-bottom : 3px;
            padding-right : 13px;
            padding-left : 13px;
            border:1px solid black;
            border-collapse:collapse;
        }
        .table-bordered tr th{
            border:1px solid black;
            padding-right : 5px;
            padding-left : 5px;
        }
        .text-center{
            text-align:center;
        }
        .table3{
            width:100%;
        }
        .table3 tr td { width:33%; }
    </style>
</head>
<body>
    <br>
    <div class="header">
        <div>RAPOR PESERTA DIDIK</div>
        <div>{{ strtoupper($school->jenjang) }}</div>
        <div>({{ $school->singkatan_jenjang }})</div>
    </div>
    <br>
    <br>
    <center><img width="200" src="{{ $school->naungan }}" alt=""></center>
    <br>
    <br>
    <br>
    <center><strong>Nama Peserta Didik:</strong></center>
    <center><div style="width:200px; border-style: solid; margin:auto; margin-top:10px; padding:3px">{{ $data->name }}</div></center>
    <br>
    <br>
    <br>
    <center><strong>NISN:</strong></center>
    <center><div style="width:200px; border-style: solid; margin:auto; margin-top:10px; padding:3px">{{ $data->NISN }}</div></center>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="header">
        <div>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</div>
        <div>REPUBLIK INDONESIA</div>
    </div>

    <div class="page-break"></div>

    <div class="header">
        <div>RAPOR PESERTA DIDIK</div>
        <div>{{ strtoupper($school->jenjang) }}</div>
        <div>({{ $school->singkatan_jenjang }})</div>
        <br>
    </div>
    <center>
        <div style="width:500px; margin:auto;">
            <table class="table-info-sekolah">
                <tbody>
                    <tr>
                        <td>Nama Sekolah</td>
                        <td>:</td>
                        <td>{{ $school->name }}</td>
                    </tr>
                    <tr>
                        <td>NPSN</td>
                        <td>:</td>
                        <td>{{ $school->NPSN }}</td>
                    </tr>
                    <tr>
                        <td>NIS/NSS/NDS</td>
                        <td>:</td>
                        <td>{{ $school->NDS }}</td>
                    </tr>
                    @php
                        $school_address = explode( "\n", wordwrap( $school->address, 35));
                        $address = isset($school_address[0]) ? $school_address[0] : '';
                        foreach($school_address as $key => $value){
                            if($key !== 0){
                                $address .= '<tr><td><td><td>'.$value.'</td></td></td></tr>';
                            }
                        }
                    @endphp
                    <tr>
                        <td>Alamat Sekolah</td>
                        <td>:</td>
                        <td>{!! $address !!}</td>
                    </tr>
                    <tr>
                        <td>Kode Pos</td>
                        <td>:</td>
                        <td>{{ $school->zip_code }}</td>
                    </tr>
                    <tr>
                        <td>Kelurahan</td>
                        <td>:</td>
                        <td>{{ $school->village }}</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td>{{ $school->district->name }}</td>
                    </tr>
                    <tr>
                        <td>Kota/Kabupaten</td>
                        <td>:</td>
                        <td>{{ $school->district->city->type }} {{ $school->district->city->name }}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>:</td>
                        <td>{{ $school->district->city->province->name }}</td>
                    </tr>
                    <tr>
                        <td>Website</td>
                        <td>:</td>
                        <td>{{ $school->website }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{{ $school->email }}</td>
                    </tr>
                    <tr>
                        <td>Telp.</td>
                        <td>:</td>
                        <td>{{ $school->phone }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </center>

    <div class="page-break"></div>
    <div class="header">
        <div>KETERANGAN TENTANG DIRI PESERTA DIDIK</div>
        <br>
    </div>
    <center>
        <div style="width:580px; margin:auto;">
            <table class="table-info-siswa">
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Nama Peserta Didik (Lengkap)</td>
                        <td>:</td>
                        <td>{{ $data->name }}</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Nomor Induk/NISN</td>
                        <td>:</td>
                        <td>{{ $data->NISN }}</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>:</td>
                        <td>{{ $data->place_of_birth }}, {{ $data->date_of_birth }}</td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td>{{ $data->gender }}</td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Agama/Kepercayaan</td>
                        <td>:</td>
                        <td>{{ ucwords(strtolower($data->religion)) }}</td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Status Dalam Keluarga</td>
                        <td>:</td>
                        <td>{{ $data->status_in_family }}</td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Anak Ke</td>
                        <td>:</td>
                        <td>{{ $data->child_number }}</td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td>Alamat Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $data->address }}</td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Nomor Telepon Rumah</td>
                        <td>:</td>
                        <td>{{ $data->phone }}</td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Sekolah Asal</td>
                        <td>:</td>
                        <td>{{ $data->origin_school }}</td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td>Diterima di sekolah ini</td>
                        <td></td>
                        <td style="border: none"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Di kelas</td>
                        <td>:</td>
                        <td>{{ $data->start_class_id }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Pada tanggal</td>
                        <td>:</td>
                        <td>{{ $data->register_date->isoFormat('DD MMMM YYYY') }}</td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td>Nama Orang Tua</td>
                        <td></td>
                        <td style="border: none"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>{{ $data->parent->father_name }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>{{ $data->parent->mother_name }}</td>
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td>Alamat Orang Tua</td>
                        <td>:</td>
                        <td>{{ $data->parent->phone }}</td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td>Pekerjaan Orang Tua</td>
                        <td></td>
                        <td style="border: none"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>a. Ayah</td>
                        <td>:</td>
                        <td>{{ $data->parent->father_job }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b. Ibu</td>
                        <td>:</td>
                        <td>{{ $data->parent->mother_job }}</td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td>Nama Wali Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $data->guardian->name }}</td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td>Alamat Wali Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $data->guardian->address }}</td>
                    </tr>
                    <tr>
                        <td>18.</td>
                        <td>Nomor Telepon Wali Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $data->guardian->phone }}</td>
                    </tr>
                    <tr>
                        <td>18.</td>
                        <td>Pekerjaan Wali Peserta Didik</td>
                        <td>:</td>
                        <td>{{ $data->guardian->job }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </center>
    <table>
        <tbody>
            <tr>
                <td>
                    <div style="width:113px; height:151px; margin-left:100px; margin-top:10px;">
                    <img style="width:100%; height:100%" src="data:image/jpeg;base64, {{ $data->photo_b64 }}"/>
                    </div>
                </td>
                <td style="width: 170px"> </td>
                <td style="vertical-align: top;">
                    <div style="margin-top: 15px">{{ $school->district->city->name }}, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM YYYY') }}</div>
                    <div>Kepala Sekolah,</div>
                    <div style="height: 70px">
                        <img style="height:100%" src="data:image/jpeg;base64, {{ $school->headmaster_signature_base64 }}"/>
                    </div>
                    <div style="width: 200px; border-bottom: solid black 1px">{{ ucwords($school->headmaster) }}</div>
                    <div>NIP {{ $school->headmaster_nip }}</div>
                </td>
            </tr>
        </tbody>
    </table>
    @foreach($data->rapors as $rapor)
    <div class="page-break"></div>
    <table>
        <tbody>
            <tr>
                <td style="width:150px">Nama Peserta Didik</td>
                <td style="width:20px"><center>:</center></td>
                <td>{{ $data->name }}</td>
            </tr>
            <tr>
                <td style="width:150px">NISN/NIS</td>
                <td style="width:20px"><center>:</center></td>
                <td>{{ $data->NISN ? $data->NISN : $data->NDS }}</td>
            </tr>
            <tr>
                <td style="width:150px">Kelas</td>
                <td style="width:20px"><center>:</center></td>
                <td>{{ $rapor->class->class_name }}</td>
            </tr>
            <tr>
                <td style="width:150px">Semester</td>
                <td style="width:20px"><center>:</center></td>
                <td>{{ $rapor->semester }}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <strong>A.    Nilai Akademik</strong>
    <table class="table-bordered">
        <thead>
            <tr>
                <th width="10">No</th>
                <th width="200">Mata Pelajaran</th>
                <th width="10">Pengetahuan</th>
                <th width="10">Keterampilan</th>
                <th width="10">Nilai Akhir</th>
                <th width="10">Predikat</th>
            </tr>
        </thead>
        <tbody>
            @php
                $nomorHuruf = 'A';
            @endphp
            @foreach($rapor->academics->groupBy('subjects.subjects_category_id') as $academic)
            <tr>
                <td colspan="6">
                    <strong>{{$nomorHuruf}}.    {{ $academic[0]->subjects->subjects_category->name }}</strong>
                </td>
            </tr>
            @foreach($academic as $key => $subjecs)
            <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $subjecs->subjects->name }}</td>
                <td class="text-center">{{ round($subjecs->pengetahuan) }}</td>
                <td class="text-center">{{ round($subjecs->keterampilan) }}</td>
                <td class="text-center">{{ round($subjecs->final_point) }}</td>
                <td class="text-center">{{ $subjecs->predicate }}</td>
            </tr>
            @endforeach
            @php
                $nomorHuruf++;
            @endphp
            @endforeach
        </tbody>
    </table>
    <br>
    <strong>B.    Catatan Akademik</strong>
    <div style="width:100%; border: solid 1px; padding: 10px">
        {{ $rapor->catatan_akademik ? $rapor->catatan_akademik : '-' }}
    </div>
    <br>
    <div class="page-break-avoid">
        <div>
            <strong>C.    Praktik Kerja Lapangan</strong>
            <table class="table-bordered">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th width="130">Mitra</th>
                        <th width="100">Lokasi</th>
                        <th class="text-center" width="10">Lamanya (bulan)</th>
                        <th width="130">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                @if($rapor->pkls()->first())
                    @foreach($rapor->pkls as $pkl)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $pkl->mitra }}</td>
                        <td>{{ $pkl->location }}</td>
                        <td>{{ $pkl->duration }}</td>
                        <td>{{ $pkl->description }}</td>
                    </tr>
                    @if($loop->last && $loop->iteration < 3)
                    @for($x = $loop->iteration+1; $x <= 3; $x++)
                    <tr>
                        <td class="text-center">{{ $x }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endfor
                    @endif
                    @endforeach
                @else
                    @for($x = 1; $x <= 3; $x++)
                    <tr>
                        <td class="text-center">{{ $x }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endfor
                @endif
                </tbody>
            </table>
        </div>
        <br>
        <div>
            <strong>D.    Ekstrakurikuler</strong>
            <table class="table-bordered">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th width="200">Kegiatan Ekstrakurikuler</th>
                        <th width="200">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                @if($rapor->extracurriculars()->first())
                    @foreach($rapor->extracurriculars as $extracurricular)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>{{ $extracurricular->activity }}</td>
                        <td>{{ $extracurricular->description }}</td>
                    </tr>
                    @if($loop->last && $loop->iteration < 3)
                    @for($x = $loop->iteration+1; $x <= 3; $x++)
                    <tr>
                        <td class="text-center">{{ $x }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endfor
                    @endif
                    @endforeach
                @else
                    @for($x = 1; $x <= 3; $x++)
                    <tr>
                        <td class="text-center">{{ $x }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endfor
                @endif
                </tbody>
            </table>
        </div>
        <br>
        <div>
            <strong>E.    Ketidakhadiran</strong>
            <table class="table-bordered" style="width:300px">
                <tbody>
                    <tr>
                        <td>Sakit</td>
                        <td><center>:</center></td>
                        <td>{{ $rapor->sakit ? $rapor->sakit : '-' }} hari</td>
                    </tr>
                    <tr>
                        <td>Izin</td>
                        <td><center>:</center></td>
                        <td>{{ $rapor->izin ? $rapor->izin : '-' }} hari</td>
                    </tr>
                    <tr>
                        <td>Tanpa Keterangan</td>
                        <td><center>:</center></td>
                        <td>{{ $rapor->tanpa_keterangan ? $rapor->tanpa_keterangan : '-' }} hari</td>
                    </tr>
                </tbody>
            </table>
        </div>
        @if($rapor->kenaikan_kelas != 'Semester' && $rapor->kenaikan_kelas != null)
        <br>
        <div>
            <strong>F.    Kenaikan Kelas</strong>
            <div style="width:100%; border: solid 1px; padding: 10px">
                @if($rapor->kenaikan_kelas == 'Naik Kelas')
                Naik/<s>Tidak naik</s>
                @else
                <s>Naik</s>/Tidak Naik
                @endif
                @if($rapor->kenaikan_kelas == 'Naik Kelas')
                ke kelas {{ $rapor->nextClass->class_name }}
                @endif
            </div>
        </div>
        @endif
        <br>
        <br>
        <table class="table3">
            <tr>
                <td>
                    <div style="width:180px; border-bottom: solid 1px; ">
                        <div>Mengetahui:</div>
                        <div>Orang Tua/Wali,</div>
                        <div style="height: 70px">
                        </div>
                    </div>
                </td>
                <td></td>
                <td>
                    <div style="width:180px; border-bottom: solid 1px; ">
                        <div>{{ $school->district->city->name }}, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM YYYY') }}</div>
                        <div>Wali Kelas,</div>
                        <div style="height: 70px">
                            @if($rapor->homeroom_teacher->signature_base64)
                            <img style="height:100%" src="data:image/jpeg;base64, {{ $rapor->homeroom_teacher->signature_base64 }}"/>
                            @endif
                        </div>
                        {{ ucwords($rapor->homeroom_teacher->name) }}
                    </div>
                    NIP {{ $rapor->homeroom_teacher->NIP }}
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div style="width:180px; border-bottom: solid 1px; margin-top:20px">
                        <div>Mengetahui:</div>
                        <div>Kepala Sekolah,</div>
                        <div style="height: 70px">
                            <img style="height:100%" src="data:image/jpeg;base64, {{ $school->headmaster_signature_base64 }}"/>
                        </div>
                        {{ ucwords($school->headmaster) }}
                    </div>
                    NIP {{ $school->headmaster_nip }}
                </td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="page-break"></div>
    <br>
    <div>
        <strong>G.    Deskripsi Perkembangan Karakter</strong>
        <table class="table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="10">No</th>
                    <th width="150">Karakter Yang Dibangun</th>
                    <th width="300">Deskripsi</th>
                </tr>
            </thead>
            <tbody>
            @if($rapor->characters()->first())
                @foreach($rapor->characters as $character)
                <tr>
                    <td class="text-center" style="height:70px">{{ $loop->iteration }}</td>
                    <td>{{ $character->assessment_type_ref->name }}</td>
                    <td>{{ $character->description }}</td>
                </tr>
                @endforeach
            @else
                @for($x = 1; $x <= 5; $x++)
                <tr>
                    <td class="text-center" style="height:70px">{{ $x }}</td>
                    <td></td>
                    <td></td>
                </tr>
                @endfor
            @endif
            </tbody>
        </table>
    </div>
    <br>
    <div>
        <strong>H.    Catatan Perkembangan Karakter</strong>
        <div style="width:100%; border: solid 1px; padding: 10px">
            {{ $rapor->catatan_perkembangan_karakter ? $rapor->catatan_perkembangan_karakter : '-' }}
        </div>
    </div>
    @endforeach
    <br>
    <br>
    <table class="table3">
        <tr>
            <td>
                <div style="width:180px; border-bottom: solid 1px; ">
                    <div>Mengetahui:</div>
                    <div>Orang Tua/Wali,</div>
                    <div style="height: 70px">
                    </div>
                </div>
            </td>
            <td></td>
            <td>
                <div style="width:180px; border-bottom: solid 1px; ">
                    <div>{{ $school->district->city->name }}, {{ \Carbon\Carbon::now()->isoFormat('DD MMMM YYYY') }}</div>
                    <div>Wali Kelas,</div>
                    <div style="height: 70px">
                        @if($rapor->homeroom_teacher->signature_base64)
                        <img style="height:100%" src="data:image/jpeg;base64, {{ $rapor->homeroom_teacher->signature_base64 }}"/>
                        @endif
                    </div>
                    {{ ucwords($rapor->homeroom_teacher->name) }}
                </div>
                NIP {{ $rapor->homeroom_teacher->NIP }}
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <div style="width:180px; border-bottom: solid 1px; margin-top:20px">
                    <div>Mengetahui:</div>
                    <div>Kepala Sekolah,</div>
                    <div style="height: 70px">
                        <img style="height:100%" src="data:image/jpeg;base64, {{ $school->headmaster_signature_base64 }}"/>
                    </div>
                    {{ ucwords($school->headmaster) }}
                </div>
                NIP {{ $school->headmaster_nip }}
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>
