{{ Form::open(['id' => 'destroy'.$dt->id, 'route' => ['school-libraries.destroy', $dt->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
{{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
{{ Form::close() }}
<a href="{{ route('school-libraries.edit', $dt->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>