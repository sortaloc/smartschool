@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('school-libraries.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Judul Buku</th>
                        <th>Kategori</th>
                        <th>Pengarang</th>
                        <th>Nomor Buku</th>
                        <th width="200">Cover</th>
                        <th>File</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>

<script>
    $(document).ready( function () {
        $('#table2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('school-libraries.index') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'title', name: 'title', orderable: true, searchable: true },
                    { data: 'category.name', name: 'category.name', orderable: true, searchable: true },
                    { data: 'author', name: 'author', orderable: true, searchable: true },
                    { data: 'book_number', name: 'book_number', "defaultContent": "Belum ada kelas", orderable: true, searchable: true},
                    { data: 'cover_buku', name: 'cover_buku', "width": "80px", orderable: false, searchable: false },
                    { data: 'file_buku', name: 'file_buku', "width": "80px", orderable: false, searchable: false },
                    { data: 'action', name: 'action', "width": "120px", orderable: false, searchable: false}
                    ]
        });
    });
</script>
@endsection
