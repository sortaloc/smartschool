<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript"
            src="https://app.midtrans.com/snap/snap.js"
            data-client-key="{{$client_key}}"></script>
  </head>
  <body>
    <button id="pay-button" style="display:none;"></button>
    <input type="hidden" id="status" value="{{$token}}">
    <script type="text/javascript">
      var payButton = document.getElementById('pay-button');
      var $status = document.getElementById('status').value;
      payButton.addEventListener('click', function () {
        snap.pay('{{$token}}');
      });
      if($status == 'pending'){
        alert('Transaksi sebelumnya sedang diproses, Silahkan melakukan pembayaran.')
      }else{
        window.onload = function(){
            document.getElementById('pay-button').click();
        }
      }
    </script>
  </body>
</html>
