<li class="dropdown dropdown-user">
    <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
        <span>
            <div class="image-square-30">
                <img class="image-square" src="{{ AdminHelp::getAuthAvatar() }}">
            </div>
        </span>
        <span class="ml-3"></span>{{ AdminHelp::getAuthName() }}<i class="fa fa-angle-down m-l-5"></i></a>
    <ul class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="{{ route('profile.index') }}"><i class="fa fa-user"></i>Profile</a>
        <li class="dropdown-divider"></li>
        {{ Form::open(['id' => 'logout1', 'route' => ['logout'], 'method' => 'POST', 'class' => 'd-inline']) }}
        <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout1').submit();"><i class="fa fa-power-off"></i>Logout</a>
        {{ Form::close() }}
    </ul>
</li>
