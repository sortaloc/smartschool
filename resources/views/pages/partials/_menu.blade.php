@role('Super Admin|Admin SD|Admin SMP|Admin SMA|Admin SMK|Admin UTBK|Admin Olimpiade|Admin Marketing')
<li>
    <a href="{{ route('config-schools.index') }}"><i class="sidebar-item-icon fa fa-university"></i>
        <span class="nav-label">Smart School</span>
    </a>
</li>
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-book"></i>
        <span class="nav-label">Perpustakaan</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('config-book-categories.index') }}">Kategori Buku</a></li>
    </ul>
    <ul class="nav-2-level collapse">
        <li><a href="{{ route('config-libraries.index') }}">Daftar Buku</a></li>
    </ul>
</li>
<li>
    <a href="javascript:;"><i class="sidebar-item-icon fa fa-gear"></i><span class="nav-label">Konfigurasi</span><i class="fa fa-angle-left arrow"></i></a>
    <ul class="nav-2-level collapse">
        @role('Super Admin')
        <li><a href="{{ route('config-admin.index') }}">Admin</a></li>
        <li><a href="{{ route('config-wilayah.index') }}">Data Wilayah</a></li>
        <li><a href="{{ route('app-version.index') }}">Versi Aplikasi</a></li>
        @endrole
    </ul>
</li>
@elserole('Super Admin Sekolah|Admin Sekolah')
    @include('pages.smart_school.menu')
@endrole
