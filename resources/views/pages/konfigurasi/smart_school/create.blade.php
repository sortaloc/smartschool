@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'schoolCreate', 'route' => ['config-schools.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('', 'Nama Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('nama_sekolah', null, ['class' => 'form-control', 'placeholder' => 'Nama Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Tipe Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::select('tipe_sekolah', ['kemendikbud' => 'Kementerian Pendidikan dan Kebudayaan', 'kemenag' => 'Kementrian Agama' ], null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NPSN', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NPSN', null, ['class' => 'form-control', 'placeholder' => 'NPSN']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'NIS/NSS/NDS', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('NIS_NSS_NDS', null, ['class' => 'form-control', 'placeholder' => 'NIS/NSS/NDS']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Jumlah Akun', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('jumlah_akun', null, ['class' => 'form-control', 'placeholder' => 'Jumlah Akun']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Email Admin Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email Admin Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Kontak/No HP Admin Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('no_hp', null, ['class' => 'form-control', 'placeholder' => 'Kontak/No HP Admin Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Masa Aktif', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('masa_aktif', old('masa_aktif'), ['class' => 'form-control datepicker']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah Sekolah', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
$('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    startDate: '-3d',
    todayHighlight: true
});
</script>
@endsection