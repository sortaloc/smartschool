@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('config-schools.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat Sekolah</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama Sekolah</th>
                    <th>Jumlah Akun</th>
                    <th>Kontak</th>
                    <th>Masa Aktif</th>
                    <th width="100">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $scl)
                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $scl->name }}</td>
                    <td>{{ $scl->total_students }}</td>
                    <td>{{ $scl->phone }}</td>
                    <td>{{ $scl->expired_at }}</td>
                    <td align="center">
                    <!-- <form id="{{$scl->id}}" action="{{route('config-schools.destroy',$scl->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                    </form> -->
                        <a href="{{ route('config-schools.edit', $scl->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
