@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }} |
            <a href="{{ route('config-libraries.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat {{$title}}</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
            <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="10">No</th>
                        <th>Judul Buku</th>
                        <th>Kategori</th>
                        <th>Pengarang</th>
                        <th>Nomor Buku</th>
                        <th width="200">Cover</th>
                        <th>File</th>
                        <th width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $data->title }}</td>
                        <td>{{ $data->category->name }}</td>
                        <td>{{ $data->author }}</td>
                        <td>{{ $data->book_number }}</td>
                        <td>
                            <img src="{{ $data->cover }}" alt="">
                        </td>
                        <td>
                            <a href="{{ $data->file }}" class="badge badge-primary" target="_blank">Buka</a>
                        </td>
                        <td>
                            {{ Form::open(['id' => 'destroy'.$data->id, 'route' => ['config-libraries.destroy', $data->id], 'method' => 'DELETE', 'class' => 'd-inline']) }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                            <a href="{{ route('config-libraries.edit', $data->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="form-group">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
