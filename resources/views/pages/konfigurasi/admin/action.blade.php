@role('Super Admin')
    <form id="delete{{$adm->id}}" action="{{route('config-admin.destroy',$adm->id)}}" method="post" style="display:inline">
    @method('DELETE')
    @csrf
        <button onclick="return confirm('Yakin Ingin Hapus Admin Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash"></i></button>
    </form>
    @if($adm->is_active == true)
    <form id="nonaktif{{$adm->id}}" action="{{route('config-admin.update',$adm->id)}}" method="post" style="display:inline">
    @method('PATCH')
    @csrf
    <input type="hidden" name="type" value="nonActive">
        <button onclick="return confirm('Yakin Ingin Menonaktifkan Admin Ini?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-times"></i></button>
    </form>
    @else
    <form id="aktif{{$adm->id}}" action="{{route('config-admin.update',$adm->id)}}" method="post" style="display:inline">
    @method('PATCH')
    @csrf
    <input type="hidden" name="type" value="active">
        <button onclick="return confirm('Yakin Ingin Mengaktifkan Admin Ini?')"  class="btn  btn-success" type="submit" value="Delete"><i class="fa fa-check"></i></button>
    </form>
    @endif
@endrole