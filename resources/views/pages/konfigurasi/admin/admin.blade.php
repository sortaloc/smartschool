@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
            {{-- <a href="{{ route('config-admin.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat Admin</a> --}}
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Avatar</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th width="100">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
$(document).ready( function () {
    $('#table2').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('config-admin.index') }}",
        columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'avatar', name: 'avatar', orderable: false, searchable: false },
                { data: 'name', name: 'name', orderable: true, searchable: true },
                { data: 'username', name: 'username', orderable: true, searchable: true },
                { data: 'email', name: 'email', orderable: true, searchable: true },
                { data: 'level', name: 'level' },
                { data: 'status', name: 'status', orderable: false, searchable: false},
                { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
    });
});
</script>
@endsection
