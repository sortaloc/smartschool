@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'babCreate', 'route' => ['headline-news.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                <div class="form-group">
                    {{ Form::label('name', 'Judul', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('title', old('title') ? old('title') : $data->title, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Judul']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        {{ Form::textarea('headline_news', old('headline_news') ? old('headline_news') : $data->headline_news, ['class' => 'form-control editor', 'readOnly']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('logo', 'Gambar', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="image-upload">
                            <div class="image-edit">
                                <input name="image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="image-preview">
                                <div id="imagePreview" style="background-image: url('{{$data->image}}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Cancel</button>
                    {{ Form::button('<i class="fa fa-save"></i> Edit Headline News', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script src="/template/vendors/tinymce/tinymce.min.js"></script>
<script src="/template/vendors/tinymce/config_modal.js"></script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
@endsection
