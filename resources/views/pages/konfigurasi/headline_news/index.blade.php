@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
            <a href="{{ route('headline-news.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat Headline News</a>
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Judul</th>
                    <th>Gambar</th>
                    <th>Headline News</th>
                    <th width="100">Tanggal</th>
                    <th width="100">Tindakan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $d->title }}</td>
                    <td><img src="{{ $d->image }}" width="100" alt=""></td>
                    <td>{!! $d->headline_news !!}</td>
                    <td>{{ date('d-M-Y', strtotime($d->created_at)) }}</td>
                    <td align="center">
                    <form id="{{$d->id}}" action="{{route('headline-news.destroy',$d->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                        <a href="{{ route('headline-news.edit', $d->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
