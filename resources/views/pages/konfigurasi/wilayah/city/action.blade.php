<form id="{{$val->id}}" action="{{route('config-city.destroy',$val->id)}}" method="post" style="display:inline">
@method('DELETE')
@csrf
<button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
</form>
<a href="{{ route('config-city.edit', $val->id) }}" class="btn btn-secondary"><i class="fa fa-pencil"></i></a>