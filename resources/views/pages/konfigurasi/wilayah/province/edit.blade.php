@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'provinceCreate', 'route' => ['config-province.update', $data->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('nama', 'Nama', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('name', $data->name, ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Nama Provinsi']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('bsni', 'BSNI', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('p_bsni', $data->p_bsni, ['class' => 'form-control', 'id' => 'bsni', 'placeholder' => 'BSNI Provinsi']) }}
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Ubah Provinsi', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endsection
