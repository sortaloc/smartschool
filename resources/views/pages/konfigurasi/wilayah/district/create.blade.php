@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'DistrictCreate', 'route' => ['config-district.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('kot', 'Kota / Kab', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        <select class="city form-control" name="city_id"></select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('nama', 'Nama', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Nama Kecamatan']) }}
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah Kecamatan', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
$('.city').select2({
    ajax: {
        url: '/ajax-city',
        dataType: 'json'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});
</script>
@endsection
