@extends('pages.home')
@section('content')
@include('flash')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Provinsi |
            <a href="{{ route('config-province.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">NO</th>
                    <th>Nama</th>
                    <th>BSNI</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Kota / Kab |
            <a href="{{ route('config-city.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table3" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">NO</th>
                    <th>Nama</th>
                    <th>BSNI</th>
                    <th>Provinsi</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            Kecamatan |
            <a href="{{ route('config-district.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>

    <div class="ibox-body">
        <table class="table table-striped table-bordered table-hover" id="table4" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">NO</th>
                    <th>Nama</th>
                    <th>Kota / Kab</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#table2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('province.datatable') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    { data: 'name', name: 'name' },
                    { data: 'p_bsni', name: 'p_bsni' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
            });
        });
</script>
<script>
    $(document).ready( function () {
        $('#table3').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('city.datatable') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    { data: 'name', name: 'name' },
                    { data: 'c_bsni', name: 'c_bsni' },
                    { data: 'province.name', name: 'province.name' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
            });
        });
</script>
<script>
    $(document).ready( function () {
        $('#table4').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('district.datatable') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                    { data: 'name', name: 'name' },
                    { data: 'city.name', name: 'city.name' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
            });
        });
</script>
@endsection
