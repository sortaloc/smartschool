@extends('pages.home')
@section('content')
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    {{ $title }} Forms
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="ibox-body">
                {{ Form::open(['id' => 'VersionCreate', 'route' => ['app-version.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

                <div class="form-group">
                    {{ Form::label('version', 'Versi Aplikasi', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('version', null, ['class' => 'form-control', 'id' => 'version', 'placeholder' => 'Versi Aplikasi']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Description', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Deskripsi']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Tambah Versi Aplikasi', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@endsection
