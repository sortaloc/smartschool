@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
            {{-- <a href="{{ route('config-admin.create') }}" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat Admin</a> --}}
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Versi Aplikasi</th>
                    <th>Deskripsi</th>
                    <th>Tanggal</th>
                    <th width="100">Tindakan</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $v)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $v->version }}</td>
                    <td class="soal">{{ $v->description }}</td>
                    <td>{{ $v->created_at }}</td>
                    <td align="center">
                    @role('Super Admin')
                    <form id="delete{{$v->id}}" action="{{route('app-version.destroy',$v->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Yakin Ingin Hapus Versi Aplikasi Ini ?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash"></i></button>
                    </form>
                    <a href="{{ route('app-version.edit', $v->id) }}"><i class="btn btn-small btn-warning fa fa-pencil"></i></a>
                    @endrole
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    // Append all paragraphs
    $("#table_length").append('<a  href="{{ route('app-version.create') }}"> <button type="button" class="btn btn-outline-primary ml-3">Tambah</button></a>');
});
</script>
@endsection
