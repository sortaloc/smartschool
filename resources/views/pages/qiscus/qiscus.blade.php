@extends('pages.home')
@section('content')
    <link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans" rel="stylesheet" />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
      crossorigin="anonymous"
    />

    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/login-page.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/chat-list-page.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/users-page.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/toolbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/create-group.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/room-info.css') }}" />
    <style>
    .qis-button {
        position: fixed;
        bottom: 0;
    }
    .qis-chat {
        position: fixed;
        bottom: 0;
        width : 500px;
    }
    </style>

    <button type="button" class="toggle-widget-btn qis-button">Chat</button>
    <div id="qiscus-widget" class="widget-container qis-chat">
        <div class="title-bar">
        {{ $title }}
        </div>
        <input type="file" id="input-image" accept="image/*" style="display:none" />
        <input type="file" id="input-file" style="display:none" />
        <div class="widget-content" id="widget-content"></div>
    </div>
    <input type="hidden" id="genius_id" value="{{$app_id}}">
    <input type="hidden" id="user_email" value="{{ Auth::user()->email }}">
    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" id="user_key" value="{{ Auth::user()->profile->phone_number }}">
    <input type="hidden" id="display_name" value="{{ Auth::user()->profile->name }}">
    <input type="hidden" id="avatar" value="{{ Auth::user()->profile->avatar }}">
    <input type="hidden" id="position" value="{{ auth()->user()->level }}">
    @if(auth()->user()->mentor)
    <input type="hidden" id="extras" value="{{ $extras }}">
    <input type="hidden" id="mentor_id" value="{{ auth()->user()->mentor->id }}">
    @endif
    <script src="/js/lib/qiscus-sdk-core.min.js"></script>

    <script src="https://unpkg.com/showdown"></script>
    <script src="{{ asset('js/lib/require.js') }}" data-main="{{ asset('js/main_qiscus.js') }}"></script>
@endsection
