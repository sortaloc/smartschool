
@if($usr->is_active == true)
<span class="badge badge-success">Verified</span>
@else
<form id="aktif{{$usr->id}}" action="{{route('data-user.update',$usr->id)}}" method="post" style="display:inline">
@method('PATCH')
@csrf
<input type="hidden" name="type" value="active">
<button type="hidden" onclick="return confirm('Yakin Ingin Memverifikasi User Ini?')" value="submit" style="display:none" id="submit{{$usr->id}}"></button>
<span class="btn badge badge-danger" onclick="document.getElementById('submit{{$usr->id}}').click()" type="submit" value="Delete">Unverified</span>
</form>
@endif