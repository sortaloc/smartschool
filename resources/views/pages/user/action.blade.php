<div style="min-width:70px" class="text-center">
    <a href="{{route('data-user.show', $usr->id)}}" target="_blank" class="btn  btn-info"><i class="fa fa-info"></i></a>
    @role('Super Admin|Operator')
    <form id="delete{{$usr->id}}" action="{{route('data-user.destroy',$usr->id)}}" method="post" style="display:inline">
    @method('DELETE')
    @csrf
    <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash"></i></button>
    </form>
    @endrole
</div>
