@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Kabupaten</th>
                    <th>Nama Paket</th>
                    <th>Countdown</th>
                    <th>Kadaluarsa</th>
                    <th width="40">Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($member as $usr)
                <tr class="{{$usr->paket->countdown < 15 ? 'table-warning' : ''}} {{$usr->paket->countdown < 5 ? 'table-danger' : ''}}">
                    <td>{{$loop->iteration}}</td>
                    <td>
                        <a target="_blank" href="{{route('data-user.show', $usr->user_id)}}">{{$usr->user->username}}</a>
                    </td>
                    <td>{{$usr->user->email}}</td>
                    @if($usr->user->profile->district)
                    <td>{{$usr->user->profile->district->city->name}}</td>
                    @elseif($usr->user->profile->city)
                    <td>{{$usr->user->profile->city->name}}</td>
                    @else
                    <td></td>
                    @endif
                    <td>{{$usr->paket->name}}</td>
                    <td>
                        {{$usr->paket->countdown}} Hari
                    </td>
                    <td>{{ date('d-M-Y', strtotime($usr->paket->expired_at)) }}</td>
                    <td class="text-center">
                        @role('Super Admin|Operator')
                        <form id="delete{{$usr->id}}" action="{{route('member.destroy',$usr->id)}}" method="post" style="display:inline">
                        @method('DELETE')
                        @csrf
                            <button onclick="return confirm('Yakin Ingin Hapus Data?')"  class="btn  btn-danger" type="submit" value="Delete"><i class="fa fa-trash"></i></button>
                        </form>
                        @endrole
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@role('Super Admin|Operator')
<script type="text/javascript">
$(document).ready(function(){
    // Append all paragraphs
    $("#table_length").append('<a  href="{{ route('member-export') }}"> <button type="button" class="btn btn-outline-primary ml-3">Export ke Excel</button></a>');
});
</script>
@endrole
@endsection
