<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
    <div class="card">
        <div class="card-body">
        <div class="text-center">
            <h1>DATA MEMBER GENIUS</h1>
        </div>
            <table id="data-admin-2" class="table table-bordered">
            <thead>
                <tr>
                    <td>NAMA</td>
                    <td>EMAIL</td>
                    <td>NO. HP</td>
                    <td>ALAMAT</td>
                    <td>KECAMATAN</td>
                    <td>KABUPATEN</td>
                    <td>PROVINSI</td>
                    <td>NAMA PAKET</td>
                    <td>COUNTDOWN</td>
                    <td>TANGGAL KADALUARSA</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $usr)
                <tr>
                    <td>{{$usr->user->profile->name}}</td>
                    <td>{{$usr->user->email}}</td>
                    <td>{{$usr->user->profile->phone_number}}</td>
                    <td>{{$usr->user->profile->address}}</td>
                    @if($usr->user->profile->district)
                    <td>{{$usr->user->profile->district->name}}</td>
                    <td>{{$usr->user->profile->district->city->name}}</td>
                    <td>{{$usr->user->profile->district->city->province->name}}</td>
                    @elseif($usr->user->profile->city)
                    <td></td>
                    <td>{{$usr->user->profile->city->name}}</td>
                    <td>{{$usr->user->profile->city->province->name}}</td>
                    @else
                    <td></td>
                    <td></td>
                    <td></td>
                    @endif
                    <td>{{$usr->paket->name}}</td>
                    <td>{{$usr->paket->countdown}} hari</td>
                    <td>{{ date('d-M-Y', strtotime($usr->paket->expired_at)) }}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</body>
</html>