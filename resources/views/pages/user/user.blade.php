@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
        </div>
    </div>

    <div class="ibox-body">
        @include('flash')
        <table class="table table-striped table-bordered table-hover" id="table2" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>No Handphone</th>
                    <th>Kabupaten</th>
                    <th>Level</th>
                    <th>Status Bayar</th>
                    <th>Status User</th>
                    <th width="80">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#table2').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('user.paginate') }}",
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'username', name: 'username' },
                    { data: 'email', name: 'email' },
                    { data: 'phone_number', name: 'user_profiles.phone_number', orderable: false, searchable: false},
                    { data: 'city', name: 'city1.name', orderable: false, searchable: false },
                    { data: 'level', name: 'level', searchable: false},
                    { data: 'is_premium', name: 'member', searchable: false},
                    { data: 'is_verified', name: 'is_active' , searchable: false},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
        });
    });
    @role('Super Admin|Operator')
    $(document).ready(function(){
        $("#table2_length").append('<a  href="{{ route('user-export') }}"> <button type="button" class="btn btn-outline-primary ml-3">Export ke Excel</button></a>');
    });
    @endrole
</script>
@endsection
