<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
    <div class="card">
        <div class="card-body">
        <div class="text-center">
            <h1>DATA USER GENIUS</h1>
        </div>
            <table id="data-admin-2" class="table table-bordered">
            <thead>
                <tr>
                    <td>NAMA</td>
                    <td>EMAIL</td>
                    <td>NO. HP</td>
                    <td>NO. HP ORANG TUA</td>
                    <td>JENIS KELAMIN</td>
                    <td>TANGGAL LAHIR</td>
                    <td>ALAMAT</td>
                    <td>KECAMATAN</td>
                    <td>KABUPATEN</td>
                    <td>PROVINSI</td>
                    <td>SEKOLAH</td>
                    <td>NO. KIP</td>
                    <td>STATUS LANGGANAN</td>
                    <td>STATUS USER</td>
                    <td>TANGGAL BERGABUNG</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $usr)
                <tr>
                    <td>{{$usr->profile->name}}</td>
                    <td>{{$usr->email}}</td>
                    <td>{{$usr->profile->phone_number}}</td>
                    <td>{{$usr->profile->parents_phone_number}}</td>
                    <td>{{$usr->profile->gender}}</td>
                    <td>{{$usr->profile->date_of_birth}}</td>
                    <td>{{$usr->profile->address}}</td>
                    @if($usr->profile->district)
                    <td>{{$usr->profile->district->name}}</td>
                    <td>{{$usr->profile->district->city->type}} {{$usr->profile->district->city->name}}</td>
                    <td>{{$usr->profile->district->city->province->name}}</td>
                    @elseif($usr->profile->city)
                    <td>-</td>
                    <td>{{$usr->profile->city->type}} {{$usr->profile->city->name}}</td>
                    <td>{{$usr->profile->city->province->name}}</td>
                    @else
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    @endif
                    <td>{{$usr->profile->school_name}}</td>
                    <td>{{$usr->profile->kip_number}}</td>
                    <td>
                        @if($usr->is_premium == true)
                            Langganan
                        @else
                            Gratis
                        @endif
                    </td>
                    <td>
                        @if($usr->is_active == true)
                            Verified
                        @else
                            Unverified
                        @endif
                    </td>
                    <td>{{$usr->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</body>
</html>