@extends('pages.home')
@section('content')
<style>
.avatar-wrapper {
	 position: relative;
	 height: 200px;
	 width: 200px;
	 margin: 50px auto;
	 border-radius: 50%;
	 overflow: hidden;
	 box-shadow: 1px 1px 15px -5px black;
	 transition: all 0.3s ease;
}
 .avatar-wrapper:hover {
	 transform: scale(1.05);
	 cursor: pointer;
}
 .avatar-wrapper:hover .profile-pic {
	 opacity: 0.5;
}
 .avatar-wrapper .profile-pic {
	 height: 100%;
	 width: 100%;
	 transition: all 0.3s ease;
}
 .avatar-wrapper .profile-pic:after {
	 font-family: FontAwesome;
	 content: "\f007";
	 top: 0;
	 left: 0;
	 width: 100%;
	 height: 100%;
	 position: absolute;
	 font-size: 190px;
	 background: #ecf0f1;
	 color: #34495e;
	 text-align: center;
}
 .avatar-wrapper .upload-button {
	 position: absolute;
	 top: 0;
	 left: 0;
	 height: 100%;
	 width: 100%;
}
 .avatar-wrapper .upload-button .fa-arrow-circle-up {
	 position: absolute;
	 font-size: 234px;
	 top: -17px;
	 left: 0;
	 text-align: center;
	 opacity: 0;
	 transition: all 0.3s ease;
	 color: #34495e;
}
 .avatar-wrapper .upload-button:hover .fa-arrow-circle-up {
	 opacity: 0.9;
}

</style>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @else
    @include('flash')
    @endif
<div class="row">
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    Profile
                </div>
            </div>
            <div class="ibox-body">
                {{ Form::open(['id' => 'profileCreate', 'route' => ['profile.update', auth()->user()->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                <input type="hidden" name="type" value="change-profile">
                <div class="form-group">
                    {{ Form::label('avatarLabel', 'Avatar', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="avatar-wrapper">
                            <img class="profile-pic" src="{{ auth()->user()->profile->avatar }}" />
                            <div class="upload-button">
                                <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                            </div>
                            <input name="avatar" class="file-upload" type="file" accept="image/*"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('nama', 'Nama', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('name', auth()->user()->profile->name, ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Nama']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('nama_sekolah', 'Nama Sekolah', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('school_name', auth()->user()->guru->school_name, ['class' => 'form-control', 'id' => 'nama_sekolah', 'placeholder' => 'Nama Sekolah']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('mata_pelajaran', 'Mata Pelajaran', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('mata_pelajaran', auth()->user()->guru->mata_pelajaran, ['class' => 'form-control', 'id' => 'matapelajaran', 'placeholder' => 'Mata Pelajaran']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('nip', 'NIP', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::number('nip', auth()->user()->guru->nip, ['class' => 'form-control', 'id' => 'nip', 'placeholder' => 'Nomor Induk Pegawai']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('emailLabel', 'Email', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::text('email', auth()->user()->email, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Update', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">
                    Password
                </div>
            </div>
            <div class="ibox-body">
                {{ Form::open(['id' => 'passwordUpdate', 'route' => ['profile.update', auth()->user()->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) }}
                <input type="hidden" name="type" value="change-password">

                <div class="form-group">
                    {{ Form::label('null', 'Password Lama', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::password('old_password', ['class' => 'form-control', 'id' => 'old_password', 'placeholder' => 'Password Lama']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('null', 'Password Baru', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password', 'placeholder' => 'Password Baru']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('null', 'Konfirmasi Password Baru', ['class' => 'font-bold']) }}
                    <div class="input-group-icon left">
                        <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                        {{ Form::password('new_password_confirmation', ['class' => 'form-control', 'id' => 'new_password_confirmation', 'placeholder' => 'Konfirmasi Password Baru']) }}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger" onclick="window.history.go(-1)" type="button"><i class="fa fa-arrow-left"></i>
                        Batal</button>
                    {{ Form::button('<i class="fa fa-save"></i> Update', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {

    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
</script>
@endsection
