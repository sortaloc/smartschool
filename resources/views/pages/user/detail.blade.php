@extends('pages.home')
@section('content')
<div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">
            {{ $title }}
            {{-- <a href="" class="btn btn-sm btn-outline-primary"><i class="fa fa-plus"></i> Buat User</a> --}}
        </div>
    </div>
    <style>
    table, tr, td {
        border: 0!important;
        }
    </style>
    <div class="ibox-body">
        @include('flash')
        <table class="table table-borderless" id="table" cellspacing="0" cellpadding="0" style="border: none;">
            <tbody>
                <tr style="border: 0;">
                    <td width="200">Nama Lengkap</td>
                    <td>:  {{$user->profile->name}}</td>
                </tr>
                <tr>
                    <td width="200">Email</td>
                    <td>:  {{$user->email}}</td>
                </tr>
                <tr>
                    <td width="200">Username</td>
                    <td>:  {{$user->username}}</td>
                </tr>
                <tr>
                    <td width="200">No Hp</td>
                    <td>:  {{$user->profile->phone_number}}</td>
                </tr>
                <tr>
                    <td width="200">No Hp Orang Tua</td>
                    <td>:  {{$user->profile->parents_phone_number}}</td>
                </tr>
                <tr>
                    <td width="200">Jenis Kelamin</td>
                    <td>:  {{$user->profile->gender == 'L' ? 'Laki-laki' : 'Perempuan'}}</td>
                </tr>
                <tr>
                    <td width="200">Tanggal Lahir</td>
                    <td>:  {{$user->profile->date_of_birth}}</td>
                </tr>
                <tr>
                    <td width="200">No KIP</td>
                    <td>:  {{$user->profile->kip_number}}</td>
                </tr>
                <tr>
                    <td width="200">Nama Sekolah</td>
                    <td>:  {{$user->profile->school_name}}</td>
                </tr>
                @if($user->profile->district)
                <tr>
                    <td width="200">Kecamatan</td>
                    <td>:  {{$user->profile->district->name}}</td>
                </tr>
                <tr>
                    <td width="200">Kabupaten / Kota</td>
                    <td>:  {{$user->profile->district->city->type.' '.$user->profile->district->city->name}}</td>
                </tr>
                <tr>
                    <td width="200">Provinsi</td>
                    <td>: {{$user->profile->district->city->province->name}}</td>
                </tr>
                @endif
                <tr>
                    <td width="200">Alamat</td>
                    <td>:  {{$user->profile->address}}</td>
                </tr>
                <tr>
                    <td width="200">Avatar</td>
                    <td>:  <img src="{{$user->profile->avatar}}" width="150" alt="" srcset=""></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
