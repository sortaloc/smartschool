<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Genius Backoffice</title>
    @include('layout._header_users')
    @include('layout._footer_users')
    @include('pages.partials._colors')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="{{ route('home.index') }}">
                    <span class="brand">PASINAON&nbsp;
                        <span class="brand-tip"></span>
                    </span>
                    <span class="brand-mini">
                    <img src="{{ asset('template/images/logo2.png') }}"/>
                    </span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    <li>
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    @include('pages.partials._user')
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <a href="{{ route('profile.index') }}">
                    <div class="admin-block d-flex">
                        <div class="image-square-45">
                            <img class="image-square" src="{{ AdminHelp::getAuthAvatar() }}" class="rounded-circle"/>
                        </div>
                        <div class="admin-info">
                            <div class="font-strong">{{ AdminHelp::getAuthName() }}</div><small>{{ AdminHelp::getAuthLevel() }}</small>
                        </div>
                    </div>
                </a>
                <ul class="side-menu metismenu">
                    @include('pages.partials._menu')
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            @if(isset($title) && isset($sub_title))
            <div class="page-heading">
                <h1 class="page-title">{{ $title }}</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">{{ $sub_title }}</li>
                </ol>
            </div>
            @endif
            <div class="page-content fade-in-up">
                @yield('content')
            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">{{ date('Y') }} © <b>Eduraya</b> - All rights reserved.</div>
                <a class="px-4" href="#">Version 0.0.01</a>
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>

    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
</body>

</html>
