<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Admin | Register</title>
    @include('layout._header_users')
    @include('layout._footer_users')

    <!-- PAGE LEVEL STYLES-->
    <link href="{{ asset('template/css/pages/auth-light.css') }}" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content">
        <div class="brand"></div>

        {{ Form::open(['id' => 'register-form', 'autocomplete' => 'off', 'route' => ['register.store'], 'method' => 'POST', 'enctype' => "multipart/form-data"]) }}
            <center>
                <img src="{{ asset('template/images/logo1.png') }}" class="login-title" width="220" alt="">
            </center>
            <br>
            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                {{ Form::label('email', 'Email', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-warning"></i></div>
                    {{ Form::email('email', old('email'), ['class' => 'form-control text-uppercase', 'autocomplete' => 'new-password', 'id' => 'email', 'placeholder' => 'Email']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('nama', 'Nama Lengkap', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-user text-warning"></i></div>
                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Nama Lengkap']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('username', 'Username', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-user text-warning"></i></div>
                    {{ Form::text('username', old('username'), ['class' => 'form-control text-uppercase', 'autocomplete' => 'new-password', 'id' => 'username', 'placeholder' => 'Username']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-key text-warning"></i></div>
                    <input type="password" value="{{ old('password') }}" autocomplete="new-password" name="password" class="form-control" placeholder="Masukan Password">
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password_confirmation', 'Konfirmasi Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-key text-warning"></i></div>
                    <input type="password" value="{{ old('konfirmasi_password') }}" name="konfirmasi_password" class="form-control" placeholder="Masukan Ulang Password">
                </div>
            </div>

            <div class="form-group" id="pilih-job">
                {{ Form::label('jenjang', 'Pilih Role', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-header text-warning"></i></div>
                    {{ Form::select('role', [1 => 'Admin', 2 => 'Super Admin'], old('role'), ['class' => 'form-control', 'id' => 'job', 'placeholder' => '-- Pilih Role --']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('avatar', 'Avatar (max 4MB)', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-image text-warning"></i></div>
                    {{ Form::file('avatar', ['class' => 'form-control', 'id' => 'avatar', 'placeholder' => 'avatar']) }}
                </div>
            </div>
            <div class="form-group d-flex justify-content-between">
                <a href="{{ route('login') }}">Sudah Punya Akun?</a>
            </div>
            <div class="form-group">
                {{ Form::button('<i class="fa fa-save"></i> Daftar', ['type' => 'submit', 'class' => 'btn btn-warning btn-block']) }}
            </div>
            <label for=""></label>
        {{ Form::close() }}
    </div>
    <br>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <script src="{{ asset('template/vendors/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#register-forms').validate({
                errorClass: "help-block",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });
        });
    </script>
</body>

</html>
