<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Admin | Login</title>
    @include('layout._header_users')

    <!-- PAGE LEVEL STYLES-->
    <link href="{{ asset('template/css/pages/auth-light.css') }}" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content">
        <div class="brand">
            <br/>
        </div>

        {{ Form::open(['id' => 'login-form', 'route' => ['login.store'], 'method' => 'POST']) }}

            <center>
                <img src="{{ asset('template/images/logo1.png') }}" class="login-title" width="220" alt=""> &nbsp;
                {{-- <img src="{{ asset('template/images/') }}" width="230" alt=""> --}}

            </center>

            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                {{ Form::label('username', 'Username or Email', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-warning"></i></div>
                    {{ Form::text('username', null, ['class' => 'form-control', 'id' => 'username', 'placeholder' => 'Username or Email']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('password', 'Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-warning"></i></div>
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password']) }}
                </div>
            </div>
            <div class="form-group d-flex justify-content-between">
            </div>
            <div class="form-group">
                {{ Form::button('<i class="fa fa-sign-in"></i> Login', ['type' => 'submit', 'class' => 'btn btn-warning btn-block']) }}
            </div>
        {{ Form::close() }}
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    @include('layout._footer_users')
</body>

</html>
