<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Admin | Register</title>
    @include('layout._header_users')
    @include('layout._footer_users')

    <!-- PAGE LEVEL STYLES-->
    <link href="{{ asset('template/css/pages/auth-light.css') }}" rel="stylesheet" />
</head>

<body class="bg-silver-300">
    <div class="content">
        <div class="brand"></div>

        {{ Form::open(['id' => 'register-form', 'autocomplete' => 'off', 'route' => ['register.store'], 'method' => 'POST', 'enctype' => "multipart/form-data"]) }}
            <center>
                <img src="{{ asset('template/images/genius.png') }}" class="login-title" width="120" alt="">
            </center>
            <br>
            @if ($errors->any())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @foreach ($errors->all() as $error)
                            <ul>
                                <li><strong>{{ $error }}</strong></li>
                            </ul>
                    @endforeach
                </div>
            @endif
            <div class="form-group">
                {{ Form::label('email', 'Email', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-envelope text-info"></i></div>
                    {{ Form::email('email', old('email'), ['class' => 'form-control text-uppercase', 'autocomplete' => 'new-password', 'id' => 'email', 'placeholder' => 'Email']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('username', 'Username', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-user text-info"></i></div>
                    {{ Form::text('username', old('username'), ['class' => 'form-control text-uppercase', 'autocomplete' => 'new-password', 'id' => 'username', 'placeholder' => 'Username']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password', 'Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-key text-info"></i></div>
                    <input type="password" value="{{ old('password') }}" autocomplete="new-password" name="password" class="form-control" placeholder="Masukan Password">
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('password_confirmation', 'Konfirmasi Password', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-key text-info"></i></div>
                    <input type="password" value="{{ old('konfirmasi_password') }}" name="konfirmasi_password" class="form-control" placeholder="Masukan Ulang Password">
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('nama', 'Nama Lengkap dan Gelar', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-user text-info"></i></div>
                    {{ Form::text('nama', old('nama'), ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Nama Lengkap']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('nomor_hp', 'Nomor Handphone', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-phone text-info"></i></div>
                    {{ Form::number('nomor_hp', old('nomor_hp'), ['class' => 'form-control', 'id' => 'nomor_hp', 'placeholder' => 'Nomor Handphone']) }}
                </div>
            </div>

            <div class="form-group" id="pilih-job">
                {{ Form::label('jenjang', 'Pilih Job', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-header text-info"></i></div>
                    {{ Form::select('pilih_job', $role, null, ['class' => 'form-control', 'id' => 'job', 'placeholder' => '-- Pilih Job --']) }}
                </div>
            </div>

            <div id="custom-mentor"></div>

            <div class="form-group">
                {{ Form::label('lok_tugas', 'Lokasi Tugas', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-map text-info"></i></div>
                    {{ Form::text('lokasi_tugas', old('pilih_job'), ['class' => 'form-control', 'id' => 'lok_tugas', 'placeholder' => 'Lokasi Tugas']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('avatar', 'Avatar (max 4MB)', ['class' => 'font-bold']) }}
                <div class="input-group-icon left">
                    <div class="input-icon"><i class="fa fa-image text-info"></i></div>
                    {{ Form::file('avatar', ['class' => 'form-control', 'id' => 'avatar', 'placeholder' => 'avatar']) }}
                </div>
            </div>
            <div class="form-group d-flex justify-content-between">
                <a href="{{ route('login.admin') }}">Sudah Punya Akun?</a>
            </div>
            <div class="form-group">
                {{ Form::button('<i class="fa fa-save"></i> Daftar', ['type' => 'submit', 'class' => 'btn btn-primary btn-block']) }}
            </div>
            <label for=""></label>
        {{ Form::close() }}
    </div>
    <br>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <script src="{{ asset('template/vendors/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $('#register-forms').validate({
                errorClass: "help-block",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });
        });
        $('#job').change(function(){
            if(this.value == "Mentor"){
                $('#custom-mentor').append('<div class="form-group">\
                    <label for="spesialisasi" class="font-bold"><u>Spesialisasi</u></label></br>\
                    <label class="font-bold">Jenjang :</label>\
                    <div ml-4">\
                        '+jenjang()+'\
                    </div>\
                    <label class="font-bold">Mata Pelajaran :</label>\
                    <div ml-4">\
                        '+mapel()+'\
                    </div>\
                    <label class="font-bold">Jam Layanan :</label>\
                    <div ml-4">\
                        '+jamLayanan()+'\
                    </div>\
                    <div ml-4">\
                        <div class="form-group">\
                            <label><b>Alumni</b></label>\
                            <div class="input-group-icon left">\
                                <div class="input-icon"><i class="fa fa-university text-info"></i></div>\
                                <input name="alumni" type="text" class="form-control" placeholder="Alumni">\
                            </div>\
                        </div>\
                    </div>\
                    <div ml-4">\
                        <div class="form-group">\
                            <label><b>Berpengalaman Sejak</b></label>\
                            <div class="input-group-icon left">\
                                <div class="input-icon"><i class="fa fa-calendar text-info"></i></div>\
                                <input name="pengalaman" type="date" class="form-control">\
                            </div>\
                        </div>\
                    </div>\
                </div>');
            }else{
                $('#custom-mentor').empty();
            }
        });
        function jenjang(){
            var jenjangs = {!! json_encode($jenjang) !!}
            var el = ''
            for(var x in jenjangs){
                el2 = '<div class="form-check form-check-inline pl-4">\
                            <input name="mentor_jenjang['+x+']" class="form-check-input" type="checkbox" value="'+jenjangs[x].id+'" id="cbj'+x+'">\
                            <label class="form-check-label pl-1" for="cbj'+x+'">\
                                '+jenjangs[x].name+'\
                            </label>\
                        </div>'
                el = el + el2;
            }
            return el;
        }
        function mapel(){
            var mapels = {!! json_encode($mapel) !!}
            var el = ''
            for(var x in mapels){
                el2 = '<div class="form-check pl-4">\
                            <input name="mentor_mapel['+x+']" class="form-check-input" type="checkbox" value="'+mapels[x].name+'" id="cbm'+x+'">\
                            <label class="form-check-label pl-1" for="cbm'+x+'">\
                                '+mapels[x].name+'\
                            </label>\
                        </div>'
                el = el + el2;
            }
            return el;
        }
        function jamLayanan(){
            return '<div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Senin</label>\
                        <div class="col-sm-4">\
                            <input name="senin1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="senin2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Selasa</label>\
                        <div class="col-sm-4">\
                            <input name="selasa1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="selasa2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Rabu</label>\
                        <div class="col-sm-4">\
                            <input name="rabu1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="rabu2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Kamis</label>\
                        <div class="col-sm-4">\
                            <input name="kamis1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="kamis2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Jumat</label>\
                        <div class="col-sm-4">\
                            <input name="jumat1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="jumat2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Sabtu</label>\
                        <div class="col-sm-4">\
                            <input name="sabtu1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="sabtu2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>\
                    <div class="form-group row">\
                        <label class="col-sm-2 col-form-label">Minggu</label>\
                        <div class="col-sm-4">\
                            <input name="Minggu1" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                        <div class="col-sm-1">\
                            -\
                        </div>\
                        <div class="col-sm-4">\
                            <input name="minggu2" type="time" class="form-control" placeholder="boleh kosong">\
                        </div>\
                    </div>'
        }
    </script>
</body>

</html>
