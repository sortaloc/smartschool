<?php

namespace App\Imports;

use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolStudent;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolClassRef;
use App\Models\SmartSchools\SchoolSubclassRef;
use App\Models\SmartSchools\SchoolYear;
use App\Models\SmartSchools\SchoolStudentParent;
use App\Models\SmartSchools\SchoolStudentGuardian;
use App\Models\SmartSchools\SchoolStudentClass;
use App\Models\SmartSchools\SchoolGeneration;
use App\Models\Konfigurasi\District;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StudentsImport implements ToCollection, WithStartRow, WithMultipleSheets
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        $school_id = mySchool();
        $datas = [];
        foreach($rows->toArray() as $key => $val){
            if(isset($val[0])){
                $datas[$key]["nama"] = $val[1];
                $datas[$key]["email"] = $val[2];
                $datas[$key]["NIPD"] = $val[3];
                $datas[$key]["NISN"] = $val[4];
                $datas[$key]["tempat_lahir"] = $val[5];
                $tanggal_lahir[$key] = is_numeric($val[6]) ? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($val[6]) : null;
                $datas[$key]["tanggal_lahir"] = $tanggal_lahir[$key] ? $tanggal_lahir[$key]->format('d-m-Y') : null;
                $datas[$key]["jenis_kelamin"] = $val[7];
                $datas[$key]["agama"] = $val[8];
                $datas[$key]["status_dalam_keluarga"] = $val[9];
                $datas[$key]["anak_ke"] = $val[10];
                $datas[$key]["alamat"] = $val[11];
                $datas[$key]["desa"] = $val[12];
                $datas[$key]["kecamatan"] = $val[13];
                $datas[$key]["kota"] = $val[14];
                $datas[$key]["provinsi"] = $val[15];
                $datas[$key]["kode_pos"] = $val[16];
                $datas[$key]["no_telp"] = $val[17];
                $datas[$key]["sekolah_asal"] = $val[18];
                $datas[$key]["kelas_sekarang"] = $val[19];
                $datas[$key]["sub_kelas_sekarang"] = $val[20];
                $datas[$key]["tahun_angkatan"] = $val[21];
                $datas[$key]["diterima_di_kelas"] = $val[22];
                $datas[$key]["diterima_di_sub_kelas"] = $val[23];
                $diterima_tanggal[$key] = is_numeric($val[24]) ? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($val[21]) : null;
                $datas[$key]["diterima_tanggal"] = $diterima_tanggal[$key] ? $diterima_tanggal[$key]->format('d-m-Y') : null;
                $datas[$key]["nama_ayah"] = $val[25];
                $datas[$key]["nama_ibu"] = $val[26];
                $datas[$key]["alamat_orang_tua"] = $val[27];
                $datas[$key]["desa_orang_tua"] = $val[28];
                $datas[$key]["kecamatan_orang_tua"] = $val[29];
                $datas[$key]["kota_orang_tua"] = $val[30];
                $datas[$key]["provinsi_orang_tua"] = $val[31];
                $datas[$key]["kode_pos_orang_tua"] = $val[32];
                $datas[$key]["no_telp_orang_tua"] = $val[33];
                $datas[$key]["pekerjaan_ayah"] = $val[34];
                $datas[$key]["pekerjaan_ibu"] = $val[35];
                $datas[$key]["nama_wali"] = $val[36];
                $datas[$key]["alamat_wali"] = $val[37];
                $datas[$key]["desa_wali"] = $val[38];
                $datas[$key]["kecamatan_wali"] = $val[39];
                $datas[$key]["kota_wali"] = $val[40];
                $datas[$key]["provinsi_wali"] = $val[41];
                $datas[$key]["kode_pos_wali"] = $val[42];
                $datas[$key]["no_telp_wali"] = $val[43];
                $datas[$key]["pekerjaan_wali"] = $val[44];
                Validator::make($datas[$key],
                    [
                        'nama' => 'required|string|max:150',
                        'email' => 'required|email|max:150|unique:users,email',
                        'NIPD' => 'nullable|numeric|max:999999999999999',
                        'NISN' => 'required|numeric|max:999999999999999|unique:users,username',
                        'tempat_lahir' => 'nullable|string|max:50',
                        'tanggal_lahir' => 'nullable|date_format:d-m-Y',
                        'jenis_kelamin' => 'nullable|in:L,P',
                        'agama' => 'nullable|in:ISLAM,KRISTEN,KATHOLIK,HINDU,BUDHA,KHONG HUCU',
                        'status_dalam_keluarga' => 'nullable|string|max:50',
                        'anak_ke' => 'nullable|string|max:50',
                        'alamat' => 'nullable|string|max:150',
                        'desa' => 'nullable|string|max:100',
                        'kecamatan' => 'nullable|string|max:150',
                        'kode_pos' => 'nullable',
                        'no_telp' => 'nullable',
                        'sekolah_asal' => 'nullable|string|max:50',
                        'kelas_sekarang' => ['nullable', Rule::exists('school_class_refs', 'name')->where(function ($query) use($school_id) {
                            return $query->where('school_id', $school_id);
                        })],
                        'sub_kelas_sekarang' => ['nullable', Rule::exists('school_subclass_refs', 'name')->where(function ($query) use($school_id) {
                            return $query->where('school_id', $school_id);
                        })],
                        'tahun_angkatan' => 'nullable|numeric|max:2030|min:1999',
                        'diterima_di_kelas' => ['nullable', Rule::exists('school_class_refs', 'name')->where(function ($query) use($school_id) {
                            return $query->where('school_id', $school_id);
                        })],
                        'diterima_di_sub_kelas' => ['nullable', Rule::exists('school_subclass_refs', 'name')->where(function ($query) use($school_id) {
                            return $query->where('school_id', $school_id);
                        })],
                        'diterima_tanggal' => 'nullable|date_format:d-m-Y',
                        'nama_ayah' => 'nullable|string|max:100',
                        'nama_ibu' => 'nullable|string|max:100',
                        'alamat_orang_tua' => 'nullable|string|max:150',
                        'desa_orang_tua' => 'nullable|string|max:100',
                        'kecamatan_orang_tua' => 'nullable|string|max:150',
                        'kode_pos_orang_tua' => 'nullable|',
                        'no_telp_orang_tua' => 'nullable',
                        'pekerjaan_ayah' => 'nullable|string|max:100',
                        'pekerjaan_ibu' => 'nullable|string|max:100',
                        'nama_wali' => 'nullable|string|max:100',
                        'alamat_wali' => 'nullable|string|max:150',
                        'desa_wali' => 'nullable|string|max:100',
                        'kecamatan_wali' => 'nullable|string|max:100',
                        'kode_pos_wali' => 'nullable',
                        'no_telp_wali' => 'nullable',
                        'pekerjaan_wali' => 'nullable|string|max:100'
                    ],
                    [],
                    [
                        'nama' => 'Nama Peserta Didik di data ke '.($key+1),
                        'email' => 'Email di data ke '.($key+1),
                        'NIPD' => 'NIPD di data ke '.($key+1),
                        'NISN' => 'NISN di data ke '.($key+1),
                        'tempat_lahir' => 'Tempat Lahir di data ke '.($key+1),
                        'tanggal_lahir' => 'Tanggal Lahir di data ke '.($key+1),
                        'jenis_kelamin' => 'Jenis Kelamin di data ke '.($key+1),
                        'agama' => 'Agama di data ke '.($key+1),
                        'status_dalam_keluarga' => 'Status Dalam Keluarga di data ke '.($key+1),
                        'anak_ke' => 'Anak Ke di data ke '.($key+1),
                        'alamat' => 'Alamat Peserta Didik di data ke '.($key+1),
                        'desa' => 'Desa Peserta Didik di data ke '.($key+1),
                        'kecamatan' => 'Kecamatan Peserta Didik di data ke '.($key+1),
                        'kode_pos' => 'Kode Pos Peserta Didik di data ke '.($key+1),
                        'no_telp' => 'No Telp di data ke '.($key+1),
                        'sekolah_asal' => 'Sekolah Asal di data ke '.($key+1),
                        'kelas_sekarang' => 'Sub Kelas Sekarang di data ke '.($key+1),
                        'sub_kelas_sekarang' => 'Kelas Sekarang di data ke '.($key+1),
                        'tahun_angkatan' => 'Tahun Angkatan di data ke '.($key+1),
                        'diterima_di_kelas' => 'Diterima Di kelas di data ke '.($key+1),
                        'diterima_di_sub_kelas' => 'Diterima Di sub kelas di data ke '.($key+1),
                        'diterima_tanggal' => 'Diterima Tanggal di data ke '.($key+1),
                        'nama_ayah' => 'Nama Ayah di data ke '.($key+1),
                        'nama_ibu' => 'Nama Ibu di data ke '.($key+1),
                        'alamat_orang_tua' => 'Alamat Orang Tua di data ke '.($key+1),
                        'desa_orang_tua' => 'Desa Orang Tua di data ke '.($key+1),
                        'kecamatan_orang_tua' => 'Kecamatan Orang Tua di data ke '.($key+1),
                        'kode_pos' => 'Kode Pos Orang Tua di data ke '.($key+1),
                        'no_telp_orang_tua' => 'No Telp Orang Tua di data ke '.($key+1),
                        'pekerjaan_ayah' => 'Pekerjaan Aya di data ke '.($key+1),
                        'pekerjaan_ibu' => ' di data ke '.($key+1),
                        'nama_wali' => 'Nama Wali di data ke '.($key+1),
                        'alamat_wali' => 'Alamat Wali di data ke '.($key+1),
                        'desa_wali' => 'Desa Wali di data ke '.($key+1),
                        'kecamatan_wali' => 'Kecamatan Wali di data ke '.($key+1),
                        'kode_pos_wali' => 'Kode Pos Wali di data ke '.($key+1),
                        'no_telp_wali' => 'No Telp Wali di data ke '.($key+1),
                        'pekerjaan_wali' => 'Pekerjaan Wali di data ke '.($key+1)
                    ]

                )->validate();
            }
        }

        $school = School::findOrFail($school_id);

        if($school->total_students < ($school->students->count() + count($datas))){
            return redirect()->back()->withErrors(['Kuota pendaftaran siswa melebihi batas. Maksimal '.$school->total_students.' akun. Akun yang terdaftar sekarang yaitu '.$school->students->count()])->withInput();
        }

        $school_year = mySchoolYear();

        foreach ($datas as $request) {
            $password = str_replace("-","",$request['tanggal_lahir']);

            $user  = \App\User::create([
                "name"      => $request["nama"],
                "username"  => $request["NISN"],
                "email"     => $request["email"],
                "password"  => bcrypt($password),
                "is_active" => 1
            ]);

            $user->assignRole('Siswa');

            $foto_siswa = null;

            $student = SchoolStudent::create([
                'school_id'             => $school_id,
                'user_id'               => $user->id,
                'name'                  => $request['nama'],
                'NIPD'                  => $request['NIPD'],
                'NISN'                  => $request['NISN'],
                'place_of_birth'        => $request['tempat_lahir'],
                'date_of_birth'         => $request['tanggal_lahir'] ? date_format(date_create_from_format('d-m-Y', $request['tanggal_lahir']), 'Y-m-d') : null,
                'gender'                => $this->getJenisKelamin($request['jenis_kelamin']),
                'religion'              => $request['agama'],
                'status_in_family'      => $request['status_dalam_keluarga'],
                'child_number'          => $request['anak_ke'],
                'address'               => $request['alamat'],
                'district_id'           => $this->getKecamatan($request['kecamatan'],  $request['kota'],  $request['provinsi']),
                'village'               => $request['desa'],
                'zip_code'              => $request['kode_pos'],
                'phone'                 => $request['no_telp'],
                'origin_school'         => $request['sekolah_asal'],
                'start_class_id'        => $this->getKelas($school_id, $request['diterima_di_kelas'], $request['diterima_di_sub_kelas']),
                'register_date'         => $request['diterima_tanggal'] ? date_format(date_create_from_format('d-m-Y', $request['diterima_tanggal']), 'Y-m-d') : null,
                'year_generation_id'    => $this->getAngkatan($school_id, $request['tahun_angkatan']),
                'photo'                 => $foto_siswa,
            ]);

            $kelas_sekarang = $this->getKelas($school_id, $request['kelas_sekarang'], $request['sub_kelas_sekarang']);

            if($kelas_sekarang){
                $class = SchoolStudentClass::create([
                    'school_student_id'     => $student->id,
                    'class_id'              => $kelas_sekarang,
                    'school_year_id'        => $school_year->id
                ]);
            }

            $parent = SchoolStudentParent::create([
                'student_id'            => $student->id,
                'father_name'           => $request['nama_ayah'],
                'mother_name'           => $request['nama_ibu'],
                'address'               => $request['alamat_orang_tua'],
                'district_id'           => $this->getKecamatan($request['kecamatan_orang_tua'],  $request['kota_orang_tua'],  $request['provinsi_orang_tua']),
                'village'               => $request['desa_orang_tua'],
                'zip_code'              => $request['kode_pos_orang_tua'],
                'phone'                 => $request['no_telp_orang_tua'],
                'father_job'            => $request['pekerjaan_ayah'],
                'mother_job'            => $request['pekerjaan_ibu']
            ]);

            $guardian = SchoolStudentGuardian::create([
                'student_id'            => $student->id,
                'name'                  => $request['nama_wali'],
                'address'               => $request['alamat_wali'],
                'district_id'           => $this->getKecamatan($request['kecamatan_wali'],  $request['kota_wali'],  $request['provinsi_wali']),
                'village'               => $request['desa_wali'],
                'zip_code'              => $request['kode_pos_wali'],
                'phone'                 => $request['no_telp_wali'],
                'job'                   => $request['pekerjaan_wali']
            ]);
        }
    }
    public function startRow(): int
    {
        return 13;
    }
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
    public function getKecamatan($district, $city, $province){
        $data = District::whereHas('city', function($q) use($city, $province){
            $q->where('name', $city)->whereHas('province', function($r)use($province){
                $r->where('name', $province);
            });
        })->where('name', $district)->first();
        return $data ? $data->id : null;
    }
    public function getKelas($school_id, $kelas, $sub_kelas){
        $data_kelas = SchoolClass::whereHas('class', function($q)use($kelas){
            $q->where('name', $kelas);
        })->whereHas('sub_class', function($q)use($sub_kelas){
            $q->where('name', $sub_kelas);
        })->where('school_id', $school_id)->first();
        if(!$data_kelas){
            $kelas = SchoolClassRef::where('school_id', $school_id)->where('name', $kelas)->first();
            $sub_kelas = SchoolSubclassRef::where('school_id', $school_id)->where('name', $sub_kelas)->first();
            if($kelas && $sub_kelas){
                $data_kelas = SchoolClass::create([
                    'school_id' => $school_id,
                    'class_id' => $kelas->id,
                    'subclass_id' => $sub_kelas->id
                ]);
            }
        }
        return $data_kelas ? $data_kelas->id : null;
    }
    public function getAngkatan($school_id, $angkatan){
        $data = SchoolGeneration::where('school_id', $school_id)->where('year', $angkatan)->first();
        return $data ? $data->id : null;
    }
    public function getJenisKelamin($jk){
        return $jk == 'L' ? 'Laki-laki' : 'Perempuan';
    }
}
