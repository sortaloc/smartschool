<?php

namespace App\Imports;

use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskQuestion;
use App\Models\SmartSchools\SchoolTaskOption;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Validator;

class QuestionsImport implements ToCollection, WithStartRow
{
    use Importable;
    protected $task_id;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function  __construct($task_id)
    {
        $this->task_id = $task_id;
    }
    public function collection(Collection $rows)
    {
        $school_id = mySchool();
        $datas = [];
        foreach($rows->toArray() as $key => $val){
            if(isset($val[0])){
                $datas[$key]["number"] = $val[0];
                $datas[$key]["question"] = $val[1];
                $datas[$key]["justification"] = $val[2];
                $datas[$key]["A"] = $val[3];
                $datas[$key]["B"] = $val[4];
                $datas[$key]["C"] = $val[5];
                $datas[$key]["D"] = $val[6];
                $datas[$key]["E"] = $val[7];
                $datas[$key]["correct_answer"] = $val[8];
                Validator::make($datas[$key], 
                    [
                        'number' => 'required|numeric',
                        'question' => 'required|string|max:50000',
                        'justification' => 'nullable|string|max:50000',
                        'A' => 'required|string|max:50000',
                        'B' => 'required|string|max:50000',
                        'C' => 'required|string|max:50000',
                        'D' => 'required|string|max:50000',
                        'E' => 'nullable|string|max:50000',
                        'correct_answer' => 'required|string|in:A,B,C,D,E'
                    ],
                    [],
                    [
                        'number' => 'Nomor soal di data ke '.($key+1), 
                        'question' => 'Pertanyaan soal di data ke '.($key+1), 
                        'justification' => 'pembahasan soal di data ke '.($key+1), 
                        'A' => 'Pilihan jawaban A di data ke '.($key+1), 
                        'B' => 'Pilihan jawaban B di data ke '.($key+1), 
                        'C' => 'Pilihan jawaban C di data ke '.($key+1), 
                        'D' => 'Pilihan jawaban D di data ke '.($key+1), 
                        'E' => 'Pilihan jawaban E di data ke '.($key+1), 
                        'correct_answer' => 'Jawaban benar di data ke '.($key+1), 
                    ]
                
                )->validate();
            }
        }
        foreach ($datas as $request) {
            $question = SchoolTaskQuestion::create([
                "school_task_id"  => $this->task_id,
                "number"  => $request["number"],
                "question"     => $request["question"],
                "correct_answer"  => $request["correct_answer"],
                "justification"     => $request["justification"]
            ]);

            $option = SchoolTaskOption::create([
                "school_task_question_id"  => $question->id,
                "A"  => $request["A"],
                "B"     => $request["B"],
                "C"  => $request["C"],
                "D"     => $request["D"],
                "E"  => $request["E"]
            ]);
        }
    }
    public function startRow(): int
    {
        return 7;
    }
}
