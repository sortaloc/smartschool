<?php

namespace App\Imports;

use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolAdmin;
use App\Models\SmartSchools\SchoolTeacher;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Validator;

class TeachersImport implements ToCollection, WithStartRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        $school_id = mySchool();
        $datas = [];
        foreach($rows->toArray() as $key => $val){
            if(isset($val[0])){
                $datas[$key]["nama"] = $val[1];
                $datas[$key]["email"] = $val[2];
                $datas[$key]["NIP"] = $val[3];
                $datas[$key]["NUPTK"] = $val[4];
                $datas[$key]["tempat_lahir"] = $val[5];
                $tanggal_lahir[$key] = $val[6] ? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($val[6]) : null;
                $datas[$key]["tanggal_lahir"] = $tanggal_lahir[$key] ? $tanggal_lahir[$key]->format('d-m-Y') : null;
                $datas[$key]["jenis_kelamin"] = $val[7];
                $datas[$key]["agama"] = $val[8];
                $datas[$key]["jabatan_struktural"] = $val[9];
                $datas[$key]["tugas_tambahan"] = $val[10];
                $datas[$key]["golongan"] = $val[11];
                $datas[$key]["no_telp"] = $val[12];
                Validator::make($datas[$key], 
                    [
                        'nama' => 'required|string|max:150',
                        'email' => 'required|email|max:100|unique:users,email',
                        'NIP' => 'nullable|numeric|digits_between:1,20',
                        'NUPTK' => 'required|numeric|digits_between:1,20|unique:users,username',
                        'tempat_lahir' => 'nullable|string|max:50',
                        'tanggal_lahir' => 'nullable|date_format:d-m-Y',
                        'jenis_kelamin' => 'nullable|in:L,P',
                        'agama' => 'nullable|in:ISLAM,KRISTEN,KATHOLIK,HINDU,BUDHA,KHONG HUCU',
                        'jabatan_struktural' => 'nullable|string|max:100',
                        'tugas_tambahan' => 'nullable|string|max:100',
                        'golongan' => 'nullable|string|max:100',
                        'no_telp' => 'nullable',
                    ],
                    [],
                    [
                        'nama' => 'Nama Guru di data ke '.($key+1), 
                        'email' => 'Email di data ke '.($key+1), 
                        'NIP' => 'NIP di data ke '.($key+1),
                        'NUPTK' => 'NUPTK di data ke '.($key+1), 
                        'tempat_lahir' => 'Tempat Lahir di data ke '.($key+1), 
                        'tanggal_lahir' => 'Tanggal Lahir di data ke '.($key+1),
                        'jenis_kelamin' => 'Jenis Kelamin di data ke '.($key+1), 
                        'agama' => 'Agama di data ke '.($key+1), 
                        'jabatan_struktural' => 'Jabatan Struktural di data ke '.($key+1),
                        'tugas_tambahan' => 'Tugas Tambahan di data ke '.($key+1),  
                        'golongan' => 'Golongan di data ke '.($key+1), 
                        'no_telp' => 'No Telp di data ke '.($key+1), 
                    ]
                
                )->validate();
            }
        }

        foreach ($datas as $request) {
            $password = str_replace("-","",$request['tanggal_lahir']);

            $user  = \App\User::create([
                "username"  => $request["NIP"] ? $request["NIP"] : $request["email"],
                "email"     => $request["email"],
                "password"  => bcrypt($password),
                "level"     => "Guru Sekolah",
                "is_active" => 1
            ]);

            $user->assignRole('Admin Sekolah');
            $user->syncPermissions(['Lihat Jadwal Kelas', 'Tambah Jadwal Kelas', 
            'Ubah Jadwal Kelas', 'Hapus Jadwal Kelas', 'Lihat Jurnal Kelas', 'Tambah Jurnal Kelas', 
            'Ubah Jurnal Kelas', 'Hapus Jurnal Kelas', 'Lihat Soal', 'Tambah Soal', 'Ubah Soal', 'Hapus Soal',
            'Lihat Rapor', 'Tambah Rapor', 'Ubah Rapor', 'Hapus Rapor',
            'Lihat Materi Guru', 'Tambah Materi Guru', 'Ubah Materi Guru', 'Hapus Materi Guru']);

            $teacher = SchoolTeacher::create([
                'school_id'             => $school_id,
                'user_id'               => $user->id,
                'name'                  => $request['nama'],
                'NIP'                   => $request['NIP'],
                'NUPTK'                 => $request['NUPTK'],
                'place_of_birth'        => $request['tempat_lahir'],
                'date_of_birth'         => $request['tanggal_lahir'] ? date_format(date_create_from_format('d-m-Y', $request['tanggal_lahir']), 'Y-m-d') : null,
                'gender'                => $this->getJenisKelamin($request['jenis_kelamin']),
                'religion'              => $request['agama'],
                'structural_position'   => $request['jabatan_struktural'],
                'additional_position'   => $request['tugas_tambahan'],
                'phone'   => $request['no_telp'],
                'PNS_rank'              => $request['golongan']
            ]);

            $admin = SchoolAdmin::create([
                'school_id'             => $school_id,
                'user_id'               => $user->id
            ]);
        }
    }
    public function startRow(): int
    {
        return 13;
    }
    public function getJenisKelamin($jk){
        return $jk == 'L' ? 'Laki-laki' : 'Perempuan';
    }
}
