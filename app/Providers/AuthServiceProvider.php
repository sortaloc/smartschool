<?php

namespace App\Providers;

use App\Models\SmartSchools\School;
use App\Policies\SmartSchoolPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        School::class => SmartSchoolPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('isMySchool', function ($user, $school_id) {
            if(auth()->user()->has('school_admin')){
                if($school_id === auth()->user()->school_admin->school_id){
                    return true;
                }
            }elseif(auth()->user()->has('school_teacher')){
                if($school_id === auth()->user()->school_teacher->school_id){
                    return true;
                }
            }
            return false;
        });
        //
    }
}
