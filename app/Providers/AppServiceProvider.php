<?php

namespace App\Providers;

use App\Helpers\AdminHelpers;
use App\Helpers\SmartSchool;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AdminHelpers::class,
            SmartSchool::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale('id');
        Schema::defaultStringLength(191);
    }
}
