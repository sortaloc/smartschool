<?php
namespace App\Http\Traits;
use App\User;
use App\Models\Marketing\Marketing;
use App\Models\Marketing\MarketingWilayah;
use App\Models\Marketing\RewardMarketing;
use App\Models\User\UserFinance;

trait rewardMarketingTrait {
    public function rewardMarketing($referal, $user_id){
        $marketing_type = null;
        $user = User::find($user_id);
        $wilayah_id = $user->profile->district_id;
        $area_id = $user->profile->district ? $user->profile->district->city_id : $user->profile->city_id;
        $marketing = Marketing::whereHas('user', function($q) use($referal){
            $q->where('username', $referal);
        })->first();
        if($marketing){
            $marketing_area = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) use($area_id) {
                $q->where('jabatan', 'area')->where('wilayah_id', $area_id);
            })->first();
            if($marketing_area){
                $marketing_type = 'area';
            }else{
                $marketing_wilayah = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) use($wilayah_id) {
                    $q->where('jabatan', 'wilayah')->where('wilayah_id', $wilayah_id);
                })->first();
                if($marketing_wilayah){
                    $marketing_type = 'wilayah';
                }else{
                    $marketing_user = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) {
                        $q->where('jabatan', 'user')->where('wilayah_id', null);
                    })->first();
                    if($marketing_user){
                        $marketing_type = 'user';
                    }else{
                        $new = MarketingWilayah::create([
                            'marketing_id'  => $marketing->id,
                            'jabatan'       => 'user'
                        ]);
                        $marketing_type = 'user';
                    }
                }
            }
        }
        if($marketing_type){
            $marketing_reward = RewardMarketing::where('jabatan', $marketing_type)->first();
            if($marketing_reward){
                $marketing_finance = UserFinance::whereHas('user', function($q) use($referal){
                    $q->where('username', $referal);
                })->first();
                if($marketing_finance){
                    $marketing_finance->balance = $marketing_finance->balance + $marketing_reward->reward;
                    $marketing_finance->save();
                }else{
                    UserFinance::create([
                        'user_id' => $marketing->user_id,
                        'balance' => $marketing_reward->reward
                    ]);
                }
            }
        }
    }
}