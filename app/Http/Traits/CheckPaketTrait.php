<?php
namespace App\Http\Traits;
use App\Member;
use App\Models\Konfigurasi\Kelas;
trait CheckPaketTrait {
    public function checkPaket($kelas_id) {

        $authorize = false;
        if(auth()->user()->level == 'Siswa'){
            $paket1 = auth()->user()->school_student->school->pakets()->whereHas('paket', function ($query) use ($kelas_id) {
                $query->where('reference_name', 'kelas')->where('reference_id', $kelas_id);
            })->first();

            if(!$paket1){
                $kelas = Kelas::find($kelas_id);
                $paket2 = auth()->user()->school_student->school->pakets()->whereHas('paket', function ($query) use ($kelas) {
                    $query->where('reference_name', 'jenjang')->where('reference_id', $kelas->jenjang_id);
                })->first();
    
                if($paket2){
                    $authorize = true;
                }
            }else{
                $authorize = true;
            }
        }
        if($authorize == false){
            $paket1 = auth()->user()->active_paket()->whereHas('paket', function ($query) use ($kelas_id) {
                $query->where('reference_name', 'kelas')->where('reference_id', $kelas_id);
            })->first();
    
            if(!$paket1){
                $kelas = Kelas::find($kelas_id);
                $paket2 = auth()->user()->active_paket()->whereHas('paket', function ($query) use ($kelas) {
                    $query->where('reference_name', 'jenjang')->where('reference_id', $kelas->jenjang_id);
                })->first();
    
                if($paket2){
                    $authorize = true;
                }
            }else{
                $authorize = true;
            }
        }
        if($authorize == false){
            return ['code' => 401,'authorized' => false, 'message' => 'Silahkan Berlangganan Untuk Membuka Materi Ini !'];
        }
        return ['authorized' => true];
    }
}