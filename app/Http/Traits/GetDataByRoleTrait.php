<?php
namespace App\Http\Traits;
use App\Models\Konfigurasi\Kelas;
use App\Models\Konfigurasi\Mapel;
use App\Models\Konfigurasi\Bab;
use App\Models\Konfigurasi\Materi;
use App\Models\Konfigurasi\Rangkuman;
trait GetDataByRoleTrait {
    public function getKelas() {
        $data = null;
        if(auth()->user()->level == 'Super Admin'){
            $data = Kelas::all();
        }elseif(auth()->user()->level == 'Admin SD'){
            $data = Kelas::where('admin', 'like', '%' . 'Admin SD' . '%')->get();
        }elseif(auth()->user()->level == 'Admin SMP'){
            $data = Kelas::where('admin', 'like', '%' . 'Admin SMP' . '%')->get();
        }elseif(auth()->user()->level == 'Admin SMA'){
            $data = Kelas::where('admin', 'like', '%' . 'Admin SMA' . '%')->get();
        }elseif(auth()->user()->level == 'Admin Olimpiade'){
            $data = Kelas::where('name', 'like', '%' . 'Admin Olimpiade' . '%')->get();
        }
        return $data;
    }
    public function getMapel() {
        $data = null;
        if(auth()->user()->level == 'Super Admin'){
            $data = Mapel::all();
        }elseif(auth()->user()->level == 'Admin SD'){
            $data = Mapel::whereHas('kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SD' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMP'){
            $data = Mapel::whereHas('kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMP' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMA'){
            $data = Mapel::whereHas('kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMA' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin Olimpiade'){
            $data = Mapel::whereHas('kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin Olimpiade' . '%');
            })->get();
        }
        return $data;
    }
    public function getBab() {
        $data = null;
        if(auth()->user()->level == 'Super Admin'){
            $data = Bab::all();
        }elseif(auth()->user()->level == 'Admin SD'){
            $data = Bab::whereHas('mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SD' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMP'){
            $data = Bab::whereHas('mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMP' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMA'){
            $data = Bab::whereHas('mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMA' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin Olimpiade'){
            $data = Bab::whereHas('mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin Olimpiade' . '%');
            })->get();
        }
        return $data;
    }
    public function getMateri() {
        $data = null;
        if(auth()->user()->level == 'Super Admin'){
            $data = Materi::all();
        }elseif(auth()->user()->level == 'Admin SD'){
            $data = Materi::whereHas('bab.mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SD' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMP'){
            $data = Materi::whereHas('bab.mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMP' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin SMA'){
            $data = Materi::whereHas('bab.mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin SMA' . '%');
            })->get();
        }elseif(auth()->user()->level == 'Admin Olimpiade'){
            $data = Materi::whereHas('bab.mapel.kurikulum.kelas', function ($query) {
                $query->where('admin', 'like', '%' . 'Admin Olimpiade' . '%');
            })->get();
        }
        return $data;
    }
}