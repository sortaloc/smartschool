<?php
namespace App\Http\Traits;
use App\Models\Konfigurasi\Kelas;
use App\Models\Konfigurasi\MataPelajaran;
use App\Models\Konfigurasi\Bab;
use App\Models\Konfigurasi\Materi;
use App\Models\LatihanSoal\LatihanSoal;
use App\Models\LatihanSoal\LatihanOption;
use App\Models\Rangkuman\Rangkuman;
use App\Models\Quiz\Quiz;
use App\Models\Quiz\QuizOption;
use Illuminate\Support\Facades\Storage;
trait CopyTrait 
{
    public function copyMapel($mapel_id, $kurikulum_id) {
        $mapel = MataPelajaran::findOrFail($mapel_id);
        $logo = null;
        $or_logo = $mapel->getOriginal('logo');
        $cp_logo = null;
        if($or_logo){
            $cp_logo = 'konfigurasi/mapel/'.time().'_'.substr($or_logo, strrpos($or_logo, '/') + 1);
            if (Storage::exists($or_logo)) {
                $logo = Storage::copy($or_logo, $cp_logo);
            }
        }
        $data = MataPelajaran::create([
            'kurikulum_id' => $kurikulum_id,
            'name' => $mapel->name,
            'logo' => $cp_logo
        ]);
        $this->copyBab(null, $mapel_id, $data->id);
    }

    public function copyBab($bab_id, $old_mapel_id, $new_mapel_id) {
        $new_bab_id = null;
        if($bab_id){
            $bab = Bab::findOrFail($bab_id);
            $logo = null;
            $or_logo = $bab->getOriginal('logo');
            $cp_logo = null;
            if($or_logo){
                $cp_logo = 'konfigurasi/bab/'.time().'_'.substr($or_logo, strrpos($or_logo, '/') + 1);
                if (Storage::exists($or_logo)) {
                    $logo = Storage::copy($or_logo, $cp_logo);
                }
            }
            $data = Bab::create([
                'mapel_id' => $new_mapel_id,
                'name' => $bab->name,
                'logo' => $cp_logo,
                'number' => $bab->number,
                'is_free' => $bab->getOriginal('is_free')
            ]);
            $new_bab_id = $data->id;
            $this->copyMateri(null, $bab_id, $data->id);
            $this->copyLatihanSoal($bab_id, $data->id);
        }elseif(!$bab_id && $old_mapel_id && $new_mapel_id){
            $babs = Bab::where('mapel_id', $old_mapel_id)->get();
            foreach ($babs as $key => $bab) {
                $logo = null;
                $or_logo = $bab->getOriginal('logo');
                $cp_logo = null;
                if($or_logo){
                    $cp_logo = 'konfigurasi/bab/'.time().'_'.substr($or_logo, strrpos($or_logo, '/') + 1);
                    if (Storage::exists($or_logo)) {
                        $logo = Storage::copy($or_logo, $cp_logo);
                    }
                }
                $data = Bab::create([
                    'mapel_id' => $new_mapel_id,
                    'name' => $bab->name,
                    'logo' => $cp_logo,
                    'number' => $bab->number,
                    'is_free' => $bab->getOriginal('is_free')
                ]);
                $this->copyMateri(null, $bab->id, $data->id);
                $this->copyLatihanSoal($bab->id, $data->id);
            }
        }
    }
    public function copyMateri($materi_id, $old_bab_id, $new_bab_id) {
        if($materi_id){
            $materi = Materi::findOrFail($materi_id);
            $logo = null;
            $video = null;
            $or_logo = $materi->getOriginal('logo');
            $cp_logo = null;
            $or_video = $materi->getOriginal('video');
            $cp_video = null;
            if($or_logo){
                $cp_logo = 'konfigurasi/materi/'.time().'_'.substr($or_logo, strrpos($or_logo, '/') + 1);
                if (Storage::exists($or_logo)) {
                    $logo = Storage::copy($or_logo, $cp_logo);
                }
            }
            if($or_video){
                $cp_video = 'konfigurasi/materi/'.time().'_'.substr($or_video, strrpos($or_video, '/') + 1);
                if (Storage::exists($or_video)) {
                    $video = Storage::copy($or_video, $cp_video);
                }
            }
            $data = Materi::create([
                'bab_id' => $new_bab_id,
                'name' => $materi->name,
                'logo' => $cp_logo,
                'video' => $cp_video,
                'number' => $materi->number,
                'is_free' => $materi->getOriginal('is_free')
            ]);
            $this->copyRangkuman($materi_id, $data->id);
            $this->copyQuiz($materi_id, $data->id);
        }elseif(!$materi_id && $old_bab_id && $new_bab_id){
            $materis = Materi::where('bab_id', $old_bab_id)->get();
            foreach ($materis as $key => $materi) {
                $logo = null;
                $video = null;
                $or_logo = $materi->getOriginal('logo');
                $cp_logo = null;
                $or_video = $materi->getOriginal('video');
                $cp_video = null;
                if($or_logo){
                    $cp_logo = 'konfigurasi/materi/'.time().'_'.substr($or_logo, strrpos($or_logo, '/') + 1);
                    if (Storage::exists($or_logo)) {
                        $logo = Storage::copy($or_logo, $cp_logo);
                    }
                }
                if($or_video){
                    $cp_video = 'konfigurasi/materi/'.time().'_'.substr($or_video, strrpos($or_video, '/') + 1);
                    if (Storage::exists($or_video)) {
                        $video = Storage::copy($or_video, $cp_video);
                    }
                }
                $data = Materi::create([
                    'bab_id' => $new_bab_id,
                    'name' => $materi->name,
                    'logo' => $cp_logo,
                    'video' => $cp_video,
                    'number' => $materi->number,
                    'is_free' => $materi->getOriginal('is_free')
                ]);
                $this->copyRangkuman($materi->id, $data->id);
                $this->copyQuiz($materi->id, $data->id);
            }
        }
    }
    public function copyLatihanSoal($old_bab_id, $new_bab_id) {
        $soals = LatihanSoal::where('bab_id', $old_bab_id)->get();
        foreach ($soals as $key => $soal) {
            $soal_g = null;
            $pembahasan_g = null;
            $pembahasan_pdf = null;
            $or_soal_g = $soal->getOriginal('soal_gambar');
            $or_pembahasan_g = $soal->getOriginal('pembahasan_gambar');
            $or_pembahasan_pdf = $soal->getOriginal('pembahasan_pdf');
            $cp_soal_g = null;
            $cp_pembahasan_g = null;
            $cp_pembahasan_pdf = null;

            if($or_soal_g){
                $cp_soal_g = 'LatihanSoal/SoalGambar/'.time().'_'.substr($or_soal_g, strrpos($or_soal_g, '/') + 1);
                if (Storage::exists($or_soal_g)) {
                    $soal_g = Storage::copy($or_soal_g, $cp_soal_g);
                }
            }
            if($or_pembahasan_g){
                $cp_pembahasan_g = 'LatihanSoal/PembahasanGambar/'.time().'_'.substr($or_pembahasan_g, strrpos($or_pembahasan_g, '/') + 1);
                if (Storage::exists($or_pembahasan_g)) {
                    $pembahasan_g = Storage::copy($or_pembahasan_g, $cp_pembahasan_g);
                }
            }
            if($or_pembahasan_pdf){
                $cp_pembahasan_pdf = 'LatihanSoal/PembahasanPdf/'.time().'_'.substr($or_pembahasan_pdf, strrpos($or_pembahasan_pdf, '/') + 1);
                if (Storage::exists($or_pembahasan_pdf)) {
                    $pembahasan_pdf = Storage::copy($or_pembahasan_pdf, $cp_pembahasan_pdf);
                }
            }
            $new_soal = LatihanSoal::create([
                "soal"              => $soal->soal,
                "soal_gambar"       => $cp_soal_g,
                "bab_id"            => $new_bab_id,
                "level"             => $soal->level,
                "jawaban"           => $soal->jawaban,
                "pembahasan"        => $soal->pembahasan,
                "pembahasan_gambar" => $cp_pembahasan_g,
                "pembahasan_pdf"    => $cp_pembahasan_pdf
            ]);
            foreach($soal->option as $key => $value){
                $pilihan_g = null;
                $or_pilihan_g = $value->getOriginal('pilihan_gambar');
                $cp_pilihan_g = null;
                if($or_pilihan_g){
                    $cp_pilihan_g = 'LatihanSoal/Option/'.time().'_'.substr($or_pilihan_g, strrpos($or_pilihan_g, '/') + 1);
                    if (Storage::exists($or_pilihan_g)) {
                        $pilihan_g = Storage::copy($or_pilihan_g, $cp_pilihan_g);
                    }
                }
                LatihanOption::create([
                    "soal_id"           => $new_soal->id,
                    "pilihan"           => $value->pilihan,
                    "pilihan_text"      => $value->pilihan_text,
                    "pilihan_gambar"    => $cp_pilihan_g
                ]);
            }
        }
    }
    public function copyRangkuman($old_materi_id, $new_materi_id) {
        $rangkumans = Rangkuman::where('materi_id', $old_materi_id)->get();
        foreach ($rangkumans as $key => $rangkuman) {
            $file = null;
            $or_file = $rangkuman->getOriginal('file');
            $cp_file = null;
            if($or_file){
                $cp_file = 'rangkuman/file/'.time().'_'.substr($or_file, strrpos($or_file, '/') + 1);
                if (Storage::exists($or_file)) {
                    $file = Storage::copy($or_file, $cp_file);
                }
            }
            Rangkuman::create([
                "materi_id"     => $new_materi_id,
                "name"          => $rangkuman->name,
                "file"          => $cp_file
            ]);
        }
    }
    public function copyQuiz($old_materi_id, $new_materi_id) {
        $soals = Quiz::where('materi_id', $old_materi_id)->get();
        foreach ($soals as $key => $soal) {
            $soal_g = null;
            $pembahasan_g = null;
            $pembahasan_pdf = null;
            $or_soal_g = $soal->getOriginal('soal_gambar');
            $or_pembahasan_g = $soal->getOriginal('pembahasan_gambar');
            $or_pembahasan_pdf = $soal->getOriginal('pembahasan_pdf');
            $cp_soal_g = null;
            $cp_pembahasan_g = null;
            $cp_pembahasan_pdf = null;
            if($or_soal_g){
                $cp_soal_g = 'Quiz/SoalGambar/'.time().'_'.substr($or_soal_g, strrpos($or_soal_g, '/') + 1);
                if (Storage::exists($or_soal_g)) {
                    $soal_g = Storage::copy($or_soal_g, $cp_soal_g);
                }
            }
            if($or_pembahasan_g){
                $cp_pembahasan_g = 'Quiz/PembahasanGambar/'.time().'_'.substr($or_pembahasan_g, strrpos($or_pembahasan_g, '/') + 1);
                if (Storage::exists($or_pembahasan_g)) {
                    $pembahasan_g = Storage::copy($or_pembahasan_g, $cp_pembahasan_g);
                }
            }
            if($or_pembahasan_pdf){
                $cp_pembahasan_pdf = 'Quiz/PembahasanPdf/'.time().'_'.substr($or_pembahasan_pdf, strrpos($or_pembahasan_pdf, '/') + 1);
                if (Storage::exists($or_pembahasan_pdf)) {
                    $pembahasan_pdf = Storage::copy($or_pembahasan_pdf, $cp_pembahasan_pdf);
                }
            }
            $new_soal = Quiz::create([
                "soal"              => $soal->soal,
                "soal_gambar"       => $cp_soal_g,
                "materi_id"         => $new_materi_id,
                "level"             => $soal->level,
                "jawaban"           => $soal->jawaban,
                "pembahasan"        => $soal->pembahasan,
                "pembahasan_gambar" => $cp_pembahasan_g,
                "pembahasan_pdf"    => $cp_pembahasan_pdf
            ]);
            foreach($soal->option as $key => $value){
                $pilihan_g = null;
                $or_pilihan_g = $value->getOriginal('pilihan_gambar');
                $cp_pilihan_g = null;
                if($or_pilihan_g){
                    $cp_pilihan_g = 'LatihanSoal/Option/'.time().'_'.substr($or_pilihan_g, strrpos($or_pilihan_g, '/') + 1);
                    if (Storage::exists($or_pilihan_g)) {
                        $pilihan_g = Storage::copy($or_pilihan_g, $cp_pilihan_g);
                    }
                }
                QuizOption::create([
                    "quiz_id"           => $new_soal->id,
                    "pilihan"           => $value->pilihan,
                    "pilihan_text"      => $value->pilihan_text,
                    "pilihan_gambar"    => $cp_pilihan_g
                ]);
            }
        }
    }
}