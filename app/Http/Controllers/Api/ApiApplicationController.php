<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function serverTime()
    {
        return response()->json(['time' => time(), 'default_datetime' => date('Y-m-d H:i:s', time()), 'custom_datetime' => \Carbon\Carbon::now()->isoFormat('DD MMMM Y, HH:mm')], 200);
    }
}
