<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiFCMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = auth()->user();
        if(!$fcm_token = $request->fcm_token){
            return response()->json(['code' => 422, 'message' => 'FCM token tidak boleh kosong'], 422);
        }
        $data->fcm_token = $fcm_token;
        $data->save();

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengupdate FCM token'], 200);
    }
}
