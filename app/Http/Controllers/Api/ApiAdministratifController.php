<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Konfigurasi\Province;
use App\Models\Konfigurasi\City;
use App\Models\Konfigurasi\District;
use Illuminate\Http\Request;

class ApiAdministratifController extends Controller
{
    public function province(Request $request)
    {
        $data = Province::whereNotNull('id');

        if(request()->has('name')){
            $data->where('name', 'like', '%' . request('name') .'%');
        }
        $response = $data->get();
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Provinsi', 'data' => $response], 200);
    }

    public function city(Request $request)
    {
        $data = City::whereNotNull('id');

        if(request()->has('name')){
            $data->where('name', 'like', '%' . request('name') .'%');
        }

        if(request()->has('province_id')){
            $data->where('province_id', request('province_id'));
        }
        $response = $data->get();
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Kota / Kabupaten', 'data' => $response], 200);
    }

    public function district(Request $request)
    {
        $request->pageLength ? $perPage = $request->pageLength : $perPage = 50;
        $data = District::whereNotNull('id');

        if(request()->has('name')){
            $data->where('name', 'like', '%' . request('name') .'%');
        }

        if(request()->has('city_id')){
            $data->where('city_id', request('city_id'));
        }
        $response = $data->paginate($perPage);
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Kecamatan', 'data' => $response], 200);
    }
}
