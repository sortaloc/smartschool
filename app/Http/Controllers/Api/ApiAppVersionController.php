<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Konfigurasi\AppVersion;
use Illuminate\Http\Request;

class ApiAppVersionController extends Controller
{
    public function index(Request $request)
    {
        $data = AppVersion::get()->last();
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Versi Aplikasi', 'data' => $data], 200);
    }
}
