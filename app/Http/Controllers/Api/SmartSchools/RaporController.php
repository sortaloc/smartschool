<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\School;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolStudent;
use PDF;
use Storage;

class RaporController extends Controller
{
    public function rapor()
    {
        $data = SchoolStudent::findOrFail(auth()->user()->school_student->id);
        if(auth()->user()->school_student->school->rapor_distribution == 1){
            if($data->rapor){
                $rapor = $data->rapor;
                return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Rapor', 'data' => $rapor], 200);
            }else{
                return response()->json(['code' => 404, 'message' => 'Rapor Tidak Ditemukan'], 404);
            }
        }
        return response()->json(['code' => 403, 'message' => 'Belum Waktunya Mengambil Rapor'], 403);
    }
}
