<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskClass;
use App\Models\SmartSchools\SchoolTaskAnswer;
use App\Models\SmartSchools\SchoolTaskQuestion;
use App\Models\SmartSchools\SchoolTaskResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use StdClass;

class TaskController extends Controller
{
    public function take(Request $request)
    {
        $task = SchoolTask::find($request->id_tugas);
        $student_id = auth()->user()->profile->id;

        if(!$task){
            return response()->json(['code' => 404, 'message' => 'Tugas Tidak Ditemukan'], 404);
        }

        if(!$task->my_result->first()){
            if($task->my_class->first()->start_time > time()){
                return response()->json(['code' => 422, 'message' => 'Belum waktunya mengerjakan soal ini'], 422);
            }elseif($task->my_class->first()->end_time < time()){
                return response()->json(['code' => 422, 'message' => 'Waktu mengerjakan soal sudah selesai'], 422);
            }
        }

        $student_class_id = auth()->user()->profile->class->first()->class->id;

        $task_class = $task->task_classes()->whereHas('teacher_subjects', function($q) use($student_class_id){
            $q->where('school_class_id', $student_class_id);
        })->first();

        if(!$task_class){
            return response()->json(['code' => 403, 'message' => 'Anda tidak diizinkan mengerjakan tugas ini'], 403);
        }

        $result = SchoolTaskResult::firstOrCreate(
            ['school_task_id' => $task->id, 'task_class_id' => $task_class->id, 'school_student_id' => $student_id],[
                'status' => 'Proses'
            ]
        );

        $taskLast = $task->taskLast->first();
        $taskLastEnd = $taskLast->end_time;

        $data = $task->questions;

        if($taskLastEnd < time()){
            $data->each(function($q){
                $q->setHidden(['created_at', 'updated_at', 'school_task_id']);
                $answer = $q->your_answer()->first() ? $q->your_answer()->first()->setHidden(['created_at', 'updated_at', 'student_id', 'school_task_question_id', 'id']) : null;
                $q->your_answer = $answer;
            });
        }else{
            $data->each(function($q){
                $q->setHidden(['created_at', 'updated_at', 'school_task_id', 'correct_answer', 'justification']);
                $q->your_answer = null;
                $answer = $q->your_answer()->first();
                $q->your_answer = $answer;
            });
        }

        if($task->question_type == 'Pilihan Ganda'){
            $task->questions->load('option');
        }

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Tugas', 'data' => $data], 200);
    }
    public function saveAnswer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_soal' => 'required|exists:school_task_questions,id'
        ]);

        $soal = SchoolTaskQuestion::find($request->id_soal);

        if($soal){
            if($soal->task->question_type == 'Pilihan Ganda'){
                $validator = Validator::make($request->all(), [
                    'jawaban_pilihan' => 'required|in:A,B,C,D,E'
                ]);
            }elseif($soal->task->question_type == 'Essay'){
                $validator = Validator::make($request->all(), [
                    'jawaban_text' => 'required_without:jawaban_file|string|max:50000',
                    'jawaban_file' => 'required_without:jawaban_text|nullable|mimes:doc,docx,ppt,pptx,xls,xlsx,pdf,csv,txt,mp4,mkv,avi,mpeg,wmp,3gp,flv,swf,png,jpg,jpeg|max:50000'
                ]);
            }elseif($soal->task->question_type == 'File'){
                $validator = Validator::make($request->all(), [
                    'jawaban_file' => 'required|mimes:doc,docx,ppt,pptx,xls,xlsx,pdf,csv,txt,mp4,mkv,avi,mpeg,wmp,3gp,flv,swf,png,jpg,jpeg|max:50000'
                ]);
            }
        }

        if ($validator->fails()) {
            return response()->json(['code' => 422, 'messages' => $validator->errors()], 422);
        }

        if(!$soal->task->my_result->first()){
            return response()->json(['code' => 422, 'message' => 'Anda tidak diizinkan mengerjakan soal ini'], 422);
        }elseif($soal->task->my_result->first()->status != 'Proses'){
            return response()->json(['code' => 422, 'message' => 'Jawaban Sudah Tidak Dapat Diubah Kembali'], 422);
        }

        if(!$soal->task->my_class->first()) {
            return response()->json(['code' => 422, 'message' => 'Anda tidak diizinkan mengerjakan soal ini'], 422);
        }else{
            if($soal->task->my_result->first()->task_class->start_time > time()){
                return response()->json(['code' => 422, 'message' => 'Belum waktunya mengerjakan soal ini'], 422);
            }elseif($soal->task->my_result->first()->task_class->end_time < time()){
                return response()->json(['code' => 422, 'message' => 'Waktu mengerjakan soal sudah selesai'], 422);
            }
        }

        $student_id = auth()->user()->profile->id;

        $jawaban_file = null;
        if($jawaban_file = $request->file("jawaban_file")){
            $jawaban_file = $request->file("jawaban_file")->store("SmartSchool/".$soal->task->school_id."/task");
        }

        $point = 0;

        if($soal->task->question_type == 'Pilihan Ganda'){
            $point = 0;
            if($request->jawaban_pilihan == $soal->correct_answer){
                $point = 1;
            }
        }

        $answer = SchoolTaskAnswer::updateOrCreate(
            ['school_task_question_id' => $request->id_soal, 'student_id' => $student_id],
            [
                'option_answer' => $request->jawaban_pilihan,
                'text_answer' => $request->jawaban_text,
                'file_answer' => $jawaban_file,
                'point' => $point
            ]
        );

        return response()->json(['code' => 200, 'message' => 'Berhasil Menyimpan jawaban'], 200);
    }
    public function done(Request $request)
    {
        $student_id = auth()->user()->profile->id;
        $task = SchoolTaskResult::where('school_task_id', $request->id_tugas)->where('school_student_id', $student_id)->first();
        if(!$task){
            return response()->json(['code' => 404, 'message' => 'Tugas Tidak Ditemukan'], 404);
        }
        if($task->status == 'Selesai'){
            return response()->json(['code' => 422, 'message' => 'Tugas sudah diselesaikan'], 422);
        }elseif($task->status == 'Proses'){
            if($task->task->question_type == 'Pilihan Ganda'){
                $total_point = SchoolTaskAnswer::where('student_id', $student_id)->whereHas('question',function($q)use($task){
                    $q->where('school_task_id', $task->school_task_id);
                })->sum('point');
                $total_question = $task->task->questions->count();
                $final_point = $total_point / $total_question * 100;
                $task->point = round($final_point);
                $task->status = 'Selesai';
                $task->finish = now();
                $task->save();
            }else{
                $task->status = 'Sedang Dikoreksi';
                $task->finish = now();
                $task->save();
            }
        }else{
            return response()->json(['code' => 422, 'message' => 'Tugas sedang dikoreksi'], 422);
        }

        return response()->json(['code' => 200, 'message' => 'Berhasil Menyelesaikan Tugas'], 200);
    }
    public function myTaskStatistik(Request $request)
    {
        $student_id = auth()->user()->profile->id;
        $class_id = auth()->user()->profile->class->first()->class->id;
        $selesai = SchoolTaskResult::where('school_student_id', $student_id)->where('status', 'Selesai')->count();
        $proses = SchoolTaskResult::where('school_student_id', $student_id)->where('status', 'Proses')->count();
        $belum = SchoolTaskClass::whereHas('teacher_subjects', function($q) use($class_id){
            $q->where('school_class_id', $class_id);
        })->whereDoesntHave('task.results', function($r) use($student_id){
            $r->where('school_student_id', $student_id);
        })->count();

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Progres Tugas Kamu', 'data' =>
                ['selesai' => $selesai, 'proses' => $proses, 'belum_dikerjakan' => $belum]
                ], 200);
    }
    public function myTask(Request $request)
    {
        $student_id = auth()->user()->profile->id;
        $task_temp = SchoolTaskResult::where('school_student_id', $student_id)->select('id', 'school_task_id', 'school_student_id', 'point as nilai', 'status')->latest();

        $perPage = $request->perPage ? $request->perPage : 100;
        $task = $task_temp->paginate($perPage);

        $task->each(function($q){
            $q->id = $q->task->id;
            $q->nama_tugas = $q->task->name;
            $q->tipe_tugas = $q->task->question_type;
            $q->nama_guru = $q->task->my_class->first() ? $q->task->my_class->first()->teacher_subjects->teacher->name : null;
            $q->mata_pelajaran = $q->task->my_class->first() ? $q->task->my_class->first()->teacher_subjects->subjects->name : null;
            $q->status = $q->status;
            $q->kunci_jawaban = $q->task->kunci_jawaban;
            $q->setHidden(['school_task_id', 'school_student_id', 'task']);
        });

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Tugas Kamu', 'data' => $task], 200);
    }
}
