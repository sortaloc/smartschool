<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\SchoolBookCategory;
use App\Models\SmartSchools\SchoolLibrary;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function category(Request $request)
    {
        $school_id = auth()->user()->profile->school_id;
        $data = SchoolBookCategory::schoolId($school_id)->orWhere('school_id', null)->get();
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Kategori Buku', 'data' => $data], 200);
    }
    public function book(Request $request)
    {
        $school_id = auth()->user()->profile->school_id;
        $data_temp = SchoolLibrary::schoolId($school_id)->orWhere('school_id', null);

        $perPage = 20;
        if($request->perPage){
            $perPage = $request->perPage;
        }
        if($request->id_kategori){
            $data_temp->where('category_id', $request->id_kategori);
        }
        if($request->kata_kunci){
            $data_temp->where(function($q) use($request){
                $q->where('title', 'like', '%'.$request->kata_kunci.'%')
                ->orWhere('author', 'like', '%'.$request->kata_kunci.'%');
            });
        }

        $data = $data_temp->paginate($perPage);
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Buku', 'data' => $data], 200);
    }
}
