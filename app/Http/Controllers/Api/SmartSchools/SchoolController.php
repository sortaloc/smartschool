<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolAnnouncement;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function index(Request $request)
    {
        $school_id = auth()->user()->profile->school_id;
        $data = School::with('district.city.province', 'wall_magazines:id,school_id,title,logo,file', 'teachers:id,school_id,name,photo')->where('id', $school_id)
        ->select('id', 'name as nama', 'logo', 'address as alamat', 'village as desa', 'zip_code as kode_pos', 'district_id')
        ->withCount('teachers as total_guru')->withCount('students as total_siswa')->withCount('books as total_buku')->first();
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Sekolah', 'data' => $data], 200);
    }
    public function announcement(Request $request)
    {
        $school_id = auth()->user()->profile->school_id;
        $perPage = $request->perPage ? $request->perPage : 20;
        $data = SchoolAnnouncement::schoolId($school_id)->where('status', 1)->select('id', 'title as judul', 'message as pesan')
        ->orderBy('updated_at', 'desc')->paginate($perPage);
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Pengumuman Sekolah', 'data' => $data], 200);
    }
}
