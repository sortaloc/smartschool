<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\SchoolClassSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use StdClass;

class ScheduleController extends Controller
{
    public function index(Request $request)
    {
        $class_id = auth()->user()->profile->class->first()->class->id;
        $data = SchoolClassSchedule::whereHas('teacher_subjects', function($q)use($class_id){
            $q->where('school_class_id', $class_id);
        })->orderBy('day', 'asc')->orderBy('start_at', 'asc')->get()->groupBy('day');
        $data->each(function($s){
            $s->each(function($q){
                $q->id_guru = $q->teacher_subjects->teacher->id;
                $q->nama_guru = $q->teacher_subjects->teacher->name;
                $q->foto_guru = $q->teacher_subjects->teacher->photo;
                $q->mata_pelajaran = $q->teacher_subjects->subjects->name;
                $q->setHidden(['created_at', 'updated_at', 'deleted_at', 'school_year_id', 'school_teacher_subjects_id', 'id', 'teacher_subjects', 'day']);
            });
        });

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Jadwal', 'data' => $data], 200);
    }
}
