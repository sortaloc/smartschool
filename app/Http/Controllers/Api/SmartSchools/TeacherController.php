<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolTeacherFile;
use App\Models\SmartSchools\SchoolClassJournal;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskClass;
use Illuminate\Http\Request;
use StdClass;

class TeacherController extends Controller
{
    public function show($id)
    {
        $class = auth()->user()->profile->class->first()->class->class_name;
        $teacher = SchoolTeacher::where('id', $id)->select('id', 'user_id', 'name as nama', 'photo')->first();
        if(!$teacher){
            return response()->json(['code' => 404, 'message' => 'Guru tidak ditemukan'], 404);
        }
        $teacher->email = $teacher->user->email;
        $teacher->mata_pelajaran = $teacher->subjects->unique('school_subjects_id')->load('subjects');
        $teacher->daftar_mata_pelajaran = $teacher->mata_pelajaran->pluck('subjects.name');
        $teacher->setHidden(['user', 'user_id', 'subjects', 'mata_pelajaran']);

        $end_at = date('H:i:s', time());

        $journal = SchoolClassJournal::orderBy('start_at', 'asc')->whereDate('date', now())
                    ->whereTime('end_at', '>', $end_at)
                    ->whereHas('class_schedule.teacher_subjects', function($q) use($teacher){
                        $q->where('school_teacher_id', $teacher->id);
                    })->first();

        if($journal){
            $teacher->jadwal = new StdClass();
            $teacher->jadwal->id = $journal->id;
            $teacher->jadwal->kelas = $journal->class_schedule->teacher_subjects->class->class_name;
            $teacher->jadwal->mata_pelajaran = $journal->class_schedule->teacher_subjects->subjects->name;
            $teacher->jadwal->waktu_mulai = $journal->start_at;
            $teacher->jadwal->waktu_selesai = $journal->end_at;

            $now = time();
            $start = strtotime(date('Y-m-d H:i:s', strtotime($journal->start_at)));
            $end = strtotime(date('Y-m-d H:i:s', strtotime($journal->end_at)));

            if($start < $now && $end > $now){
                $teacher->jadwal->sedang_mengajar = true;
            }else{

                $teacher->jadwal->sedang_mengajar = false;
            }

        }else{
            $teacher->jadwal = null;
        }

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Guru', 'data' => $teacher], 200);
    }
    public function file(Request $request)
    {
        $data_temp = SchoolTeacherFile::where('teacher_id', $request->id_guru)->select('name as nama', 'type as tipe', 'file')->latest();

        $perPage = 20;
        if($request->perPage){
            $perPage = $request->perPage;
        }
        if($request->nama_materi){
            $data_temp->where('name', $request->nama_materi);
        }

        $data = $data_temp->paginate($perPage);

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Materi Guru', 'data' => $data], 200);
    }
    public function task(Request $request)
    {
        $class_id = auth()->user()->profile->class->first()->class->id;
        $data_temp = SchoolTaskClass::whereHas('teacher_subjects', function($q) use($request, $class_id){
            $q->where('school_teacher_id', $request->id_guru)
            ->where('school_class_id', $class_id);
        })->whereHas('task', function($q){
            $q->visible();
        });

        $perPage = 20;
        if($request->perPage){
            $perPage = $request->perPage;
        }
        if($request->nama_tugas){
            $data_temp->whereHas('task', function($q) use($request){
                $q->where('name', $request->nama_tugas);
            });
        }

        $data_temp->select('id', 'school_task_id', 'teacher_subjects_id', 'start_at', 'end_at')->latest();

        $data = $data_temp->paginate($perPage);

        foreach ($data as $item)
        {
        $item->id = $item->task->id;
        $item->nama_tugas = $item->task->name;
        $item->tipe_tugas = $item->task->task_type;
        $item->tipe_soal = $item->task->question_type;
        $item->jenis_penilaian = $item->task->assessment_type->name;
        $item->deskripsi_tugas = $item->task->description;
        if($result = $item->task->my_result->first()){
            $item->status = $result->status;
        }else{
            $item->status = 'Belum Dikerjakan';
        }
        $item->kunci_jawaban = $item->task->kunci_jawaban;
        $item->unsetRelation('task');
        $item->setHidden(['school_task_id', 'teacher_subjects_id']);
        }

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Tugas Guru', 'data' => $data], 200);
    }
}
