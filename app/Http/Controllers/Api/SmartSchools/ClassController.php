<?php

namespace App\Http\Controllers\Api\SmartSchools;

use App\Http\Controllers\Controller;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolStudent;
use App\Models\SmartSchools\SchoolStudentPresence;
use App\Models\SmartSchools\SchoolClassJournal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use StdClass;

class ClassController extends Controller
{
    public function index(Request $request)
    {
        $class = auth()->user()->profile->class->first()->class;
        $school_id = auth()->user()->profile->school_id;
        $class_journal = SchoolClassJournal::schoolId($school_id)->classId($class->id)->whereDate('date', now())->get();
        $jurnal = [];
        foreach ($class_journal as $key => $value) {
            $now = time();
            $start = strtotime(date('Y-m-d H:i:s', strtotime('-15 minutes', strtotime($value->start_at))));
            $end = strtotime(date('Y-m-d H:i:s', strtotime($value->end_at)));
            $jurnal[$key] = new StdClass();
            $jurnal[$key]->id_jadwal = $value->id;
            $jurnal[$key]->mata_pelajaran = $value->class_schedule->teacher_subjects->subjects->name;
            $jurnal[$key]->id_pengampu = $value->class_schedule->teacher_subjects->school_teacher_id;
            $jurnal[$key]->pengampu = $value->class_schedule->teacher_subjects->teacher->name;
            $jurnal[$key]->foto_pengampu = $value->class_schedule->teacher_subjects->teacher->photo;
            $jurnal[$key]->mulai = $value->start_at;
            $jurnal[$key]->selesai = $value->end_at;
            if($start < $now && $end > $now){
                $jurnal[$key]->sedang_berlangsung = true;
            }else{
                
                $jurnal[$key]->sedang_berlangsung = false;
            }
        }
        $data = [];
        $data['kelas'] = $class->class_name;
        $data['jadwal'] = $jurnal;
        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Ruang Kelas', 'data' => $data], 200);
    }
    public function student(Request $request)
    {
        $class_id = auth()->user()->profile->class->first()->class->id;
        $data = SchoolStudent::whereHas('class', function($q) use($class_id){
            $q->where('is_active', 1)->where('class_id', $class_id);
        })->select('name as nama', 'NISN as nisn', 'photo')->orderBy('name', 'asc')->get();

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Siswa Kelas', 'data' => $data], 200);
    }
    public function joinRoom(Request $request)
    {
        $id = $request->id_jadwal;
        $journal = SchoolClassJournal::find($id);

        if(!$journal){
            return response()->json(['code' => 404, 'message' => 'Jadwal tidak ditemukan'], 404);
        }

        $start = strtotime('+15 minutes');
        $start = date("H:i", $start);

        $end = date("H:i", time());

        if($journal->start_at > $start){
            return response()->json(['code' => 403, 'message' => 'Kelas belum dimulai'], 403);
        }
        if($journal->end_at < $end){
            return response()->json(['code' => 403, 'message' => 'Kelas sudah selesai'], 403);
        }

        $presence = SchoolStudentPresence::where('school_class_journal_id', $id)->where('student_id', auth()->user()->school_student->id)->first();
        
        if(!$presence){
            return response()->json(['code' => 403, 'message' => 'Anda tidak dapat masuk di kelas ini'], 403);
        }

        if($request->alasan){
            $validator = Validator::make($request->all(), [
                'alasan' => 'required|in:S,I',
                'surat' => 'required|max:5000|mimes:pdf,txt,doc,docx'
            ]);

            if ($validator->fails()) {
                return response()->json(['code' => 422, 'messages' => $validator->errors()], 422);
            }

            $surat = null;
            if($surat = $request->file("surat")){
                $surat = $request->file("surat")->store("SmartSchool/".$presence->student->school_id."/presence/".$presence->student_id."/letter");
            }

            $presence->presence = $request->alasan;
            $presence->letter= $surat;
            $presence->save();
            return response()->json(['code' => 200, 'message' => 'Berhasil Melakukan Izin'], 200);
        }

        if($presence->presence == 'A'){
            $presence->joined_at = now();
            $presence->presence = 'H';
            $late = strtotime('+15 minutes', $presence->class_journal->start_at_time);
            if($late < time()){
                $presence->presence = 'T';
            }
            $presence->save();
        }

        return response()->json(['code' => 200, 'message' => 'Berhasil Mengambil Data Ruang Kelas', 'data' => ['room' => 'genius-smartschool-'.$journal->id, 'domain' => 'https://meet.jit.si']], 200);
    }
}
