<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserProfile\UserProfile;
use App\Models\User\UserReferal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Notifications\Activation;
use App\Notifications\Activated;
use App\Notifications\PasswordReset;
use App\Notifications\PasswordResetted;
use App\Http\Requests\Api\ApiRegisterRequest;
use App\Http\Requests\Api\ApiEmailRequest;
use App\Http\Requests\Api\ApiResetPasswordRequest;
use App\Http\Requests\Api\ApiChangePasswordRequest;
use App\Jobs\ReferalCalculation;
use Carbon\Carbon;
use Auth;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $loginField = request('email');
        $credentials = null;

        if ($loginField !== null) {
            $loginType = filter_var($loginField, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            request()->merge([ $loginType => $loginField ]);

            $credentials = request([ $loginType, 'password' ]);
        } else {
            return response()->json(['code' => 422, 'message' => 'email tidak boleh kosong'], 422);
        }

        $data_user = User::where($loginType, $loginField)->select('last_session')->whereHas('profile')->first();

        try {
            if(isset($data_user->last_session)){
                \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($data_user->last_session), $forceForever = false);
            }
        } catch (JWTException $e) {

        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['code' => 400, 'message' => 'Alamat Email Atau Password Salah'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['code' => 500, 'message' => 'Tidak Dapat Membuat Token'], 500);
        }
        $user = User::where('id', auth()->user()->id)->first();
        $user->last_session = $token;
        $user->save();
        $user->profile;
        isset($user->profile->district) ? $user->profile->district->city->province : '';
        if($user->level == 'Siswa'){
            //untuk qiscus error soalnya
            $hmm = 2020;
            $user->profile->phone_number = $user->id.$hmm;
        }
        if($user->level == 'Siswa'){
            if($user->school_student->class->first()){
                $user->profile->class_id = $user->school_student->class->first()->class->id;
                $user->profile->class_name = $user->school_student->class->first()->class->class_name;
            }
            $user->makeHidden(['school_student']);
            if($user->school_student->status == 0){
                try {
                    if(isset($user->last_session)){
                        \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($user->last_session), $forceForever = false);
                    }
                } catch (JWTException $e) {

                }
                return response()->json(['code' => 403, 'message' => 'Akun anda dinonaktifkan oleh pihak Sekolah'], 403);
            }
        }
        return response()->json(['code' => 200, 'message' => 'Berhasil Login', 'data' => ['token' => $token, 'user' => $user]], 200);
    }

    public function check()
    {
        try {
            $user=JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['code' => 401, 'authenticated' => false], 401);
        }
        return response()->json(['code' => 200, 'authenticated' => true], 200);
    }

    public function logout()
    {
        $user = auth()->user()->update(['last_session' => null]);
        try {
            $token = JWTAuth::getToken();
            if ($token) {
                JWTAuth::invalidate($token);
            }
        } catch (JWTException $e) {
            return response()->json(['code' => 500, 'message' => $e->getMessage()], 500);
        }
        return response()->json(['code' => 200, 'message' => 'Berhasil Keluar !'], 200);
    }

    public function register(ApiRegisterRequest $request)
    {
        $create_user = User::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'referal' => $request->get('referal'),
            'password' => Hash::make($request->get('password')),
            'level' => 'User'
        ]);

        $profile = UserProfile::create([
            "user_id"               => $create_user->id,
            "name"                  => $request->get('name'),
            'phone_number'          => $request->get('phone_number'),
            "parents_phone_number"  => $request->get('parents_phone_number'),
            'gender'                => $request->get('gender'),
            'date_of_birth'         => Carbon::parse($request->get('date_of_birth')),
            'school_name'           => $request->get('school_name'),
            'district_id'           => $request->get('district_id'),
            'address'               => $request->get('address'),
            'kip_number'            => $request->get('kip_number'),
        ]);

        if($request->referal){
            ReferalCalculation::dispatch($create_user->id,$request->referal);
        }

        $user = User::with('profile.district.city.province')->where('id', $create_user->id)->first();
        $token = JWTAuth::fromUser($user);

        if ( $token ) {
            $user->last_session = session()->getId();
            $user->save();
            return response()->json(['code' => 200, 'message' => 'Berhasil Mendaftar', 'data' => ['user' => $user, 'token' => $token]],200);
        }
        return response()->json(['code' => 500, 'message' => 'Terjadi Masalah, Silahkan Coba Lagi Nanti'],500);
    }

    public function getAuthenticatedUser()
    {
        $user = auth()->user();
        $user->profile = auth()->user()->profile;
        $user->profile->district = auth()->user()->profile->district;
        if($user->profile->district){
            $user->profile->district->city = auth()->user()->profile->district->city;
            $user->profile->district->city->province = auth()->user()->profile->district->city->province;
        }
        if($user->level == 'Siswa'){
            if($user->school_student->class->first()){
                $user->class_id = $user->school_student->class->first()->class->id;
                $user->class_name = $user->school_student->class->first()->class->class_name;
            }
            $user->makeHidden(['school_student']);
        }
        return response()->json(['code' => 200, 'message' => 'Berhasil Mendapatkan Data User', 'data' => ['user' => $user]],200);
    }

    public function requestNewActivateToken()
    {
        $token = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,6);
        $user = User::where('id', auth()->user()->id)->first();
        $user->activation_token = $token;
        $user->save();
        $user->notify(new Activation($user));
        return response()->json(['code' => 200, 'message' => 'Token telah di kirim ke email anda', 'data' => ['user' => $user]],200);
    }

    public function activate(Request $request)
    {
        if(auth()->user()->activation_token == $request->activation_token){
            User::where('id', auth()->user()->id)->where('level', 'User')->update(['is_active' => 1, 'activation_token' => null]);
            return response()->json(['code' => 200, 'message' => 'Berhasil Mengaktifkan User'],200);
        }
        return response()->json(['code' => 404, 'message' => 'Token Tidak Valid'],404);
    }

    public function password(ApiEmailRequest $request){
        $user = User::whereEmail(request('email'))->first();
        $token = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,6);
        $delete = \DB::table('password_resets')->updateOrInsert(
            ['email' => $request->email],
            ['token' => $token]
        );
        $user->notify(new PasswordReset($request,$token));
        return response()->json(['code' => 200, 'message' => 'Berhasil mengirim kode, Silahkan cek email Anda.'], 200);
    }

    public function resetPassword(ApiResetPasswordRequest $request){
        $user = User::whereEmail(request('email'))->first();
        $user->password = bcrypt(request('password'));
        $user->save();
        $user->notify(new PasswordResetted($user));
        return response()->json(['code' => 200, 'message' => 'Password anda berhasil di ubah. Silahkan login kembali'], 200);
    }

    public function changePassword(ApiChangePasswordRequest $request){
        $user = auth()->user();
        $user->password = bcrypt(request('password'));
        $user->save();
        return response()->json(['code' => 200, 'message' => 'Password Anda berhasil di ubah'], 200);
    }
}
