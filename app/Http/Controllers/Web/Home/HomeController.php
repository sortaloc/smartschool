<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Events\Event;
use DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Home";
        $sub_title = "";
        if(auth()->user()->hasRole('Super Admin')){
            $dates = [];
            $new_users = [];
            $i = 0;
            $users = \App\User::whereDate('created_at', '>=', date('c', strtotime('-30 days')))->select('id', DB::raw('count(*) as total, DATE(created_at) as date'))
            ->groupBy('date')
            ->get()->groupBy('date');
            for($x = 29; $x >= 0; $x--){
                $dates[$i] = date('d M', strtotime('-'.$x.' days'));
                $index = date('Y-m-d', strtotime('-'.$x.' days'));
                if(isset($users[$index])){
                    $new_users[$i] = $users[$index][0]->total;
                }else{
                    $new_users[$i] = 0;
                }
                $i++;
            }
            return view('pages.index', compact('title', 'sub_title', 'dates', 'new_users'));
        }else{
            return view('pages.index', compact('title', 'sub_title'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
