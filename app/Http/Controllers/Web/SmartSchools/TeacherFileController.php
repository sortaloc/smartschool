<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTeacherFile;
use App\Models\SmartSchools\SchoolBookCategory;

class TeacherFileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Materi Guru'])->only('index', 'show');
        $this->middleware(['permission:Tambah Materi Guru'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Materi Guru'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Materi Guru'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolTeacherFile::role()->schoolId($school_id)->latest()->get();
        $title = 'Materi Guru';
        $sub_title = 'Home / Materi Guru';
        return view('pages.smart_school.teacher_file.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Materi Guru Baru';
        $sub_title = 'Home / Materi Guru Baru';
        return view('pages.smart_school.teacher_file.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $request->validate([
            'nama_materi' => 'required|string|max:150',
            'tipe_materi' => 'required|in:Document,Video',
        ]);
        if($request->tipe_materi == 'Document'){
            $request->validate([
                'file' => 'required|max:50000|mimes:doc,docx,ppt,pptx,xls,xlsx,pdf,csv,txt',
            ]);
        }elseif($request->tipe_materi == 'Video'){
            $request->validate([
                'file' => 'required|max:100000|mimes:mp4,mkv,avi,mpeg,wmp,3gp,flv,swf',
            ]);
        }

        $file = null;
        if($file = $request->file("file")){
            $file = $request->file("file")->store("SmartSchool/".$school_id."/teacher-file");
        }

        SchoolTeacherFile::create([
            "school_id" => $school_id,
            "teacher_id" => auth()->user()->school_teacher->id,
            "name" => $request->nama_materi,
            "type" => $request->tipe_materi,
            "file" => $file
        ]);

        return redirect()->route('school-teacher-files.index')->with('success', 'Berhasil Menambah Materi Guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTeacherFile $SchoolTeacherFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTeacherFile $SchoolTeacherFile)
    {
        isMySchool($SchoolTeacherFile->school_id);
        $title = 'Ubah Materi Guru';
        $sub_title = 'Home / Ubah Materi Guru';
        $data = $SchoolTeacherFile;
        return view('pages.smart_school.teacher_file.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolTeacherFile $SchoolTeacherFile)
    {
        isMySchool($SchoolTeacherFile->school_id);
        $school_id = $SchoolTeacherFile->school_id;
        $request->validate([
            'nama_materi' => 'required|string|max:150',
            'tipe_materi' => 'required|in:Document,Video',
        ]);
        if($request->tipe_materi == 'Document'){
            $request->validate([
                'file' => 'max:50000|mimes:doc,docx,ppt,pptx,xls,xlsx,pdf,csv,txt',
            ]);
        }elseif($request->tipe_materi == 'Video'){
            $request->validate([
                'file' => 'max:100000|mimes:mp4,mkv,avi,mpeg,wmp,3gp,flv,swf',
            ]);
        }

        $file = $SchoolTeacherFile->getOriginal('file');
        if($request->file("file")){
            if (Storage::exists($file)) {
                Storage::delete($file);
            }
            $file = $request->file("file")->store("SmartSchool/".$school_id."/teacher-file");
        }

        SchoolTeacherFile::where('id', $SchoolTeacherFile->id)->update([
            "name" => $request->nama_materi,
            "type" => $request->tipe_materi,
            "file" => $file
        ]);

        return redirect()->route('school-teacher-files.index')->with('success', 'Berhasil Mengubah Materi Guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTeacherFile $SchoolTeacherFile)
    {
        if(!auth()->user()->can('Semua Hak Akses')){
            if(auth()->user()->school_teacher->id != $SchoolTeacherFile->teacher_id){
                return redirect()->back()->with('danger', 'Kecuali Super Admin Tidak Dapat Menghapus Materi Guru Lain');
            }
        }
        $file = $SchoolTeacherFile->getOriginal('file');
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $SchoolTeacherFile->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Materi Guru');
    }
}
