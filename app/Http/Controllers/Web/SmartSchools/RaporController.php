<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskClass;
use App\Models\SmartSchools\SchoolTaskResult;
use App\Models\SmartSchools\SchoolStudent;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolYear;
use App\Models\SmartSchools\SchoolPredicate;
use App\Models\SmartSchools\SchoolRapor;
use App\Models\SmartSchools\SchoolRaporPkl;
use App\Models\SmartSchools\SchoolRaporAcademic;
use App\Models\SmartSchools\SchoolRaporCharacter;
use App\Models\SmartSchools\SchoolRaporExtracurricular;
use App\Models\SmartSchools\SchoolAssessmentCategory;
use App\Models\SmartSchools\SchoolAssessmentType;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use PDF;

class RaporController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Rapor'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Rapor'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Rapor'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Rapor'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $year = mySchoolYear();
        $data_temp = SchoolStudent::where('school_id', $school_id);
        if($request->class_id){
            $data_temp->whereHas('class', function($q) use($request){
                $q->where('class_id', $request->class_id);
            });
        }
        $datas = $data_temp->get();
        $title = 'Rapor Siswa';
        $sub_title = 'Home / Rapor Siswa';
        $classes = SchoolClass::schoolId($school_id)->orderBy('class_id', 'asc')->get();
        $years = SchoolYear::schoolId($school_id)->orderBy('start_year', 'desc')->get();
        $school = School::findOrFail($school_id);
        $rapor_distribution = $school->rapor_distribution;
        $school_id = $school->id;

        return view('pages.smart_school.rapor.index', compact('datas', 'classes', 'years', 'rapor_distribution', 'school_id', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $school_id = mySchool();
        $year = mySchoolYear();
        $title = 'Rapor Siswa Baru';
        $sub_title = 'Home / Rapor Siswa Baru';
        $kenaikan_kelas = null;
        if($year->semester == 'Ganjil'){
            $kenaikan_kelas = 'Semester';
        }
        $student = SchoolStudent::where('school_id', $school_id)->where('id', $request->student_id)->firstOrFail();
        $characters = SchoolAssessmentTypeRef::where('school_id', $school_id)->where('category', 'Sikap')->pluck('name', 'id');

        if(!$class = $student->class()->first()){
            return redirect()->back()->with('danger',
                    'Siswa Tidak Memiliki Kelas ');
        }

        if(!$teacher_id = $student->class()->first()->class->homeroom_teacher_id){
            return redirect()->back()->with('danger',
                    'Wali kelas tidak boleh kosong ');
        }
        $rapor = SchoolRapor::firstOrCreate([
            'school_id' => $school_id,
            'student_id' => $request->student_id,
            'year_id' => $year->id,
            'class_id' => $class->class_id,
            'semester' => $year->semester],[
            'teacher_id' => $teacher_id,
            'sakit' => $student->presenceSThisYear()->count(),
            'izin' => $student->presenceIThisYear()->count(),
            'tanpa_keterangan' => $student->presenceAThisYear()->count(),
            'kenaikan_kelas' => $kenaikan_kelas
        ]);
        if(!$student->raporThisYear()->first()->academics()->first()){
            $tugas_utamas = SchoolTaskClass::whereHas('task', function($q) use($school_id, $student){
                $q->schoolId($school_id)->thisYear()->utama();
            })->classId($student->class()->firstOrFail()->class_id)->get();
            $subjects = [];
            $x = 0;
            foreach($tugas_utamas as $key => $tu){
                $hasil_utama = SchoolTaskResult::where('school_task_id', $tu->school_task_id)->studentId($student->id)->first();
                $bobot = SchoolAssessmentType::where('assessment_type_ref_id', $tu->task->assessment_type->id)
                        ->whereHas('assessment_category', function($q) use($tu){
                            $q->where('school_class_id', $tu->teacher_subjects->class->class_id)
                            ->where('school_subjects_id', $tu->teacher_subjects->school_subjects_id);
                        })->first();
                if(!$bobot){
                    return redirect()->back()->with('danger',
                    'Bobot penilaian "'.$tu->task->assessment_type->name.'" tidak ditemukan untuk mata pelajaran '.$tu->teacher_subjects->subjects->name.' - kelas '.$tu->teacher_subjects->class->class->name);
                }else{
                    $assessment_category = $tu->task->assessment_type->category;
                    $assessment_type = $bobot->id;
                    $task_id = $tu->school_task_id;
                    $subjects[$tu->teacher_subjects->school_subjects_id]['bobot_pengetahuan'] = $bobot->assessment_category->pengetahuan;
                    $subjects[$tu->teacher_subjects->school_subjects_id]['bobot_keterampilan'] = $bobot->assessment_category->keterampilan;
                    $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['bobot'] =  $bobot->weight;
                }
                $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['hasil'][$task_id]['point_preference'] = $tu->task->point_preference;
                $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['hasil'][$task_id]['max_point'] = $tu->task->max_point;
                $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['hasil'][$task_id]['utama'] = $hasil_utama ? $hasil_utama->point : 0;
                $hasil_susulan = SchoolTaskResult::whereHas('task', function($q)use($task_id, $student){
                    $q->where('old_task_id', $task_id)->susulan();
                })->studentId($student->id)->first();
                $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['hasil'][$task_id]['susulan'] = $hasil_susulan ? $hasil_susulan->point : 0;
                $hasil_perbaikan = SchoolTaskResult::whereHas('task', function($q)use($task_id, $student){
                    $q->where('old_task_id', $task_id)->perbaikan();
                })->studentId($student->id)->first();
                $subjects[$tu->teacher_subjects->school_subjects_id][$assessment_category][$assessment_type]['hasil'][$task_id]['perbaikan'] = $hasil_perbaikan ? $hasil_perbaikan->point : 0;
            }
            foreach($subjects as $key => $value){
                $bobot_pengetahuan = $value['bobot_pengetahuan'];
                $bobot_keterampilan = $value['bobot_keterampilan'];
                $akademik = new SchoolRaporAcademic;
                $akademik->rapor_id = $rapor->id;
                $akademik->subjects_id = $key;
                $hasil_pengetahuan = 0;
                $jumlah_jenis_tugas = 0;
                $total_bobot_jenis_tugas = 0;
                if(isset($value['Pengetahuan'])){
                    foreach($value['Pengetahuan']  as $pengetahuan){
                        $total_bobot_jenis_tugas += $pengetahuan['bobot'];
                        $jumlah_jenis_tugas++;
                        $total_hasil_tugas = 0;
                        $total_jumlah_tugas = 0;
                        $jumlah_tugas = 1;
                        foreach($pengetahuan['hasil'] as $hasil){
                            $hasil_tugas = 0;
                            $total_jumlah_tugas++;
                            if($hasil['utama']){
                                $hasil_tugas = $hasil['utama'];
                            }elseif($hasil['susulan']){
                                $hasil_tugas = $hasil['susulan'];
                            }
                            if($hasil['perbaikan']){
                                $hasil_tugas = $hasil['perbaikan'];
                            }
                            $total_hasil_tugas += $hasil_tugas;
                        }
                        $hasil_pengetahuan += $total_hasil_tugas / $total_jumlah_tugas * $pengetahuan['bobot'];
                    }
                }
                if($hasil_pengetahuan != 0 && $total_bobot_jenis_tugas != 0){
                    $akademik->pengetahuan = $hasil_pengetahuan / $total_bobot_jenis_tugas;
                }else{
                    $akademik->pengetahuan = 0;
                }
                //keterampilan
                $hasil_keterampilan = 0;
                $jumlah_jenis_tugas = 0;
                $total_bobot_jenis_tugas = 0;
                if(isset($value['Keterampilan'])){
                    foreach($value['Keterampilan']  as $keterampilan){
                        $total_bobot_jenis_tugas += $keterampilan['bobot'];
                        $jumlah_jenis_tugas++;
                        $total_hasil_tugas = 0;
                        $total_jumlah_tugas = 0;
                        $jumlah_tugas = 1;
                        foreach($keterampilan['hasil'] as $hasil){
                            $hasil_tugas = 0;
                            $total_jumlah_tugas++;
                            if($hasil['utama']){
                                $hasil_tugas = $hasil['utama'];
                            }elseif($hasil['susulan']){
                                $hasil_tugas = $hasil['susulan'];
                            }
                            if($hasil['perbaikan']){
                                if($hasil['point_preference'] == 'Terbaik'){
                                    $hasil_tugas = max($hasil['utama'], $hasil['susulan'], $hasil['perbaikan']);
                                }elseif($hasil['point_preference'] == 'Maksimal Nilai'){
                                    $hasil_tugas = $hasil['perbaikan'] >= $hasil['max_point'] ? $hasil['max_point'] : $hasil['perbaikan'];
                                }elseif($hasil['point_preference'] == 'Perbaikan'){
                                    $hasil_tugas = $hasil['perbaikan'];
                                }
                            }
                            $total_hasil_tugas += $hasil_tugas;
                        }
                        $hasil_keterampilan += $total_hasil_tugas / $total_jumlah_tugas * $keterampilan['bobot'];
                    }
                }
                if($hasil_keterampilan != 0 && $total_bobot_jenis_tugas != 0){
                    $akademik->keterampilan = $hasil_keterampilan / $total_bobot_jenis_tugas;
                }else{
                    $akademik->keterampilan = 0;
                }
                $akademik->final_point = (($akademik->pengetahuan * $bobot_pengetahuan) + ($akademik->keterampilan * $bobot_keterampilan)) / ($bobot_pengetahuan + $bobot_keterampilan);
                $predicate = SchoolPredicate::where('school_id', $school_id)->where('assessment_type_ref_id', null)->where('lower_limit', '<=', $akademik->final_point)->where('upper_limit', '>=', $akademik->final_point)->first();
                $akademik->predicate = $predicate ? $predicate->description : '-';
                $akademik->save();
            }
        }
        $rapor = $student->raporThisYear()->first();
        $classes = SchoolClass::schoolId($school_id)->get();
        return view('pages.smart_school.rapor.create', compact('title', 'rapor', 'classes', 'characters', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = SchoolStudent::with('parent', 'guardian',
        'school.district.city.province', 'rapors.academics', 'rapors.pkls',
        'rapors.extracurriculars', 'rapors.homeroom_teacher'
        )->where('id', $request->student_id)->firstOrFail();
        $name = "rapor_siswa_".$data->id.'_'.time().".pdf";

        $school = $data->school;
        if($school->type == 'kemendikbud'){
            $school->naungan = 'img/twh.png';
        }else{
            $school->naungan = 'img/kemenag.png';
        }
        $class = $data->class()->first();

        $data->photo_b64 = $data->photo ? base64_encode(file_get_contents($data->photo)) : null;
        $pdf = PDF::loadview('pages.smart_school.rapor.download', compact('school', 'data', 'name'));

        $pdf->setOptions(['isPhpEnabled' => true]);
        $pdf->setPaper('a4', 'potrait');

        $filename = "SmartSchool/".$data->school_id."/rapor/rapor_siswa_".$data->id.'_'.time().".pdf";
        Storage::put($filename, $pdf->output());
        $data = SchoolStudent::findOrFail($request->student_id);
        $data->rapor = $filename;
        $rapor = $data->raporThisYear()->first();
        if($rapor){
            $rapor->rapor = $filename;
            $rapor->save();
        }
        $data->save();

        return redirect()->back()->with('success', 'Berhasil Membuat Rapor PDF');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
        $data = SchoolRapor::findOrFail($id);
        /* $name = 'RAPOR '.$data->name.'pdf';

        $school = $data->school;
        if($school->type == 'kemendikbud'){
            $school->naungan = 'img/twh.png';
        }else{
            $school->naungan = 'img/kemenag.png';
        }

        $pdf = PDF::loadview('pages.smart_school.rapor.download', compact('school', 'data', 'name'));

        $pdf->setOptions(['isPhpEnabled' => true]);
        $pdf->setPaper('a4', 'potrait'); */
        return $pdf->stream($data->rapor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolRapor $SchoolRapor)
    {
        $SchoolRapor->academics()->delete();
        return redirect()->route('school-rapors.create', ['student_id' => $SchoolRapor->student_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolRapor $SchoolRapor)
    {
        isMySchool($SchoolRapor->school_id);
        if($request->catatan_akademik){
            $validatedData = $request->validate([
                'catatan_akademik' => 'required|string|max:10000'
            ]);
            $SchoolRapor->catatan_akademik = $request->catatan_akademik;
            $SchoolRapor->save();
            return redirect()->back()->with('success', 'Berhasil Mengubah Catatan Akademik Siswa');
        }
        if($request->kegiatan_ekstrakurikuler){
            $validatedData = $request->validate([
                'kegiatan_ekstrakurikuler' => 'required|string|max:100',
                'keterangan' => 'required|string|max:150',
            ]);
            SchoolRaporExtracurricular::create([
                'rapor_id' => $SchoolRapor->id,
                'activity' => $request->kegiatan_ekstrakurikuler,
                'description' => $request->keterangan
            ]);
            return redirect()->back()->with('success', 'Berhasil Menambah Ekstrakurikuler Siswa');
        }
        if($request->catatan_karakter){
            $validatedData = $request->validate([
                'catatan_karakter' => 'required|string|max:10000'
            ]);
            $SchoolRapor->catatan_perkembangan_karakter = $request->catatan_karakter;
            $SchoolRapor->save();
            return redirect()->back()->with('success', 'Berhasil Mengubah Catatan Karakter Siswa');
        }
        if($request->mitra){
            $validatedData = $request->validate([
                'mitra' => 'required|string|max:100',
                'lokasi' => 'required|string|max:150',
                'lamanya' => 'required|numeric|max:50',
                'keterangan' => 'required|string|max:150'
            ]);
            SchoolRaporPkl::create([
                'rapor_id' => $SchoolRapor->id,
                'mitra' => $request->mitra,
                'location' => $request->lokasi,
                'duration' => $request->lamanya,
                'description' => $request->keterangan
            ]);
            return redirect()->back()->with('success', 'Berhasil Menambah Data PKL Siswa');
        }
        if($request->karakter){
            $validatedData = $request->validate([
                'karakter' => 'required',
                'keterangan' => 'required|string|max:200'
            ]);
            SchoolRaporCharacter::create([
                'rapor_id' => $SchoolRapor->id,
                'assessment_type_ref_id' => $request->karakter,
                'description' => $request->keterangan
            ]);
            return redirect()->back()->with('success', 'Berhasil Menambah Karakter Siswa');
        }
        if($request->kenaikan_kelas){
            $validatedData = $request->validate([
                'kenaikan_kelas' => 'required|string|in:Tidak Naik Kelas,Naik Kelas',
                'ke_kelas'          => 'required|exists:school_classes,id'
            ]);
            $SchoolRapor->kenaikan_kelas = $request->kenaikan_kelas;
            $SchoolRapor->ke_kelas = $request->ke_kelas;
            $SchoolRapor->save();
            return redirect()->back()->with('success', 'Berhasil Mengubah Catatan Karakter Siswa');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolRapor $SchoolRapor)
    {
        abort(403);
        $SchoolRapor->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Rapor Siswa');
    }
}
