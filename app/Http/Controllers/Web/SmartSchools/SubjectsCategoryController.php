<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolSubjectsCategory;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class SubjectsCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Mata Pelajaran'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Mata Pelajaran'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Mata Pelajaran'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Mata Pelajaran'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolSubjectsCategory::where('school_id', $school_id)->orderBy('name')->get();
        $title = 'Kategori Mata Pelajaran';
        $sub_title = 'Home / Kategori Mata Pelajaran';
        return view('pages.smart_school.subjects_category.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Kategori Mata Pelajaran Baru';
        $sub_title = 'Home / Kategori Mata Pelajaran Baru';
        $subjects_categories = SchoolSubjectsCategory::where('school_id', $school_id)->pluck('name', 'id');
        return view('pages.smart_school.subjects_category.create', compact('subjects_categories', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $val = SchoolSubjectsCategory::where('school_id', $school_id)->pluck('name')->toArray();
        $val = implode (", ", $val);
        $validatedData = $request->validate([
            'kategori_mata_pelajaran' => 'bail|required|not_in:'.$val,
        ]);
        $data = SchoolSubjectsCategory::create([
            'school_id' => $school_id,
            'name' => $request->kategori_mata_pelajaran
        ]);

        return redirect()->route('school-subjects-categories.index')->with('success', 'Berhasil Menambah Kategori Mata Pelajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSubjects $SchoolSubjectsCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSubjectsCategory $SchoolSubjectsCategory)
    {
        $title = 'Ubah Kategori Mata Pelajaran';
        $sub_title = 'Home / Ubah Kategori Mata Pelajaran';
        $data = $SchoolSubjectsCategory;
        return view('pages.smart_school.subjects_category.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolSubjectsCategory $SchoolSubjectsCategory)
    {
        isMySchool($SchoolSubjectsCategory->school_id);
        $val = SchoolSubjectsCategory::where('school_id', $SchoolSubjectsCategory->school_id)->pluck('name')->toArray();
        if (($key = array_search($SchoolSubjectsCategory->name, $val)) !== false) {
            unset($val[$key]);
        }
        $val = implode (", ", $val);
        $validatedData = $request->validate([
            'kategori_mata_pelajaran' => 'bail|required|not_in:'.$val
        ]);
        $SchoolSubjectsCategory->name = $request->kategori_mata_pelajaran;
        $SchoolSubjectsCategory->save();

        return redirect()->route('school-subjects-categories.index')->with('success', 'Berhasil Mengubah Kategori Mata Pelajaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolSubjects $SchoolSubject)
    {
        //
    }
}
