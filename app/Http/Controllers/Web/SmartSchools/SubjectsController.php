<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolSubjectsCategory;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class SubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Mata Pelajaran'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Mata Pelajaran'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Mata Pelajaran'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Mata Pelajaran'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolSubjects::where('school_id', $school_id)->orderBy('name');

        if($request->ajax()){
            if($request->class_id && $request->without == 'class_subjects'){
                $datas = $data_temp->whereDoesntHave('class_subjects', function($q) use($request){
                    $q->where('class_id', $request->class_id);
                });
                $datas = $data_temp->get();
                $data = [];
                $datas->each(function($q, $key) use(&$data){
                    $data[$key]['id'] = $q->id;
                    $data[$key]['text'] = $q->name;
                });
                return response()->json(['results' => $data]);
            }
        }

        $datas = $data_temp->get();

        $title = 'Mata Pelajaran';
        $sub_title = 'Home / Mata Pelajaran';
        return view('pages.smart_school.subjects.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Mata Pelajaran Baru';
        $sub_title = 'Home / Mata Pelajaran Baru';
        $subjects_categories = SchoolSubjectsCategory::where('school_id', $school_id)->pluck('name', 'id');
        return view('pages.smart_school.subjects.create', compact('subjects_categories', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $val = SchoolSubjects::where('school_id', $school_id)->pluck('name')->toArray();
        $val = implode (", ", $val);
        $validatedData = $request->validate([
            'mata_pelajaran' => 'bail|required|not_in:'.$val,
            'kategori_mata_pelajaran' => 'bail|required|exists:school_subjects_categories,id'
        ]);
        $data = SchoolSubjects::create([
            'school_id' => $school_id,
            'subjects_category_id' => $request->kategori_mata_pelajaran,
            'name' => $request->mata_pelajaran
        ]);

        return redirect()->route('school-subjects.index')->with('success', 'Berhasil Menambah Mata Pelajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSubjects $SchoolSubject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSubjects $SchoolSubject)
    {
        $title = 'Ubah Mata Pelajaran';
        $sub_title = 'Home / Ubah Mata Pelajaran';
        $data = $SchoolSubject;
        $subjects_categories = SchoolSubjectsCategory::where('school_id', $SchoolSubject->school_id)->pluck('name', 'id');
        return view('pages.smart_school.subjects.edit', compact('data', 'subjects_categories', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolSubjects $SchoolSubject)
    {
        isMySchool($SchoolSubject->school_id);
        $val = SchoolSubjects::where('school_id', $SchoolSubject->school_id)->pluck('name')->toArray();
        if (($key = array_search($SchoolSubject->name, $val)) !== false) {
            unset($val[$key]);
        }
        $val = implode (", ", $val);
        $validatedData = $request->validate([
            'mata_pelajaran' => 'bail|required|not_in:'.$val,
            'kategori_mata_pelajaran' => 'bail|required|exists:school_subjects_categories,id'
        ]);
        $SchoolSubject->name = $request->mata_pelajaran;
        $SchoolSubject->subjects_category_id = $request->kategori_mata_pelajaran;
        $SchoolSubject->save();

        return redirect()->route('school-subjects.index')->with('success', 'Berhasil Mengubah Mata Pelajaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolSubjects $SchoolSubject)
    {
        //
    }
}
