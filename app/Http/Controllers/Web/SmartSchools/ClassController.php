<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClassRef;

class ClassController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Kelas'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolClassRef::where('school_id', $school_id)->orderBy('name', 'ASC')->get();
        $title = 'Data Referensi Kelas';
        $sub_title = 'Home / Data Kelas';
        return view('pages.smart_school.class.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Kelas Referensi Baru';
        $sub_title = 'Home / Kelas Referensi Baru';
        return view('pages.smart_school.class.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_kelas' => 'bail|required|string|max:50'
        ]);
        $school_id = mySchool();
        SchoolClassRef::create([
            'name' => $request->nama_kelas,
            'school_id' => $school_id
        ]);

        return redirect()->route('classes.index')->with('success', 'Berhasil Menambah Data Referensi Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClassRef $class)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClassRef $class)
    {
        $title = 'Ubah Referensi Kelas';
        $sub_title = 'Home / Ubah Referensi Kelas';
        $data = $class;
        return view('pages.smart_school.class.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClassRef $class)
    {
        $validatedData = $request->validate([
            'nama_kelas' => 'bail|required|string|max:50'
        ]);
        isMySchool($class->school_id);
        $class->name = $request->nama_kelas;
        $class->save();
        return redirect()->route('classes.index')->with('success', 'Berhasil Mengubah Data Referensi Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClassRef $class)
    {
        isMySchool($class->school_id);
        if($class->classes()->first()){
            return redirect()->back()->with('danger', 'Data Referensi Kelas Yang Dipakai Di Data Kelas Tidak Dapat Dihapus');
        }
        $class->delete();

        return redirect()->back()->with('success', 'Berhasil Menghapus Data Referensi Kelas');
    }
}
