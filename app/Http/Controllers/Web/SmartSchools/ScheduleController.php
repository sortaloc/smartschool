<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClassSchedule;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Exports\SmartSchoolScheduleExport;
use Maatwebsite\Excel\Facades\Excel;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Jadwal Kelas'])->only('show', 'index');
        $this->middleware(['permission:Semua Hak Akses|Tambah Jadwal Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Jadwal Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Jadwal Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolClassSchedule::role($school_id)->thisYear()->orderBy('day')->orderBy('start_at');

        if($request->ajax()){
            if($request->class_id){
                $data_temp->whereHas('teacher_subjects', function($q) use($request){
                    $q->where('school_class_id', $request->class_id);
                });
            }
            if($request->date){
                $d = strtoupper(Carbon::parse($request->date)->isoFormat('dddd'));
                $data_temp->where('day', $d);
            }
            $datas = $data_temp->get();
            $data = [];
            $datas->each(function($q, $key) use(&$data){
                $data[$key]['id'] = $q->id;
                $data[$key]['text'] = $q->teacher_subjects->subjects->name.' - Kelas '.$q->teacher_subjects->class->class_name;
                $data[$key]['teacher'] = $q->teacher_subjects->teacher->name;
            });
            return response()->json(['results' => $data]);
        }

        if($request->class_id){
            $data_temp->whereHas('teacher_subjects', function($q)use($request){
                $q->where('school_class_id', $request->class_id);
            });
        }

        $datas = $data_temp->get();

        if($request->teacher_id){
            $teacher_id = $request->teacher_id;
        }elseif(isset(auth()->user()->school_teacher->id)){
            $teacher_id = auth()->user()->school_teacher->id;
        }else{
            $teacher_id = null;
        }

        if($request->download){
            $school = School::findOrFail($school_id);
            $school_year = $school->school_year()->firstOrFail();
            $filter = [
                'class' => $request->class_id ? SchoolClass::findOrFail($request->class_id)->class_name : 'Semua Kelas',
                'school_year' => 'Semester '.$school_year->semester.' '.$school_year->start_year.' - '.$school_year->end_year
            ];
            return Excel::download(new SmartSchoolScheduleExport($school, $datas, $filter), 'jadwal kelas smartschool genius '.Carbon::now()->isoFormat('D-M-Y').'.xlsx');
        }

        $classes = SchoolTeacherSubjects::whereHas('teacher', function($q) use($school_id, $teacher_id){
            $q->where('school_id', $school_id);
            if($teacher_id){
                $q->where('id', $teacher_id);
            };
        })->get();
        $title = 'Jadwal Kelas';
        $sub_title = 'Home / Jadwal Kelas';
        return view('pages.smart_school.schedules.index', compact('datas', 'classes', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Jadwal Kelas Baru';
        $sub_title = 'Home / Jadwal Kelas Baru';
        $classes = SchoolClass::where('school_id', $school_id)->get();
        return view('pages.smart_school.schedules.create', compact('classes', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'mata_pelajaran' => 'bail|required|exists:school_teacher_subjects,id',
            'hari' => 'bail|required|in:SENIN,SELASA,RABU,KAMIS,JUMAT,SABTU,MINGGU',
            'kelas' => 'bail|required|exists:school_classes,id',
            'waktu_mulai' => 'bail|required|date_format:H:i',
            'waktu_selesai' => 'bail|required|date_format:H:i|after:waktu_mulai'
        ]);

        $val = SchoolClassSchedule::whereHas('school_year', function($q) use($school_id){
            $q->where('school_id', $school_id);
        })->whereHas('teacher_subjects', function($q) use($request){
            $q->where('school_class_id', $request->kelas);
        })->where('day', $request->hari)
            ->where(function ($q) use($request) {
                $q->where(function ($q1) use($request) {
                    $q1->where('start_at', '<', $request->waktu_mulai)->where('end_at', '>', $request->waktu_mulai);
                })->orWhere(function ($q2) use($request) {
                    $q2->where('start_at', '<', $request->waktu_selesai)->where('end_at', '>', $request->waktu_selesai);
                })->orWhere(function ($q3) use($request) {
                    $q3->where('start_at', '>=', $request->waktu_mulai)->where('end_at', '<=', $request->waktu_selesai);
                });
            })->first();
        if($val){
            return redirect()->back()->withErrors(['Waktu Jadwal Bertabrakan Dengan Mata Pelajaran Lain'])->withInput();
        }

        $year = mySchoolYear();

        $data = SchoolClassSchedule::create([
            'school_teacher_subjects_id' => $request->mata_pelajaran,
            'school_year_id' => $year->id,
            'day' => $request->hari,
            'start_at' => $request->waktu_mulai,
            'end_at' => $request->waktu_selesai
        ]);

        return redirect()->route('school-schedules.index')->with('success', 'Berhasil Menambah Jadwal Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClassSchedule $SchoolSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClassSchedule $SchoolSchedule)
    {
        $title = 'Ubah Jadwal Kelas';
        $sub_title = 'Home / Ubah Jadwal Kelas';
        $data = $SchoolSchedule;
        $classes = SchoolClass::where('school_id', $SchoolSchedule->school_year->school_id)->get();
        return view('pages.smart_school.schedules.edit', compact('data', 'classes', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClassSchedule $SchoolSchedule)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'hari' => 'bail|required|in:SENIN,SELASA,RABU,KAMIS,JUMAT,SABTU,MINGGU',
            'waktu_mulai' => 'bail|required|date_format:H:i',
            'waktu_selesai' => 'bail|required|date_format:H:i|after:waktu_mulai'
        ]);

        $val = SchoolClassSchedule::where('id', '!=', $SchoolSchedule->id)->whereHas('school_year', function($q) use($school_id){
            $q->where('school_id', $school_id);
        })->whereHas('teacher_subjects', function($q) use($SchoolSchedule){
            $q->where('school_class_id', $SchoolSchedule->teacher_subjects->school_class_id);
        })->where('day', $request->hari)
            ->where(function ($q) use($request) {
                $q->where(function ($q1) use($request) {
                    $q1->where('start_at', '<', $request->waktu_mulai)->where('end_at', '>', $request->waktu_mulai);
                })->orWhere(function ($q2) use($request) {
                    $q2->where('start_at', '<', $request->waktu_selesai)->where('end_at', '>', $request->waktu_selesai);
                })->orWhere(function ($q3) use($request) {
                    $q3->where('start_at', '>=', $request->waktu_mulai)->where('end_at', '<=', $request->waktu_selesai);
                });
            })->first();
        if($val){
            return redirect()->back()->withErrors(['Waktu Jadwal Bertabrakan Dengan Mata Pelajaran Lain'])->withInput();
        }

        $year = mySchoolYear();

        $data = SchoolClassSchedule::where('id', $SchoolSchedule->id)->update([
            'day' => $request->hari,
            'start_at' => $request->waktu_mulai,
            'end_at' => $request->waktu_selesai
        ]);

        return redirect()->route('school-schedules.index')->with('success', 'Berhasil Mengubah Jadwal Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClassSchedule $SchoolSchedule)
    {
        $check = $SchoolSchedule->class_journals->first();
        if($check){
            return redirect()->back()->with('danger', 'Jadwal yang berisi jurnal kelas tidak dapat dihapus !');
        }
        $SchoolSchedule->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data jadwal kelas');
    }
}
