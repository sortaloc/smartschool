<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolAdmin;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolAdmin::where('school_id', $school_id)->get();
        $title = 'Data Admin';
        $sub_title = 'Home / Data Admin';
        return view('pages.smart_school.admin.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Tambah Admin';
        $sub_title = 'Home / Tambah Admin';
        $teachers = SchoolTeacher::doesntHave('user.school_admin')->where('school_id', $school_id)->pluck('name', 'id');
        $permissions = Permission::where('type', 'SMARTSCHOOL')->pluck('name', 'id');
        return view('pages.smart_school.admin.create', compact('teachers', 'permissions', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();

        $teacher = SchoolTeacher::findOrFail($request->guru);

        SchoolAdmin::create([
            'school_id' => $school_id,
            'user_id' => $teacher->user_id
        ]);

        if($request->permissions){
            $teacher->user->syncPermissions($request->permissions);
            $teacher->user->assignRole('Admin Sekolah');
        }

        return redirect()->route('school-admins.index')->with('success', 'Berhasil Menambah Data Admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolAdmin $SchoolAdmin)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolAdmin $SchoolAdmin)
    {
        if($SchoolAdmin->user->hasRole('Super Admin Sekolah')){
            abort(403);
        }
        $school_id = mySchool();
        $title = 'Ubah Data Admin';
        $sub_title = 'Home / Ubah Data Admin';
        $data = $SchoolAdmin;
        $roles = Role::get();
        $permissions = Permission::where('type', 'SMARTSCHOOL')->get();
        return view('pages.smart_school.admin.edit', compact('data', 'roles', 'permissions', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolAdmin $SchoolAdmin)
    {
        if($SchoolAdmin->user->hasRole('Super Admin Sekolah')){
            abort(403);
        }
        isMySchool($SchoolAdmin->school_id);
        if($request->permissions){
            $SchoolAdmin->user->syncPermissions($request->permissions);
        }

        return redirect()->route('school-admins.index')->with('success', 'Berhasil Mengubah Data Admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolAdmin $SchoolAdmin)
    {
        isMySchool($SchoolAdmin->school_id);
        if($SchoolAdmin->user->hasRole('Super Admin Sekolah')){
            abort(403);
        }

        $SchoolAdmin->user->syncPermissions([]);
        $SchoolAdmin->delete();

        return redirect()->back()->with('danger', 'Berhasil Menghapus Data Admin');
    }
}
