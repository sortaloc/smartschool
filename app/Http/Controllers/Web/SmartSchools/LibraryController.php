<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolLibrary;
use App\Models\SmartSchools\SchoolBookCategory;
use DataTables;

class LibraryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Buku'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Buku'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Buku'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Buku'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        if($request->ajax()){
            $data = SchoolLibrary::with('category')->schoolId($school_id);
            return Datatables::eloquent($data)
            ->addIndexColumn()
            ->addColumn('cover_buku', function ($dt) {
                return '<img src="'.$dt->cover.'" width="100px alt="">';
            })
            ->addColumn('file_buku', function ($dt) {
                return '<a href="'.$dt->file.'" class="badge badge-primary" target="_blank">Buka</a>';
            })
            ->addColumn('action', function ($dt) {
                return view('pages.smart_school.library.action', compact('dt'))->render();
            })
            ->rawColumns(['cover_buku', 'file_buku', 'action'])
            ->make(true);
        }
        $title = 'Daftar Buku';
        $sub_title = 'Home / Daftar Buku';
        $datas = SchoolLibrary::schoolId($school_id)->latest()->get();
        return view('pages.smart_school.library.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Buku Baru';
        $sub_title = 'Home / Buku Baru';
        $categories = SchoolBookCategory::schoolId($school_id)->orWhere('school_id', null)->pluck('name', 'id');
        return view('pages.smart_school.library.create', compact('categories', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'kategori' => 'required|exists:school_book_categories,id',
            'nomor_buku' => 'required|string|max:50',
            'judul' => 'required|string|max:150',
            'pengarang' => 'required|string|max:150',
            'cover' => 'required|image|max:10000',
            'file' => 'required|mimes:pdf|max:100000',
        ]);

        $file = null;
        if($file = $request->file("file")){
            $file = $request->file("file")->store("SmartSchool/".$school_id."/library");
        }
        $cover = null;
        if($cover = $request->file("cover")){
            $cover = $request->file("cover")->store("SmartSchool/".$school_id."/library/cover");
        }

        SchoolLibrary::create([
            "school_id" => $school_id,
            "category_id" => $request->kategori,
            "book_number" => $request->nomor_buku,
            "title" => $request->judul,
            "author" => $request->pengarang,
            "cover" => $cover,
            "file" => $file
        ]);

        return redirect()->route('school-libraries.index')->with('success', 'Berhasil Menambah Buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolLibrary $SchoolLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolLibrary $SchoolLibrary)
    {
        isMySchool($SchoolLibrary->school_id);
        $title = 'Ubah Buku';
        $sub_title = 'Home / Ubah Buku';
        $data = $SchoolLibrary;
        $categories = SchoolBookCategory::schoolId($SchoolLibrary->school_id)->orWhere('school_id', null)->pluck('name', 'id');
        return view('pages.smart_school.library.edit', compact('data', 'categories', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolLibrary $SchoolLibrary)
    {
        isMySchool($SchoolLibrary->school_id);
        $validatedData = $request->validate([
            'nomor_buku' => 'required|string|max:50',
            'kategori' => 'required|exists:school_book_categories,id',
            'judul' => 'required|string|max:150',
            'pengarang' => 'required|string|max:150',
            'cover' => 'image|max:10000',
            'file' => 'mimes:pdf|max:100000',
        ]);

        $file = $SchoolLibrary->getOriginal('file');
        if($request->file("file")){
            if (Storage::exists($file)) {
                Storage::delete($file);
            }
            $file = $request->file("file")->store("SmartSchool/".$SchoolLibrary->school_id."/library");
        }
        $cover = $SchoolLibrary->getOriginal('cover');
        if($request->file("cover")){
            if (Storage::exists($cover)) {
                Storage::delete($cover);
            }
            $cover = $request->file("cover")->store("SmartSchool/".$SchoolLibrary->school_id."/library/cover");
        }

        SchoolLibrary::where('id', $SchoolLibrary->id)->update([
            "category_id" => $request->kategori,
            "book_number" => $request->nomor_buku,
            "title" => $request->judul,
            "author" => $request->pengarang,
            "cover" => $cover
        ]);

        return redirect()->route('school-libraries.index')->with('success', 'Berhasil Mengubah Buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolLibrary $SchoolLibrary)
    {
        $file = $SchoolLibrary->getOriginal('file');
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $cover = $SchoolLibrary->getOriginal('cover');
        if (Storage::exists($cover)) {
            Storage::delete($cover);
        }
        $SchoolLibrary->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Buku');
    }
}
