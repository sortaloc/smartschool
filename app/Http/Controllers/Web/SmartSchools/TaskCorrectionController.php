<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskResult;
use App\Models\SmartSchools\SchoolTaskAnswer;
use App\Models\SmartSchools\SchoolTaskClass;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;

class TaskCorrectionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Soal'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Soal'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Soal'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Soal'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $task = SchoolTask::findOrFail($request->task_id);
        $data_temp = SchoolTaskResult::where('school_task_id', $task->id);
        if($request->class_id){
            $data_temp->whereHas('task_class', function($q) use($request){
                $q->whereHas('teacher_subjects', function($r) use($request){
                    $r->where('school_class_id', $request->class_id);
                    if($request->subjects_id){
                        $r->where('school_subjects_id', $request->subjects_id);
                    }
                });
            });
        }
        $data = $data_temp->get();
        isMySchool($task->school_id);
        $title = 'Koreksi Soal';
        $sub_title = 'Home / Koreksi Soal';
        $classes = SchoolClass::schoolId($task->school_id)->get();
        $subjects = SchoolSubjects::schoolId($task->school_id)->pluck('name', 'id');
        return view('pages.smart_school.task.koreksi.index', compact('data', 'task', 'title', 'sub_title', 'classes', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Koreksi Jawaban';
        $sub_title = 'Home / Koreksi Jawaban';
        $result = SchoolTaskResult::findOrFail($request->result_id);
        $datas = SchoolTaskAnswer::whereHas('question', function($q) use($result){
            $q->where('school_task_id', $result->school_task_id);
        })->where('student_id', $result->school_student_id)->get()->sortBy('question.number');
        $task = $result->task;
        $previous_result = SchoolTaskResult::where('school_task_id', $result->school_task_id)->where('task_class_id', $result->task_class_id)
                        ->where('id', '<', $result->id)->first();
        $next_result = SchoolTaskResult::where('school_task_id', $result->school_task_id)->where('task_class_id', $result->task_class_id)
                        ->where('id', '>', $result->id)->first();
        return view('pages.smart_school.task.koreksi.create', compact('title', 'task', 'datas', 'sub_title', 'next_result', 'previous_result'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTask $SchoolTaskCorrection)
    {
        $title = 'Hasil Tugas Siswa';
        $sub_title = 'Home / Hasil Tugas Siswa';
        $data = $SchoolTaskCorrection;
        return view('pages.smart_school.task.hasil', compact('title', 'data', 'sub_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTaskAnswer $SchoolTaskCorrection)
    {
        $title = 'Koreksi Soal';
        $sub_title = 'Home / Koreksi Soal';
        $data = $SchoolTaskCorrection;
        if($data->question->task->question_type == 'Essay'){
            return view('pages.smart_school.task.koreksi.essay', compact('data', 'title', 'sub_title'));
        }
        return view('pages.smart_school.task.koreksi.file', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolTaskAnswer $SchoolTaskCorrection)
    {
        $point = $SchoolTaskCorrection->point;
        $result = SchoolTaskResult::where('school_task_id', $SchoolTaskCorrection->question->school_task_id)->where('school_student_id', $SchoolTaskCorrection->student_id)->firstOrFail();

        $max_point = 100 - ($result->point - $point);

        $validatedData = $request->validate([
            'answer_id' => 'required|exists:school_task_answers,id',
            'koreksi' => 'nullable|string|max:50000',
            'nilai' => 'required|numeric|max:'.$max_point,
        ]);

        if($point){
            $result->point = ($result->point - $point) + $request->nilai;
        }else{
            $result->point = $result->point + $request->nilai;
        }
        if($result->status != 'Selesai'){
            $result->status = 'Selesai';
        }
        $result->save();

        $SchoolTaskCorrection->point = $request->nilai;
        $SchoolTaskCorrection->correction = $request->koreksi;
        $SchoolTaskCorrection->save();

        $class_id = isset($result->task_class->teacher_subjects->school_class_id) ? $result->task_class->teacher_subjects->school_class_id : null;
        $subjects_id = isset($result->task_class->teacher_subjects->school_subjects_id) ? $result->task_class->teacher_subjects->school_subjects_id : null;

        return redirect()->route('school-task-corrections.create', ['result_id' => $result->id, 'class_id' => $class_id, 'subjects_id' => $subjects_id])->with('success', 'Berhasil MengKoreksi Soal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTaskAnswer $SchoolTaskCorrection)
    {
        abort(404);
    }
}
