<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolClassJournal;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use App\Models\SmartSchools\SchoolStudentClass;
use App\Models\SmartSchools\SchoolStudentPresence;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class ClassPresenceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Jurnal Kelas'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Jurnal Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Jurnal Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Jurnal Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, SchoolClassJournal $SchoolClassPresence)
    {
        $title = 'Data Presensi';
        $sub_title = 'Home / Data Presensi';
        $journal = $SchoolClassPresence;
        $status = $request->status;
        if(!$status){
            $presences = $SchoolClassPresence->student_presences;
        }elseif($status == 'H'){
            $presences = $SchoolClassPresence->student_presences->where('presence', $status);
        }elseif($status == 'T'){
            $presences = $SchoolClassPresence->student_presences->where('presence', $status);
        }elseif($status == 'I'){
            $presences = $SchoolClassPresence->student_presences->where('presence', $status);
        }elseif($status == 'S'){
            $presences = $SchoolClassPresence->student_presences->where('presence', $status);
        }elseif($status == 'A'){
            $presences = $SchoolClassPresence->student_presences->where('presence', $status);
        }else{
            abort(403);
        }

        return view('pages.smart_school.class_presence.show', compact('presences', 'journal', 'title', 'sub_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolStudentPresence $SchoolClassPresence)
    {
        $title = 'Ubah Data Presensi';
        $sub_title = 'Home / Ubah Data Presensi';
        $data = $SchoolClassPresence;
        return view('pages.smart_school.class_presence.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolStudentPresence $SchoolClassPresence)
    {
        isMySchool($SchoolClassPresence->student->school_id);
        $validatedData = $request->validate([
            'status' => 'bail|required|in:H,I,S,A,T',
        ]);

        $SchoolClassPresence->presence = $request->status;
        $SchoolClassPresence->save();

        return redirect()->route('school-class-presences.show', $SchoolClassPresence->school_class_journal_id)->with('success', 'Berhasil Mengubah Presensi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolStudentPresence $SchoolClassPresence)
    {

    }
}
