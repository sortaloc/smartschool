<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolPredicate;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;

class PredicateCharacterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Predikat'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Predikat'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Predikat'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Predikat'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolPredicate::where('school_id', $school_id)->whereHas('assessment_type_ref');
        if($request->ajax()){
            $datas= $data_temp->where('assessment_type_ref_id', $request->assessment_type_ref_id)->get();
            $data = [];
            $datas->each(function($q, $key) use(&$data){
                $data[$key]['id'] = $q->id;
                $data[$key]['text'] = '('.$q->lower_limit.' - '.$q->upper_limit.')'.' '.$q->description;
                $data[$key]['description'] = $q->description;
            });
            return response()->json(['results' => $data]);
        }
        $datas= $data_temp->get();
        $title = 'Predikat Karakter';
        $sub_title = 'Home / Predikat Karakter';
        return view('pages.smart_school.predicate.character.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Predikat Karakter Baru';
        $sub_title = 'Home / Predikat Karakter Baru';
        $assessments = SchoolAssessmentTypeRef::where('school_id', $school_id)->where('category', 'Sikap')->pluck('name', 'id');
         return view('pages.smart_school.predicate.character.create', compact('title', 'assessments', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'tipe_penilaian' => 'required',
            'batas_bawah' => 'required|numeric',
            'batas_atas' => 'required|numeric|min:'.($request->batas_bawah+0.01),
            'deskripsi' => 'required|string|max:150'
        ]);
        $data = SchoolPredicate::create([
            'school_id' => $school_id,
            'assessment_type_ref_id' => $request->tipe_penilaian,
            'lower_limit' => $request->batas_bawah,
            'upper_limit' => $request->batas_atas,
            'description' => $request->deskripsi
        ]);

        return redirect()->route('school-character-predicates.index')->with('success', 'Berhasil Menambah Predikat Karakter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolPredicate $SchoolCharacterPredicate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolPredicate $SchoolCharacterPredicate)
    {
        $title = 'Ubah Predikat Karakter';
        $sub_title = 'Home / Ubah Predikat Karakter';
        $data = $SchoolCharacterPredicate;
        return view('pages.smart_school.predicate.character.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolPredicate $SchoolCharacterPredicate)
    {
        isMySchool($SchoolCharacterPredicate->school_id);
        $validatedData = $request->validate([
            'batas_bawah' => 'required|numeric',
            'batas_atas' => 'required|numeric|min:'.($request->batas_bawah+0.01),
            'deskripsi' => 'required|string|max:150'
        ]);
        $data = SchoolPredicate::where('id', $SchoolCharacterPredicate->id)->update([
            'lower_limit' => $request->batas_bawah,
            'upper_limit' => $request->batas_atas,
            'description' => $request->deskripsi
        ]);

        return redirect()->route('school-character-predicates.index')->with('success', 'Berhasil Mengubah Predikat Karakter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolPredicate $SchoolCharacterPredicate)
    {
        $SchoolCharacterPredicate->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Predikat Karakter');
    }
}
