<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolGeneration;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class SchoolGenerationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Angkatan'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Angkatan'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Angkatan'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Angkatan'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolGeneration::where('school_id', $school_id)->orderBy('year')->get();
        $title = 'Tahun Angkatan';
        $sub_title = 'Home / Tahun Angkatan';
        return view('pages.smart_school.school_generation.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tahun Angkatan Baru';
        $sub_title = 'Home / Tahun Angkatan Baru';
        return view('pages.smart_school.school_generation.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'tahun' => 'bail|required|digits:4|integer|min:2000'
        ]);
        $school_id = mySchool();
        $data = SchoolGeneration::create([
            'school_id' => $school_id,
            'year' => $request->tahun
        ]);

        return redirect()->route('school-generations.index')->with('success', 'Berhasil Menambah Tahun Angkatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolGeneration $SchoolGeneration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolGeneration $SchoolGeneration)
    {
        abort(404);
        $title = 'Ubah Tahun Angkatan';
        $sub_title = 'Home / Ubah Tahun Angkatan';
        $data = $SchoolGeneration;
        return view('pages.smart_school.school_generation.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolGeneration $SchoolGeneration)
    {
        abort(404);
        isMySchool($SchoolGeneration->school_id);
        $validatedData = $request->validate([
            'tahun' => 'bail|required|digits:4|integer|min:2000'
        ]);
        $SchoolGeneration->year = $request->tahun;
        $SchoolGeneration->save();

        return redirect()->route('school-generations.index')->with('success', 'Berhasil Mengubah Tahun Angkatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolGeneration $SchoolGeneration)
    {
        if($SchoolGeneration->students->first()){
            return redirect()->back()->with('danger', 'Tahun angkatan tidak dapat dihapus karena dipakai di data siswa !');
        }
        $SchoolGeneration->delete();
        return redirect()->back()->with('success', 'Tahun angkatan berhasil dihapus !');
    }
}
