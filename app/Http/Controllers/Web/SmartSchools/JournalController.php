<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClassJournal;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use App\Models\SmartSchools\SchoolStudentClass;
use App\Models\SmartSchools\SchoolStudentPresence;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use App\Exports\SmartSchoolJournalExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use DataTables;

class JournalController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Lihat Jurnal Kelas'])->only('show');
        $this->middleware(['permission:Semua Hak Akses|Lihat Jurnal Kelas'])->only('index');
        $this->middleware(['permission:Semua Hak Akses|Tambah Jurnal Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Jurnal Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Jurnal Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolClassJournal::with(
            'class_schedule.teacher_subjects.class.class',
            'class_schedule.teacher_subjects.teacher',
            'class_schedule.teacher_subjects.subjects'
            )->role()->thisYear()->orderBy('date', 'desc')->orderBy('start_at', 'asc');
        
        if($request->ajax()){
            if($request->class_id){
                $data_temp->whereHas('class_schedule.teacher_subjects', function($q) use($request){
                    $q->where('school_class_id', $request->class_id);
                });
            }
            $periode = 'Semua Periode';
            if($request->date == 'today'){
                $data_temp->whereDate('date', now());
                $periode = date('d-m-Y', time());
            }elseif($request->year){
                $data_temp->whereYear('date', $request->year);
                $periode = $request->year;
                if($request->month){
                    $data_temp->whereMonth('date', $request->month);
                    $periode = $request->month.'-'.$request->year;
                }
            }
            
            return Datatables::eloquent($data_temp)
            ->addIndexColumn()
            ->addColumn('kelas', function ($dt) {
                return $dt->class_schedule->teacher_subjects->class->class_name;
            })
            ->addColumn('hari', function ($dt) {
                return $dt->date->isoFormat('dddd, DD MMMM Y');
            })
            ->addColumn('waktu', function ($dt) {
                return $dt->start_at.' - '.$dt->end_at;
            })
            ->addColumn('mapel', function ($dt) {
                return $dt->class_schedule->teacher_subjects->subjects->name;
            })
            ->addColumn('pengampu', function ($dt) {
                return $dt->class_schedule->teacher_subjects->teacher->name;
            })
            ->addColumn('presensi', function ($dt) {
                return '<a href="'.route('school-class-presences.show', $dt->id).'">'.$dt->student_presence_description.'</a>';
            })
            ->addColumn('action', function ($dt) {
                return view('pages.smart_school.journal.action', compact('dt'))->render();
            })
            ->rawColumns(['kelas', 'hari', 'waktu', 'mapel', 'pengampu', 'presensi', 'action'])
            ->make(true);
        }
        $datas = $data_temp->get();
        $title = 'Jurnal Kelas';
        $sub_title = 'Home / Jurnal Kelas';
        $classes = SchoolClass::where('school_id', $school_id)->get();
        if($request->download){
            $school = School::findOrFail($school_id);
            $school_year = $school->school_year()->firstOrFail();
            $filter = [
                'class' => $request->class_id ? SchoolClass::findOrFail($request->class_id)->class_name : 'Semua Kelas',
                'periode' => $periode,
                'school_year' => 'Semester '.$school_year->semester.' '.$school_year->start_year.' - '.$school_year->end_year
            ];
            return Excel::download(new SmartSchoolJournalExport($school, $datas, $filter), 'jurnal kelas smartschool genius '.Carbon::now()->isoFormat('D-M-Y').'.xlsx');
        }
        return view('pages.smart_school.journal.index', compact('datas', 'classes', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Jurnal Kelas Baru';
        $sub_title = 'Home / Jurnal Kelas Baru';
        $classes = SchoolClass::with('homeroom:id,name')->where('school_id', $school_id)->get();
        return view('pages.smart_school.journal.create', compact('classes', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kelas' => 'bail|required|exists:school_classes,id',
            'mata_pelajaran' => 'bail|required|exists:school_class_schedules,id',
            'hari' => 'bail|required|date_format:Y-m-d',
            'waktu_mulai' => 'bail|required|date_format:H:i',
            'waktu_selesai' => 'bail|required|date_format:H:i|after:waktu_mulai',
            'materi' => 'bail|required|string|max:150'
        ]);

        $school_id = mySchool();

        $val = SchoolClassJournal::whereHas('class_schedule.school_year', function($q) use($school_id){
            $q->where('school_id', $school_id);
        })->whereHas('class_schedule.teacher_subjects', function($q) use($request){
            $q->where('school_class_id', $request->kelas);
        })->whereDate('date', $request->hari)
            ->where(function ($q) use($request) {
                $q->where(function ($q1) use($request) {
                    $q1->where('start_at', '<', $request->waktu_mulai)->where('end_at', '>', $request->waktu_mulai);
                })->orWhere(function ($q2) use($request) {
                    $q2->where('start_at', '<', $request->waktu_selesai)->where('end_at', '>', $request->waktu_selesai);
                })->orWhere(function ($q3) use($request) {
                    $q3->where('start_at', '>=', $request->waktu_mulai)->where('start_at', '<=', $request->waktu_selesai);
                });
            })->first();
        if($val){
            return redirect()->back()->withErrors(['Waktu Jurnal Bertabrakan Dengan Mata Pelajaran Lain'])->withInput();
        }

        $year = mySchoolYear();

        $data = SchoolClassJournal::create([
            'school_class_schedule_id' => $request->mata_pelajaran,
            'date' => $request->hari,
            'materi' => $request->materi,
            'start_at' => $request->waktu_mulai,
            'end_at' => $request->waktu_selesai
        ]);

        $students = SchoolStudentClass::where('class_id', $request->kelas)->where('is_active', 1)->get();

        foreach ($students as $key => $value) {
            $presences = SchoolStudentPresence::create([
                'school_class_journal_id' => $data->id,
                'student_id' => $value->school_student_id
            ]);
        }

        return redirect()->route('school-journals.index')->with('success', 'Berhasil Menambah Jurnal Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClassJournal $SchoolJournal)
    {
        $title = 'Sesi';
        $sub_title = 'Home / Sesi';
        $data = $SchoolJournal;
        $teacher = auth()->user()->school_teacher;
        if(!$teacher){
            abort(403);
        }
        return view('pages.smart_school.journal.show', compact('data', 'teacher', 'title', 'sub_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClassJournal $SchoolJournal)
    {
        $title = 'Ubah Jurnal Kelas';
        $sub_title = 'Home / Ubah Jurnal Kelas';
        $data = $SchoolJournal;
        $classes = SchoolClass::with('homeroom:id,name')->where('school_id', $SchoolJournal->class_schedule->school_year->school_id)->get();
        return view('pages.smart_school.journal.edit', compact('data', 'classes', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClassJournal $SchoolJournal)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'hari' => 'bail|required|date_format:Y-m-d',
            'waktu_mulai' => 'bail|required|date_format:H:i',
            'waktu_selesai' => 'bail|required|date_format:H:i|after:waktu_mulai',
            'materi' => 'bail|required|string|max:150',
            'catatan' => 'bail|string|max:150'
        ]);

        $val = SchoolClassJournal::where('id', '!=', $SchoolJournal->id)->whereHas('class_schedule.school_year', function($q) use($school_id){
            $q->where('school_id', $school_id);
        })->whereHas('class_schedule.teacher_subjects', function($q) use($request){
            $q->where('school_class_id', $request->kelas);
        })->whereDate('date', $request->hari)
            ->where(function ($q) use($request) {
                $q->where(function ($q1) use($request) {
                    $q1->where('start_at', '<', $request->waktu_mulai)->where('end_at', '>', $request->waktu_mulai);
                })->orWhere(function ($q2) use($request) {
                    $q2->where('start_at', '<', $request->waktu_selesai)->where('end_at', '>', $request->waktu_selesai);
                })->orWhere(function ($q3) use($request) {
                    $q3->where('start_at', '>=', $request->waktu_mulai)->where('start_at', '<=', $request->waktu_selesai);
                });
            })->first();
        if($val){
            return redirect()->back()->withErrors(['Waktu Jurnal Bertabrakan Dengan Mata Pelajaran Lain'])->withInput();
        }

        $year = mySchoolYear();

        $data = SchoolClassJournal::where('id', $SchoolJournal->id)->update([
            'date' => $request->hari,
            'materi' => $request->materi,
            'note' => $request->catatan,
            'start_at' => $request->waktu_mulai,
            'end_at' => $request->waktu_selesai
        ]);

        return redirect()->route('school-journals.index')->with('success', 'Berhasil Mengubah Jurnal Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClassJournal $SchoolJournal)
    {
        $check = $SchoolJournal->student_presences->where('presence', 'H')->first();
        if($check){
            return redirect()->back()->with('danger', 'Jurnal yang yang sudah di isi siswa tidak dapat dihapus !');
        }
        $SchoolJournal->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus data jurnal kelas');
    }
}
