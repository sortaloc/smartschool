<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolClassRef;
use App\Models\SmartSchools\SchoolSubclassRef;
use App\Models\SmartSchools\SchoolYear;
use App\Models\SmartSchools\SchoolTeacher;
use Illuminate\Validation\Rule;

class SchoolClassController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Kelas'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolClass::where('school_id', $school_id)->orderBy('class_id', 'ASC')->get();
        $title = 'Data Kelas';
        $sub_title = 'Home / Data Kelas';
        return view('pages.smart_school.school_class.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Kelas Baru';
        $sub_title = 'Home / Kelas Baru';
        $school_id = mySchool();
        $classes = SchoolClassRef::where('school_id', $school_id)->get();
        $sub_classes = SchoolSubclassRef::where('school_id', $school_id)->get();
        $teachers = SchoolTeacher::where('school_id', $school_id)->get();
        return view('pages.smart_school.school_class.create', compact('classes', 'sub_classes', 'teachers', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'wali_kelas' => 'bail|exists:school_teachers,id',
            'kelas_referensi' => ['required', 'exists:school_class_refs,id', Rule::unique('school_classes', 'class_id')->where(function ($query) use($school_id, $request) {
                return $query->where('school_id', $school_id)
                ->where('class_id', $request->kelas_referensi)
                ->where('subclass_id', $request->sub_kelas_referensi);
            })],
            'sub_kelas_referensi' => 'bail|required|exists:school_subclass_refs,id',
        ]);
        $school_id = mySchool();
        SchoolClass::create([
            'school_id' => $school_id,
            'school_year_id' => $request->tahun_ajaran,
            'homeroom_teacher_id' => $request->wali_kelas,
            'class_id' => $request->kelas_referensi,
            'subclass_id' => $request->sub_kelas_referensi,
        ]);

        if($teacher_id = $request->wali_kelas){
            $teacher = SchoolTeacher::findOrFail($teacher_id);
            $user = $teacher->user;
            $user->givePermissionTo(['Tambah Rapor', 'Ubah Rapor', 'Hapus Rapor']);
        }

        return redirect()->route('school-classes.index')->with('success', 'Berhasil Menambah Data Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClass $SchoolClass)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClass $SchoolClass)
    {
        isMySchool($SchoolClass->school_id);
        $title = 'Ubah Kelas';
        $sub_title = 'Home / Ubah Kelas';
        $data = $SchoolClass;
        $classes = SchoolClassRef::where('school_id', $SchoolClass->school_id)->get();
        $sub_classes = SchoolSubclassRef::where('school_id', $SchoolClass->school_id)->get();
        $teachers = SchoolTeacher::where('school_id', $SchoolClass->school_id)->get();
        return view('pages.smart_school.school_class.edit', compact('data', 'classes', 'sub_classes', 'teachers', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClass $SchoolClass)
    {
        $validatedData = $request->validate([
            'wali_kelas' => 'bail|exists:school_teachers,id',
        ]);
        isMySchool($SchoolClass->school_id);
        if($SchoolClass->homeroom_teacher_id !== $request->wali_kelas){
            if($teacher_id = $request->wali_kelas){
                $teacher = SchoolTeacher::findOrFail($teacher_id);
                $user = $teacher->user;
                $user->givePermissionTo(['Tambah Rapor', 'Ubah Rapor', 'Hapus Rapor']);
            }
        }
        $SchoolClass->homeroom_teacher_id = $request->wali_kelas;
        $SchoolClass->save();
        return redirect()->route('school-classes.index')->with('success', 'Berhasil Mengubah Data Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClass $SchoolClass)
    {
        isMySchool($SchoolClass->school_id);
        $SchoolClass->delete();

        return redirect()->back()->with('danger', 'Berhasil Menghapus Data Kelas');
    }
}
