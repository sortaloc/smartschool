<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;

class AssessmentTypeRefController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Jenis Penilaian'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Jenis Penilaian'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Jenis Penilaian'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Jenis Penilaian'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolAssessmentTypeRef::where('school_id', $school_id)->orderBy('category')->get();
        $title = 'Referensi Jenis Penilaian';
        $sub_title = 'Home / Referensi Jenis Penilaian';
        return view('pages.smart_school.assessment_type_ref.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Referensi Jenis Penilaian Baru';
        $sub_title = 'Home / Referensi Jenis Penilaian Baru';
        return view('pages.smart_school.assessment_type_ref.create', compact('title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'kategori_penilaian' => 'required|in:Pengetahuan,Keterampilan,Sikap',
            'jenis_penilaian' => ['required',Rule::unique('school_assessment_type_refs', 'name')->where(function ($query) use($school_id, $request) {
                return $query->where('school_id', $school_id)
                ->where('name', $request->jenis_penilaian)
                ->where('category', $request->kategori_penilaian);
            })]
        ]);
        $data = SchoolAssessmentTypeRef::create([
            'school_id' => $school_id,
            'category' => $request->kategori_penilaian,
            'name' => $request->jenis_penilaian,
        ]);

        return redirect()->route('school-assessment-type-refs.index')->with('success', 'Berhasil Menambah Referensi Jenis Penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolAssessmentTypeRef $SchoolAssessmentTypeRef)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolAssessmentTypeRef $SchoolAssessmentTypeRef)
    {
        $title = 'Ubah Referensi Jenis Penilaian';
        $sub_title = 'Home / Ubah Referensi Jenis Penilaian';
        $data = $SchoolAssessmentTypeRef;
        return view('pages.smart_school.assessment_type_ref.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolAssessmentTypeRef $SchoolAssessmentTypeRef)
    {
        isMySchool($SchoolAssessmentTypeRef->school_id);
        $validatedData = $request->validate([
            'jenis_penilaian' => ['required',Rule::unique('school_assessment_type_refs', 'name')->where(function ($query) use($SchoolAssessmentTypeRef, $request) {
                return $query->where('school_id', $SchoolAssessmentTypeRef->school_id)
                ->where('name', $request->jenis_penilaian)
                ->where('category', $SchoolAssessmentTypeRef->category)
                ->where('id', '!=', $SchoolAssessmentTypeRef->id);
            })]
        ]);
        $data = SchoolAssessmentTypeRef::where('id', $SchoolAssessmentTypeRef->id)->update([
            'name' => $request->jenis_penilaian,
        ]);

        return redirect()->route('school-assessment-type-refs.index')->with('success', 'Berhasil Mengubah Referensi Jenis Penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolAssessmentTypeRef $SchoolAssessmentTypeRef)
    {
        $check = SchoolTask::whereHas('assessment_type', function($q) use($SchoolAssessmentTypeRef){
            $q->where('assessment_type_ref_id', $SchoolAssessmentTypeRef->id);
        })->first();
        if($check){
            return redirect()->back()->with('danger', 'Referensi jenis penilaian yang berisi tugas / ulangan / ujian tidak dapat dihapus !');
        }

        $SchoolAssessmentTypeRef->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Referensi jenis Penilaian');
    }
}
