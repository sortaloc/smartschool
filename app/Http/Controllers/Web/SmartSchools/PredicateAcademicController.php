<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolPredicate;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;

class PredicateAcademicController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Predikat'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Predikat'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Predikat'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Predikat'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolPredicate::where('school_id', $school_id)->where('assessment_type_ref_id', null)->get();
        $title = 'Predikat Akademik';
        $sub_title = 'Home / Predikat Akademik';
        return view('pages.smart_school.predicate.academic.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Predikat Akademik Baru';
        $sub_title = 'Home / Predikat Akademik Baru';
        return view('pages.smart_school.predicate.academic.create', compact('title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'batas_bawah' => 'required|numeric',
            'batas_atas' => 'required|numeric|min:'.($request->batas_bawah+0.01),
            'deskripsi' => 'required|string|max:150'
        ]);
        $data = SchoolPredicate::create([
            'school_id' => $school_id,
            'lower_limit' => $request->batas_bawah,
            'upper_limit' => $request->batas_atas,
            'description' => $request->deskripsi
        ]);

        return redirect()->route('school-academic-predicates.index')->with('success', 'Berhasil Menambah Predikat Akademik');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolPredicate $SchoolAcademicPredicate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolPredicate $SchoolAcademicPredicate)
    {
        $title = 'Ubah Predikat Akademik';
        $sub_title = 'Home / Ubah Predikat Akademik';
        $data = $SchoolAcademicPredicate;
        return view('pages.smart_school.predicate.academic.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolPredicate $SchoolAcademicPredicate)
    {
        isMySchool($SchoolAcademicPredicate->school_id);
        $validatedData = $request->validate([
            'batas_bawah' => 'required|numeric',
            'batas_atas' => 'required|numeric|min:'.($request->batas_bawah+0.01),
            'deskripsi' => 'required|string|max:150'
        ]);
        $data = SchoolPredicate::where('id', $SchoolAcademicPredicate->id)->update([
            'lower_limit' => $request->batas_bawah,
            'upper_limit' => $request->batas_atas,
            'description' => $request->deskripsi
        ]);

        return redirect()->route('school-academic-predicates.index')->with('success', 'Berhasil Mengubah Predikat Akademik');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolPredicate $SchoolAcademicPredicate)
    {
        $SchoolAcademicPredicate->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Predikat Akademik');
    }
}
