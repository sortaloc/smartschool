<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskClass;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Soal'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Soal'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Soal'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Soal'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolTask::role()->where('school_id', $school_id)->latest();
        if($request->class_id){
            $data_temp->whereHas('task_classes', function($q) use($request){
                $q->whereHas('teacher_subjects', function($r) use($request){
                    $r->where('school_class_id', $request->class_id);
                    if($request->subjects_id){
                        $r->where('school_subjects_id', $request->subjects_id);
                    }
                });
            });
        }
        if($request->status == 'upcoming'){
            $data_temp->whereHas('task_classes', function($q){
                $q->whereDate('end_at', '>=', now());
            });
        }
        $datas = $data_temp->get();
        $title = 'Soal';
        $sub_title = 'Home / Soal';
        $classes = SchoolClass::schoolId($school_id)->get();
        $subjects = SchoolSubjects::schoolId($school_id)->pluck('name', 'id');
        return view('pages.smart_school.task.index', compact('datas', 'title', 'sub_title', 'classes', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Soal Baru';
        $sub_title = 'Home / Soal Baru';
        $subjects = SchoolTeacherSubjects::role()->school($school_id)->get();
        $assessments = SchoolAssessmentTypeRef::school($school_id)->get();
        $tasks = SchoolTask::schoolId($school_id)->get();
        return view('pages.smart_school.task.create', compact('title', 'tasks', 'subjects', 'assessments', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_soal' => 'required|string|max:150',
            'deskripsi' => 'required|string|max:150',
            'kategori_soal' => 'required|in:Utama,Susulan,Perbaikan',
            'jenis_soal' => 'required|in:Pilihan Ganda,Essay,File',
            'jenis_penilaian' => 'required|exists:school_assessment_type_refs,id',
            'kelas.*' => 'required|exists:school_teacher_subjects,id',
            'waktu_mulai.*' => "required|date_format:Y-m-d\TH:i|after:2000-01-01T00:00|before:waktu_selesai.*",
            'waktu_selesai.*' => "required|date_format:Y-m-d\TH:i|before:2030-01-01T00:00|after:waktu_mulai.*"
        ]);

        if($request->kategori_soal == 'Susulan' || $request->kategori_soal == 'Perbaikan'){
            $validatedData = $request->validate([
                'tugas_lama' => 'required|exists:school_tasks,id'
            ]);
        }
        if($request->kategori_soal == 'Perbaikan'){
            $validatedData = $request->validate([
                'tugas_lama' => 'required|exists:school_tasks,id',
                'preferensi_nilai' => 'required|in:Terbaik,Perbaikan,Maksimal Nilai',
                'maksimal_nilai' => 'nullable|numeric|max:100'
            ]);
        }

        $school_id = mySchool();
        $school_year = mySchoolYear();
        $tugas = SchoolTask::create([
            "school_id" => $school_id,
            "user_id" => auth()->user()->id,
            "school_year_id" => $school_year->id,
            "assessment_type_ref_id" => $request->jenis_penilaian,
            "task_type" => $request->kategori_soal,
            "question_type" => $request->jenis_soal,
            "old_task_id" => $request->jenis_soal != 'Utama' ? $request->tugas_lama : null,
            'point_preference' => $request->preferensi_nilai ? $request->preferensi_nilai : null,
            'max_point' => $request->maksimal_nilai,
            "name" => $request->nama_soal,
            "description" => $request->deskripsi
        ]);

        foreach ($request->kelas as $key => $value) {
            SchoolTaskClass::create([
                "school_task_id" => $tugas->id,
                "teacher_subjects_id" => $value,
                "start_at" => $request->waktu_mulai[$key],
                "end_at" => $request->waktu_selesai[$key]
            ]);
        }
        return redirect()->route('school-task-questions.create', ['task_id' => $tugas->id])->with('success', 'Berhasil Menambah Soal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTask $SchoolTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTask $SchoolTask)
    {
        isMySchool($SchoolTask->school_id);
        $title = 'Ubah Soal';
        $sub_title = 'Home / Ubah Soal';
        $data = $SchoolTask;
        $subjects = SchoolTeacherSubjects::role()->school($SchoolTask->school_id)->get();
        $assessments = SchoolAssessmentTypeRef::school($SchoolTask->school_id)->get();
        return view('pages.smart_school.task.edit', compact('data', 'subjects', 'assessments', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolTask $SchoolTask)
    {
        isMySchool($SchoolTask->school_id);

        if($request->type == 'change_status'){
            $validatedData = $request->validate([
                'is_visible' => 'required|in:0,1'
            ]);
            $SchoolTask->is_visible = $request->is_visible;
            $SchoolTask->save();
            return redirect()->back()->with('success', 'Berhasil Mengubah Status Soal');
        }

        $validatedData = $request->validate([
            'nama_soal' => 'required|string|max:150',
            'deskripsi' => 'required|string|max:150',
            'jenis_penilaian' => 'required|exists:school_assessment_type_refs,id',
            'kelas.*' => 'required|exists:school_teacher_subjects,id',
            'waktu_mulai.*' => "required|date_format:Y-m-d\TH:i|after:2000-01-01T00:00|before:waktu_selesai.*",
            'waktu_selesai.*' => "required|date_format:Y-m-d\TH:i|before:2030-01-01T00:00|after:waktu_mulai.*"
        ]);

        $school_id = $SchoolTask->school_id;
        $school_year = mySchoolYear();

        $tugas = SchoolTask::where('id', $SchoolTask->id)->update([
            "assessment_type_ref_id" => $request->jenis_penilaian,
            "name" => $request->nama_soal,
            "description" => $request->deskripsi
        ]);

        $del = SchoolTaskClass::where('school_task_id', $SchoolTask->id)->whereNotIn('teacher_subjects_id', $request->kelas)->delete();

        foreach ($request->kelas as $key => $value) {
            SchoolTaskClass::UpdateOrCreate([
                "school_task_id" => $SchoolTask->id,
                "teacher_subjects_id" => $value],[
                "start_at" => $request->waktu_mulai[$key],
                "end_at" => $request->waktu_selesai[$key]
            ]);
        }

        return redirect()->route('school-tasks.index')->with('success', 'Berhasil Mengubah Soal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTask $SchoolTask)
    {
        $SchoolTask->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Soal');
    }
}
