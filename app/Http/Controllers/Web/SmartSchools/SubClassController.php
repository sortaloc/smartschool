<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolSubclassRef;

class SubClassController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Kelas'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Kelas'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Kelas'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Kelas'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolSubclassRef::where('school_id', $school_id)->orderBy('name', 'ASC')->get();
        $title = 'Data Referensi Sub Kelas';
        $sub_title = 'Home / Data Referensi Sub Kelas';
        return view('pages.smart_school.class.sub_class.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Referensi Sub Kelas Baru';
        $sub_title = 'Home / Referensi Sub Kelas Baru';
        return view('pages.smart_school.class.sub_class.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_sub_kelas' => 'bail|required|string|max:50'
        ]);
        $school_id = mySchool();
        SchoolSubclassRef::create([
            'name' => $request->nama_sub_kelas,
            'school_id' => $school_id
        ]);

        return redirect()->route('sub-classes.index')->with('success', 'Berhasil Menambah Data Referensi Sub Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolSubclassRef $class)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolSubclassRef $subClass)
    {
        $title = 'Ubah Referensi Sub Kelas';
        $sub_title = 'Home / Ubah Referensi Sub Kelas';
        $data = $subClass;
        return view('pages.smart_school.class.sub_class.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolSubclassRef $subClass)
    {
        $validatedData = $request->validate([
            'nama_sub_kelas' => 'bail|required|string|max:50'
        ]);
        isMySchool($subClass->school_id);
        $subClass->name = $request->nama_sub_kelas;
        $subClass->save();
        return redirect()->route('sub-classes.index')->with('success', 'Berhasil Mengubah Data Referensi Sub Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolSubclassRef $subClass)
    {
        isMySchool($subClass->school_id);

        if($subClass->classes()->first()){
            return redirect()->back()->with('danger', 'Data Referensi Sub Kelas Yang Dipakai Di Data Kelas Tidak Dapat Dihapus');
        }
        $subClass->delete();

        return redirect()->back()->with('success', 'Berhasil Menghapus Data Referensi Sub Kelas');
    }
}
