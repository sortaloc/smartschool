<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolWallMagazine;
use App\Models\SmartSchools\SchoolBookCategory;

class WallMagazineController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Mading'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Mading'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Mading'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Mading'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolWallMagazine::schoolId($school_id)->latest()->get();
        $title = 'Mading';
        $sub_title = 'Home / Mading';
        return view('pages.smart_school.wall_magazine.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Mading Baru';
        $sub_title = 'Home / Mading Baru';
        return view('pages.smart_school.wall_magazine.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'judul' => 'required|string|max:150',
            'logo' => 'required|image|max:10000',
            'file' => 'required|mimes:pdf|max:100000',
        ]);

        $file = null;
        if($file = $request->file("file")){
            $file = $request->file("file")->store("SmartSchool/".$school_id."/wall_magazine");
        }
        $logo = null;
        if($logo = $request->file("logo")){
            $logo = $request->file("logo")->store("SmartSchool/".$school_id."/wall_magazine/logo");
        }

        SchoolWallMagazine::create([
            'school_id' => $school_id,
            "user_id" => auth()->user()->id,
            "title" => $request->judul,
            "logo" => $logo,
            "file" => $file
        ]);

        return redirect()->route('school-wall-magazines.index')->with('success', 'Berhasil Menambah Mading');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolWallMagazine $SchoolWallMagazine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolWallMagazine $SchoolWallMagazine)
    {
        isMySchool($SchoolWallMagazine->school_id);
        $title = 'Ubah Mading';
        $sub_title = 'Home / Ubah Mading';
        $data = $SchoolWallMagazine;
        return view('pages.smart_school.wall_magazine.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolWallMagazine $SchoolWallMagazine)
    {
        isMySchool($SchoolWallMagazine->school_id);
        if($request->update_status != null){
            $SchoolWallMagazine->status = $request->update_status;
            $SchoolWallMagazine->save();
            return redirect()->route('school-wall-magazines.index')->with('success', 'Berhasil Mengubah Mading');
        }
        $validatedData = $request->validate([
            'judul' => 'required|string|max:150',
            'logo' => 'image|max:10000',
            'file' => 'mimes:pdf|max:100000',
        ]);

        $file = $SchoolWallMagazine->getOriginal('file');
        if($request->file("file")){
            if (Storage::exists($file)) {
                Storage::delete($file);
            }
            $file = $request->file("file")->store("SmartSchool/".$SchoolWallMagazine->school_id."/library");
        }
        $logo = $SchoolWallMagazine->getOriginal('logo');
        if($request->file("logo")){
            if (Storage::exists($logo)) {
                Storage::delete($logo);
            }
            $logo = $request->file("logo")->store("SmartSchool/".$SchoolWallMagazine->school_id."/library/logo");
        }

        SchoolWallMagazine::where('id', $SchoolWallMagazine->id)->update([
            "title" => $request->judul,
            "logo" => $logo,
            "file" => $file
        ]);

        return redirect()->route('school-wall-magazines.index')->with('success', 'Berhasil Mengubah Mading');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolWallMagazine $SchoolWallMagazine)
    {
        $file = $SchoolWallMagazine->getOriginal('file');
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $logo = $SchoolWallMagazine->getOriginal('logo');
        if (Storage::exists($logo)) {
            Storage::delete($logo);
        }
        $SchoolWallMagazine->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Mading');
    }
}
