<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SmartSchools\TeacherRequest;
use Illuminate\Support\Facades\Storage;

class TeacherProfileController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['permission:Admin Sekolah']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $title = 'Data Guru';
        $sub_title = 'Home / Data Guru';
        $data = auth()->user()->school_teacher;
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $subjects = SchoolSubjects::where('school_id', $school_id)->get();
        return view('pages.smart_school.teacher_profile.index', compact('data', 'classes', 'subjects', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTeacher $SchoolTeacher)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTeacher $SchoolTeacher)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, SchoolTeacher $SchoolTeacherProfile)
    {
        $school_id = mySchool();

        if($SchoolTeacherProfile->id != auth()->user()->school_teacher->id){
            abort(403);
        }

        $foto_guru = $SchoolTeacherProfile->getOriginal('photo');

        if($request->foto){
            if($foto_guru){
                if (Storage::exists($foto_guru)) {
                    Storage::delete($foto_guru);
                }
            }
            $foto_guru = $request->file("foto")->store("SmartSchool/".$school_id."/Teacher/Photo");
        }

        $ttd = $SchoolTeacherProfile->getOriginal('signature');

        if($request->file("tanda_tangan")){
            if (Storage::exists($ttd)) {
                Storage::delete($ttd);
            }
            $ttd = $request->file("tanda_tangan")->store("SmartSchool/".$school_id."/Teacher/Signature");
        }

        $teacher = SchoolTeacher::where('id', $SchoolTeacherProfile->id)->update([
            'name'                  => $request->nama_guru,
            'NIP'                   => $request->NIP_NIK,
            'NUPTK'                 => $request->NUPTK,
            'place_of_birth'        => $request->tempat_lahir,
            'date_of_birth'         => $request->tanggal_lahir,
            'gender'                => $request->jenis_kelamin,
            'religion'              => $request->agama,
            'structural_position'   => $request->jabatan_struktural,
            'additional_position'   => $request->tugas_tambahan,
            'PNS_rank'              => $request->golongan,
            'phone'                 => $request->no_hp,
            'photo'                 => $foto_guru,
            'signature'             => $ttd
        ]);

        /* if($request->subjects){
            foreach ($request->subjects as $key => $value) {
                SchoolTeacherSubjects::create([
                    'school_teacher_id' => $teacher->id,
                    'school_class_id' => $request->class[$key],
                    'school_subjects_id' => $value
                ]);
            }
        } */

        return redirect()->route('school-teacher-profiles.index')->with('success', 'Berhasil Mengubah Data Guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTeacher $SchoolTeacher)
    {
    }
}
