<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolClassRef;
use App\Models\SmartSchools\SchoolClassSubjects;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class ClassSubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat KKM'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah KKM'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah KKM'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus KKM'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolClassSubjects::where('school_id', $school_id)->orderBy('class_id', 'asc')->orderBy('subjects_id', 'asc')->get();
        $title = 'Mata Pelajaran Kelas';
        $sub_title = 'Home / Mata Pelajaran Kelas';
        return view('pages.smart_school.class_subjects.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Mata Pelajaran Kelas Baru';
        $sub_title = 'Home / Mata Pelajaran Kelas Baru';
        $classes = SchoolClassRef::schoolId($school_id)->pluck('name', 'id');
        return view('pages.smart_school.class_subjects.create', compact('classes', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'kelas' => 'bail|required',
            'kkm' => 'bail|required|numeric',
            'mata_pelajaran' => 'bail|required|exists:school_subjects,id'
        ]);
        $data = SchoolClassSubjects::create([
            'school_id' => $school_id,
            'subjects_id' => $request->mata_pelajaran,
            'class_id' => $request->kelas,
            'kkm' => $request->kkm,
        ]);
        return redirect()->route('school-class-subjects.index')->with('success', 'Berhasil Menambah Mata Pelajaran Kelas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClassSubjects $SchoolClassSubject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClassSubjects $SchoolClassSubject)
    {
        $title = 'Ubah Mata Pelajaran Kelas';
        $sub_title = 'Home / Ubah Mata Pelajaran Kelas';
        $data = $SchoolClassSubject;
        return view('pages.smart_school.class_subjects.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClassSubjects $SchoolClassSubject)
    {
        isMySchool($SchoolClassSubject->school_id);
        $school_id = mySchool();
        $validatedData = $request->validate([
            'kkm' => 'bail|required|numeric',
        ]);
        $SchoolClassSubject->kkm = $request->kkm;
        $SchoolClassSubject->save();

        return redirect()->route('school-class-subjects.index')->with('success', 'Berhasil Mengubah Mata Pelajaran Kelas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClassSubjects $SchoolClassSubject)
    {
        if($SchoolClassSubject->schedules->first() || $SchoolClassSubject->tasks->first()){
            return redirect()->back()->with('danger', 'Mata Pelajaran Kelas yang berisi tugas/soal dan jadwal Mata Pelajaran Kelas tidak dapat dihapus !');
        }
        $SchoolClassSubject->delete();
        return redirect()->back()->with('success', 'Mata Pelajaran Kelas berhasil dihapus !');
    }
}
