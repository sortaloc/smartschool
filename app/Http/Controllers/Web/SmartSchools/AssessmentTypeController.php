<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolAssessmentCategory;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use App\Models\SmartSchools\SchoolAssessmentType;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolClassRef;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;

class AssessmentTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Jenis Penilaian'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Jenis Penilaian'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Jenis Penilaian'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Jenis Penilaian'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolAssessmentType::whereHas('assessment_category', function($q) use($school_id, $request){
            $q->where('school_id', $school_id);
            if($request->class_id){
                $q->where(function ($query) use($request) {
                    $query->where('school_class_id', $request->class_id);
                });
            }
            if($request->subjects_id){
                $q->where(function ($query) use($request)  {
                    $query->where('school_subjects_id', $request->subjects_id);
                });
            }
        })->orderBy('assessment_category_id');
        $datas = $data_temp->get();
        $title = 'Jenis Penilaian';
        $sub_title = 'Home / Jenis Penilaian';
        $class_select = [0 => 'Semua Kelas'];
        $subjects_select = [0 => 'Semua Mata Pelajaran'];
        $classes = SchoolClassRef::where('school_id', $school_id)->pluck('name', 'id')->toArray();
        $classes = array_replace($class_select, $classes);
        $subjects = SchoolSubjects::where('school_id', $school_id)->pluck('name', 'id')->toArray();
        $subjects = array_replace($subjects_select, $subjects);
        return view('pages.smart_school.assessment_type.index', compact('datas', 'classes', 'subjects', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Jenis Penilaian Baru';
        $sub_title = 'Home / Jenis Penilaian Baru';
        $assessment_categories = SchoolAssessmentCategory::where('school_id', $school_id)->orderBy('school_subjects_id', 'asc')->orderBy('school_class_id', 'asc')->get();
        $assessment_types = SchoolAssessmentTypeRef::where('school_id', $school_id)->orderBy('name', 'asc')->get();
        return view('pages.smart_school.assessment_type.create', compact('assessment_categories', 'assessment_types', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'kategori_penilaian' => 'required|exists:school_assessment_categories,id',
            'bobot' => 'required|numeric',
            'jenis_penilaian' => ['required', 'exists:school_assessment_type_refs,id', Rule::unique('school_assessment_types', 'assessment_type_ref_id')->where(function ($query) use($school_id, $request) {
                return $query->where('assessment_category_id', $request->kategori_penilaian)
                ->where('assessment_type_ref_id', $request->jenis_penilaian);
            })]
        ]);
        $data = SchoolAssessmentType::create([
            'assessment_category_id' => $request->kategori_penilaian,
            'assessment_type_ref_id' => $request->jenis_penilaian,
            'weight' => $request->bobot,
        ]);

        return redirect()->route('school-assessment-types.index')->with('success', 'Berhasil Menambah Jenis Penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolAssessmentType $SchoolAssessmentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolAssessmentType $SchoolAssessmentType)
    {
        $title = 'Ubah Jenis Penilaian';
        $sub_title = 'Home / Ubah Jenis Penilaian';
        $data = $SchoolAssessmentType;
        return view('pages.smart_school.assessment_type.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolAssessmentType $SchoolAssessmentType)
    {
        isMySchool($SchoolAssessmentType->assessment_category->school_id);
        $validatedData = $request->validate([
            'bobot' => 'required|numeric'
        ]);
        $data = SchoolAssessmentType::where('id', $SchoolAssessmentType->id)->update([
            'weight' => $request->bobot,
        ]);


        return redirect()->route('school-assessment-types.index')->with('success', 'Berhasil Mengubah Jenis Penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolAssessmentType $SchoolAssessmentType)
    {
        $check = SchoolTask::whereHas('assessment_type', function($q) use($SchoolAssessmentType){
            $q->where('assessment_type_id', $SchoolAssessmentType->id);
        })->first();
        if($check){
            return redirect()->back()->with('danger', 'Jenis Penilaian yang berisi tugas / ulangan / ujian tidak dapat dihapus !');
        }

        $SchoolAssessmentType->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Jenis Penilaian');
    }
}
