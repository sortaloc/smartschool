<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SmartSchools\TeacherRequest;
use Illuminate\Support\Facades\Storage;

class TeacherSubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Info Guru'])->only('show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Info Guru'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Info Guru'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Info Guru'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $data_temp = SchoolTeacherSubjects::whereHas('teacher', function($q) use($school_id){
            $q->where('school_id', $school_id);
        });
        if($request->ajax()){
            if($request->q){
                $data_temp->whereHas('subjects', function($q) use($request){
                    $q->where('name', 'like', '%'.$request->q.'%');
                });
            }
            if($request->class_id){
                $data_temp->whereHas('class', function($q) use($request){
                    $q->where('school_class_id', $request->class_id);
                });
            }
            $datas = $data_temp->get();
            $data = [];
            $datas->each(function($q, $key) use(&$data){
                $data[$key]['id'] = $q->id;
                $data[$key]['text'] = $q->subjects->name.' - Kelas '.$q->class->class_name.' - '.$q->teacher->name;
                $data[$key]['teacher'] = $q->teacher->name;
            });
            return response()->json(['results' => $data]);
        }
        $datas = $data_temp->get();
        $title = 'Data Guru Mata Pelajaran';
        $sub_title = 'Home / Data Guru Mata Pelajaran';
        return view('pages.smart_school.teacher_subjects.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Tambah Guru Mata Pelajaran';
        $sub_title = 'Home / Tambah Guru Mata Pelajaran';
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $subjects = SchoolSubjects::where('school_id', $school_id)->pluck('name', 'id');
        $teachers = SchoolTeacher::where('school_id', $school_id)->pluck('name', 'id');
        return view('pages.smart_school.teacher_subjects.create', compact('classes', 'subjects', 'teachers', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();

        $validatedData = $request->validate([
            'mata_pelajaran' => 'bail|required|exists:school_subjects,id',
            'guru' => 'bail|required|exists:school_teachers,id',
            'kelas' => 'bail|required|exists:school_classes,id',
        ]);

        SchoolTeacherSubjects::create([
            'school_teacher_id' => $request->guru,
            'school_class_id' => $request->kelas,
            'school_subjects_id' => $request->mata_pelajaran
        ]);

        return redirect()->route('school-teacher-subjects.index')->with('success', 'Berhasil Menambah Data Guru Mata Pelajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTeacherSubjects $SchoolTeacherSubject)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTeacherSubjects $SchoolTeacherSubject)
    {
        /* $school_id = mySchool();
        $title = 'Ubah Data Guru Mata Pelajaran';
        $sub_title = 'Home / Ubah Data Guru Mata Pelajaran';
        $data = $SchoolTeacherSubject;
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $subjects = SchoolSubjects::where('school_id', $school_id)->pluck('name', 'id');
        $teachers = SchoolTeacher::where('school_id', $school_id)->pluck('name', 'id');
        return view('pages.smart_school.teacher_subjects.edit', compact('data', 'classes', 'subjects', 'teachers', 'title', 'sub_title')); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolTeacherSubjects $SchoolTeacherSubject)
    {
        /* $validatedData = $request->validate([
            'mata_pelajaran' => 'bail|required|exists:school_subjects,id',
            'guru' => 'bail|required|exists:school_teachers,id',
            'kelas' => 'bail|required|exists:school_classes,id',
        ]);

        SchoolTeacherSubjects::where('id', $SchoolTeacherSubject->id)->update([
            'school_teacher_id' => $request->guru,
            'school_class_id' => $request->kelas,
            'school_subjects_id' => $request->mata_pelajaran
        ]);

        return redirect()->route('school-teacher-subjects.index')->with('success', 'Berhasil Mengubah Data Guru Mata Pelajaran'); */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTeacherSubjects $SchoolTeacherSubject)
    {
        isMySchool($SchoolTeacherSubject->teacher->school_id);

        if($SchoolTeacherSubject->schedules->first() || $SchoolTeacherSubject->task_classes->first()){
            return redirect()->back()->with('danger', 'Mata pelajaran yang dipakai di jadwal dan tugas / soal tidak dapat dihapus');
        }

        $SchoolTeacherSubject->forceDelete();

        return redirect()->back()->with('success', 'Berhasil Menghapus Data Guru Mata Pelajaran');
    }
}
