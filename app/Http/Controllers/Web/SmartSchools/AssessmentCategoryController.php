<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolAssessmentCategory;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolClassRef;
use App\Models\SmartSchools\SchoolTask;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class AssessmentCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Kategori Penilaian'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Kategori Penilaian'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Kategori Penilaian'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Kategori Penilaian'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolAssessmentCategory::where('school_id', $school_id)->orderBy('school_class_id', 'asc')->orderBy('school_subjects_id', 'asc')->get();
        $title = 'Kategori Penilaian';
        $sub_title = 'Home / Kategori Penilaian';
        return view('pages.smart_school.assessment_category.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Kategori Penilaian Baru';
        $sub_title = 'Home / Kategori Penilaian Baru';
        $classes = SchoolClassRef::where('school_id', $school_id)->pluck('name', 'id');
        $subjects = SchoolSubjects::where('school_id', $school_id)->pluck('name', 'id');
        return view('pages.smart_school.assessment_category.create', compact('classes', 'subjects', 'title',  'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $val = SchoolAssessmentCategory::where('school_id', $school_id)->where('school_class_id', $request->kelas)->pluck('school_subjects_id')->toArray();
        $val = implode (", ", $val);
        $validatedData = $request->validate([
            'kelas' => 'bail|required|exists:school_class_refs,id',
            'mata_pelajaran' => 'bail|required|exists:school_subjects,id|not_in:'.$val,
            'bobot_pengetahuan' => 'bail|nullable|numeric',
            'bobot_keterampilan' => 'bail|nullable|numeric',
            'bobot_sikap' => 'bail|nullable|numeric'
        ]);
        $data = SchoolAssessmentCategory::create([
            'school_id' => $school_id,
            'school_class_id' => $request->kelas,
            'school_subjects_id' => $request->mata_pelajaran,
            'pengetahuan' => $request->bobot_pengetahuan ? $request->bobot_pengetahuan : 0,
            'keterampilan' => $request->bobot_keterampilan ? $request->bobot_keterampilan : 0,
            'sikap' => $request->bobot_sikap ? $request->bobot_sikap : 0
        ]);

        return redirect()->route('school-assessment-categories.index')->with('success', 'Berhasil Menambah Kategori Penilaian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolAssessmentCategory $SchoolAssessmentCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolAssessmentCategory $SchoolAssessmentCategory)
    {
        $title = 'Ubah Kategori Penilaian';
        $sub_title = 'Home / Ubah Kategori Penilaian';
        $data = $SchoolAssessmentCategory;
        return view('pages.smart_school.assessment_category.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolAssessmentCategory $SchoolAssessmentCategory)
    {
        isMySchool($SchoolAssessmentCategory->school_id);
        if($SchoolAssessmentCategory){
            $validatedData = $request->validate([
                'bobot_pengetahuan' => 'bail|nullable|numeric',
                'bobot_keterampilan' => 'bail|nullable|numeric',
                'bobot_sikap' => 'bail|nullable|numeric'
            ]);

            $data = SchoolAssessmentCategory::where('id', $SchoolAssessmentCategory->id)->update([
                'pengetahuan' => $request->bobot_pengetahuan ? $request->bobot_pengetahuan : 0,
                'keterampilan' => $request->bobot_keterampilan ? $request->bobot_keterampilan : 0,
                'sikap' => $request->bobot_sikap ? $request->bobot_sikap : 0
            ]);
        }else{
            return 2;
        }

        return redirect()->route('school-assessment-categories.index')->with('success', 'Berhasil Mengubah Kategori Penilaian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolAssessmentCategory $SchoolAssessmentCategory)
    {
        $scheck = SchoolTask::whereHas('assessment_type', function($q) use($SchoolAssessmentCategory){
            $q->where('assessment_category_id', $SchoolAssessmentCategory->id);
        })->first();
        if($scheck){
            return redirect()->back()->with('danger', 'Kategori penilaian yang berisi tugas / ulangan / ujian tidak dapat dihapus !');
        }

        $SchoolAssessmentCategory->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Kategori Penilaian');
    }
}
