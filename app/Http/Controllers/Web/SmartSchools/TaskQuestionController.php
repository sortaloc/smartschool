<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskQuestion;
use App\Models\SmartSchools\SchoolTaskOption;
use App\Imports\QuestionsImport;
use Maatwebsite\Excel\Facades\Excel;

class TaskQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Soal'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Soal'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Soal'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Soal'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = SchoolTask::findOrFail($request->task_id);
        $title = "Daftar Soal";
        $sub_title = "Home / Daftar Soal";

        if($data->question_type == 'Pilihan Ganda'){
            return view('pages.smart_school.task.pilihan.index', compact('title', 'sub_title', 'data'));
        }elseif($data->question_type == 'Essay'){
            return view('pages.smart_school.task.essay.index', compact('title', 'sub_title', 'data'));
        }elseif($data->question_type == 'File'){
            return view('pages.smart_school.task.file.index', compact('title', 'sub_title', 'data'));
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('bulk_import')){
            $this->bulkImport($request->file('bulk_import'), $request->task_id);
            return redirect()->route('school-task-questions.create', ['task_id' => $request->task_id])->with('success', 'Berhasil Menambah Data Soal');
        }
        $data = SchoolTask::findOrFail($request->task_id);
        isMySchool($data->school_id);
        if($data->question_type == 'Pilihan Ganda'){
            $validatedData = $request->validate([
                'nomor_soal' => ['required', Rule::unique('school_task_questions', 'number')->where(function ($query) use($data, $request) {
                    return $query->where('school_task_id', $data->id);
                })],
                'pertanyaan' => 'required|string|max:50000',
                'pembahasan' => 'nullable|string|max:50000',
                'jawaban_benar' => 'required|string|in:A,B,C,D,E',
                "pilihan.a"          => "required|max:50000",
                "pilihan.b"          => "required|max:50000",
                "pilihan.c"          => "required|max:50000",
                "pilihan.d"          => "required|max:50000",
                "pilihan.e"          => "max:50000"
            ]);
            $soal = SchoolTaskQuestion::create([
                        "school_task_id"    => $request->task_id,
                        "number"            => $request->nomor_soal,
                        "question"          => $request->pertanyaan,
                        "correct_answer"    => $request->jawaban_benar,
                        "justification"     => $request->pembahasan,
                    ]);
            SchoolTaskOption::create([
                "school_task_question_id"   => $soal->id,
                "A"     => $request->pilihan['a'],
                "B"     => $request->pilihan['b'],
                "C"     => $request->pilihan['c'],
                "D"     => $request->pilihan['d'],
                "E"     => isset($request->pilihan['e']) ? $request->pilihan['e'] : null,

            ]);
        }else{
            $validatedData = $request->validate([
                'nomor_soal' => ['required', Rule::unique('school_task_questions', 'number')->where(function ($query) use($data, $request) {
                    return $query->where('school_task_id', $data->id);
                })],
                'pertanyaan' => 'required|string|max:50000',
                'pembahasan' => 'nullable|string|max:50000'
            ]);
            $soal = SchoolTaskQuestion::create([
                        "school_task_id"    => $request->task_id,
                        "number"            => $request->nomor_soal,
                        "question"          => $request->pertanyaan,
                        "justification"     => $request->pembahasan,
                    ]);
        }
        return redirect()->back()->with('success', "Berhasil Menambah Soal");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Soal";
        $sub_title = "Home / Edit Soal";
        $data = SchoolTaskQuestion::findOrFail($id);

        if($data->task->question_type == 'Pilihan Ganda'){
            return view('pages.smart_school.task.pilihan.edit', compact('title', 'sub_title', 'data'));
        }elseif($data->task->question_type == 'Essay'){
            return view('pages.smart_school.task.essay.edit', compact('title', 'sub_title', 'data'));
        }elseif($data->task->question_type == 'File'){
            return view('pages.smart_school.task.file.edit', compact('title', 'sub_title', 'data'));
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SchoolTaskQuestion::findOrFail($id);
        isMySchool($data->task->school_id);
        if($data->task->question_type == 'Pilihan Ganda'){
            $validatedData = $request->validate([
                'nomor_soal' => ['required', Rule::unique('school_task_questions', 'number')->where(function ($query) use($data, $request) {
                    return $query->where('id', '!=', $data->id)->where('school_task_id', $data->school_task_id);
                })],
                'pertanyaan' => 'required|string|max:50000',
                'pembahasan' => 'nullable|string|max:50000',
                'jawaban_benar' => 'required|string|in:A,B,C,D,E',
                "pilihan_a"          => "required|max:50000",
                "pilihan_b"          => "required|max:50000",
                "pilihan_c"          => "required|max:50000",
                "pilihan_d"          => "required|max:50000",
                "pilihan_e"          => "max:50000"
            ]);
            $soal = SchoolTaskQuestion::where('id', $data->id)->update([
                        "school_task_id"    => $data->school_task_id,
                        "number"            => $request->nomor_soal,
                        "question"          => $request->pertanyaan,
                        "correct_answer"    => $request->jawaban_benar,
                        "justification"     => $request->pembahasan,
                    ]);
            SchoolTaskOption::where('school_task_question_id', $data->id)->update([
                "A"     => $request->pilihan_a,
                "B"     => $request->pilihan_b,
                "C"     => $request->pilihan_c,
                "D"     => $request->pilihan_d,
                "E"     => $request->pilihan_e ? $request->pilihan_e : null,

            ]);
        }else{
            $validatedData = $request->validate([
                'nomor_soal' => ['required', Rule::unique('school_task_questions', 'number')->where(function ($query) use($data, $request) {
                    return $query->where('id', '!=', $data->id)->where('school_task_id', $data->school_task_id);
                })],
                'pertanyaan' => 'required|string|max:50000',
                'pembahasan' => 'nullable|string|max:50000'
            ]);

            SchoolTaskQuestion::where('id', $data->id)->update([
                "number"            => $request->nomor_soal,
                "question"          => $request->pertanyaan,
                "justification"     => $request->pembahasan,
            ]);
        }
        return redirect()->route('school-task-questions.create', ['task_id' => $data->school_task_id])->with('success', "Berhasil Mengubah Soal");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $SchoolTask = SchoolTaskQuestion::findOrFail($id);
        $SchoolTask->delete();
        return redirect()->back()->with('success', "Berhasil Menghapus Soal");
    }
    public function bulkImport($file, $task_id){
        Excel::import(new QuestionsImport($task_id), $file);
    }
}
