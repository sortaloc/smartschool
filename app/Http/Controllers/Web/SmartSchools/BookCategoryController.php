<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolBookCategory;

class BookCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Buku'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Buku'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Buku'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Buku'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolBookCategory::schoolId($school_id)->orWhere('school_id', null)->orderBy('name', 'asc')->get();
        $title = 'Kategori Buku';
        $sub_title = 'Home / Kategori Buku';
        return view('pages.smart_school.book_category.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Kategori Buku Baru';
        $sub_title = 'Home / Kategori Buku Baru';
        return view('pages.smart_school.book_category.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'nama_kategori' => ['required', Rule::unique('school_book_categories', 'name')->where(function ($query) use($school_id, $request) {
                return $query->where('name', $request->nama_kategori)->where(function($q) use($school_id){
                    $q->where('school_id', $school_id)->orWhere('school_id', null);
                });
            })],
        ]);

        SchoolBookCategory::create([
            "school_id" => $school_id,
            "name" => $request->nama_kategori
        ]);

        return redirect()->route('school-book-categories.index')->with('success', 'Berhasil Menambah Kategori Buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolBookCategory $SchoolBookCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolBookCategory $SchoolBookCategory)
    {
        isMySchool($SchoolBookCategory->school_id);
        $title = 'Ubah Kategori Buku';
        $sub_title = 'Home / Ubah Kategori Buku';
        $data = $SchoolBookCategory;
        return view('pages.smart_school.book_category.edit', compact('data','title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolBookCategory $SchoolBookCategory)
    {
        isMySchool($SchoolBookCategory->school_id);
        $validatedData = $request->validate([
            'nama_kategori' => ['required', Rule::unique('school_book_categories', 'name')->where(function ($query) use($SchoolBookCategory, $request) {
                return $query->where('name', $request->nama_kategori)->where('id', '!=', $SchoolBookCategory->id)->where(function($q) use($SchoolBookCategory){
                    $q->where('school_id', $SchoolBookCategory->school_id)->orWhere('school_id', null);
                });
            })],
        ]);

        SchoolBookCategory::where('id', $SchoolBookCategory->id)->update([
            "name" => $request->nama_kategori
        ]);

        return redirect()->route('school-book-categories.index')->with('success', 'Berhasil Mengubah Kategori Buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolBookCategory $SchoolBookCategory)
    {
        if($SchoolBookCategory->books()->exists()){
            return redirect()->back()->with('danger', 'Kategori yang didalamnya terdapat buku tidak dapat dihapus');
        }
        $SchoolBookCategory->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Kategori Buku');
    }
}
