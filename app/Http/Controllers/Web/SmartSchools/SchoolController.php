<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Ubah Info Sekolah']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $data = School::findOrFail($school_id);
        $data->load('district.city.province');
        $title = 'Informasi Sekolah';
        $sub_title = 'Informasi Sekolah';
        return view('pages.smart_school.school.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $data = School::findOrFail($school_id);
        $logo = $data->getOriginal('logo');
        $ttd = $data->getOriginal('headmaster_signature');

        $validatedData = $request->validate([
            'jenjang' => 'nullable|string|max:150',
            'alamat' => 'nullable|string|max:150',
            'kecamatan' => 'nullable|exists:address_districts,id',
            'desa' => 'nullable|string|max:150',
            'kode_pos' => 'nullable|string|max:10',
            'fax' => 'nullable|string|max:50',
            'no_telp' => 'nullable|string|max:15',
            'website' => 'nullable|string|max:150',
            'email' => 'nullable|email|max:150',
            'nama_kepala_sekolah' => 'nullable|string|max:150',
            'ttd_kepala_sekolah' => 'nullable|image|max:10000',
            'nip_kepala_sekolah' => 'nullable|string|max:50',
            'logo' => 'nullable|image|max:10000',
        ]);

        if($request->file("logo")){
            $old_logo = $data->getOriginal('logo');
            if (Storage::exists($old_logo)) {
                Storage::delete($old_logo);
            }
            $logo = $request->file("logo")->store("SmartSchool/".$data->id."/logo");
        }
        if($request->file("ttd_kepala_sekolah")){
            $old_ttd = $data->getOriginal('headmaster_signature');
            if (Storage::exists($old_ttd)) {
                Storage::delete($old_ttd);
            }
            $ttd = $request->file("ttd_kepala_sekolah")->store("SmartSchool/".$data->id."/headmaster_signature");
        }

        $data->jenjang = $request->jenjang;
        $data->address = $request->alamat;
        $data->district_id = $request->kecamatan;
        $data->village = $request->desa;
        $data->zip_code = $request->kode_pos;
        $data->fax = $request->fax;
        $data->phone = $request->no_telp;
        $data->website = $request->website;
        $data->email = $request->email;
        $data->headmaster = $request->nama_kepala_sekolah;
        $data->headmaster_nip = $request->nip_kepala_sekolah;
        $data->headmaster_signature = $ttd;
        $data->logo = $logo;
        $data->updated_by = auth()->user()->id;
        $data->save();
        return redirect()->back()->with('success', 'Berhasil Mengubah Data Sekolah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(School $School)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(School $School)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, School $School)
    {
        $School->rapor_distribution = $request->distribution;
        $School->save();
        if($request->distribution == 1){
            return redirect()->back()->with('success', 'Berhasil Membagikan Rapor');
        }
        return redirect()->back()->with('danger', 'Berhasil Menarik Rapor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(School $School)
    {
        //
    }
}
