<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\SchoolYear;
use Illuminate\Support\Facades\Gate;
use App\Models\Konfigurasi\Paket;
use Illuminate\Support\Facades\Storage;

class SchoolYearController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Angkatan'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Angkatan'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Angkatan'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Angkatan'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_id = mySchool();
        $datas = SchoolYear::where('school_id', $school_id)->orderBy('start_year', 'asc')->orderBy('end_year', 'asc')->get();
        $title = 'Tahun Ajaran';
        $sub_title = 'Home / Tahun Ajaran';
        return view('pages.smart_school.school_year.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tahun Ajaran Baru';
        $sub_title = 'Home / Tahun Ajaran Baru';
        return view('pages.smart_school.school_year.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start_year = $request->tahun_mulai;
        $val_year = $request->tahun_mulai+1;
        $validatedData = $request->validate([
            'tahun_mulai' => 'bail|required|digits:4|integer|min:2000',
            'tahun_selesai' => 'bail|required|digits:4|integer|min:'.$start_year.'|max:'.$val_year,
            'semester'  => 'bail|required|in:Ganjil,Genap'
        ]);
        $school_id = mySchool();
        $data = SchoolYear::create([
            'school_id' => $school_id,
            'start_year' => $request->tahun_mulai,
            'end_year' => $request->tahun_selesai,
            'semester' => $request->semester,
            'is_active' => 0
        ]);

        return redirect()->route('school-years.index')->with('success', 'Berhasil Menambah Tahun Ajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolYear $SchoolYear)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolYear $SchoolYear)
    {
        $title = 'Ubah Kelas';
        $sub_title = 'Home / Ubah Kelas';
        $data = $SchoolYear;
        return view('pages.smart_school.school_year.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolYear $SchoolYear)
    {
        isMySchool($SchoolYear->school_id);
        if($status = $request->update_status){
            SchoolYear::where('school_id', $SchoolYear->school_id)->update([
                'is_active' => 0
            ]);
            $SchoolYear->is_active = $status;
        }else{
            $val_year = $request->tahun_mulai+1;
            $validatedData = $request->validate([
                'semester'  => 'bail|required|in:Ganjil,Genap'
            ]);
            $SchoolYear->semester = $request->semester;
        }
        $SchoolYear->save();

        return redirect()->route('school-years.index')->with('success', 'Berhasil Mengubah Tahun Ajaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolYear $SchoolYear)
    {
        if($SchoolYear->schedules->first() || $SchoolYear->tasks->first() || $SchoolYear->rapors->first()){
            return redirect()->back()->with('danger', 'Tahun ajaran yang berisi tugas/soal dan jadwal kelas tidak dapat dihapus !');
        }
        $SchoolYear->delete();
        return redirect()->back()->with('success', 'Tahun ajaran berhasil dihapus !');
    }
}
