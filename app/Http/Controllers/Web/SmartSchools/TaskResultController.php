<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolTask;
use App\Models\SmartSchools\SchoolTaskResult;
use App\Models\SmartSchools\SchoolTaskClass;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use App\Models\SmartSchools\SchoolAssessmentTypeRef;
use App\Exports\SmartSchoolRekapNilaiExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class TaskResultController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Soal'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Soal'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Soal'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Soal'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $results = SchoolTaskResult::whereHas('task_class.teacher_subjects', function($q) use($request){
            $q->where('school_class_id', $request->class_id)->where('school_subjects_id', $request->subjects_id);
        })->whereHas('task', function($q) use($school_id, $request){
            $q->where('id', $request->task_id)->where('school_id', $school_id);
        })->get();
        if($request->type == 'rekap'){
            $title = 'Rekap Nilai Siswa';
            $sub_title = 'Home / Rekap Nilai Siswa';
            if($request->teacher_id){
                $teacher_id = $request->teacher_id;
            }elseif(isset(auth()->user()->school_teacher->id)){
                $teacher_id = auth()->user()->school_teacher->id;
            }else{
                $teacher_id = null;
            }

            $results = SchoolTaskResult::whereHas('task_class.teacher_subjects', function($q) use($request){
                $q->where('school_class_id', $request->class_id)->where('school_subjects_id', $request->subjects_id);
            })->whereHas('task', function($q) use($school_id){
                $q->where('school_id', $school_id);
            })->get()->groupBy('school_student_id');
            $tasks = SchoolTask::whereHas('task_classes.teacher_subjects', function($q) use($request, $teacher_id){
                $q->where('school_class_id', $request->class_id)->where('school_subjects_id', $request->subjects_id);
            })->where('school_id', $school_id)->orderBy('assessment_type_ref_id', 'asc')->get();

            $datas = [];
            foreach ($results as $keyResult => $students) {
                $datas[$keyResult]['name'] = $students[0]->student->name;
                foreach ($tasks->groupBy('assessment_type_ref_id') as $keyAssessment => $assessment) {
                    foreach ($assessment as $keyTask => $task) {
                        if($task->type == 'Susulan'){
                            $point = $students->where('school_task_id', $task->id)->first();
                            $datas[$keyResult]['results'][$task->old_task_id] = $point ? $point->point : 0;
                        }elseif($task->type == 'Perbaikan'){
                            $point = $students->where('school_task_id', $task->id)->first();
                            $datas[$keyResult]['results'][$task->old_task_id] = $point ? $point->point : 0;
                        }else{
                            $point = $students->where('school_task_id', $task->id)->first();
                            $datas[$keyResult]['results'][$task->id] = $point ? $point->point : 0;
                        }
                    }
                }
            }

            $classes = SchoolTeacherSubjects::whereHas('teacher', function($q) use($school_id, $teacher_id){
                $q->where('school_id', $school_id);
                if($teacher_id){
                    $q->where('id', $teacher_id);
                };
            })->get();
            return view('pages.smart_school.task.result.rekap', compact('title', 'classes', 'datas', 'tasks', 'sub_title'));
        }
        $title = 'Hasil Tugas Siswa';
        $sub_title = 'Home / Hasil Tugas Siswa';
        $classes = SchoolClass::schoolId($school_id)->get();
        $subjects = SchoolSubjects::schoolId($school_id)->pluck('name', 'id');
        return view('pages.smart_school.task.result.index', compact('title', 'results', 'sub_title', 'classes', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $school_id = mySchool();
        if($request->teacher_id){
            $teacher_id = $request->teacher_id;
        }elseif(isset(auth()->user()->school_teacher->id)){
            $teacher_id = auth()->user()->school_teacher->id;
        }else{
            $teacher_id = null;
        }

        $results = SchoolTaskResult::whereHas('task_class.teacher_subjects', function($q) use($request, $teacher_id){
            $q->where('school_class_id', $request->class_id)->where('school_subjects_id', $request->subjects_id);
        })->whereHas('task', function($q) use($school_id){
            $q->where('school_id', $school_id);
        })->get()->groupBy('school_student_id');
        $tasks = SchoolTask::whereHas('task_classes.teacher_subjects', function($q) use($request, $teacher_id){
            $q->where('school_class_id', $request->class_id)->where('school_subjects_id', $request->subjects_id);
        })->where('school_id', $school_id)->orderBy('assessment_type_ref_id', 'asc')->get();

        $datas = [];

        foreach ($results as $keyResult => $students) {
            $datas[$keyResult]['name'] = $students[0]->student->name;
            foreach ($tasks->groupBy('assessment_type_ref_id') as $keyAssessment => $assessment) {
                foreach ($assessment as $keyTask => $task) {
                    $point = $students->where('school_task_id', $task->id)->first();
                    $datas[$keyResult]['results'][$task->id] = $point ? $point->point : 0;
                }
            }
        }
        return Excel::download(new SmartSchoolRekapNilaiExport($tasks, $datas), 'rekap nilai smartschool genius '.Carbon::now()->isoFormat('D-M-Y').'.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTask $SchoolTaskResult)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTaskResult $SchoolTaskResult)
    {
        $title = 'Edit Hasil Tugas Siswa';
        $sub_title = 'Home / Edit Hasil Tugas Siswa';
        $data = $SchoolTaskResult;
        return view('pages.smart_school.task.result.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolTaskResult $SchoolTaskResult)
    {
        $message = '';
        if($request->kesempatan){
            $SchoolTaskResult->status = 'Proses';
            $SchoolTaskResult->save();
            return redirect()->route('school-task-corrections.index', ['task_id' => $SchoolTaskResult->school_task_id])->with('success', 'Berhasil memberi kesempatan Siswa mengerjakan soal lagi');
        }else{
            $validatedData = $request->validate([
                'nilai' => 'required|numeric|max:100',
            ]);
            isMySchool($SchoolTaskResult->task->school_id);
            $SchoolTaskResult->point = $request->nilai;
            $message = 'Berhasil Mengubah Nilai Siswa';
            $SchoolTaskResult->save();
            return redirect()->route('school-task-results.index', ['task_id' => $SchoolTaskResult->school_task_id])->with('success', 'Berhasil Mengubah Nilai Siswa');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTaskResult $SchoolTaskResult)
    {
        abort(404);
    }
}
