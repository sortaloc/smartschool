<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolAdmin;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolSubjects;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolTeacherSubjects;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SmartSchools\TeacherRequest;
use Illuminate\Support\Facades\Storage;
use App\Imports\TeachersImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Exports\SmartSchoolTeacherExport;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Info Guru'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Info Guru'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Info Guru'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Info Guru'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolTeacher::where('school_id', $school_id)->get();
        if($request->download){
            $school = School::findOrFail($school_id);
            $school_year = $school->school_year()->firstOrFail();
            return Excel::download(new SmartSchoolTeacherExport($school, $datas), 'data guru smartschool genius '.Carbon::now()->isoFormat('D-M-Y').'.xlsx');
        }
        $title = 'Data Guru';
        $sub_title = 'Home / Data Guru';
        return view('pages.smart_school.teacher.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Tambah Guru';
        $sub_title = 'Home / Tambah Guru';
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $subjects = SchoolSubjects::where('school_id', $school_id)->get();
        return view('pages.smart_school.teacher.create', compact('classes', 'subjects', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        if($request->file('bulk_import')){
            $this->bulkImport($request->file('bulk_import'));
            return redirect()->route('school-teachers.index')->with('success', 'Berhasil Menambah Data Guru');
        }

        $school_id = mySchool();

        $password = date_format(date_create_from_format('Y-m-d', $request->tanggal_lahir), 'd-m-Y');
        $password = str_replace("-","",$password);

        $user  = \App\User::create([
            "name"  => $request->nama_guru,
            "username"  => $request->NIP_NIK,
            "email"     => $request->email,
            "password"  => bcrypt($password),
            "level"     => 'Guru Sekolah',
            "is_active" => 1
        ]);

        $user->assignRole('Admin Sekolah');
        $user->syncPermissions(['Lihat Jadwal Kelas', 'Lihat Jurnal Kelas', 'Tambah Jurnal Kelas',
        'Ubah Jurnal Kelas', 'Hapus Jurnal Kelas', 'Lihat Soal', 'Tambah Soal', 'Ubah Soal', 'Hapus Soal',
        'Lihat Rapor',
        'Lihat Materi Guru', 'Tambah Materi Guru', 'Ubah Materi Guru', 'Hapus Materi Guru']);

        $foto_guru = $request->file("foto")->store("SmartSchool/".$school_id."/Teacher/Photo");

        $ttd = null;

        if($request->file('tanda_tangan')){
            $ttd = $request->file("tanda_tangan")->store("SmartSchool/".$school_id."/Teacher/Signature");
        }

        $teacher = SchoolTeacher::create([
            'school_id'             => $school_id,
            'user_id'               => $user->id,
            'name'                  => $request->nama_guru,
            'NIP'                   => $request->NIP_NIK,
            'NUPTK'                 => $request->NUPTK,
            'place_of_birth'        => $request->tempat_lahir,
            'date_of_birth'         => $request->tanggal_lahir,
            'gender'                => $request->jenis_kelamin,
            'religion'              => $request->agama,
            'structural_position'   => $request->jabatan_struktural,
            'additional_position'   => $request->tugas_tambahan,
            'PNS_rank'              => $request->golongan,
            'phone'                 => $request->no_hp,
            'photo'                 => $foto_guru,
            'signature'             => $ttd
        ]);

        $admin = SchoolAdmin::create([
            'school_id'             => $school_id,
            'user_id'               => $user->id
        ]);

        if($request->mata_pelajaran){
            foreach ($request->mata_pelajaran as $key => $value) {
                if($value){
                    SchoolTeacherSubjects::create([
                        'school_teacher_id' => $teacher->id,
                        'school_class_id' => $request->kelas[$key],
                        'school_subjects_id' => $value
                    ]);
                }
            }
        }

        return redirect()->route('school-teachers.index')->with('success', 'Berhasil Menambah Data Guru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolTeacher $SchoolTeacher)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolTeacher $SchoolTeacher)
    {
        $school_id = mySchool();
        $title = 'Ubah Data Guru';
        $sub_title = 'Home / Ubah Data Guru';
        $data = $SchoolTeacher;
        return view('pages.smart_school.teacher.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, SchoolTeacher $SchoolTeacher)
    {
        $school_id = mySchool();

        $user  = \App\User::where('id', $SchoolTeacher->user_id)->update([
            "name"  => $request->nama_guru,
            "email"     => $request->email,
        ]);

        $foto_guru = $SchoolTeacher->getOriginal('photo');
        $ttd = $SchoolTeacher->getOriginal('signature');

        if($request->file("foto")){
            if (Storage::exists($foto_guru)) {
                Storage::delete($foto_guru);
            }
            $foto_guru = $request->file("foto")->store("SmartSchool/".$school_id."/Teacher/Photo");
        }
        if($request->file("tanda_tangan")){
            if (Storage::exists($ttd)) {
                Storage::delete($ttd);
            }
            $ttd = $request->file("tanda_tangan")->store("SmartSchool/".$school_id."/Teacher/Signature");
        }

        $teacher = SchoolTeacher::where('id', $SchoolTeacher->id)->update([
            'name'                  => $request->nama_guru,
            'NIP'                   => $request->NIP_NIK,
            'NUPTK'                 => $request->NUPTK,
            'place_of_birth'        => $request->tempat_lahir,
            'date_of_birth'         => $request->tanggal_lahir,
            'gender'                => $request->jenis_kelamin,
            'religion'              => $request->agama,
            'structural_position'   => $request->jabatan_struktural,
            'additional_position'   => $request->tugas_tambahan,
            'PNS_rank'              => $request->golongan,
            'phone'                 => $request->no_hp,
            'photo'                 => $foto_guru,
            'signature'             => $ttd
        ]);

        return redirect()->route('school-teachers.index')->with('success', 'Berhasil Mengubah Data Guru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolTeacher $SchoolTeacher)
    {
        isMySchool($SchoolTeacher->school_id);
        if($SchoolTeacher->tasks()->first() || $SchoolTeacher->subjects()->first()){
            return redirect()->back()->with('danger', 'Guru yang sedang mengajar atau memiliki tugas tidak dapat dihapus !');
        }

        $foto_guru = $SchoolTeacher->getOriginal('photo');

        if($foto_guru){
            if (Storage::exists($foto_guru)) {
                Storage::delete($foto_guru);
            }
        }

        $SchoolTeacher->user->forceDelete();

        return redirect()->back()->with('success', 'Berhasil Menghapus Data Guru');
    }
    public function bulkImport($file){
        Excel::import(new TeachersImport, $file);
    }
}
