<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolAnnouncement;

class AnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Pengumuman'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Pengumuman'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Pengumuman'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Pengumuman'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        $datas = SchoolAnnouncement::schoolId($school_id)->latest()->get();
        $title = 'Pengumuman';
        $sub_title = 'Home / Pengumuman';
        return view('pages.smart_school.announcement.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Pengumuman Baru';
        $sub_title = 'Home / Pengumuman Baru';
        return view('pages.smart_school.announcement.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $school_id = mySchool();
        $validatedData = $request->validate([
            'judul' => 'required|string|max:100',
            'pesan' => 'required|string|max:50000',
        ]);

        SchoolAnnouncement::create([
            'school_id' => $school_id,
            "title" => $request->judul,
            "message" => $request->pesan,
        ]);

        return redirect()->route('school-announcements.index')->with('success', 'Berhasil Menambah Pengumuman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolAnnouncement $SchoolAnnouncement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolAnnouncement $SchoolAnnouncement)
    {
        isMySchool($SchoolAnnouncement->school_id);
        $title = 'Ubah Pengumuman';
        $sub_title = 'Home / Ubah Pengumuman';
        $data = $SchoolAnnouncement;
        return view('pages.smart_school.announcement.edit', compact('data', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolAnnouncement $SchoolAnnouncement)
    {
        isMySchool($SchoolAnnouncement->school_id);
        if($request->update_status != null){
            $SchoolAnnouncement->status = $request->update_status;
            $SchoolAnnouncement->save();
            return redirect()->route('school-announcements.index')->with('success', 'Berhasil Mengubah Pengumuman');
        }
        $validatedData = $request->validate([
            'judul' => 'required|string|max:100',
            'pesan' => 'image|string|max:50000',
        ]);

        SchoolAnnouncement::where('id', $SchoolAnnouncement->id)->update([
            "title" => $request->judul,
            "message" => $request->pesan
        ]);

        return redirect()->route('school-announcements.index')->with('success', 'Berhasil Mengubah Pengumuman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolAnnouncement $SchoolAnnouncement)
    {
        $file = $SchoolAnnouncement->getOriginal('file');
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $logo = $SchoolAnnouncement->getOriginal('logo');
        if (Storage::exists($logo)) {
            Storage::delete($logo);
        }
        $SchoolAnnouncement->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Pengumuman');
    }
}
