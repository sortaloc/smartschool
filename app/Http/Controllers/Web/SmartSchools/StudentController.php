<?php

namespace App\Http\Controllers\Web\SmartSchools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolClass;
use App\Models\SmartSchools\SchoolStudent;
use App\Models\SmartSchools\SchoolStudentClass;
use App\Models\SmartSchools\SchoolStudentParent;
use App\Models\SmartSchools\SchoolStudentGuardian;
use App\Models\SmartSchools\SchoolGeneration;
use App\Models\SmartSchools\SchoolYear;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SmartSchools\StudentRequest;
use Illuminate\Support\Facades\Storage;
use App\Imports\StudentsImport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Exports\SmartSchoolStudentExport;
use DataTables;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:Semua Hak Akses|Lihat Info Siswa'])->only('index', 'show');
        $this->middleware(['permission:Semua Hak Akses|Tambah Info Siswa'])->only('create', 'store');
        $this->middleware(['permission:Semua Hak Akses|Ubah Info Siswa'])->only('edit', 'update');
        $this->middleware(['permission:Semua Hak Akses|Hapus Info Siswa'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_id = mySchool();
        if($request->ajax()){
            $data = SchoolStudent::with('user', 'class.class', 'generation')->where('school_id', $school_id)->withTrashed();
            return Datatables::eloquent($data)
            ->addIndexColumn()
            ->addColumn('action', function ($val) {
                return view('pages.smart_school.student.action', compact('val'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        $datas = SchoolStudent::where('school_id', $school_id)->withTrashed()->get();
        $title = 'Data Siswa';
        $sub_title = 'Home / Data Siswa';
        if($request->download){
            $school = School::findOrFail($school_id);
            $school_year = $school->school_year()->firstOrFail();
            return Excel::download(new SmartSchoolStudentExport($school, $datas), 'data siswa smartschool genius '.Carbon::now()->isoFormat('D-M-Y').'.xlsx');
        }
        return view('pages.smart_school.student.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_id = mySchool();
        $title = 'Siswa Baru';
        $sub_title = 'Home / Siswa Baru';
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $generations = SchoolGeneration::where('school_id', $school_id)->get();
        return view('pages.smart_school.student.create', compact('classes', 'generations', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        if($request->file('bulk_import')){
            $this->bulkImport($request->file('bulk_import'));
            return redirect()->route('school-students.index')->with('success', 'Berhasil Menambah Data Siswa');
        }

        $school_id = mySchool();

        $school = School::findOrFail($school_id);

        if($school->total_students < ($school->students->count() + 1)){
            return redirect()->back()->with('danger', 'Kuota pendaftaran siswa melebihi batas. Maksimal '.$school->total_students.' akun');
        }

        $password = date_format(date_create_from_format('Y-m-d', $request->tanggal_lahir), 'd-m-Y');
        $password = str_replace("-","",$password);

        $school_year = SchoolYear::where('school_id', $school_id)->where('is_active', 1)->firstOrFail();

        $foto_siswa = $request->file("foto_siswa")->store("SmartSchool/".$school_id."/Student/Photo");

        $user  = \App\User::create([
            "name"      => $request->nama_siswa,
            "username"  => $request->NISN,
            "email"     => $request->email,
            "password"  => bcrypt($password),
            "is_active" => 1,
            'avatar'    => $foto_siswa,
        ]);

        $user->assignRole('Siswa');


        $student = SchoolStudent::create([
            'school_id'             => $school_id,
            'user_id'               => $user->id,
            'name'                  => $request->nama_siswa,
            'NIPD'                  => $request->NIPD,
            'NISN'                  => $request->NISN,
            'place_of_birth'        => $request->tempat_lahir,
            'date_of_birth'         => $request->tanggal_lahir,
            'gender'                => $request->jenis_kelamin,
            'religion'              => $request->agama,
            'status_in_family'      => $request->status_dalam_keluarga,
            'child_number'          => $request->anak_ke,
            'address'               => $request->alamat_siswa,
            'district_id'           => $request->kecamatan_siswa,
            'village'               => $request->desa_siswa,
            'zip_code'              => $request->kode_pos_siswa,
            'phone'                 => $request->no_telp_siswa,
            'origin_school'         => $request->asal_sekolah,
            'start_class_id'        => $request->kelas_diterima,
            'register_date'         => $request->tanggal_diterima,
            'year_generation_id'    => $request->angkatan,
            'photo'                 => $foto_siswa,
        ]);

        $class = SchoolStudentClass::create([
            'school_student_id'     => $student->id,
            'class_id'              => $request->kelas,
            'school_year_id'        => $school_year->id
        ]);

        $parent = SchoolStudentParent::create([
            'student_id'            => $student->id,
            'father_name'           => $request->nama_ayah,
            'mother_name'           => $request->nama_ibu,
            'address'               => $request->alamat_orang_tua,
            'district_id'           => $request->kecamatan_orang_tua,
            'village'               => $request->desa_orang_tua,
            'zip_code'              => $request->kode_pos_orang_tua,
            'phone'                 => $request->no_telp_orang_tua,
            'father_job'            => $request->pekerjaan_ayah,
            'mother_job'            => $request->pekerjaan_ibu
        ]);

        $guardian = SchoolStudentGuardian::create([
            'student_id'            => $student->id,
            'name'                  => $request->nama_wali,
            'address'               => $request->alamat_wali,
            'district_id'           => $request->kecamatan_wali,
            'village'               => $request->desa_wali,
            'zip_code'              => $request->kode_pos_wali,
            'phone'                 => $request->no_telp_wali,
            'job'                   => $request->pekerjaan_wali
        ]);

        return redirect()->route('school-students.index')->with('success', 'Berhasil Menambah Data Siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolStudent $SchoolStudent)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolStudent $SchoolStudent)
    {
        $school_id = mySchool();
        $title = 'Ubah Data Siswa';
        $sub_title = 'Home / Ubah Data Siswa';
        $data = $SchoolStudent;
        $classes = SchoolClass::where('school_id', $school_id)->get();
        $generations = SchoolGeneration::where('school_id', $school_id)->get();
        return view('pages.smart_school.student.edit', compact('data', 'classes', 'generations', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, SchoolStudent $SchoolStudent)
    {
        if($request->update_status != null || intval($request->update_status) === 1){
            $SchoolStudent->status = $request->update_status;
            $SchoolStudent->save();
            try {
                if(isset($SchoolStudent->user->last_session)){
                    \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($SchoolStudent->user->last_session), $forceForever = false);
                }
            } catch (JWTException $e) {

            }
            return redirect()->route('school-students.index')->with('success', 'Berhasil Mengubah Status Siswa');
        }

        $school_id = mySchool();
        isMySchool($SchoolStudent->school_id);

        $password = date_format(date_create_from_format('Y-m-d', $SchoolStudent->date_of_birth), 'd-m-Y');
        $password = str_replace("-","",$password);

        $new_password = date_format(date_create_from_format('Y-m-d', $request->tanggal_lahir), 'd-m-Y');
        $new_password = str_replace("-","",$password);

        $old_password = $SchoolStudent->user->password;

        if (Hash::check($old_password, $password)) {
            $password = $new_password;
        }

        $school_year = mySchoolYear();

        $foto_siswa = $SchoolStudent->getOriginal('photo');

        if($request->file("foto_siswa")){
            if (Storage::exists($foto_siswa)) {
                Storage::delete($foto_siswa);
            }
            $foto_siswa = $request->file("foto_siswa")->store("SmartSchool/".$school_id."/Student/Photo");
        }

        $user  = \App\User::where('id', $SchoolStudent->user_id)->update([
            "name"      => $request->nama_siswa,
            "username"  => $request->NISN,
            "email"     => $request->email,
            "password"  => bcrypt($password),
            "is_active" => 1,
            "avatar"    => $foto_siswa
        ]);




        $student = SchoolStudent::where('id', $SchoolStudent->id)->update([
            'name'                  => $request->nama_siswa,
            'NIPD'                  => $request->NIPD,
            'NISN'                  => $request->NISN,
            'place_of_birth'        => $request->tempat_lahir,
            'date_of_birth'         => $request->tanggal_lahir,
            'gender'                => $request->jenis_kelamin,
            'religion'              => $request->agama,
            'status_in_family'      => $request->status_dalam_keluarga,
            'child_number'          => $request->anak_ke,
            'address'               => $request->alamat_siswa,
            'district_id'           => $request->kecamatan_siswa,
            'village'               => $request->desa_siswa,
            'zip_code'              => $request->kode_pos_siswa,
            'phone'                 => $request->no_telp_siswa,
            'origin_school'         => $request->asal_sekolah,
            'start_class_id'        => $request->kelas_diterima,
            'register_date'         => $request->tanggal_diterima,
            'year_generation_id'    => $request->angkatan,
            'photo'                 => $foto_siswa
        ]);

        SchoolStudentClass::where('school_student_id', $SchoolStudent->id)->where('is_active', 1)->update([
            'is_active'              => 0
        ]);

        $class = SchoolStudentClass::updateOrCreate([
            'class_id'              => $request->kelas,
            'school_year_id'        => $school_year->id,
            'school_student_id'     => $SchoolStudent->id
            ],[
            'is_active'             => 1
            ]
        );

        $parent = SchoolStudentParent::where('student_id', $SchoolStudent->id)->update([
            'father_name'           => $request->nama_ayah,
            'mother_name'           => $request->nama_ibu,
            'address'               => $request->alamat_orang_tua,
            'district_id'           => $request->kecamatan_orang_tua,
            'village'               => $request->desa_orang_tua,
            'zip_code'              => $request->kode_pos_orang_tua,
            'phone'                 => $request->no_telp_orang_tua,
            'father_job'            => $request->pekerjaan_ayah,
            'mother_job'            => $request->pekerjaan_ibu
        ]);

        $guardian = SchoolStudentGuardian::where('student_id', $SchoolStudent->id)->update([
            'name'                  => $request->nama_wali,
            'address'               => $request->alamat_wali,
            'district_id'           => $request->kecamatan_wali,
            'village'               => $request->desa_wali,
            'zip_code'              => $request->kode_pos_wali,
            'phone'                 => $request->no_telp_wali,
            'job'                   => $request->pekerjaan_wali
        ]);

        return redirect()->route('school-students.index')->with('success', 'Berhasil Mengubah Data Siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolStudent $SchoolStudent)
    {
        isMySchool($SchoolStudent->school_id);

        $foto_siswa = $SchoolStudent->getOriginal('photo');

        if($foto_siswa){
            if (Storage::exists($foto_siswa)) {
                Storage::delete($foto_siswa);
            }
        }

        $SchoolStudent->delete();
        $SchoolStudent->user->delete();

        return redirect()->back()->with('danger', 'Berhasil Menghapus Data Siswa');
    }
    public function bulkImport($file){
        Excel::import(new StudentsImport, $file);
    }
}
