<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Models\Konfigurasi\Province;
use App\Models\Konfigurasi\City;
use App\Models\Konfigurasi\District;
use Request;

class WilayahController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator']);
    }
    public function index()
    {
        $title = "Data Wilayah";
        $sub_title = "Home / Data Wilayah";

        return view('pages.konfigurasi.wilayah.index', compact('title', 'sub_title'));
    }
}
