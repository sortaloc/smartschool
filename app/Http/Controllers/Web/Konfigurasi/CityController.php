<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Konfigurasi\City;
use App\Http\Requests\Konfigurasi\CityRequest;
use DataTables;

class CityController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator'])->except('ajaxCity');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function ajaxCity(Request $request)
    {
        $city_temp = City::where('name', 'like', '%'.$request->q.'%');
        if($province_id = $request->province_id){
            $city_temp->where('province_id', $province_id);
        }
        $cities = $city_temp->get()->take(10);
        $cities->map(function($city) {
            $city['id'] = $city->id;
            $city['text'] = $city->type.' '.$city->name.' - '.$city->province->name;
            return $city;
        });
        return response()->json(['results' => $cities]);
    }

    public function datatable(){

        $cities = City::with('province');

        return Datatables::eloquent($cities)
            ->addIndexColumn()
            ->addColumn('action', function ($val) {
                return view('pages.konfigurasi.wilayah.city.action', compact('val'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Kota / Kab';
        return view('pages.konfigurasi.wilayah.city.create', compact('title'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        City::create([
            'name' => $request->name,
            'type' => $request->type,
            'ibukota' => $request->ibukota,
            'province_id' => $request->province_id,
            'c_bsni' => $request->c_bsni
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menambhaklan data kota / kab');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Detail Kota / Kab";
        $data = City::findOrFail($id);
        return view('pages.konfigurasi.wilayah.city.show', compact('data', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Kota / Kab";
        $data = City::findOrFail($id);
        return view('pages.konfigurasi.wilayah.city.edit', compact('data', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        City::where('id', $id)->update([
            'name' => $request->name,
            'type' => $request->type,
            'ibukota' => $request->ibukota,
            'province_id' => $request->province_id,
            'c_bsni' => $request->c_bsni
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil mengubah data kota / kab');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = City::findOrFail($id);
        $data->delete();
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menghapus data kota / kab');
    }
}
