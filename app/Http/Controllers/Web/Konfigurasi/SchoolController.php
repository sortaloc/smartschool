<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Http\Requests\Konfigurasi\SchoolRequest;
use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolAdmin;
use App\Models\SmartSchools\SchoolPaket;
use App\Models\Konfigurasi\Paket;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Smart School']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Smart School";
        $sub_title = "Home / Smart School";
        $data = School::get();
        return view('pages.konfigurasi.smart_school.index', compact('title', 'sub_title', 'data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = "Buat Sekolah";
        $sub_title = "Home / Buat Sekolah";
        return view('pages.konfigurasi.smart_school.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request)
    {
        $validatedData = $request->validate([
            'nama_sekolah' => 'bail|required|string|max:150',
            'tipe_sekolah' => 'bail|required|in:kemendikbud,kemenag',
            'jumlah_akun' => 'bail|required|numeric',
            'no_hp' => 'bail|required|unique:users,username|unique:schools,phone|max:255',
            'email' => 'bail|required|email|unique:users,email|unique:schools,email|max:255',
            'NPSN' => 'bail|unique:users,username|unique:schools,NPSN|max:255',
            'masa_aktif' => 'bail|nullable|date_format:d-m-Y'
        ]);

        $school = School::create([
            "name"          => $request->nama_sekolah,
            "type"          => $request->tipe_sekolah,
            "NPSN"          => $request->NPSN,
            "NDS"           => $request->NIS_NSS_NDS,
            "email"         => $request->email,
            "total_students"    => $request->jumlah_akun,
            "phone"         => $request->no_hp,
            "expired_at"    => date_create_from_format('d-m-Y', $request->masa_aktif)
        ]);

        $user  = \App\User::create([
            "name"  => $request->nama_sekolah,
            "username"  => $request->NPSN ? $request->NPSN : $request->email,
            "email"     => $request->email,
            "password"  => bcrypt($request->no_hp),
            "is_active" => 1
        ]);

        $admin = SchoolAdmin::create([
            "school_id" => $school->id,
            "user_id"   => $user->id
        ]);

        $user->assignRole('Super Admin Sekolah');
        $user->syncPermissions(['Semua Hak Akses']);

        return redirect()->route('config-schools.index')->with('success', 'Sekolah Berhasil Ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(School $ConfigSchool)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(School $ConfigSchool)
    {
        $title = "Edit Sekolah";
        $sub_title = "Home / Edit Sekolah";
        $data = $ConfigSchool;
        return view('pages.konfigurasi.smart_school.edit', compact('title', 'sub_title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_sekolah' => 'bail|required|string|max:150',
            'tipe_sekolah' => 'bail|required|in:kemendikbud,kemenag',
            'jumlah_akun' => 'bail|required|numeric',
            'no_hp' => 'bail|required|max:255|unique:schools,phone,'.$id,
            'email' => 'bail|required|max:255|unique:schools,email,'.$id,
            'NPSN' => 'bail|max:255',
            'masa_aktif' => 'bail|nullable|date_format:d-m-Y'
        ]);

        $school = School::where('id', $id)->update([
            "name"          => $request->nama_sekolah,
            "type"          => $request->tipe_sekolah,
            "NPSN"          => $request->NPSN,
            "NDS"           => $request->NIS_NSS_NDS,
            "email"         => $request->email,
            "total_students"    => $request->jumlah_akun,
            "phone"         => $request->no_hp,
            "expired_at"    => date_create_from_format('d-m-Y', $request->masa_aktif)
        ]);

        return redirect()->route('config-schools.index')->with('success', 'Data Sekolah Berhasil Di Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
