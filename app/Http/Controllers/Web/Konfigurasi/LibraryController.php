<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolLibrary;
use App\Models\SmartSchools\SchoolBookCategory;

class LibraryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:Super Admin|Smart School']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $datas = SchoolLibrary::schoolId(null)->latest()->get();
        $title = 'Daftar Buku';
        $sub_title = 'Home / Daftar Buku';
        return view('pages.konfigurasi.smart_school.library.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Buku Baru';
        $sub_title = 'Home / Buku Baru';
        $categories = SchoolBookCategory::schoolId(null)->pluck('name', 'id');
        return view('pages.konfigurasi.smart_school.library.create', compact('categories', 'title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'kategori' => 'required|exists:school_book_categories,id',
            'nomor_buku' => 'required|string|max:50',
            'judul' => 'required|string|max:150',
            'pengarang' => 'required|string|max:150',
            'cover' => 'required|image|max:10000',
            'file' => 'required|mimes:pdf|max:100000',
        ]);

        $file = null;
        if($file = $request->file("file")){
            $file = $request->file("file")->store("SmartSchool/admin/library");
        }
        $cover = null;
        if($cover = $request->file("cover")){
            $cover = $request->file("cover")->store("SmartSchool/admin/library/cover");
        }

        SchoolLibrary::create([
            "school_id" => null,
            "category_id" => $request->kategori,
            "book_number" => $request->nomor_buku,
            "title" => $request->judul,
            "author" => $request->pengarang,
            "cover" => $cover,
            "file" => $file
        ]);

        return redirect()->route('config-libraries.index')->with('success', 'Berhasil Menambah Buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolLibrary $ConfigLibrary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolLibrary $ConfigLibrary)
    {
        isMySchool($ConfigLibrary->school_id);
        $title = 'Ubah Buku';
        $sub_title = 'Home / Ubah Buku';
        $data = $ConfigLibrary;
        $categories = SchoolBookCategory::schoolId($ConfigLibrary->school_id)->pluck('name', 'id');
        return view('pages.konfigurasi.smart_school.library.edit', compact('data', 'categories', 'title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolLibrary $ConfigLibrary)
    {
        isMySchool($ConfigLibrary->school_id);
        $validatedData = $request->validate([
            'nomor_buku' => 'required|string|max:50',
            'kategori' => 'required|exists:school_book_categories,id',
            'judul' => 'required|string|max:150',
            'pengarang' => 'required|string|max:150',
            'cover' => 'image|max:10000',
            'file' => 'mimes:pdf|max:100000',
        ]);

        $file = $ConfigLibrary->getOriginal('file');
        if($request->file("file")){
            if (Storage::exists($file)) {
                Storage::delete($file);
            }
            $file = $request->file("file")->store("SmartSchool/admin/library");
        }
        $cover = $ConfigLibrary->getOriginal('cover');
        if($request->file("cover")){
            if (Storage::exists($cover)) {
                Storage::delete($cover);
            }
            $cover = $request->file("cover")->store("SmartSchool/admin/library/cover");
        }

        SchoolLibrary::where('id', $ConfigLibrary->id)->update([
            "category_id" => $request->kategori,
            "book_number" => $request->nomor_buku,
            "title" => $request->judul,
            "author" => $request->pengarang,
            "cover" => $cover
        ]);

        return redirect()->route('config-libraries.index')->with('success', 'Berhasil Mengubah Buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolLibrary $ConfigLibrary)
    {
        $file = $ConfigLibrary->getOriginal('file');
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $cover = $ConfigLibrary->getOriginal('cover');
        if (Storage::exists($cover)) {
            Storage::delete($cover);
        }
        $ConfigLibrary->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Buku');
    }
}
