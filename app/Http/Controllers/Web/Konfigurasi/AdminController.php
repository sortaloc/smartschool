<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use App\User;
use Illuminate\Http\Request;
use DB;
use DataTables;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator']);
    }
    public function index(Request $request)
    {
        $title = "Admin";
        $sub_title = "Home / Admin";

        if($request->ajax()){
            $user = User::whereHas("roles", function($q){ $q->whereIn("name", ["Super Admin", "Admin"]); });
           
            return Datatables::of($user)
                ->addIndexColumn()
                ->addColumn('avatar', function ($usr) {
                    if(!$usr->avatar) return '';
                    return '<a target="blank" href="'.$usr->avatar.'">
                        <div class="image-square-45">
                            <img class="image-square" src="'.$usr->avatar.'" class="rounded-circle" alt="">
                        </div>
                    </a>';
                })
                ->addColumn('level', function ($usr) {
                    $role = $usr->getRoleNames()->first();
                    return $role;
                })
                ->addColumn('status', function ($usr) {
                    if($usr->is_active == 1)
                        return '<span class="badge badge-success">Aktif</span>';
                    else
                        return '<span class="badge badge-danger">Non Aktif</span>';
                })
                ->addColumn('action', function ($adm) {
                    return view('pages.konfigurasi.admin.action', compact('adm'))->render();
                })
                ->rawColumns(['avatar', 'level', 'status', 'action'])
                ->make(true);
        }

        $admin = User::whereHas("roles", function($q){ $q->whereIn("name", ["Super Admin", "Admin"]); })->orderBy('id', 'asc')->get();

        return view('pages.konfigurasi.admin.admin', compact('title', 'sub_title', 'admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
       //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $message= '';
        if(request('type') == "active"){
            $data->is_active = 1;
            $message = 'Mengaktifkan';
        }elseif(request('type') == "nonActive"){
            $data->is_active = 0;
            $message = 'Menonaktifkan';
        }
        $data->save();
        return redirect()->back()->with('success','Berhasil '.$message.' Admin !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->hasRole('Supe rAmin')){
            $admin = User::find($id)->delete();
        }else{
            return redirect()->back()->with('danger','Hanya super admin yang boleh melekukan aksi ini !');
        }
        return redirect()->back()->with('success','Berhasil Menghapus Admin !');
    }
}
