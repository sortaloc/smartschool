<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Konfigurasi\District;
use App\Http\Requests\Konfigurasi\DistrictRequest;
use DataTables;

class DistrictController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator'])->except('ajaxDistrict');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function ajaxDistrict(Request $request)
    {
        $district_temp = District::where('name', 'like', '%'.$request->q.'%');
        if($request->city_id){
            $district_temp->where('city_id', $request->city_id);
        }
        $districts = $district_temp->get()->take(10);
        $districts->map(function($district) {
            $district['id'] = $district->id;
            $district['text'] = $district->name.' - '.$district->city->type.' '.$district->city->name;
            return $district;
        });
        return response()->json(['results' => $districts]);
    }
    public function datatable(){
        $districts = District::with('city');
        return Datatables::eloquent($districts)
            ->addIndexColumn()
            ->addColumn('action', function ($val) {
                return view('pages.konfigurasi.wilayah.district.action', compact('val'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Kecamatan';
        return view('pages.konfigurasi.wilayah.district.create', compact('title'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictRequest $request)
    {
        District::create([
            'name' => $request->name,
            'city_id' => $request->city_id
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menambahkan data kecamatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Detail Kecamatan";
        $data = District::findOrFail($id);
        return view('pages.konfigurasi.wilayah.district.show', compact('data', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Kecamatan";
        $data = District::findOrFail($id);
        return view('pages.konfigurasi.wilayah.district.edit', compact('data', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DistrictRequest $request, $id)
    {
        District::where('id', $id)->update([
            'name' => $request->name,
            'city_id' => $request->city_id
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil mengubah data kecamatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = District::findOrFail($id);
        $data->delete();
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menghapus data kecamatan');
    }
}
