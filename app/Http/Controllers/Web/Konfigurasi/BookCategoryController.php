<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\SmartSchools\SchoolBookCategory;

class BookCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:Super Admin|Smart School']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $datas = SchoolBookCategory::schoolId(null)->latest()->get();
        $title = 'Kategori Buku';
        $sub_title = 'Home / Kategori Buku';
        return view('pages.konfigurasi.smart_school.book_category.index', compact('datas', 'title', 'sub_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Kategori Buku Baru';
        $sub_title = 'Home / Kategori Buku Baru';
        return view('pages.konfigurasi.smart_school.book_category.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_kategori' => ['required', Rule::unique('school_book_categories', 'name')->where(function ($query) use($request) {
                return $query->where('school_id', null)->where('name', $request->nama_kategori);
            })],
        ]);

        SchoolBookCategory::create([
            "school_id" => null,
            "name" => $request->nama_kategori
        ]);

        return redirect()->route('config-book-categories.index')->with('success', 'Berhasil Menambah Kategori Buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolBookCategory $ConfigBookCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolBookCategory $ConfigBookCategory)
    {
        $title = 'Ubah Kategori Buku';
        $sub_title = 'Home / Ubah Kategori Buku';
        $data = $ConfigBookCategory;
        return view('pages.konfigurasi.smart_school.book_category.edit', compact('data','title', 'sub_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolBookCategory $ConfigBookCategory)
    {
        isMySchool($ConfigBookCategory->school_id);
        $validatedData = $request->validate([
            'nama_kategori' => ['required', Rule::unique('school_book_categories', 'name')->where(function ($query) use($ConfigBookCategory, $request) {
                return $query->where('school_id', $ConfigBookCategory->school_id)->where('name', $request->nama_kategori)->where('id', '!=', $ConfigBookCategory->id);
            })],
        ]);

        SchoolBookCategory::where('id', $ConfigBookCategory->id)->update([
            "name" => $request->nama_kategori
        ]);

        return redirect()->route('config-book-categories.index')->with('success', 'Berhasil Mengubah Kategori Buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolBookCategory $ConfigBookCategory)
    {
        if($ConfigBookCategory->books()->exists()){
            return redirect()->back()->with('danger', 'Kategori yang didalamnya terdapat buku tidak dapat dihapus');
        }
        $ConfigBookCategory->delete();
        return redirect()->back()->with('success', 'Berhasil Menghapus Kategori Buku');
    }
}
