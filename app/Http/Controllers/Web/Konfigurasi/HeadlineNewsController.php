<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Http\Requests\Konfigurasi\HeadlineNewsRequest;
use App\Models\Konfigurasi\HeadlineNews;
use Request;
use Illuminate\Support\Facades\Storage;

class HeadlineNewsController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Admin Marketing|Operator']);
    }
    public function index()
    {
        $title = "Headline News";
        $sub_title = "Home / Headline News";
        $data = HeadlineNews::get();
        return view('pages.konfigurasi.headline_news.index', compact('title', 'sub_title', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create Headline News";
        $sub_title = "Home / Create Headline News";

        return view('pages.konfigurasi.headline_news.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HeadlineNewsRequest $request)
    {
        $image = $request->file("image")->store("konfigurasi/headline_news");
        HeadlineNews::create([
            'title' => $request->title,
            'headline_news' => request('headline_news'),
            'image' => $image
        ]);
        return redirect()->route('headline-news.index')->with('success','Berhasil menambah headline news !');
    }
    /*
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Headline News";
        $sub_title = "Home / Edit Headline News";
        $data = HeadlineNews::findOrFail($id);
        return view('pages.konfigurasi.headline_news.edit', compact('title', 'sub_title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HeadlineNewsRequest $request, $id)
    {
        $data = HeadlineNews::where('id', $id)->firstOrFail();
        $image = $data->getOriginal('image');
        if($request->image){
            if (Storage::exists($image)) {
                Storage::delete($image);
            }
            $image = $request->file("image")->store("konfigurasi/headline_news");
        }
        $data->update([
            'title' => $request->title,
            'headline_news' => request('headline_news'),
            'image' => $image
        ]);
        return redirect()->route('headline-news.index')->with('success','Berhasil mengubah headline news !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = HeadlineNews::findOrFail($id)->firstOrFail();
        $image = $data->getOriginal('image');
        if (Storage::exists($image)) {
            Storage::delete($image);
        }
        $data->delete();
        return redirect()->back()->with('success','Berhasil Menghapus Data !');
    }
}
