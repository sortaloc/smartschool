<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Konfigurasi\Province;
use App\Http\Requests\Konfigurasi\ProvinceRequest;
use DataTables;

class ProvinceController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator'])->except('ajaxProvince');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function ajaxProvince(Request $request)
    {
        $provinces = Province::where('name', 'like', '%'.$request->q.'%')->get()->take(10);
        $provinces->map(function($province) {
            $province['id'] = $province->id;
            $province['text'] = $province->name;
            return $province;
        });
        return response()->json(['results' => $provinces]);
    }
    public function datatable(){
        $provinces = Province::whereNotNull('id');
        return Datatables::eloquent($provinces)
            ->addIndexColumn()
            ->addColumn('action', function ($val) {
                return view('pages.konfigurasi.wilayah.province.action', compact('val'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Provinsi';
        return view('pages.konfigurasi.wilayah.province.create', compact('title'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProvinceRequest $request)
    {
        Province::create([
            'name' => $request->name,
            'p_bsni' => $request->p_bsni
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menambahkan data provinsi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Detail Provinsi";
        $data = Province::findOrFail($id);
        return view('pages.konfigurasi.wilayah.province.show', compact('data', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Provinsi";
        $data = Province::findOrFail($id);
        return view('pages.konfigurasi.wilayah.province.edit', compact('data', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinceRequest $request, $id)
    {
        Province::where('id', $id)->update([
            'name' => $request->name,
            'p_bsni' => $request->p_bsni
        ]);
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil mengubah data provinsi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Province::findOrFail($id);
        $data->delete();
        return redirect()->route('config-wilayah.index')->with('success', 'Berhasil menghapus data provinsi');
    }
}
