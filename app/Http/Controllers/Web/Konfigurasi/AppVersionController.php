<?php

namespace App\Http\Controllers\Web\Konfigurasi;

use App\Http\Controllers\Controller;
use App\Http\Requests\Konfigurasi\AppVersionRequest;
use App\Models\Konfigurasi\AppVersion;

class AppVersionController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator']);
    }
    public function index()
    {
        $title = "Versi Aplikasi";
        $sub_title = "Home / Versi Aplikasi";
        $data = AppVersion::orderBy('id', 'desc')->get();

        return view('pages.konfigurasi.app_version.index', compact('title', 'sub_title', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Versi Aplikasi Baru";
        $sub_title = "Home / Versi Aplikasi Baru";

        return view('pages.konfigurasi.app_version.create', compact('title', 'sub_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppVersionRequest $request)
    {
       $data = AppVersion::create([
           'version'        => $request->version,
           'description'    => $request->description
       ]);

       return redirect()->route('app-version.index')->with('success', 'Versi Aplikasi Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Versi Applikasi";
        $sub_title = "Home / Edit Versi Applikasi";
        $data = AppVersion::find($id);

        return view('pages.konfigurasi.app_version.edit', compact('title', 'sub_title', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AppVersionRequest $request, $id)
    {
        $data = AppVersion::where('id', $id)->update([
            'version'        => $request->version,
            'description'    => $request->description
        ]);

        return redirect()->route('app-version.index')->with('success', 'Versi Aplikasi Berhasil Di Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = AppVersion::find($id)->delete();
        return redirect()->back()->with('success','Berhasil Menghapus Versi Aplikasi !');
    }
}
