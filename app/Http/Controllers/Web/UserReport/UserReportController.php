<?php

namespace App\Http\Controllers\Web\UserReport;

use App\Http\Controllers\Controller;
use App\Models\UserReport\UserReportDetail;
use App\Models\UserReport\UserReport;
use App\Models\Konfigurasi\Kelas;
use Illuminate\Http\Request;

class UserReportController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator|Admin SD|Admin SMP|Admin SMA|Admin SMK|Admin UTBK|Admin Olimpiade']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "Rapor User";
        $sub_title = "Home / Rapor User";
        $kelas = Kelas::role()->get();
        $data = null;
        if($kur_id = request('kurikulum_id')){
            $data = UserReport::where('kurikulum_id', $kur_id)->get();
        }
        return view('pages.user_report.index', compact('title', 'sub_title', 'kelas', 'data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Detail Rapor User";
        $sub_title = "Home / Detail Rapor User";
        $data = UserReport::find($id);
        $latihan = UserReportDetail::where('user_report_id', $id)->where('type', 'latihan')->get();
        $quiz = UserReportDetail::where('user_report_id', $id)->where('type', 'quiz')->get();
        $tryout = UserReportDetail::where('user_report_id', $id)->where('type', 'tryout')->get();
        return view('pages.user_report.report_detail', compact('title', 'sub_title', 'latihan', 'quiz', 'tryout'));
    }
}
