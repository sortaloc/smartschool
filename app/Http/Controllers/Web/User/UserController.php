<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use DB;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator|Admin SD|Admin SMP|Admin SMA|Admin SMK|Admin UTBK|Admin Olimpiade|Event'])->only('index', 'paginate', 'ajaxUser', 'show');
        $this->middleware(['role:Super Admin|Operator'])->only('update', 'destroy', 'export');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "User";
        $sub_title = "Home / User";
        $user = User::where('level', '=', 'user')->get();
        return view('pages.user.user', compact('title', 'sub_title', 'user'));
    }

    public function paginate(){
        $user = DB::table('users')
        ->where('users.deleted_at', null)
        ->whereIn('level', ['User', 'Siswa'])
        ->leftjoin('user_profiles', 'users.id', 'user_profiles.user_id')
        ->leftjoin('school_students', 'users.id', 'school_students.user_id')
        ->leftJoin('address_districts', 'address_districts.id', 'user_profiles.district_id')
        ->leftjoin('address_cities as city1', 'address_districts.city_id', 'city1.id')
        ->leftJoin('address_cities as city2', 'city2.id', 'user_profiles.city_id')
        ->leftJoin('address_districts as student_address', 'student_address.id', 'school_students.district_id')
        ->leftjoin('address_cities as student_city', 'student_address.city_id', 'student_city.id')
        ->select('users.username',
                'users.id',
                'users.email',
                'users.is_active',
                'users.level',
                'user_profiles.phone_number',
                'user_profiles.name',
                'school_students.phone as student_phone',
                'school_students.name as student_name',
                'student_city.name as student_city',
                'student_city.type as student_city_type',
                'city1.name as city1_name',
                'city1.type as city1_type',
                'city2.name as city2_name',
                'city2.type as city2_type',
                DB::raw("(select user_id from members where user_id = users.id and deleted_at IS NULL limit 1) as member")
            );
        return Datatables::of($user)
            ->addIndexColumn()
            ->addColumn('name', function ($usr) {
                return $usr->name ? $usr->name : $usr->student_name;
            })
            ->addColumn('phone_number', function ($usr) {
                return $usr->phone_number ? $usr->phone_number : $usr->student_phone;
            })
            ->addColumn('city', function ($usr) {
                if($usr->city1_name){
                    return $usr->city1_type .' '. $usr->city1_name;
                }elseif($usr->city2_name){
                    return $usr->city2_type .' '. $usr->city2_name;
                }elseif($usr->student_city){
                    return $usr->student_city_type .' '. $usr->student_city;
                }
                return '';
            })
            ->addColumn('is_premium', function ($usr) {
                if($usr->member != null)
                    return '<span class="badge badge-success">Langganan</span>';
                else
                    return '<span class="badge badge-danger">Gratis</span>';
            })
            ->addColumn('is_verified', function ($usr) {
                return view('pages.user.status', compact('usr'))->render();
            })
            ->addColumn('action', function ($usr) {
                return view('pages.user.action', compact('usr'))->render();
            })
            ->rawColumns(['name', 'phone_number', 'is_verified', 'kabupaten', 'is_premium', 'action'])
            ->make(true);
    }
    public function ajaxUser(Request $request){
        $users = User::select('id', 'username')->where('username', 'like', '%'.$request->q.'%')->get()->take(10);
        $users->map(function($user) {
            $user['id'] = $user->id;
            $user['text'] = $user->username.' - '.$user->profile->name;
            $user->unsetRelation('profile');
            return $user;
        });
        return response()->json(['results' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "Detail User";
        $sub_title = "Home / User / Detail User";
        $user = User::find($id);
        return view('pages.user.detail', compact('title', 'sub_title', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $message= '';
        if(request('type') == "active"){
            $data->is_active = 1;
            $message = 'Mengaktifkan';
        }elseif(request('type') == "nonActive"){
            $data->is_active = 0;
            $message = 'Menonaktifkan';
        }
        $data->save();
        return redirect()->back()->with('success','Berhasil '.$message.' User !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return redirect()->back()->with('success','Berhasil Menghapus User !');
    }
    public function export(){
        return Excel::download(new UserExport, 'user_'.now().'.xlsx');
    }
}
