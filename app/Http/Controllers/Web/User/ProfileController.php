<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use App\Http\Requests\User\ChangeProfileRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        $title = "Profile";
        $sub_title = "Home / Profile";
        $data = auth()->user();
        if($data->level == 'Guru'){
            return view('pages.user.profile.profile_guru', compact('title', 'sub_title', 'data'));
        }
        return view('pages.user.profile.index', compact('title', 'sub_title', 'data'));
    }
    public function update(ChangeProfileRequest $request, $id)
    {
        if($request->type == 'change-profile'){
            if($request->email){
                $user = User::where('id', $id)->update([
                    "email"     => $request->email
                ]);
            }
            $avatar = null;
            if($request->file("avatar")){
                $avatar = $request->file("avatar")->store("user/avatar");
            }
            $profile = auth()->user();
            if($avatar){
                $del_avatar = $profile->getOriginal('avatar');
                if (Storage::exists($del_avatar)) {
                    Storage::delete($del_avatar);
                }
                $profile->avatar = $avatar;
            }
            $profile->name = $request->name;
            $profile->save();
        }elseif($request->type == 'change-password'){
            $user = User::where('id', $id)->update([
                "password"     => Hash::make($request->new_password)
            ]);
        }
        return redirect()->back()->with('success', 'Berhasil Mengubah Profile');
    }
}
