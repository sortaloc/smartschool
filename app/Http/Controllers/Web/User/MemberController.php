<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\User;
use App\Member;
use App\Exports\MemberExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct(){
        $this->middleware(['role:Super Admin|Operator|Admin SD|Admin SMP|Admin SMA|Admin SMK|Admin UTBK|Admin Olimpiade'])->only('index');
        $this->middleware(['role:Super Admin|Operator'])->only('destroy', 'export');
    }
    public function index()
    {
        $title = "Member";
        $sub_title = "Home / Member";
        $member = Member::has('user')->get();
        return view('pages.user.member', compact('title', 'sub_title', 'member'));
    }
    public function destroy($id)
    {
        $data = Member::find($id)->delete();
        return redirect()->back()->with('danger', "Berhasil Menghapus Member");
    }
    public function export(){
        return Excel::download(new MemberExport, 'member_'.now().'.xlsx');
    }
}
