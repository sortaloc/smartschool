<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $message = '';
        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $username)->first();
            if($user){
                if($user->level == 'User'){
                    return redirect()->back()->withErrors('Tidak Dapat Login Dengan Akun Ini')->withInput();
                }elseif($user->is_active == false){
                    return redirect()->back()->withErrors('Akun Anda Belum Aktif')->withInput();
                }
                Auth::attempt(['email' => $username, 'password' => $password]);
            }else{
                $message = 'Email Tidak Ditemukan';
            }
        } else {
            $user = User::where('username', $username)->first();
            if($user){
                if($user->level == 'User'){
                    return redirect()->back()->withErrors('Tidak Dapat Login Dengan Akun Ini')->withInput();
                }elseif($user->is_active == false){
                    return redirect()->back()->withErrors('Akun Anda Belum Aktif')->withInput();
                }
                Auth::attempt(['username' => $username, 'password' => $password]);
            }else{
                $message = 'Username Tidak Ditemukan';
            }
        }

        if ( Auth::check() ) {
            $user->last_session = session()->getId();
            $user->save();
            return redirect()->intended('/home');
        }
        return redirect()->back()->withErrors($message ? $message : "Password Salah")->withInput();
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
