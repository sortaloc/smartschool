<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use App\Models\UserProfile\UserProfile;
use App\User;
use App\Models\Konfigurasi\Jenjang;
use App\Models\Konfigurasi\MataPelajaran;
use App\Models\Guru\Guru;
use App\Models\Mentor\Mentor;
use App\Models\Mentor\MentorSpecialization;
use App\Models\Mentor\MentorSchedule;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function register(AdminRequest $request)
    {
        $avatar = $request->file("avatar")->store("user/avatar");
        $user = User::create([
            "name"      => $request->name,
            "username"  => strtoupper($request->username),
            "email"     => strtoupper($request->email),
            "password"  => bcrypt($request->password),
            "is_active" => 0,
            "avatar"    => $avatar
        ]);
        if($request->role == 1){
            $user->assignRole('Admin');
        }else{
            $user->assignRole('Super Admin');
        }
        return redirect('login')->with('success', 'User Berhasil Di Buat');
    }
}
