<?php

namespace App\Http\Requests\BankSoal;

use Illuminate\Foundation\Http\FormRequest;

class BankSoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "mapel_id"  => "required",
            "name"      => "required",
            "logo"      => "image|max:2000",
            "file"      => "required|mimes:pdf|max:20000"
        ];
    }
}
