<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ApiSaveAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request('soal_type') == 'File'){
            return [
                'soal_id' => 'required|exists:tugas_files,id',
                'soal_type' => 'required|in:Pilihan Ganda,Essay,File',
                'file' => 'required|file|max:50000'
            ];
        }
        if(request('soal_type') == 'Pilihan Ganda'){
            return [
                'soal_id' => 'required|exists:tugas_pilihans,id',
                'soal_type' => 'required|in:Pilihan Ganda,Essay,File',
                'answer' => 'required|in:a,b,c,d,e,A,B,C,D,E'
            ];
        }
        if(request('soal_type') == 'Essay'){
            return [
                'soal_id' => 'required|exists:tugas_essays,id',
                'soal_type' => 'required|in:Pilihan Ganda,Essay,File',
                'answer' => 'required_without:file|nullable|string|max:50000',
                'file' => 'required_without:answer|file|max:50000'
            ];
        }else{
            return [
                'soal_type' => 'required|in:Pilihan Ganda,Essay,File'
            ];
        }
    }
    public function messages()
    {
        return [
            'soal_type.required' => 'Tipe soal tidak boleh kosong',
            'soal_type.in' => 'Tipe soal harus diantara Pilihan Ganda, Essay atau File',

            'soal_id.required' => 'ID soal tidak boleh kosong',
            'soal_id.exists' => 'ID soal tidak ditemukan',

            'answer.required' => 'Jawaban tidak boleh kosong',
            'file.required' => 'Jawaban tidak boleh kosong',
            'answer.required_without' => 'Jawaban tidak boleh kosong',
            'file.required_without' => 'Jawaban tidak boleh kosong',
            'file.file' => 'Jawaban harus bertipe file seperti PDF, DOC, FOTO dll',
            'file.max' => 'Ukuran jawaban melebihi batas, 50MB untuk soal Upload File dan 50.000 karakter untuk soal Essay',
            'answer.in' => 'Jawaban harus diantara A, B, C, D, E',
            'answer.string' => 'Jawaban harus berupa string/teks',
            'answer.max' => 'Ukuran jawaban melebihi batas, 50MB untuk soal Upload File dan 50.000 karakter untuk soal Essay',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
