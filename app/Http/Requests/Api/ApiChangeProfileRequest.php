<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Referal;

class ApiChangeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rule1 = [
            'name' => 'required|string|max:255',
            'phone_number' => 'required|numeric|digits_between:5,15|unique:user_profiles,phone_number,'.auth()->user()->profile->id,
            'referal' => ['nullable', 'exists:users,username', new Referal],
            'date_of_birth' => 'date_format:Y-m-d',
            'gender' => 'in:L,P',
            'school_name' => 'max:200',
            'district_id' => 'exists:address_districts,id',
            'address' => 'max:1000',
            'kip_number' => 'max:50',
            'parents_phone_number' => 'numeric|digits_between:5,15',
            'avatar' => 'image|max:3000'
        ];

        $rule2 = [
            //siswa
            "nama"                      => "required|string|max:150",
            "NIPD"                      => "required|numeric",
            "tempat_lahir"              => "required|string",
            "tanggal_lahir"             => "required|string|date_format:Y-m-d",
            "jenis_kelamin"             => "required|string|in:Laki-laki,Perempuan",
            "agama"                     => "required|string|in:ISLAM,KRISTEN,KATHOLIK,HINDU,BUDHA,KHONG HUCU",
            "status_dalam_keluarga"     => "required|string|max:150",
            "anak_ke"                   => "required|string|max:50",
            "alamat"                    => "required|string",
            "kecamatan"                 => "required|string|exists:address_districts,id",
            "desa"                      => "required|string|max:150",
            "kode_pos"                  => "required|string",
            "no_telp"                   => "required|string",
            'foto'                      => 'image|max:3000'
            /* //orang tua
            "nama_ayah"                 => "nullable|string|max:150",
            "nama_ibu"                  => "nullable|string|max:150",
            "alamat_orang_tua"          => "nullable|string|max:150",
            "kecamatan_orang_tua"       => "nullable|string|exists:address_districts,id",
            "desa_orang_tua"            => "nullable|string|max:150",
            "kode_pos_orang_tua"        => "nullable|string|max:10",
            "no_telp_orang_tua"         => "nullable|string|max:20",
            "pekerjaan_ayah"            => "nullable|string|max:150",
            "pekerjaan_ibu"             => "nullable|string|max:150",
            //wali
            "nama_wali"                 => "nullable|string|max:150",
            "alamat_wali"               => "nullable|string|max:150",
            "kecamatan_wali"            => "nullable|string|exists:address_districts,id",
            "desa_wali"                 => "nullable|string|max:150",
            "kode_pos_wali"             => "nullable|string|max:10",
            "no_telp_wali"              => "nullable|string|max:20",
            "pekerjaan_wali"            => "nullable|string|max:150", */
        ];

        if(auth()->user()->level == 'Siswa'){
            return $rule2;
        }
        return $rule1;
    }
    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'name.max' => 'Nama tidak boleh melebihi 255 karakter',
            'phone_number.required' => 'Nomor handphone tidak boleh kosong',
            'phone_number.numeric' => 'Nomor handphone harus berupa angka',
            'phone_number.digits_between' => 'Nomor handphone harus diantara 5 - 15 digit',
            'phone_number.unique' => 'Nomor handphone sudah digunakan',
            'referal.exists' => 'Kode referal tidak dapat digunakan',
            'parents_phone_number.numeric' => 'Nomor handphone harus berupa angka',
            'parents_phone_number.digits_between' => 'Nomor handphone harus diantara 5 - 15 digit',
            'date_of_birth.date_format' => 'Format tanggal lahir harus (Tahun-Bulan-Tanggal)',
            'gender.in' => 'Jenis kelamin harus diantara L atau P',
            'school_name.max' => 'Nama sekolah tidak boleh lebih dari 200 karakter',
            'district_id.exists' => 'Kecamatan tidak ditemukan',
            'address.max' => 'Alamat tidak boleh lebih dari 1000 karakter',
            'kip_number.max' => 'Nomor KIP tidak boleh lebih dari 50 karakter',
            'avatar.image' => 'Avatar harus bertipe foto',
            'avatar.max' => 'Avatar tidak boleh lebih dari 3 MB',

        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
