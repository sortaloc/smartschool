<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Events\EventQuestion;

class ApiSaveEventQuestionAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $question = EventQuestion::find(request('question_id'));
        if(!$question){
            return [
                'question_id' => 'bail|required|exists:event_questions,id',
            ];
        }else if($question->event->question_type == 'P'){
            return [
                'question_id' => 'bail|required|exists:event_questions,id',
                'option_answer' => 'bail|required|in:A,B,C,D,E'
            ];
        }else if($question->event->question_type == 'E'){
            return [
                'question_id' => 'bail|required|exists:event_questions,id',
                'text_answer' => 'bail|required_without:file_answer|string|max:50000',
                'file_answer' => 'bail|required_without:text_answer|mimes:doc,docx,ppt,xls,pdf,csv,txt,mp4,mkv,avi,mpeg,wmp,3gp,flv,swf,png,jpg,jpeg|max:50000'
            ];
        }
    }
    public function messages()
    {
        return [
            'question_id.required' => 'Soal tidak boleh kosong',
            'question_id.exists' => 'Soal tidak ditemukan',
            'option_answer.required' => 'Jawaban tidak boleh kosong',
            'text_answer.required_without' => 'Salah satu jawaban teks atau file harus di isi',
            'file_answer.required_without' => 'Salah satu jawaban teks atau file harus di isi',
            'option_answer.in' => 'Jawaban harus diantara A, B, C, D, E',
            'file_answer.mimes' => 'Format file harus diantara doc,docx,ppt,xls,pdf,csv,txt,mp4,mkv,avi,mpeg,wmp,3gp,flv,swf,png,jpg,jpeg',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
