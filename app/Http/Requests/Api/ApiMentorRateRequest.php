<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ApiMentorRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mentor_id' => 'required|exists:users,id',
            'rating' => 'required|in:1,2,3,4,5',
            'description' => 'string|max:200',
        ];
    }
    public function messages()
    {
        return [
            'mentor_id.required' => 'ID mentor tidak boleh kosong',
            'mentor_id.exists' => 'ID mentor tidak terdaftar',
            'rating.required' => 'Rating mentor tidak boleh kosong',
            'rating.in' => 'Rating harus berisi angka diantara 1-5',
            'description.string' => 'Deskripsi harus berubah text'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
