<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ApiRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            "username"  => 'required|string|max:100|unique:users|alpha_dash',
            "referal"  => 'nullable|exists:users,username|different:username',
            'phone_number' => 'required|numeric|digits_between:5,15|unique:user_profiles',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|max:100',
            'password_confirmation' => 'required|same:password',
            'date_of_birth' => 'date',
            'gender' => 'in:L,P',
            'school_name' => 'max:200',
            'district_id' => 'required_with:referal|exists:address_districts,id',
            'address' => 'max:1000',
            'kip_number' => 'max:50',
            'parents_phone_number' => 'numeric|digits_between:5,15'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'name.max' => 'Nama tidak boleh melebihi 255 karakter',
            'username.required' => 'Username tidak boleh kosong',
            'username.unique' => 'Username sudah digunakan',
            'username.max' => 'Username tidak boleh melebihi 100 karakter',
            'username.alpha_dash' => 'Username hanya boleh berisi Huruf, Angka, Garis Tengah dan Garis Bawah',
            'referal.exists' => 'Referal tidak ditemukan',
            'referal.different' => 'Tidak boleh menggunakan referal anda sendiri',
            'phone_number.required' => 'Nomor handphone tidak boleh kosong',
            'phone_number.numeric' => 'Nomor handphone harus berupa angka',
            'phone_number.digits_between' => 'Nomor handphone harus diantara 5 - 15 digit',
            'phone_number.unique' => 'Nomor handphone sudah digunakan',
            'email.required' => 'Email tidak boleh kosong',
            'email.unique' => 'Email sudah digunakan',
            'email.max' => 'Email tidak boleh melebihi 255 karakter',
            'email.email' => 'Email tidak valid',
            'password.required' => 'Password tidak boleh kosong',
            'password.min' => 'Password tidak boleh kurang dari 6 karakter',
            'password.max' => 'Password tidak boleh lebih dari 100 karakter',
            'password_confirmation.required' => 'Konfirmasi password tidak boleh kosong',
            'password_confirmation.same' => 'Konfirmasi password harus sama dengan password',
            'parents_phone_number.numeric' => 'Nomor handphone harus berupa angka',
            'parents_phone_number.digits_between' => 'Nomor handphone harus diantara 5 - 15 digit',
            'date_of_birth.date' => 'Tanggal lahir tidak valid',
            'gender.in' => 'Jenis kelamin harus diantara L atau P',
            'school_name.max' => 'Nama sekolah tidak boleh lebih dari 200 karakter',
            'district_id.required_with' => 'Kecamatan tidak boleh kosong jika menggunakan kode referal',
            'district_id.exists' => 'Kecamatan tidak ditemukan',
            'address.max' => 'Alamat tidak boleh lebih dari 1000 karakter',
            'kip_number.max' => 'Nomor KIP tidak boleh lebih dari 50 karakter',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
