<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\formatErrors;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\IsTugasAlreadyTaken;
use App\Rules\IsTugasNotFinished;
use App\Rules\IsAllSoalFinished;

class ApiDoneAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tugas_id' => [
                'bail',
                'required',
                'exists:guru_tugas,id',
                new IsTugasAlreadyTaken,
                new IsTugasNotFinished,
                new IsAllSoalFinished,
            ],
        ];
    }
    public function messages()
    {
        return [
            'tugas_id.required' => 'ID tugas tidak boleh kosong',
            'tugas_id.exists' => 'ID tugas tidak ditemukan',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $error_message = (new ValidationException($validator))->errors();
        $error_code = $validator->failed();

        if(array_key_first($error_code['tugas_id']) == "App\\Rules\\IsAllSoalFinished"){
            $error_code = 123;
        }else{
            $error_code = 422;
        }

        $response = new JsonResponse([
                    'code' => $error_code,
                    'message' => $error_message['tugas_id'][0]
                ], 422);

        throw new ValidationException($validator, $response);
    }
}
