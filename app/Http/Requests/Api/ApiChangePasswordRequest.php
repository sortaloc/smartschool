<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\OldPassword;

class ApiChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $password = auth()->user()->password;
        return [
            'old_password' => ['required', new OldPassword],
            'password' => 'required|min:6|max:100|different:old_password',
            'password_confirmation' => 'required|same:password'
        ];
    }
    public function messages()
    {
        return [
            'old_password.required' => 'Password lama tidak boleh kosong',
            'old_password.same' => 'Password lama tidak sama',
            'password.required' => 'Password tidak boleh kosong',
            'password.min' => 'Password tidak boleh kurang dari 6 karakter',
            'password.max' => 'Password tidak boleh lebih dari 100 karakter',
            'password.different' => 'Password baru tidak boleh sama dengan password lama',
            'password_confirmation.required' => 'Konfirmasi password tidak boleh kosong',
            'password_confirmation.same' => 'Konfirmasi password harus sama dengan password',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
