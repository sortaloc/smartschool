<?php

namespace App\Http\Requests\Api;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Konfigurasi\Promo;
use App\Member;
use Illuminate\Http\Request;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $promo = Promo::where('paket_id', request('paket_id'))->get()->implode('code', ',');
        $member = Member::where('user_id', auth()->user()->id)->get()->implode('paket_id', ',');
        return [
            "paket_id" => "required|exists:pakets,id|notIn:".$member, 
            "promo_code" => "in:".$promo
        ];
    }
    public function messages()
    {
        return [
            'paket_id.required' => 'paket_id tidak boleh kosong',
            'paket_id.exists' => 'paket_id tidak ditemukan',
            'paket_id.notIn' => 'anda sudah berlangganan paket ini',
            'promo_code.in' => 'promo tidak ditemukan',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
