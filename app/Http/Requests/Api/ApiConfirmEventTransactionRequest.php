<?php

namespace App\Http\Requests\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ApiConfirmEventTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => 'bail|required|exists:event_transactions,id',
            'file' => 'bail|required|file|max:5000',

        ];
    }
    public function messages()
    {
        return [
            'transaction_id.required' => 'Transaksi tidak boleh kosong',
            'transaction_id.exists' => 'Transaksi tidak ditemukan',
            'file.required' => 'Bukti transfer tidak boleh kosong',
            'file.file' => 'Bukti transfer harus berupa file',
            'file.max' => 'Bukti transfer maksimal 5MB',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();
        $message = $errors->first();
        throw new HttpResponseException(
            response()->json(['code' => 422, 'message' => $message, 'errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
