<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\OldPassword;

class ChangeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(auth()->user()->level == 'Guru'){
            if(request('type') == 'change-profile'){
                return [
                    "email"                 => "required|string|unique:users,email,".auth()->user()->id,
                    "name"                  => "required|string",
                    "avatar"                => "image|max:5000",
                    "school_name"           => "required|max:100",
                    "mata_pelajaran"        => "required|max:100",
                    "nip"                   => "required|max:100",
                ];
            }elseif(request('type') == 'change-password'){
                return [
                    "old_password"          => [new OldPassword],
                    "new_password"          => 'required_with:old_password|different:old_password|string|min:6|regex:/[0-9]/|regex:/[a-z]/',
                    "confirm_new_password"  => 'same:new_password',
                ];
            }
        }
        if(request('type') == 'change-profile'){
            return [
                "email"                 => "required|string|unique:users,email,".auth()->user()->id,
                "name"                  => "required|string",
                "avatar"                => "image|max:5000",
            ];
        }elseif(request('type') == 'change-password'){
            return [
                "old_password"          => [new OldPassword],
                "new_password"          => 'required_with:old_password|different:old_password|string|min:6|regex:/[0-9]/|regex:/[a-z]/',
                "confirm_new_password"  => 'same:new_password',
            ];
        }
    }
    public function messages()
    {
        return [
            'email.required'                => 'Email Harus Di Isi',
            'email.unique'                  => 'Email Sudah Digunakan User Lain',
            'name.required'                 => 'Nama Harus Di Isi',
            'avatar.image'                  => 'Avatar harus bertipe foto',
            'avatar.max'                    => 'Avatar tidak boleh berukuran lebih dari 5MB',
            'new_password.required_with'    => 'Password Baru Harus Di Isi',
            'new_password.different'        => 'Password Baru Harus Berbeda Dengan Password Lama',
            'new_password.string'           => 'Format Password Baru Salah',
            'new_password.min'              => 'Password Baru Harus Lebih Dari 6 Karakter',
            'new_password.regex'            => 'Password Baru Kurang Kuat (Kombinasikan Huruf, Angka, Spesial Karakter, Huruf Besar)',
            'confirm_new_password.same'     => 'Konfirmasi Password Baru Tidak Cocok',
            'school_name.required'          => 'Nama Sekolah Harus Di Isi',
            'mata_pelajaran.required'       => 'Mata Pelajaran Harus Di Isi',
            'nip.required'                  => 'NIP Harus Di Isi',
        ];
    }
}
