<?php

namespace App\Http\Requests\Guru;

use Illuminate\Foundation\Http\FormRequest;

class TugasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"              => "required",
            "kelas"             => "required",
            "mata_pelajaran"    => "required",
            "semester"          => "required|in:1,2",
            "type"              => "required|in:Pilihan Ganda,Essay,File",
            "start"             => "required|date_format:Y-m-d\TH:i|after:2000-01-01T00:00|before:end",
            "end"               => "required|date_format:Y-m-d\TH:i|before:2030-01-01T00:00|after:start",
            "password"          => "required",
            "description"       => "required"
        ];
    }
    public function messages()
    {
        return [
            'start.required'        => 'Waktu mulai belum diisi',
            'start.before'          => 'Waktu mulai harus lebih kecil dari waktu selesai',
            'start.after'           => 'Waktu mulai harus lebih dari tahun 2000',
            'end.required'          => 'Waktu selesai belum diisi',
            'end.after'             => 'Waktu selesai harus lebih besar dari waktu mulai',
            'end.before'            => 'Waktu selesai harus kurang dari tahun 2030',

            'name.required'         => 'Nama tugas harus diisi',
            'kelas.required'        => 'Nama kelas harus diisi',
            'mata_pelajaran.required'   => 'Nama mata pelajaran harus diisi',
            'semester.required'     => 'Nama semester harus diisi',
            'type.required'         => 'Tipe tugas harus diisi',
            'password.required'     => 'Password tugas harus diisi',
            'description.required'  => 'Deskripsi tugas harus diisi'
        ];
    }
}
