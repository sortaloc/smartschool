<?php

namespace App\Http\Requests\Guru;

use Illuminate\Foundation\Http\FormRequest;

class TugasEssayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nomor_soal"        => "required|numeric|max:500",
            "soal"              => "max:50000",
            "soal_gambar"       => "image",
            "pemabahasan"       => "max:50000",
            "pembahasan_gambar" => "image",
            "pembahasan_pdf"    => "mimes:pdf|max:20000",
            "jawaban"           => "in:A,B,C,D,E",
            "poin_maksimal"     => "required|numeric|max:100"
        ];
    }
}
