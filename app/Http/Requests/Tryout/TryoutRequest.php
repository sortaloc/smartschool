<?php

namespace App\Http\Requests\TryOut;

use Illuminate\Foundation\Http\FormRequest;

class TryoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "soal"              => "max:50000",
            "soal_gambar"       => "image",
            "pemabahasan"       => "max:50000",
            "pembahasan_gambar" => "image",
            "pembahasan_pdf"    => "mimes:pdf|max:20000",
            "jawaban"           => "in:A,B,C,D,E",
            "option.a"          => "max:50000",
            "option.b"          => "max:50000",
            "option.c"          => "max:50000",
            "option.d"          => "max:50000",
            "option.e"          => "max:50000",
            "option_gambar.a"   => "image",
            "option_gambar.b"   => "image",
            "option_gambar.c"   => "image",
            "option_gambar.d"   => "image",
            "option_gambar.e"   => "image"
        ];
    }
}
