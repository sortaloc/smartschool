<?php

namespace App\Http\Requests\TryOut;

use Illuminate\Foundation\Http\FormRequest;

class TryoutListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "mapel_id"      => "required",
            "name"          => "required|string|max:150",
            "duration"      => "numeric|max:500"
        ];
    }
}
