<?php

namespace App\Http\Requests\Mentor;

use Illuminate\Foundation\Http\FormRequest;

class MentorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "senin1"    => "bail|required_with:senin2",
            "senin2"    => "bail|required_with:senin1|nullable|after:senin1",

            "selasa1"    => "bail|required_with:selasa2",
            "selasa2"    => "bail|required_with:selasa1|nullable|after:selasa1",

            "rabu1"    => "bail|required_with:rabu2",
            "rabu2"    => "bail|required_with:rabu1|nullable|after:rabu1",

            "kamis1"    => "bail|required_with:kamis2",
            "kamis2"    => "bail|required_with:kamis1|nullable|after:kamis1",

            "jumat1"    => "bail|required_with:jumat2",
            "jumat2"    => "bail|required_with:jumat1|nullable|after:jumat1",

            "sabtu1"    => "bail|required_with:sabtu2",
            "sabtu2"    => "bail|required_with:sabtu1|nullable|after:sabtu1",

            "minggu1"    => "bail|required_with:minggu2",
            "minggu2"    => "bail|required_with:minggu1|nullable|after:minggu1"
        ];
    }
    public function messages()
    {
        return [
            'senin1.required_with'    => 'Jam mulai hari senin belum diisi',
            'senin2.required_with'    => 'Jam selesai hari senin belum diisi',
            'senin2.after'          => 'Jam selesai hari senin harus lebih besar dari jam mulai',

            
            'selasa1.required_with'    => 'Jam mulai hari selasa belum diisi',
            'selasa2.required_with'    => 'Jam selesai hari selasa belum diisi',
            'selasa2.after'          => 'Jam selesai hari selasa harus lebih besar dari jam mulai',

            
            'rabu1.required_with'    => 'Jam mulai hari rabu belum diisi',
            'rabu2.required_with'    => 'Jam selesai hari rabu belum diisi',
            'rabu2.after'          => 'Jam selesai hari rabu harus lebih besar dari jam mulai',

            
            'kamis1.required_with'    => 'Jam mulai hari kamis belum diisi',
            'kamis2.required_with'    => 'Jam selesai hari kamis belum diisi',
            'kamis2.after'          => 'Jam selesai hari kamis harus lebih besar dari jam mulai',

            
            'jumat1.required_with'    => 'Jam mulai hari jumat belum diisi',
            'jumat2.required_with'    => 'Jam selesai hari jumat belum diisi',
            'jumat2.after'          => 'Jam selesai hari jumat harus lebih besar dari jam mulai',

            
            'sabtu1.required_with'    => 'Jam mulai hari sabtu belum diisi',
            'sabtu2.required_with'    => 'Jam selesai hari sabtu belum diisi',
            'sabtu2.after'          => 'Jam selesai hari sabtu harus lebih besar dari jam mulai',

            
            'minggu1.required_with'    => 'Jam mulai hari minggu belum diisi',
            'minggu2.required_with'    => 'Jam selesai hari minggu belum diisi',
            'minggu2.after'          => 'Jam selesai hari minggu harus lebih besar dari jam mulai',
        ];
    }
}
