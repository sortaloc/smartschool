<?php

namespace App\Http\Requests\SmartSchools;

use Illuminate\Foundation\Http\FormRequest;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        if(request()->has('bulk_import')){
            return [
                "bulk_import" => "file|mimes:xlsx"
            ];
        }
        $rule = [
            //siswa
            "nama_guru"                 => "required|string|max:150",
            "NUPTK"                     => "nullable|numeric|digits_between:1,20",
            "tempat_lahir"              => "required|string|max:150",
            "tanggal_lahir"             => "required|string",
            "jenis_kelamin"             => "required|string|in:Laki-laki,Perempuan",
            "agama"                     => "required|string|in:ISLAM,KRISTEN,KATHOLIK,HINDU,BUDHA,KHONG HUCU",
            'jabatan_struktural'        => 'nullable|string|max:150',
            'tugas_tambahan'            => 'nullable|string|max:150',
            'golongan'                  => 'required|string|max:150',
            'no_hp'                     => 'required|numeric',

        ];

        if($this->getMethod() == 'POST'){
            $rule += [
                "NIP_NIK"               => "required|numeric|unique:users,username|digits_between:5,20",
                "email"                 => "required|string|unique:users",
                "foto"                  => "required|image",
                "kelas"                 => "array",
                "mata_pelajaran"        => "array",
                "kelas.*"               => "required_with:mata_pelajaran.*|nullable|exists:school_classes,id",
                "mata_pelajaran.*"      => "required_with:kelas.*|nullable|exists:school_subjects,id"
            ];
        }elseif($this->getMethod() == 'PATCH'){
            $rule += [
                "NIP_NIK"               => "required|numeric|digits_between:1,20",
                "foto"                  => "image"
            ];
        }
        return $rule;
    }
}
