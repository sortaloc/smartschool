<?php

namespace App\Http\Requests\SmartSchools;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        if(request()->get('update_status') != null || request()->get('update_status') == 1){
            return [
                "update_status" => "in:0,1"
            ];
        }
        $rule = [
            //siswa
            "nama_siswa"                => "required|string|max:150",
            "NIPD"                      => "required|numeric",
            "tempat_lahir"              => "required|string",
            "tanggal_lahir"             => "required|string",
            "jenis_kelamin"             => "required|string",
            "agama"                     => "required|string|in:ISLAM,KRISTEN,KATHOLIK,HINDU,BUDHA,KHONG HUCU",
            "status_dalam_keluarga"     => "required|string|max:150",
            "anak_ke"                   => "required|string|max:50",
            "alamat_siswa"              => "required|string",
            "kecamatan_siswa"           => "required|string|exists:address_districts,id",
            "desa_siswa"                => "required|string|max:150",
            "kode_pos_siswa"            => "required|string",
            "no_telp_siswa"             => "required|string",
            "asal_sekolah"              => "required|string",
            "kelas_diterima"            => "required|string|exists:school_classes,id",
            "tanggal_diterima"          => "required|string",
            "kelas"                     => "required|string|exists:school_classes,id",
            "angkatan"                  => "required|string|exists:school_generations,id",
            //orang tua
            "nama_ayah"                 => "nullable|string|max:150",
            "nama_ibu"                  => "nullable|string|max:150",
            "alamat_orang_tua"          => "nullable|string|max:150",
            "kecamatan_orang_tua"       => "nullable|string|exists:address_districts,id",
            "desa_orang_tua"            => "nullable|string|max:150",
            "kode_pos_orang_tua"        => "nullable|string|max:10",
            "no_telp_orang_tua"         => "nullable|string|max:20",
            "pekerjaan_ayah"            => "nullable|string|max:150",
            "pekerjaan_ibu"             => "nullable|string|max:150",
            //wali
            "nama_wali"                 => "nullable|string|max:150",
            "alamat_wali"               => "nullable|string|max:150",
            "kecamatan_wali"            => "nullable|string|exists:address_districts,id",
            "desa_wali"                 => "nullable|string|max:150",
            "kode_pos_wali"             => "nullable|string|max:10",
            "no_telp_wali"              => "nullable|string|max:20",
            "pekerjaan_wali"            => "nullable|string|max:150",
        ];

        if($this->getMethod() == 'POST'){
            $rule += [
                "NISN"                  => "required|numeric|unique:users,username",
                "email"                 => "required|string|unique:users",
                "foto_siswa"            => "required|image"
            ];
        }elseif($this->getMethod() == 'PATCH'){
            $student = request()->route()->parameter('school_student');
            $rule += [
                "NISN"                  => "required|numeric|unique:users,username,".$student->user_id,
                "email"                 => "required|string|unique:users,email,".$student->user_id,
                "foto_siswa"            => "image"
            ];
        }
        if(request()->has('bulk_import')){
            return [
                "bulk_import" => "file|mimes:xlsx"
            ];
        }
        return $rule;
    }
}
