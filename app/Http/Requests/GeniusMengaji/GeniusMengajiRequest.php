<?php

namespace App\Http\Requests\GeniusMengaji;

use Illuminate\Foundation\Http\FormRequest;

class GeniusMengajiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "logo"      => "required|image",
            "file"      => "required|mimetypes:video/x-flv,video/mp4,application/x-mpegURL,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv"
        ];
    }
}
