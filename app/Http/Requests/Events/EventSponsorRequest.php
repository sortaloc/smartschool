<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventSponsorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "name"              => "required|string|max:200",
                    "website"           => "required|string|max:200",
                    "banner"            => "required|image|max:10000",
                    "description"       => "required|string|max:50000",
                ];
            case 'PATCH':
                return [
                    "name"              => "required|string|max:200",
                    "website"           => "required|string|max:200",
                    "banner"            => "image|max:10000",
                    "description"       => "required|string|max:50000",
                ];
        }
        return [
            //
        ];
    }
}
