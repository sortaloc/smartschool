<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           if($this->getMethod() == 'POST' || $this->getMethod() == 'DELETE')
           return $user->hasRole(['Event', 'Super Admin']);
           if($this->getMethod() == 'PATCH')
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "name"              => "required|string|max:200",
                    "description"       => "required|string|max:50000",
                    "question_type"     => "required|in:P,E",
                    "start_at"          => "required|date_format:Y-m-d\TH:i|after:2000-01-01T00:00|before:2030-01-01T00:00",
                    "duration"          => "required|numeric",
                    "quota"             => "required|numeric",
                    "price"             => "required|numeric",
                    "discount"          => "required|numeric",
                    "banner"            => "required|image"
                ];
            case 'PATCH':
                switch (request()->get('type'))
                {
                    case 'change_status':
                        return [
                            "is_visible"    => "required|in:0,1"
                        ];
                    case 'notification':
                        return [

                        ];
                }
                return [
                    "name"              => "required|string|max:200",
                    "description"       => "required|string|max:50000",
                    "start_at"          => "required|date_format:Y-m-d\TH:i|after:2000-01-01T00:00|before:2030-01-01T00:00",
                    "duration"          => "required|numeric",
                    "quota"             => "required|numeric",
                    "price"             => "required|numeric",
                    "discount"          => "required|numeric"
                ];
        }
        return [
            //
        ];
    }
}
