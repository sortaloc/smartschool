<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventQuestionOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "event_id"          => "required|exists:events,id",
                    "event_category_id" => "required|exists:event_categories,id",
                    "question_number"   => "required|numeric|min:1",
                    "question"          => "required|max:50000",
                    "justification"     => "max:50000",
                    "option_a"          => "required|max:50000",
                    "option_b"          => "required|max:50000",
                    "option_c"          => "required|max:50000",
                    "option_d"          => "required|max:50000",
                    "option_e"          => "max:50000",
                    "correct_answer"    => "required|in:A,B,C,D,E",
                ];
            case 'PATCH':
                return [
                    "question_number"   => "required|numeric|min:1",
                    "question"          => "required|max:50000",
                    "justification"     => "max:50000",
                    "option_a"          => "required|max:50000",
                    "option_b"          => "required|max:50000",
                    "option_c"          => "required|max:50000",
                    "option_d"          => "required|max:50000",
                    "option_e"          => "max:50000",
                    "correct_answer"    => "required|in:A,B,C,D,E",
                ];
        }
        return [
            //
        ];
    }
}
