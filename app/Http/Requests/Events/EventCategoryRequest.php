<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "name"              => "required|string|max:200",
                    "event_id"          => "required|exists:events,id",
                    "correct_answer"    => "required|numeric",
                    "wrong_answer"      => "required|numeric",
                    "no_answer"         => "required|numeric"
                ];
            case 'PATCH':
                return [
                    "name"              => "required|string|max:200",
                    "correct_answer"    => "required|numeric",
                    "wrong_answer"      => "required|numeric",
                    "no_answer"         => "required|numeric"
                ];
        }
        return [
            //
        ];
    }
}
