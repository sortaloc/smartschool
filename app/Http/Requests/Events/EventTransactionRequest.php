<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                ];
            case 'PATCH':
                switch (request()->get('type'))
                {
                    case 'change_status':
                        return [
                            "status"    => "required|in:accept,decline"
                        ];
                }
                return [
                ];
        }
        return [
            //
        ];
    }
}
