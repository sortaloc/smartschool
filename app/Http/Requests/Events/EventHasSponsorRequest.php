<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventHasSponsorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "sponsor_id"              => "required|exists:event_sponsors,id",
                ];
            case 'PATCH':
                return [
                    "sponsor_id"              => "required|exists:event_sponsors,id",
                ];
            case 'DELETE':
                return [
                    
                ];
        }
        return [
            //
        ];
    }
}
