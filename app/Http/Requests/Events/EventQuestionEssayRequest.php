<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class EventQuestionEssayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(auth()->check())
        {
           $user = auth()->user();
           return $user->hasRole(['Event', 'Super Admin', 'Panitia Event']);
        }
        return false;
    }
    public function rules()
    {
        switch ($this->getMethod())
        {
            case 'POST':
                return [
                    "event_id"          => "required|exists:events,id",
                    "question_number"   => "required|numeric|min:1",
                    "question"          => "required|max:50000",
                    "justification"     => "max:50000",
                ];
            case 'PATCH':
                return [
                    "question_number"   => "required|numeric|min:1",
                    "question"          => "required|max:50000",
                    "justification"     => "max:50000",
                ];
        }
        return [
            //
        ];
    }
}
