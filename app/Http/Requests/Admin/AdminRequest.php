<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email"     => "required|email|unique:users",
            "name"      => "required|string|max:150",
            "username"  => "required|unique:users|alpha_dash|min:4|max:50",
            "role"      => "required|in:1,2",
            "avatar"    => "required|image|max:5000",
            "password"  => [
                'required',
                'string',
                'min:6',              // must be at least 6 characters in length
                'max:20',             // must not more than 20 characters
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                //'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            "konfirmasi_password" => "required|same:password"
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Email harus diisi',
            'email.email' => 'Format email salah',
            'email.unique' => 'Email sudah terdaftar',

            'username.required' => 'Email harus diisi',
            'username.alpha_dash' => 'Username hanya boleh berisi huruf, angka, garis tengah, dan garis bawah',
            'username.unique' => 'Username sudah digunakan',
            'username.min' => 'Username minimal 4 karakter',
            'username.max' => 'Username maksimal 50 karakter',

            'nomor_hp.required' => 'Nomor HP harus diisi',
            'nomor_hp.unique' => 'Nomor HP sudah digunakan',

            'pilih_job.required' => 'Job harus diisi',

            'lokasi_tugas.required' => 'Lokasi tugas harus diisi',

            'avatar.required' => 'Avatar harus diisi',
            'avatar.image' => 'Avatar harus berupa foto',
            'avatar.max' => 'Avatar maksimal berukuran 5MB',

            'password.required' => 'Password harus diisi',
            'password.string' => 'Password harus berupa karakter',
            'password.min' => 'Password minimal 6 karakter',
            'password.max' => 'Password maksimal 50 karakter',
            'password.regex' => 'Kombinasi password harus menyertakan huruf dan angka',
            
            'konfirmasi_password.required' => 'Konfirmasi password harus diisi',
            'konfirmasi_password.same' => 'Konfirmasi password tidak sama',
        ];
    }
}
