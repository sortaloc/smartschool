<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class ProvinceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    "name"      => "required",
                    "p_bsni"    => "required|max:5"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "name"      => "required",
                    "p_bsni"    => "required|max:5"
                ];
            }
            default:break;
        }
    }
}
