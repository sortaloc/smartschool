<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class BabRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    "mapel_id"  => "required",
                    "name"      => "required",
                    "logo"      => "required|image",
                    "number"    => "required|numeric"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "name"      => "required",
                    "logo"      => "image",
                    "number"    => "required|numeric"
                ];
            }
            default:break;
        }
    }
}
