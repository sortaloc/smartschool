<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class SchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nama_sekolah"      => "required|string|max:100",
            "NPSN"              => "nullable|string|max:100",
            "NIS/NSS/NDS"       => "nullable|string|max:100",
            "jumlah_akun"       => "required|integer|max:99999999999",
            "email"             => "required|unique:schools",
            "no_hp"             => "required|unique:schools,phone"
        ];
    }
}