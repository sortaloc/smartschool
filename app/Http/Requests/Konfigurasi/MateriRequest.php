<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class MateriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "bab_id"    => "required",
            "name"      => "required",
            "logo"      => "image",
            "is_free"   => "in:0,1",
            "video"     => "mimetypes:video/x-flv,video/mp4,application/x-mpegURL,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv",
            "number"    => "required|numeric"
        ];
    }
}