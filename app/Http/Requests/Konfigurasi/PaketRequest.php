<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class PaketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"              => "required",
            "banner"            => "required|image",
            "reference_name"    => "required|in:kelas,jenjang",
            "reference_id"      => "required",
            "nonactive_at"      => "required|date_format:Y-m-d",
            "price"             => "required|numeric",
            "expired_at"        => "required|date_format:Y-m-d"     
        ];
    }
}