<?php

namespace App\Http\Requests\Konfigurasi;

use Illuminate\Foundation\Http\FormRequest;

class HeadlineNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    "title"         => "required|string|max:200",
                    "headline_news" => "string|max:50000",
                    "image"         => "required|image|max:10000"
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    "title"         => "required|string|max:200",
                    "headline_news" => "string|max:50000",
                    "image"         => "nullable|image|max:10000"
                ];
            }
            default:break;
        }
    }
}
