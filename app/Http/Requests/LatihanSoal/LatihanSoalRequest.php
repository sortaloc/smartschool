<?php

namespace App\Http\Requests\LatihanSoal;

use Illuminate\Foundation\Http\FormRequest;

class LatihanSoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "soal"              => "max:50000",
            "soal_gambar"       => "image",
            "pemabahasan"       => "max:50000",
            "pembahasan_gambar" => "image",
            "pembahasan_pdf"    => "mimes:pdf|max:20000",
            "jawaban"           => "in:A,B,C,D,E",
            "option.A.text"     => "max:50000",
            "option.B.text"     => "max:50000",
            "option.C.text"     => "max:50000",
            "option.D.text"     => "max:50000",
            "option.E.text"     => "max:50000",
            "option.A.gambar"   => "image",
            "option.B.gambar"   => "image",
            "option.C.gambar"   => "image",
            "option.D.gambar"   => "image",
            "option.E.gambar"   => "image"
        ];
    }
}
