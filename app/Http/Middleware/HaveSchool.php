<?php
namespace App\Http\Middleware;
use Closure;
class HaveSchool
{
   public function handle($request, Closure $next)
   {
        if (!auth()->user()->profile->school_id) {
                return response()->json([
                    'code' => 401,
                    'message' => 'Anda tidak tergabung dalam sekolah manapun !'
                ], 401);
        }
        return $next($request);
   }
}