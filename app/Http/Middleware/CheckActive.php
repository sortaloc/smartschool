<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckActive
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            if (!Auth::user()->is_active || Auth::user()->last_session != session()->getId()) {
                Auth::logout();
                return redirect('/login');
            }
        }
        if(!Auth::user()){
            return redirect('/login');
        }
        return $next($request);
    }
}
