<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException || $e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                try {
                    JWTAuth::setToken(JWTAuth::refresh());
                    $user = JWTAuth::authenticate();
                    return response()->json([
                        'user' => $user->profile,
                        'token'=> JWTAuth::getToken()->get(),
                     ], 200);
                } catch (TokenExpiredException $e) {
                    $status     = 401;
                    $message    = 'Silahkan Login Kembali. Sesi Anda Telah Habis';
                    return response()->json(compact('status','message'),401);
                }
                catch (JWTException $e) {
                    $status     = 401;
                    $message    = 'Silahkan Login Kembali. Refresh Token Sudah Kadaluarsa';
                    return response()->json(compact('status','message'),401);
                }          
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                $status     = 401;
                $message    = 'Token Tidak Valid. Silahkan Login';
                return response()->json(compact('status','message'),401);
            }else{
                $status     = 422;
                $message    = 'Token Harus Di Isi';
                return response()->json(compact('status','message'),422);
            }
        }
       return $next($request);
    }
}