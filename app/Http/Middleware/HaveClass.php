<?php
namespace App\Http\Middleware;
use Closure;
class HaveClass
{
   public function handle($request, Closure $next)
   {
        if (!auth()->user()->profile->class->first()) {
                return response()->json([
                    'code' => 401,
                    'message' => 'Anda tidak tergabung dalam kelas manapun !'
                ], 401);
        }
        return $next($request);
   }
}