<?php
namespace App\Http\Middleware;
use Closure;
use Response;
class IsMySchoolActive
{
   public function handle($request, Closure $next)
   {
        if (isMySchoolActive()) {
            return $next($request);
        }
        return Response::view('pages.smart_school.school.expired');
   }
}