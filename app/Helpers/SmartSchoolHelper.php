<?php
function isMySchool($school_id){ 
    if(isset(auth()->user()->school_admin->school_id)){
        if($school_id != auth()->user()->school_admin->school_id){
            abort(403, 'Anda Tidak Diperbolehkan Mengakses Informasi Sekolah Ini !');
        }
    }elseif(isset(auth()->user()->school_teacher->school_id)){
        if($school_id != auth()->user()->school_teacher->school_id){
            abort(403, 'Anda Tidak Diperbolehkan Mengakses Informasi Sekolah Ini !');
        }
    }
}
function mySchool(){ 
    if(isset(auth()->user()->school_admin->school_id)){
        return auth()->user()->school_admin->school_id;
    }elseif(isset(auth()->user()->school_teacher->school_id)){
        return auth()->user()->school_teacher->school_id;
    }
    abort(403);
}
function myClass(){ 
    if(auth()->user()->profile->class->first()){
        return auth()->user()->profile->class->first()->class->id;
    }
    return null;
}

function mySchoolYear(){ 
    $school_id = mySchool();
    $year = \App\Models\SmartSchools\SchoolYear::where('school_id', $school_id)->where('is_active', 1)->select('id', 'start_year', 'end_year', 'semester')->first();
    
    if($year){
        return $year;
    }
    return redirect()->back()->withErrors(['Silahkan Atur Tahun Ajaran Terlebih Dahulu'])->withInput()->send();
}
function isMySchoolActive(){ 
    if(isset(auth()->user()->school_teacher->school)){
        return auth()->user()->school_teacher->school->getOriginal('expired_at') > now();
    }elseif(isset(auth()->user()->school_admin->school)){
        return auth()->user()->school_admin->school->getOriginal('expired_at') > now();
    }
    return false;
}