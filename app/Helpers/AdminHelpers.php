<?php

namespace App\Helpers;

use App\Models\UserProfile\UserProfile;
use Illuminate\Support\Facades\Auth;
use Request;

class AdminHelpers
{
    public static function getAuthName()
    {
        $profile = auth()->user();
        if($profile->hasRole('Super Admin Sekolah')){
            return $profile->name ? $profile->name : $profile->school_admin->school->name;
        }
        return isset($profile) ? $profile->name : 'Admin';
    }

    public static function getAuthLevel()
    {
        return auth()->user()->getRoleNames()->first();
    }

    public static function getAuthAvatar()
    {
        $profile = auth()->user();
        if(isset($profile->avatar)){
            return $profile->avatar;
        }elseif($profile->hasRole('Super Admin Sekolah')){
            return $profile->school_admin->school->logo;
        }
        return Request::root().'/template/images/admin-avatar.png';
    }
}
