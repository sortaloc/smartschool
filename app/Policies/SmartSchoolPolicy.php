<?php

namespace App\Policies;

use App\Models\SmartSchools\School;
use App\Models\SmartSchools\SchoolTeacher;
use App\Models\SmartSchools\SchoolAdmin;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SmartSchoolPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function view($user, $school)
    {
        $authorize = false;
        if(auth()->user()->has('school_admin')){
            if($school->id === auth()->user()->school_admin->school_id){
                $authorize = true;
            }
        }elseif(auth()->user()->has('school_teacher')){
            if($school->id === auth()->user()->school_teacher->school_id){
                $authorize = true;
            }
        }
        return $authorize === true
                    ? Response::allow()
                    : Response::deny('Anda Tidak Di Perbolehkan Melakukan Tindakan Ini.');
    }
}
