<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Guru\RekapNilai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SmartSchoolRekapNilaiExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $datas;
    protected $tasks;
    public function __construct($tasks, $datas)
    {
        $this->datas = $datas;
        $this->tasks = $tasks;
    }
    public static function afterSheet(AfterSheet $event)
    {
    }
    public function view(): View
    {
        return view('pages.smart_school.task.result.export', [
            'datas' => $this->datas,
            'tasks' => $this->tasks
        ]);

    }
}
