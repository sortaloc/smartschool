<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Guru\RekapNilai;
use App\Models\Guru\Guru;
use App\Models\Guru\TugasPilihan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class HasilTugasExport implements FromView, WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $tugas;
    protected $questions;
    protected $answers;
    public function __construct($tugas, $questions, $answers)
    {
        $this->tugas = $tugas;
        $this->questions = $questions;
        $this->answers = $answers;
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $answer_column2 = 12;
                $style2 = [];
                $point_column = 'D';
                $total_participant = $this->tugas->take->count();
                for($x = 0; $x < $this->questions->count(); $x++){
                    $point_column++;
                }
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],

                    ]
                ];
                if($this->tugas->type == 'Pilihan Ganda'){
                    foreach ($this->tugas->take as $key => $d) {
                        $answer_column1 = 'D';
                        foreach($this->questions as $x){
                            //cek jawaban
                            if(isset($this->answers[$d->siswa_id][$x->id])){
                                if($this->answers[$d->siswa_id][$x->id][0]['answer'] == $x->jawaban){
                                    $answerColor = [
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'color' => array('rgb' => '00FF00')
                                        ],
                                    ];
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->siswa_id][$x->id][0]['answer']);
                                }else{
                                    //jawaban salah
                                    $answerColor = [
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'color' => array('rgb' => 'FF0000')
                                        ],
                                    ];
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->siswa_id][$x->id][0]['answer']);
                                }
                            }else{
                                //tidak dijawab
                                $answerColor = [
                                    'fill' => [
                                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                        'color' => array('rgb' => '333333')
                                    ],
                                ];
                                $event->sheet->setCellValue($answer_column1.$answer_column2, 'K');
                            }
                            //nomor
                            $event->sheet->setCellValue('A'.$answer_column2, $answer_column2-11);
                            //nama
                            $event->sheet->setCellValue('B'.$answer_column2, $d->siswa->profile->name);
                            //hp
                            $event->sheet->setCellValue('C'.$answer_column2, $d->siswa->profile->phone_number);
                            $event->sheet->horizontalAlign('C'.$answer_column2 , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $event->sheet->styleCells($answer_column1.$answer_column2, $answerColor);
                            $answer_column1++;
                        }
                        //nilai
                        $event->sheet->setCellValue($answer_column1.$answer_column2, $d->point);
                        $answer_column2++;
                        $event->sheet->horizontalAlign('D12:'.$point_column.($total_participant+12), \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    }
                    $event->sheet->styleCells('A10:'.$point_column.(11+$total_participant), $styleArray);
                }elseif($this->tugas->type == 'Essay'){
                    $no = 1;
                    foreach ($this->tugas->take as $key => $d) {
                        $answer_column1 = 'D';
                        foreach($this->questions as $x){
                            if(isset($this->answers[$d->siswa_id][$x->id])){
                                if($this->answers[$d->siswa_id][$x->id][0]['file']){
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->siswa_id][$x->id][0]['file']);
                                    $event->sheet->getCell($answer_column1.$answer_column2)->getHyperlink()->setUrl($this->answers[$d->siswa_id][$x->id][0]['file']);
                                }else{
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                                }
                                if($this->answers[$d->siswa_id][$x->id][0]['answer']){
                                    $event->sheet->setCellValue($answer_column1.($answer_column2+1), $this->answers[$d->siswa_id][$x->id][0]['answer']);
                                    $event->sheet->getDelegate()->getRowDimension(($answer_column2+1))->setRowHeight(-1);
                                    $event->sheet->getDelegate()->getStyle($answer_column1.($answer_column2+1))->getAlignment()->setWrapText(true);
                                }else{
                                    $event->sheet->setCellValue($answer_column1.($answer_column2+1), '-');
                                }
                            }else{
                                $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                                $event->sheet->setCellValue($answer_column1.($answer_column2+1), '-');
                            }
                            $event->sheet->getDelegate()->getColumnDimension($answer_column1)->setWidth(35);
                            $answer_column1++;
                        }
                        $style2 = array_merge($style2, [
                            'A'.$answer_column2.':A'.($answer_column2+1),
                            'B'.$answer_column2.':B'.($answer_column2+1),
                            'C'.$answer_column2.':C'.($answer_column2+1),
                            $point_column.$answer_column2.':'.$point_column.($answer_column2+1)
                        ]);
                        $event->sheet->horizontalAlign($point_column.$answer_column2, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $event->sheet->setCellValue('A'.$answer_column2, $no);
                        $event->sheet->setCellValue('B'.$answer_column2, $d->siswa->profile->name);
                        $event->sheet->setCellValue('C'.$answer_column2, $d->siswa->profile->phone_number);
                        $event->sheet->setCellValue($answer_column1.$answer_column2, $d->point);
                        $answer_column2 = $answer_column2 + 2;
                        $no++;
                    }
                    $event->sheet->styleCells('A10:'.$point_column.(11+($total_participant*2)), $styleArray);
                }elseif($this->tugas->type == 'File'){
                    $no = 1;
                    foreach ($this->tugas->take as $key => $d) {
                        $answer_column1 = 'D';
                        foreach($this->questions as $x){
                            if(isset($this->answers[$d->siswa_id][$x->id])){
                                if($this->answers[$d->siswa_id][$x->id][0]['file']){
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->siswa_id][$x->id][0]['file']);
                                    $event->sheet->getCell($answer_column1.$answer_column2)->getHyperlink()->setUrl($this->answers[$d->siswa_id][$x->id][0]['file']);
                                }else{
                                    $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                                }
                            }else{
                                $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                            }
                            $answer_column1++;
                        }
                        $event->sheet->horizontalAlign($point_column.$answer_column2, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $event->sheet->setCellValue('A'.$answer_column2, $no);
                        $event->sheet->setCellValue('B'.$answer_column2, $d->siswa->profile->name);
                        $event->sheet->setCellValue('C'.$answer_column2, $d->siswa->profile->phone_number);
                        $event->sheet->setCellValue($answer_column1.$answer_column2, $d->point ? $d->point : 0);
                        $answer_column2++;
                        $no++;
                    }
                    $event->sheet->styleCells('A10:'.$point_column.(11+$total_participant), $styleArray);
                }
                $answer_column1 = chr(ord($answer_column1) - 1);
                $style1 = [
                    'A1:I1',
                    'A3:B3',
                    'A4:B4',
                    'A5:B5',
                    'A6:B6',
                    'A7:B7',
                    'A8:B8',
                    'C2:E2',
                    'C3:E3',
                    'C4:z4',
                    'C5:E5',
                    'C6:E6',
                    'C7:E7',
                    'C8:E8',
                    'A10:A11', 'B10:B11', 'C10:C11',
                    $point_column.'10:'.$point_column.'11',
                    'D10:'.$answer_column1.'10'
                ];
                $style = array_merge($style1, $style2);
                $event->sheet->getDelegate()->setMergeCells($style);
                $event->sheet->horizontalAlign('A10:'.$point_column.'11', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->horizontalAlign('A10:A1000', \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(35);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(17);
                $event->sheet->horizontalAlign('C2:C'.(12+$total_participant) , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->sheet->horizontalAlign('A10:C10' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->verticalAlign('A10:'.$point_column.(11+($total_participant*2)), \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            }
        ];
    }
    public function view(): View
    {
        return view('pages.guru.tugas.hasil_export', [
            'questions' => $this->questions,
            'tugas' => $this->tugas,
            'guru' => Guru::find($this->tugas->guru_id)
        ]);
    }
}
