<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use App\Models\Guru\RekapNilai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;

class RekapNilaiIndexExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->mergeCells('A1:E1');
    }
    public function view(): View
    {
        return view('pages.guru.rekap_nilai.index_export', [
            'data' => RekapNilai::with('siswa.profile')->where('finish', '!=', null)->whereHas('tugas', function($q){
                $q->where('guru_id', auth()->user()->guru->id);
            })->get()->groupBy('siswa_id')
        ]);

    }
}
