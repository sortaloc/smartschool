<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Guru\RekapNilai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RekapNilaiShowExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $siswa;
    public function __construct($siswa)
    {
        $this->siswa = $siswa;
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->mergeCells('A1:E1');
    }
    public function view(): View
    {
        return view('pages.guru.rekap_nilai.show_export', [
            'data' => RekapNilai::where('siswa_id', $this->siswa->user_id)->where('finish', '!=', null)->whereHas('tugas', function($q){
                $q->where('guru_id', auth()->user()->guru->id);
            })->get(),
            'siswa' => $this->siswa
        ]);

    }
}
