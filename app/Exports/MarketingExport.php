<?php

namespace App\Exports;

use App\Models\Marketing\MarketingWilayah;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MarketingExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function view(): View
    {
        return view('pages.marketing.data_marketing.export', [
            'data' => MarketingWilayah::get()
        ]);

    }
}
