<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SmartSchoolJournalExport implements WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $school;
    protected $journals;
    protected $filters;
    public function __construct($school, $journals, $filters)
    {
        $this->school = $school; 
        $this->journals = $journals; 
        $this->filters = $filters; 
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $headerTitle = array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  25,
                        'bold'      =>  true
                    )
                );

                $styleBorder = [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
    
                    ]
                ];

                //set field header
                $event->sheet->setCellValue('A1', 'JURNAL KELAS');
                $event->sheet->setCellValue('A2', 'Nama Sekolah');
                $event->sheet->setCellValue('A3', 'Kelas');
                $event->sheet->setCellValue('A4', 'Periode');
                $event->sheet->setCellValue('A5', 'Tahun Ajaran');
                //set value header
                $event->sheet->setCellValue('C2', $this->school->name);
                $event->sheet->setCellValue('C3', $this->filters['class']);
                $event->sheet->setCellValue('C4', $this->filters['periode']);
                $event->sheet->setCellValue('C5', $this->filters['school_year']);

                //set field data
                $event->sheet->setCellValue('A7', 'NO');
                $event->sheet->setCellValue('B7', 'Kelas');
                $event->sheet->setCellValue('C7', 'Hari');
                $event->sheet->setCellValue('D7', 'Waktu');
                $event->sheet->setCellValue('E7', 'Mata Pelajaran');
                $event->sheet->setCellValue('F7', 'Pengampu');
                $event->sheet->setCellValue('G7', 'Presensi');
                $event->sheet->setCellValue('H7', 'Materi');
                $event->sheet->setCellValue('I7', 'Catatan');
                //set value data
                $index = 0;
                foreach ($this->journals as $key => $value) {
                    $event->sheet->setCellValue('A'.(8+$index), $index+1);
                    $event->sheet->setCellValue('B'.(8+$index), $value->class_schedule->teacher_subjects->class->class_name);
                    $event->sheet->setCellValue('C'.(8+$index), $value->date->isoFormat('dddd, DD MMMM Y'));
                    $event->sheet->setCellValue('D'.(8+$index), $value->start_at.' - '.$value->end_at);
                    $event->sheet->setCellValue('E'.(8+$index), $value->class_schedule->teacher_subjects->subjects->name);
                    $event->sheet->setCellValue('F'.(8+$index), $value->class_schedule->teacher_subjects->teacher->name);
                    $event->sheet->setCellValue('G'.(8+$index), $value->student_presence_description);
                    $event->sheet->setCellValue('H'.(8+$index), $value->materi);
                    $event->sheet->setCellValue('I'.(8+$index), $value->note);
                    $index++;
                }
                //format
                $mergeCells = [
                    'A1:D1',
                    'A2:B2',
                    'A3:B3',
                    'A4:B4',
                    'A5:B5'
                ];
                $event->sheet->styleCells('A1', $headerTitle);
                $event->sheet->getDelegate()->setMergeCells($mergeCells);
                $event->sheet->styleCells('A7:I'.(8+$index-1), $styleBorder);
            }

        ];
    }
}
