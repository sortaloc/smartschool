<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\Events\Event;
use App\Models\Events\EventParticipant as EP;
use App\Models\Events\EventQuestionAnswer as EQA;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;

class EventParticipantExport implements FromView, WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $event;
    protected $participants;
    protected $sanswers;
    public function __construct($event, $participants, $answers)
    {
        $this->event = $event;
        $this->answers = $answers;
        $this->participants = $participants;
    }
    public function registerEvents(): array
       {
           return [
               AfterSheet::class => function(AfterSheet $event) {
                   // Merge Cells
                   $total_participants = $this->participants->count();
                   $question_column = 'K';
                   for($i=0; $i < count($this->event->event_questions)-1; $i++) {
                        $event->sheet->horizontalAlign($question_column.'8' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $question_column++; // Increment letter
                    }
                    $event->sheet->horizontalAlign($question_column.'8' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $point_column = $question_column;
                    $point_column++;
                    $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(5);
                    $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
                    $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(40);
                    $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(30);
                    $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(30);
                    $merge1 = ['A1:O1', 'A2:B2', 'A3:B3', 'A4:B4', 'A5:B5', 'A7:A8', 'B7:B8',
                    'C2:O2', 'C3:O3', 'C4:O4', 'C5:O5' , 'C7:C8', 'D7:D8', 'E7:E8', 'F7:F8', 'G7:G8', 'H7:H8', 'I7:I8', 'J7:J8',
                    'K7:'.$question_column.'7', $point_column.'7:'.$point_column.'8'
                    ];
                    $merge2 = [];
                    $event->sheet->getDelegate()->setMergeCells($merge1);
                    $style2 = [];
                    $styleArray = [
                        'font' => [
                            'bold' => true,
                        ],
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            ],

                        ]
                    ];
                    $styleHeader = [
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFAE00')
                        ],
                    ];
                    $styleStriped = [
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => array('rgb' => 'ECECEC')
                        ],
                    ];

                    $event->sheet->styleCells('A7'.':'.$point_column.'8', $styleHeader);

                        //warnai jawaban
                        $answer_column2 = 9;
                        $no = 1;
                        foreach ($this->participants as $key => $d) {
                            $answer_column1 = 'K';
                                foreach($this->event->event_questions as $x){
                                //cek jawaban
                                if($this->event->question_type == 'P'){
                                    if(isset($this->answers[$d->user_id][$x->id][0]['option_answer'])){
                                        if($this->answers[$d->user_id][$x->id][0]['option_answer'] == $x->correct_answer){
                                            $answerColor = [
                                                'fill' => [
                                                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                                    'color' => array('rgb' => '00FF00')
                                                ],
                                            ];
                                            $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->user_id][$x->id][0]['option_answer']);
                                        }else{
                                            //jawaban salah
                                            $answerColor = [
                                                'fill' => [
                                                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                                    'color' => array('rgb' => 'FF0000')
                                                ],
                                            ];
                                            $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->user_id][$x->id][0]['option_answer']);
                                        }
                                    }else{
                                        //tidak dijawab
                                        $answerColor = [
                                            'fill' => [
                                                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                                'color' => array('rgb' => '333333')
                                            ],
                                        ];
                                        $event->sheet->setCellValue($answer_column1.$answer_column2, 'K');
                                    }
                                    $event->sheet->styleCells($answer_column1.$answer_column2, $answerColor);
                                }else{
                                    if(isset($this->answers[$d->user_id][$x->id])){
                                        if($this->answers[$d->user_id][$x->id][0]['file_answer']){
                                            $event->sheet->setCellValue($answer_column1.$answer_column2, $this->answers[$d->user_id][$x->id][0]['file_answer']);
                                            $event->sheet->getCell($answer_column1.$answer_column2)->getHyperlink()->setUrl($this->answers[$d->user_id][$x->id][0]['file_answer']);
                                        }else{
                                            $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                                        }
                                        if($this->answers[$d->user_id][$x->id][0]['text_answer']){
                                            $event->sheet->setCellValue($answer_column1.($answer_column2+1), $this->answers[$d->user_id][$x->id][0]['text_answer']);
                                            $event->sheet->getDelegate()->getRowDimension(($answer_column2+1))->setRowHeight(-1);
                                            $event->sheet->getDelegate()->getStyle($answer_column1.($answer_column2+1))->getAlignment()->setWrapText(true);
                                        }else{
                                            $event->sheet->setCellValue($answer_column1.($answer_column2+1), '-');
                                        }
                                    }else{
                                        $event->sheet->setCellValue($answer_column1.$answer_column2, '-');
                                        $event->sheet->setCellValue($answer_column1.($answer_column2+1), '-');
                                    }
                                    $event->sheet->getDelegate()->getColumnDimension($answer_column1)->setWidth(35);
                                    $answer_column1++;
                                }
                                //nomor
                                if($this->event->question_type == 'P'){
                                    if(($answer_column2-8) % 2 == 0){
                                        $event->sheet->styleCells('A'.$answer_column2.':F'.($answer_column2), $styleStriped);
                                    }
                                    $event->sheet->setCellValue('A'.$answer_column2, $answer_column2-8);
                                    //username
                                    $event->sheet->setCellValue('B'.$answer_column2, $d->user->username);
                                    //nama
                                    $event->sheet->setCellValue('C'.$answer_column2, $d->user->profile->name);
                                    //email
                                    $event->sheet->setCellValue('D'.$answer_column2, $d->user->email);
                                    if($d->user->level == 'Siswa'){
                                        //phone
                                        $event->sheet->setCellValue('E'.$answer_column2, $d->user->profile->phone);
                                        //parent phone
                                        $event->sheet->setCellValue('F'.$answer_column2, $d->user->profile->parent ? $d->user->profile->parent->phone : '');
                                        //sekolah
                                        $event->sheet->setCellValue('G'.$answer_column2, $d->user->profile->school->name);
                                    }else{
                                        //phone
                                        $event->sheet->setCellValue('E'.$answer_column2, $d->user->profile->phone_number);
                                        //parent phone
                                        $event->sheet->setCellValue('F'.$answer_column2, $d->user->profile->parent_phone_number);
                                        //sekolah
                                        $event->sheet->setCellValue('G'.$answer_column2, $d->user->profile->school_name);
                                    }
                                    //kec
                                    $event->sheet->setCellValue('H'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->name : '' );
                                    //kota/kab
                                    $event->sheet->setCellValue('I'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->city->type.' '.$d->user->profile->district->city->name : '' );
                                    //provinsi
                                    $event->sheet->setCellValue('J'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->city->province->name : '' );
                                    $answer_column1++;
                                }
                            }
                            //nilai
                            if($this->event->question_type == 'P'){
                                $event->sheet->setCellValue($answer_column1.$answer_column2, $d->point);
                                if(($answer_column2-8) % 2 == 0){
                                    $event->sheet->styleCells($answer_column1.$answer_column2, $styleStriped);
                                }
                                $answer_column2++;
                            }

                            if($this->event->question_type == 'E'){
                                $merge2 = array_merge($merge2, [
                                    'A'.$answer_column2.':A'.($answer_column2+1),
                                    'B'.$answer_column2.':B'.($answer_column2+1),
                                    'C'.$answer_column2.':C'.($answer_column2+1),
                                    'D'.$answer_column2.':D'.($answer_column2+1),
                                    'E'.$answer_column2.':E'.($answer_column2+1),
                                    'F'.$answer_column2.':F'.($answer_column2+1),
                                    'G'.$answer_column2.':G'.($answer_column2+1),
                                    'H'.$answer_column2.':H'.($answer_column2+1),
                                    'I'.$answer_column2.':I'.($answer_column2+1),
                                    'J'.$answer_column2.':J'.($answer_column2+1),
                                    $point_column.$answer_column2.':'.$point_column.($answer_column2+1)
                                ]);
                                $event->sheet->getDelegate()->setMergeCells(array_merge($merge1,$merge2));
                                $event->sheet->setCellValue('A'.$answer_column2, $no);
                                //username
                                $event->sheet->setCellValue('B'.$answer_column2, $d->user->username);
                                //nama
                                $event->sheet->setCellValue('C'.$answer_column2, $d->user->profile->name);
                                //email
                                $event->sheet->setCellValue('D'.$answer_column2, $d->user->email);
                                if($d->user->level == 'Siswa'){
                                    //phone
                                    $event->sheet->setCellValue('E'.$answer_column2, $d->user->profile->phone);
                                    //parent phone
                                    $event->sheet->setCellValue('F'.$answer_column2, $d->user->profile->parent ? $d->user->profile->parent->phone : '');
                                    //sekolah
                                    $event->sheet->setCellValue('G'.$answer_column2, $d->user->profile->school->name);
                                }else{
                                    //phone
                                    $event->sheet->setCellValue('E'.$answer_column2, $d->user->profile->phone_number);
                                    //parent phone
                                    $event->sheet->setCellValue('F'.$answer_column2, $d->user->profile->parent_phone_number);
                                    //sekolah
                                    $event->sheet->setCellValue('G'.$answer_column2, $d->user->profile->school_name);
                                }
                                //kec
                                $event->sheet->setCellValue('H'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->name : '' );
                                //kota/kab
                                $event->sheet->setCellValue('I'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->city->type.' '.$d->user->profile->district->city->name : '' );
                                //provinsi
                                $event->sheet->setCellValue('J'.$answer_column2, $d->user->profile->district ? $d->user->profile->district->city->province->name : '' );
                                $event->sheet->setCellValue($answer_column1.$answer_column2, $d->point);
                                if($no % 2 == 0){
                                    $event->sheet->styleCells('A'.$answer_column2.':'.$answer_column1.($answer_column2+1), $styleStriped);
                                }
                                $answer_column1++;
                                $answer_column2 = $answer_column2 + 2;
                                $no++;
                            }
                        }
                        if($this->event->question_type == 'E'){
                            $event->sheet->styleCells('A7:'.$point_column.(8+($total_participants*2)), $styleArray);
                            $event->sheet->horizontalAlign('A9:'.$point_column.(8+($total_participants*2)), \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $event->sheet->horizontalAlign($point_column, \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        }
                        if($this->event->question_type == 'P'){
                            $event->sheet->styleCells('A7:'.$point_column.(8+$total_participants), $styleArray);
                            $event->sheet->horizontalAlign('K9:'.$point_column.(9+$total_participants) , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                            $event->sheet->horizontalAlign('A9:F'.(9+$total_participants), \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        }
                        $event->sheet->horizontalAlign('C4' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $event->sheet->horizontalAlign('C5' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $event->sheet->horizontalAlign('A7:'.$point_column.'8' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $event->sheet->verticalAlign('A7:'.$point_column.(9+($total_participants*2)) , \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                        $event->sheet->horizontalAlign('A9:A'.(9+($total_participants*2)) , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                },
           ];
       }
    public function view(): View
    {
        return view('pages.events.participant.export', [
            'participants' => EP::where('event_id', $this->event->id)->get(),
            'event' => $this->event,
            'answers' => $this->answers
        ]);

    }
}
