<?php

namespace App\Exports;

use App\Models\Marketing\MarketingWithdraw;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MarketingWithdrawExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function view(): View
    {
        return view('pages.marketing.marketing_withdraw.export', [
            'data' => MarketingWithdraw::get()
        ]);

    }
}
