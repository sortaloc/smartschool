<?php

namespace App\Exports;

use App\Models\Transaction\Transaction;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransactionExport implements FromView, ShouldAutoSize
{
    use Exportable;
    public function view(): View
    {
        return view('pages.transaction.transaction_export', [
            'user' => Transaction::get()
        ]);

    }
}
