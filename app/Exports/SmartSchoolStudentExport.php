<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SmartSchoolStudentExport implements WithEvents, ShouldAutoSize, WithColumnFormatting
{
    use Exportable, RegistersEventListeners;
    protected $school;
    protected $students;
    public function __construct($school, $students)
    {
        $this->school = $school; 
        $this->students = $students; 
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $headerTitle = array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  25,
                        'bold'      =>  true
                    )
                );

                $styleBorder = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
    
                    ]
                ];

                $styleHeader = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FF7000')
                    ],
                ];

                //set field header
                $event->sheet->setCellValue('A1', 'DATA PESERTA DIDIK');
                $event->sheet->setCellValue('A2', 'Nama Sekolah');
                $event->sheet->setCellValue('A3', 'NPSN');
                $event->sheet->setCellValue('A4', 'NIS/NSS/NDS');
                $event->sheet->setCellValue('A5', 'Alamat Sekolah');
                $event->sheet->setCellValue('A6', 'Telepon');
                $event->sheet->setCellValue('A7', 'Website');
                $event->sheet->setCellValue('A13', 'Email');
                //set value header
                $event->sheet->setCellValue('C2', $this->school->name);
                $event->sheet->setCellValue('C3', $this->school->NPSN);
                $event->sheet->setCellValue('C4', $this->school->NDS);
                $event->sheet->setCellValue('C5', $this->school->address);
                $event->sheet->setCellValue('C6', $this->school->phone);
                $event->sheet->setCellValue('C7', $this->school->website);
                $event->sheet->setCellValue('C13', $this->school->email);

                //set field data
                $event->sheet->setCellValue('A11', 'NO');
                $event->sheet->setCellValue('B11', 'Nama Lengkap');
                $event->sheet->setCellValue('C11', 'Email');
                $event->sheet->setCellValue('D11', 'NIPD');
                $event->sheet->setCellValue('E11', 'NISN');
                $event->sheet->setCellValue('F11', 'Tempat Lahir');
                $event->sheet->setCellValue('G11', 'Tanggal Lahir');
                $event->sheet->setCellValue('H11', 'Jenis Kelamin');
                $event->sheet->setCellValue('I11', 'Agama');
                $event->sheet->setCellValue('J11', 'Status Dalam Keluarga');
                $event->sheet->setCellValue('K11', 'Anak Ke');
                $event->sheet->setCellValue('L11', 'Alamat');
                $event->sheet->setCellValue('M11', 'Desa');
                $event->sheet->setCellValue('N11', 'Kecamatan');
                $event->sheet->setCellValue('O11', 'Kab/Kota');
                $event->sheet->setCellValue('P11', 'Provinsi');
                $event->sheet->setCellValue('Q11', 'Kode POS');
                $event->sheet->setCellValue('R11', 'NO HP');
                $event->sheet->setCellValue('S11', 'Asal Sekolah');
                $event->sheet->setCellValue('T11', 'Kelas Sekarang');//MERGE
                $event->sheet->setCellValue('T12', 'Kelas');
                $event->sheet->setCellValue('U12', 'Sub Kelas');
                $event->sheet->setCellValue('V11', 'Tahun Angkatan');
                $event->sheet->setCellValue('W11', 'Diterima Disekolah Ini');//MERGE
                $event->sheet->setCellValue('W12', 'Kelas');
                $event->sheet->setCellValue('X12', 'Sub Kelas');
                $event->sheet->setCellValue('Y12', 'Tanggal');

                $event->sheet->setCellValue('Z11', 'Nama Orang Tua');//MERGE
                $event->sheet->setCellValue('Z12', 'Ayah');
                $event->sheet->setCellValue('AA12', 'Ibu');
                $event->sheet->setCellValue('AB11', 'Alamat Orang Tua');//MERGE
                $event->sheet->setCellValue('AB12', 'Alamat');
                $event->sheet->setCellValue('AC12', 'Desa');
                $event->sheet->setCellValue('AD12', 'Kecamatan');
                $event->sheet->setCellValue('AE12', 'Kota/Kabupaten');
                $event->sheet->setCellValue('AF12', 'Provinsi');
                $event->sheet->setCellValue('AG12', 'Kode POS');
                $event->sheet->setCellValue('AH11', 'No Telp/HP Orang Tua');
                $event->sheet->setCellValue('AI11', 'Pekerjaan Orang Tua');//MERGE
                $event->sheet->setCellValue('AI12', 'Ayah');
                $event->sheet->setCellValue('AJ12', 'Ibu');
                
                $event->sheet->setCellValue('AK11', 'Nama Wali');
                $event->sheet->setCellValue('AL11', 'Alamat Wali');//MERGE
                $event->sheet->setCellValue('AL12', 'Alamat');
                $event->sheet->setCellValue('AM12', 'Desa');
                $event->sheet->setCellValue('AN12', 'Kecamatan');
                $event->sheet->setCellValue('AO12', 'Kota/Kabupaten');
                $event->sheet->setCellValue('AP12', 'Provinsi');
                $event->sheet->setCellValue('AQ12', 'Kode POS');
                $event->sheet->setCellValue('AR11', 'No Telp/HP Wali');
                $event->sheet->setCellValue('AS11', 'Pekerjaan Wali');
                //set value data
                $index = 0;
                foreach ($this->students as $key => $value) {
                    $event->sheet->setCellValue('A'.(13+$index), $index+1);
                    $event->sheet->setCellValue('B'.(13+$index), $value->name);
                    $event->sheet->setCellValue('C'.(13+$index), $value->user->email);
                    $event->sheet->setCellValue('D'.(13+$index), $value->NIPD);
                    $event->sheet->setCellValue('E'.(13+$index), $value->NISN);
                    $event->sheet->setCellValue('F'.(13+$index), $value->place_of_birth);
                    $event->sheet->setCellValue('G'.(13+$index), date('d/m/Y', strtotime($value->date_of_birth)));
                    $event->sheet->setCellValue('H'.(13+$index), $value->gender);
                    $event->sheet->setCellValue('I'.(13+$index), $value->religion);
                    $event->sheet->setCellValue('J'.(13+$index), $value->status_in_family);
                    $event->sheet->setCellValue('K'.(13+$index), $value->child_number);
                    $event->sheet->setCellValue('L'.(13+$index), $value->address);
                    $event->sheet->setCellValue('M'.(13+$index), $value->village);
                    $event->sheet->setCellValue('N'.(13+$index), $value->district ? $value->district->name : '');
                    $event->sheet->setCellValue('O'.(13+$index), $value->district ? $value->district->city->type.' - '.$value->district->city->name : '');
                    $event->sheet->setCellValue('P'.(13+$index), $value->district ? $value->district->city->province->name : '');
                    $event->sheet->setCellValue('Q'.(13+$index), $value->zip_code);
                    $event->sheet->setCellValue('R'.(13+$index), $value->phone);
                    $event->sheet->setCellValue('S'.(13+$index), $value->origin_school);
                    $event->sheet->setCellValue('T'.(13+$index), $value->class->first() ? $value->class->first()->class->class->name : '');
                    $event->sheet->setCellValue('U'.(13+$index), $value->class->first() ? $value->class->first()->class->sub_class->name : '');
                    $event->sheet->setCellValue('V'.(13+$index), $value->generation ? $value->generation->year : '');
                    $event->sheet->setCellValue('W'.(13+$index), $value->start_class ? $value->start_class->class->name : '');
                    $event->sheet->setCellValue('X'.(13+$index), $value->start_class ? $value->start_class->sub_class->name : '');
                    $event->sheet->setCellValue('Y'.(13+$index), date('d/m/Y', strtotime($value->register_date)));
                    
                    $event->sheet->setCellValue('Z'.(13+$index), $value->parent->father_name);
                    $event->sheet->setCellValue('AA'.(13+$index), $value->parent->mother_name);
                    $event->sheet->setCellValue('AB'.(13+$index), $value->parent->address);
                    $event->sheet->setCellValue('AC'.(13+$index), $value->parent->village);
                    $event->sheet->setCellValue('AD'.(13+$index), $value->parent->district ? $value->parent->district->name : '');
                    $event->sheet->setCellValue('AE'.(13+$index), $value->parent->district ? $value->parent->district->city->name : '');
                    $event->sheet->setCellValue('AF'.(13+$index), $value->parent->district ? $value->parent->district->city->province->name : '');
                    $event->sheet->setCellValue('AG'.(13+$index), $value->parent->zip_code);
                    $event->sheet->setCellValue('AH'.(13+$index), $value->parent->phone);
                    $event->sheet->setCellValue('AI'.(13+$index), $value->parent->father_job);
                    $event->sheet->setCellValue('AJ'.(13+$index), $value->parent->mother_job);
                    $event->sheet->setCellValue('AK'.(13+$index), $value->guardian->name);
                    $event->sheet->setCellValue('AL'.(13+$index), $value->guardian->address);
                    $event->sheet->setCellValue('AM'.(13+$index), $value->guardian->village);
                    $event->sheet->setCellValue('AN'.(13+$index), $value->guardian->district ? $value->guardian->district->name : '');
                    $event->sheet->setCellValue('AO'.(13+$index), $value->guardian->district ? $value->guardian->district->city->name : '');
                    $event->sheet->setCellValue('AP'.(13+$index), $value->guardian->district ? $value->guardian->district->city->province->name : '');
                    $event->sheet->setCellValue('AQ'.(13+$index), $value->guardian->zip_code);
                    $event->sheet->setCellValue('AR'.(13+$index), $value->guardian->phone);
                    $event->sheet->setCellValue('AS'.(13+$index), $value->guardian->job);
                    $index++;
                }
                //format
                $mergeCells = [
                    'A1:D1',
                    'A2:B2',
                    'A3:B3',
                    'A4:B4',
                    'A5:B5',
                    'C2:G2',
                    'C3:G3',
                    'C4:G4',
                    'C5:G5',
                    'C6:G6',
                    'C7:G7',
                    'T11:U11',
                    'W11:Y11',
                    'Z11:AA11',
                    'AB11:AG11',
                    'AI11:AJ11',
                    'AL11:AQ11',
                    'A11:A12',
                    'B11:B12',
                    'C11:C12',
                    'D11:D12',
                    'E11:E12',
                    'F11:F12',
                    'G11:G12',
                    'H11:H12',
                    'I11:I12',
                    'J11:J12',
                    'K11:K12',
                    'L11:L12',
                    'M11:M12',
                    'N11:N12',
                    'O11:O12',
                    'P11:P12',
                    'Q11:Q12',
                    'R11:R12',
                    'S11:S12',
                    'V11:V12',
                    'AH11:AH12',
                    'AK11:AK12',
                    'AR11:AR12',
                    'AS11:AS12',
                ];
                $event->sheet->styleCells('A1', $headerTitle);
                $event->sheet->getDelegate()->setMergeCells($mergeCells);
                $event->sheet->styleCells('A11:AS'.(13+$index-1), $styleBorder);
                $event->sheet->styleCells('A11:AS12', $styleHeader);
                $event->sheet->horizontalAlign('C2:G7' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->sheet->horizontalAlign('A11:AS12' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->verticalAlign('A11:AS12', \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            }

        ];
    }
    public function columnFormats(): array
    {
        $row = $this->students->count()+13;
        return [
            'B11:C'.$row => NumberFormat::FORMAT_GENERAL,
            'D' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_GENERAL,
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_GENERAL,
            'I13:P'.$row => NumberFormat::FORMAT_GENERAL,
            'Q13:R'.$row => NumberFormat::FORMAT_NUMBER,
            'S13:U'.$row => NumberFormat::FORMAT_GENERAL,
            'V' => NumberFormat::FORMAT_NUMBER,
            'W13:X'.$row => NumberFormat::FORMAT_GENERAL,
            'Y' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'Z13:AF'.$row => NumberFormat::FORMAT_GENERAL,
            'AG13:AH'.$row => NumberFormat::FORMAT_NUMBER,
            'AI13:AP'.$row => NumberFormat::FORMAT_GENERAL,
            'AQ13:AR'.$row => NumberFormat::FORMAT_NUMBER,
            'AS' => NumberFormat::FORMAT_GENERAL,
        ];
    }
}
