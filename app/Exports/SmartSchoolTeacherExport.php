<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SmartSchoolTeacherExport implements WithEvents, ShouldAutoSize, WithColumnFormatting
{
    use Exportable, RegistersEventListeners;
    protected $school;
    protected $teachers;
    public function __construct($school, $teachers)
    {
        $this->school = $school; 
        $this->teachers = $teachers; 
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $headerTitle = array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  25,
                        'bold'      =>  true
                    )
                );

                $styleBorder = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
    
                    ]
                ];

                $styleHeader = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => 'FF7000')
                    ],
                ];

                //set field header
                $event->sheet->setCellValue('A1', 'DATA GURU');
                $event->sheet->setCellValue('A2', 'Nama Sekolah');
                $event->sheet->setCellValue('A3', 'NPSN');
                $event->sheet->setCellValue('A4', 'NIS/NSS/NDS');
                $event->sheet->setCellValue('A5', 'Alamat Sekolah');
                $event->sheet->setCellValue('A6', 'Telepon');
                $event->sheet->setCellValue('A7', 'Website');
                $event->sheet->setCellValue('A13', 'Email');
                //set value header
                $event->sheet->setCellValue('C2', $this->school->name);
                $event->sheet->setCellValue('C3', $this->school->NPSN);
                $event->sheet->setCellValue('C4', $this->school->NDS);
                $event->sheet->setCellValue('C5', $this->school->address);
                $event->sheet->setCellValue('C6', $this->school->phone);
                $event->sheet->setCellValue('C7', $this->school->website);
                $event->sheet->setCellValue('C13', $this->school->email);

                //set field data
                $event->sheet->setCellValue('A11', 'NO');
                $event->sheet->setCellValue('B11', 'Nama Lengkap');
                $event->sheet->setCellValue('C11', 'Email');
                $event->sheet->setCellValue('D11', 'NIP');
                $event->sheet->setCellValue('E11', 'NUPTK');
                $event->sheet->setCellValue('F11', 'Tempat Lahir');
                $event->sheet->setCellValue('G11', 'Tanggal Lahir');
                $event->sheet->setCellValue('H11', 'Jenis Kelamin');
                $event->sheet->setCellValue('I11', 'Agama');
                $event->sheet->setCellValue('J11', 'Jabatan Struktural');
                $event->sheet->setCellValue('K11', 'Tugas Tambahan');
                $event->sheet->setCellValue('L11', 'Golongan');
                $event->sheet->setCellValue('M11', 'NO HP');
                $event->sheet->setCellValue('N11', 'Kelas Yang Diampu');
                //set value data
                $index = 0;
                $no = 1;
                foreach ($this->teachers as $key => $value) {
                    $event->sheet->setCellValue('A'.(13+$index), $no);
                    $event->sheet->setCellValue('B'.(13+$index), $value->name);
                    $event->sheet->setCellValue('C'.(13+$index), $value->user->email);
                    $event->sheet->setCellValue('D'.(13+$index), $value->NIPD);
                    $event->sheet->setCellValue('E'.(13+$index), $value->NISN);
                    $event->sheet->setCellValue('F'.(13+$index), $value->place_of_birth);
                    $event->sheet->setCellValue('G'.(13+$index), date('d/m/Y', strtotime($value->date_of_birth)));
                    $event->sheet->setCellValue('H'.(13+$index), $value->gender);
                    $event->sheet->setCellValue('I'.(13+$index), $value->religion);
                    $event->sheet->setCellValue('J'.(13+$index), $value->structural_position);
                    $event->sheet->setCellValue('K'.(13+$index), $value->additional_position);
                    $event->sheet->setCellValue('L'.(13+$index), $value->PNS_rank);
                    $event->sheet->setCellValue('M'.(13+$index), $value->phone);
                    foreach ($value->subjects->groupBy('school_subjects_id') as $key2 => $mapel) {
                        foreach($mapel as $value2){
                            $event->sheet->setCellValue('N'.(13+$index), $value2->subjects->name.' - Kelas '.$value2->class->class_name);
                            $index++;
                        }
                    }
                    $index++;
                    $no++;
                }
                //format
                $mergeCells = [
                    'A1:D1',
                    'A2:B2',
                    'A3:B3',
                    'A4:B4',
                    'A5:B5',
                    'C2:G2',
                    'C3:G3',
                    'C4:G4',
                    'C5:G5',
                    'C6:G6',
                    'C7:G7',
                    'A11:A12',
                    'B11:B12',
                    'C11:C12',
                    'D11:D12',
                    'E11:E12',
                    'F11:F12',
                    'G11:G12',
                    'H11:H12',
                    'I11:I12',
                    'J11:J12',
                    'K11:K12',
                    'L11:L12',
                    'M11:M12',
                    'N11:N12',
                ];
                $event->sheet->styleCells('A1', $headerTitle);
                $event->sheet->getDelegate()->setMergeCells($mergeCells);
                $event->sheet->styleCells('A11:N'.(13+$index-1), $styleBorder);
                $event->sheet->styleCells('A11:N12', $styleHeader);
                $event->sheet->horizontalAlign('C2:G7' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $event->sheet->horizontalAlign('A11:N12' , \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->verticalAlign('A11:N12', \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            }

        ];
    }
    public function columnFormats(): array
    {
        $row = $this->teachers->count()+13;
        return [
            'B11:C'.$row => NumberFormat::FORMAT_GENERAL,
            'D' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_GENERAL,
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_GENERAL,
            'I13:L'.$row => NumberFormat::FORMAT_GENERAL,
            'M' => NumberFormat::FORMAT_NUMBER,
            'N' => NumberFormat::FORMAT_GENERAL,
        ];
    }
}
