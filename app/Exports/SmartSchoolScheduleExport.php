<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SmartSchoolScheduleExport implements WithEvents
{
    use Exportable, RegistersEventListeners;
    protected $school;
    protected $schedules;
    protected $filters;
    public function __construct($school, $schedules, $filters)
    {
        $this->school = $school; 
        $this->schedules = $schedules; 
        $this->filters = $filters; 
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $headerTitle = array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  25,
                        'bold'      =>  true
                    )
                );

                $styleBorder = [
                    'font' => [
                        'bold' => true,
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
    
                    ]
                ];

                //set field header
                $event->sheet->setCellValue('A1', 'JADWAL KELAS');
                $event->sheet->setCellValue('A2', 'Nama Sekolah');
                $event->sheet->setCellValue('A3', 'Kelas');
                $event->sheet->setCellValue('A4', 'Tahun Ajaran');
                //set value header
                $event->sheet->setCellValue('C2', $this->school->name);
                $event->sheet->setCellValue('C3', $this->filters['class']);
                $event->sheet->setCellValue('C4', $this->filters['school_year']);

                //set field data
                $event->sheet->setCellValue('A7', 'NO');
                $event->sheet->setCellValue('B7', 'Hari');
                $event->sheet->setCellValue('C7', 'Waktu');
                $event->sheet->setCellValue('D7', 'Mata Pelajaran');
                $event->sheet->setCellValue('E7', 'Pengampu');
                $event->sheet->setCellValue('F7', 'Kelas');
                $event->sheet->setCellValue('G7', 'No HP');
                //set value data
                $index = 0;
                foreach ($this->schedules as $key => $value) {
                    $event->sheet->setCellValue('A'.(8+$index), $index+1);
                    $event->sheet->setCellValue('B'.(8+$index), $value->day);
                    $event->sheet->setCellValue('C'.(8+$index), $value->start_at.' - '.$value->end_at);
                    $event->sheet->setCellValue('D'.(8+$index), $value->teacher_subjects->subjects->name);
                    $event->sheet->setCellValue('E'.(8+$index), $value->teacher_subjects->teacher->name);
                    $event->sheet->setCellValue('F'.(8+$index), $value->teacher_subjects->class->class_name);
                    $event->sheet->setCellValue('G'.(8+$index), $value->teacher_subjects->teacher->phone);
                    $index++;
                }
                //format
                $mergeCells = [
                    'A1:D1',
                    'A2:B2',
                    'A3:B3',
                    'A4:B4',
                    'A5:B5'
                ];
                $event->sheet->styleCells('A1', $headerTitle);
                $event->sheet->getDelegate()->setMergeCells($mergeCells);
                $event->sheet->styleCells('A7:G'.(8+$index-1), $styleBorder);
            }

        ];
    }
}
