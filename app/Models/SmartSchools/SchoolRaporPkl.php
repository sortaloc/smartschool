<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRaporPkl extends Model
{
    protected $fillable = [
        'rapor_id', 'mitra', 'location', 'duration', 'description',
         'created_at', 'updated_at'
    ];
}
