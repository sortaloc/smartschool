<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'jenjang', 'NPSN', 'NDS', 'total_students', 'address', 'district_id', 'village',
        'zip_code', 'fax', 'phone', 'website', 'email', 'headmaster', 'headmaster_nip',
        'headmaster_signature', 'logo', 'rapor_distribution', 
        'expired_at', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];

    public function students()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudent');
    }
    public function teachers()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTeacher');
    }
    public function books()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolLibrary', 'school_id', 'id');
    }
    public function wall_magazines()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolWallMagazine', 'school_id', 'id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Konfigurasi\District');
    }
    public function school_years()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolYear');
    }
    public function school_year()
    {
        return $this->school_years()->where('is_active', 1);
    }
    public function getHeadmasterSignatureAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getHeadmasterSignatureBase64Attribute()
    {
        $value = $this->headmaster_signature;
        $result = null;
        if($value){
            $result = base64_encode(file_get_contents($value));
        }
        return $result;
    }
    public function getLogoAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getSingkatanJenjangAttribute()
    {
        $words = explode(" ", $this->jenjang);
        $acronym = "";
        foreach ($words as $w) {
            $acronym .= $w[0];
        }
        return strtoupper($acronym);
    }
    public function getExpiredAtAttribute($value)
    {
        return date('d-M-Y', strtotime($value));
    }
}
