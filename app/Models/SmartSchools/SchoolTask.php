<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTask extends Model
{
    protected $fillable = [
        'school_id', 'user_id', 'school_year_id', 'assessment_type_ref_id',
        'question_type', 'task_type', 'old_task_id', 'point_preference', 'max_point', 'name', 'description',
        'created_at', 'updated_at', 'is_visible'
    ];

    protected $appends = ['kunci_jawaban'];

    public function assessment_type()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolAssessmentTypeRef', 'assessment_type_ref_id', 'id');
    }
    public function old_task()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTask', 'old_task_id', 'id');
    }
    public function task_classes()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTaskClass', 'school_task_id', 'id');
    }
    public function taskLast()
    {
        return $this->task_classes()->orderBy('end_at', 'desc')->take(1);
    }
    public function results()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTaskResult', 'school_task_id', 'id');
    }
    public function questions_all()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTaskQuestion', 'school_task_id', 'id');
    }
    public function questions()
    {
        return $this->questions_all()->orderBy('number', 'asc');
    }
    public function my_class()
    {
        return $this->task_classes()->whereHas('teacher_subjects', function($q){
            $q->where('school_class_id', auth()->user()->profile->class->first()->class->id);
        });
    }
    public function my_result()
    {
        return $this->results()->where('school_student_id', auth()->user()->profile->id)->whereHas('task_class', function($q){
            $q->where('school_task_id', $this->id);
        });
    }


    //scope
    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
    public function scopeVisible($query)
    {
        return $query->where('is_visible', 1);
    }
    public function scopeRole($query)
    {
        if(!auth()->user()->hasPermissionTo('Semua Hak Akses')){
            return $query->where('user_id', auth()->user()->id);
        }
        return null;
    }
    public function scopeThisYear($query)
    {
        $year = mySchoolYear();
        return $query->where('school_year_id', $year->id);
    }
    public function scopeUtama($query)
    {
        return $query->where('task_type', 'Utama');
    }
    public function scopeSusulan($query)
    {
        return $query->where('task_type', 'Susulan');
    }
    public function scopePerbaikan($query)
    {
        return $query->where('task_type', 'Perbaikan');
    }
    public function getKunciJawabanAttribute()
    {
        $taskLast = $this->taskLast->first();
        if($taskLast->end_time < time()){
            return true;
        }else{
            return false;
        }
    }
}
