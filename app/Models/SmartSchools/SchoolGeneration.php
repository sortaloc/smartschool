<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolGeneration extends Model
{
    protected $fillable = [
        'school_id', 'year', 'created_at', 'updated_at'
    ];

    public function students()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudentClass', 'school_year_id', 'id');
    }
}
