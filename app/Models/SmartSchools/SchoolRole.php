<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRole extends Model
{
    protected $fillable = [
        'school_id', 'role_id', 'created_at', 'updated_at'
    ];
}
