<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    protected $fillable = [
        'school_id', 'start_year', 'end_year', 'semester', 'is_active', 'created_at', 'updated_at'
    ];

    public function schedules()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolClassSchedule', 'school_year_id', 'id');
    }

    public function tasks()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolTask', 'school_year_id', 'id');
    }
    public function rapors()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolRapor', 'year_id', 'id');
    }

    //scope
    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }

    public function getIsActiveAttribute($value)
    {
        if($value == 1){
            return 'Aktif';
        }
        return 'Non Aktif';
    }
}
