<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRaporCharacter extends Model
{
    protected $fillable = [
        'rapor_id', 'assessment_type_ref_id', 'description',
         'created_at', 'updated_at'
    ];

    public function assessment_type_ref()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolAssessmentTypeRef', 'assessment_type_ref_id', 'id');
    }
}
