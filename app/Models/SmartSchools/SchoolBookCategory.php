<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolBookCategory extends Model
{
    protected $fillable = [
        'school_id', 'name', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'school_id'];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function books()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolLibrary', 'category_id', 'id');
    }

    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
}
