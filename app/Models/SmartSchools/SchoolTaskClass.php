<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SchoolTaskClass extends Model
{
    protected $fillable = [
        'school_task_id', 'teacher_subjects_id', 'start_at', 'end_at', 'created_at', 'updated_at'
    ];

    protected $appends = ['start_time', 'end_time'];

    public function teacher_subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacherSubjects', 'teacher_subjects_id', 'id')->withTrashed();
    }
    public function task()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTask', 'school_task_id', 'id');
    }

    public function getStartTimeAttribute()
    {
        return strtotime($this->getOriginal('start_at'));
    }
    public function getEndTimeAttribute()
    {
        return strtotime($this->getOriginal('end_at'));
    }
    public function getStartAtAttribute($date)
    {
        return Carbon::parse($date)->isoFormat('DD MMMM Y, HH:mm');
    }
    public function getEndAtAttribute($date)
    {
        return Carbon::parse($date)->isoFormat('DD MMMM Y, HH:mm');
    }

    //scope
    public function scopeClassId($query, $class_id)
    {
        return $query->whereHas('teacher_subjects', function($q)use($class_id){
            $q->where('school_class_id', $class_id);
        });
    }
}
