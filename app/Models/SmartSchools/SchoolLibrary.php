<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolLibrary extends Model
{
    protected $fillable = [
        'school_id', 'category_id', 'book_number', 'title', 'author', 'cover', 'file', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'school_id', 'category_id'];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolBookCategory', 'category_id', 'id');
    }

    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
    public function scopeCategoryId($query, $category_id)
    {
        return $query->where('category_id', $school_id);
    }


    public function getCoverAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getFileAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
}
