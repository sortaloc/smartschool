<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolWallMagazine extends Model
{
    protected $fillable = [
        'school_id', 'user_id', 'title', 'logo', 'file', 'status', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }


    public function getLogoAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getFileAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
}
