<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolStudent extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'school_id', 'user_id', 'name', 'NIPD', 'NISN', 'religion', 'place_of_birth', 'date_of_birth', 'gender',
        'status_in_family', 'child_number', 'address', 'district_id', 'village', 'zip_code', 'phone',
        'origin_school', 'start_class_id', 'register_date', 'year_generation_id', 'photo', 'rapor', 'status', 
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $dates = ['register_date'];

    protected $hidden = ['created_at', 'updated_at', 'rapor'];

    //relation
    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School');
    }
    public function classes()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudentClass', 'school_student_id', 'id');
    }
    public function class()
    {
        return $this->classes()->where('is_active', 1)->take(1);
    }
    public function start_class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClass', 'start_class_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Konfigurasi\District');
    }
    public function generation()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolGeneration', 'year_generation_id', 'id');
    }
    public function parent()
    {
        return $this->hasOne('App\Models\SmartSchools\SchoolStudentParent', 'student_id', 'id');
    }
    public function guardian()
    {
        return $this->hasOne('App\Models\SmartSchools\SchoolStudentGuardian', 'student_id', 'id');
    }
    public function rapors()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolRapor', 'student_id', 'id');
    }
    public function raporThisYear()
    {
        $year = mySchoolYear();
        $year = $year ? $year : null;
        return $this->rapors()->where('year_id', $year->id)->where('semester', $year->semester)->take(1);
    }
    public function presences()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudentPresence', 'student_id', 'id');
    }
    public function presenceAThisYear()
    {
        return $this->presences()->whereHas('class_journal.class_schedule.school_year', function($q){
            $q->where('is_active', 1);
        })->where('presence', 'A');
    }
    public function presenceIThisYear()
    {
        return $this->presences()->whereHas('class_journal.class_schedule.school_year', function($q){
            $q->where('is_active', 1);
        })->where('presence', 'I');
    }
    public function presenceSThisYear()
    {
        return $this->presences()->whereHas('class_journal.class_schedule.school_year', function($q){
            $q->where('is_active', 1);
        })->where('presence', 'S');
    }

    //accessor
    public function getPhotoAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getRaporAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
}
