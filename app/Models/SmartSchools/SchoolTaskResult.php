<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTaskResult extends Model
{
    protected $fillable = [
        'school_task_id', 'task_class_id', 'school_student_id', 'point', 'explanation', 'status', 'finish', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'school_student_id'];

    public function task()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTask', 'school_task_id', 'id');
    }
    public function task_class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTaskClass', 'task_class_id', 'id');
    }
    public function student()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolStudent', 'school_student_id', 'id');
    }

    //scope
    public function scopeStudentId($query, $student_id)
    {
        return $query->where('school_student_id', $student_id);
    }
}
