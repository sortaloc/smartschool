<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolAnnouncement extends Model
{
    protected $fillable = [
        'school_id', 'title', 'message', 'status', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }

    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
}
