<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolStudentGuardian extends Model
{
    protected $fillable = [
        'student_id', 'name', 'address', 'district_id', 'village', 'zip_code', 'phone',
        'job', 'created_at', 'updated_at'
    ];

    //relation=
    public function district()
    {
        return $this->belongsTo('App\Models\Konfigurasi\District');
    }
}
