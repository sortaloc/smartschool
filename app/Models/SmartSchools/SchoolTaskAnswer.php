<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTaskAnswer extends Model
{
    protected $fillable = [
        'school_task_question_id', 'student_id', 'option_answer', 'text_answer', 'file_answer', 'point', 'correction', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'student_id', 'school_task_question_id', 'id', 'point', 'correction'];

    public function question()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTaskQuestion', 'school_task_question_id', 'id');
    }

    public function getFileAnswerAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
}
