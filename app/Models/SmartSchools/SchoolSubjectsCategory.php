<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolSubjectsCategory extends Model
{
    protected $fillable = [
        'school_id', 'name', 'created_at', 'updated_at'
    ];
    
}
