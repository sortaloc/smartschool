<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolSubjects extends Model
{
    protected $fillable = [
        'school_id', 'subjects_category_id', 'name', 'created_at', 'updated_at'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function subjects_category()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubjectsCategory', 'subjects_category_id', 'id');
    }
    public function class_subjects()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolClassSubjects', 'subjects_id', 'id');
    }

    //scope
    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
}
