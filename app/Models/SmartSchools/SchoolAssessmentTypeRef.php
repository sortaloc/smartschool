<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolAssessmentTypeRef extends Model
{
    protected $fillable = [
        'school_id', 'category', 'name', 'created_at', 'updated_at'
    ];

    public function scopeSchool($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
}
