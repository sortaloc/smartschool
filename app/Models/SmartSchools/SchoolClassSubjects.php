<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolClassSubjects extends Model
{
    protected $fillable = [
        'school_id', 'class_id', 'subjects_id', 'kkm', 'created_at', 'updated_at'
    ];

    protected $hidden = ['school_id', 'class_id', 'subjects_id', 'id'];
    
    public function subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubjects', 'subjects_id', 'id');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClassRef', 'class_id', 'id');
    }
    public function school()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolSchool', 'school_id', 'id');
    }
}
