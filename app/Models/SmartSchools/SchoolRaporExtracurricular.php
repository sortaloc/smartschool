<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRaporExtracurricular extends Model
{
    protected $fillable = [
        'rapor_id', 'activity', 'description',
         'created_at', 'updated_at'
    ];
}
