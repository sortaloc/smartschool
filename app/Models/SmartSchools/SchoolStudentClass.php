<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolStudentClass extends Model
{
    protected $fillable = [
        'school_student_id', 'class_id', 'school_year_id', 'is_active', 'created_at', 'updated_at'
    ];
    
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClass', 'class_id', 'id');
    }
    public function student()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolStudent', 'school_student_id', 'id');
    }
}
