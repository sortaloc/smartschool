<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTeacherFile extends Model
{
    protected $fillable = [
        'school_id', 'teacher_id', 'name', 'type', 'file', 'created_at', 'updated_at'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function teacher()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacher', 'teacher_id', 'id');
    }

    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }
    public function scopeTeacherId($query, $teacher_id)
    {
        return $query->where('teacher_id', $teacher_id);
    }


    public function getFileAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function scopeRole($query)
    {
        if(isset(auth()->user()->school_teacher->id) && !auth()->user()->hasPermissionTo('Semua Hak Akses')){
            $teacher_id = auth()->user()->school_teacher->id;
            return $query->where('teacher_id', $teacher_id);
        }
    }
}
