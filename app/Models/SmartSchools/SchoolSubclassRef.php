<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolSubclassRef extends Model
{
    protected $fillable = [
        'school_id', 'name', 'created_at', 'updated_at'
    ];

    public function classes()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolClass', 'subclass_id', 'id');
    }
}
