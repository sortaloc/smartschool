<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolAssessmentCategory extends Model
{
    protected $fillable = [
        'school_id', 'school_class_id', 'school_subjects_id', 
        'pengetahuan', 'keterampilan', 'sikap', 'created_at', 'updated_at'
    ];

    
    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClassRef', 'school_class_id', 'id');
    }
    public function subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubjects', 'school_subjects_id', 'id');
    }
    public function assessment_types()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolAssessmentType', 'assessment_category_id', 'id');
    }
}
