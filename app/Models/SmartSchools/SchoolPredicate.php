<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolPredicate extends Model
{
    protected $fillable = [
        'school_id', 'assessment_type_ref_id', 'lower_limit', 'upper_limit', 'description', 'created_at', 'updated_at'
    ];

    public function assessment_type_ref()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolAssessmentTypeRef', 'assessment_type_ref_id', 'id');
    }
}
