<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;
use App\Models\Konfigurasi\Paket;

class SchoolPaket extends Model
{
    protected $fillable = [
        'school_id', 'paket_id', 'created_at', 'updated_at'
    ];

    public function paket()
    {
        return $this->belongsTo(Paket::class, 'paket_id', 'id')->withTrashed();
    }
}
