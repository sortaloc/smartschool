<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolAdmin extends Model
{
    protected $fillable = [
        'school_id', 'user_id', 'created_at', 'updated_at'
    ];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
