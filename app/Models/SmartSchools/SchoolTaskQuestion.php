<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTaskQuestion extends Model
{
    protected $fillable = [
        'school_task_id', 'number', 'question', 'correct_answer', 'justification', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'school_task_id'];
    
    public function option()
    {
        return $this->hasOne('App\Models\SmartSchools\SchoolTaskOption', 'school_task_question_id', 'id');
    }
    public function task()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTask', 'school_task_id', 'id');
    }
    public function answers()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTaskAnswer', 'school_task_question_id', 'id');
    }
    public function your_answer()
    {
        return $this->answers()->where('student_id', auth()->user()->profile->id);
    }
}
