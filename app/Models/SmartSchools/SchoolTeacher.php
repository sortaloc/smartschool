<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolTeacher extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id', 'school_id', 'name', 'NIP', 'NUPTK', 'place_of_birth', 'date_of_birth', 'gender', 'religion',
        'structural_position', 'additional_position', 'PNS_rank', 'phone', 'photo', 'signature', 'deleted_at', 'created_at', 'updated_at'
    ];
    protected $dates =['deleted_at'];

    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function subjects()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolTeacherSubjects', 'school_teacher_id', 'id');
    }
    public function tasks()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolTask', 'user_id', 'user_id');
    }
    public function admin()
    {
        return $this->HasOne('App\Models\SmartSchools\SchoolAdmin', 'school_teacher_id', 'id');
    }
    public function getPhotoAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getSignatureAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getSignatureBase64Attribute()
    {
        $value = $this->signature;
        $result = null;
        if($value){
            $result = base64_encode(file_get_contents($value));
        }
        return $result;
    }
}
