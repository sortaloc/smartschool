<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolClassJournal extends Model
{
    protected $fillable = [
        'school_class_schedule_id', 'date', 'start_at', 'end_at', 'materi', 'note', 'created_at', 'updated_at'
    ];

    protected $dates = ['date'];
    
    public function class_schedule()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClassSchedule', 'school_class_schedule_id', 'id');
    }
    public function student_presences()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudentPresence', 'school_class_journal_id', 'id');
    }
    public function getStudentPresenceDescriptionAttribute()
    {
        if($absen = $this->student_presences->whereNotIn('presence', ['H', 'T',])->count()){
            return $absen;
        }
        return 'Lengkap';
    }

    //scope
    public function scopeRole($query)
    {
        $school_id = mySchool();
        if(isset(auth()->user()->school_teacher->id) && !auth()->user()->hasPermissionTo('Semua Hak Akses')){
            $teacher_id = auth()->user()->school_teacher->id;
            return $query->whereHas('class_schedule.teacher_subjects.teacher', function($q) use($school_id, $teacher_id){
                $q->where('school_id', $school_id)->where('id', $teacher_id);
            });
        }elseif(auth()->user()->hasPermissionTo('Semua Hak Akses')){
            return $query->whereHas('class_schedule.teacher_subjects.teacher', function($q) use($school_id){
                $q->where('school_id', $school_id);
            });
        }
    }
    public function scopeThisYear($query)
    {
        $year = mySchoolYear();
        return $query->whereHas('class_schedule', function($q) use($year){
            $q->where('school_year_id', $year->id);
        });
    }
    //
    public function scopeSchoolId($query, $school_id)
    {
        return $query->whereHas('class_schedule.school_year', function($q) use($school_id){
            $q->where('school_id', $school_id);
        });
    }
    public function scopeClassId($query, $class_id)
    {
        return $query->whereHas('class_schedule.teacher_subjects', function($q) use($class_id){
            $q->where('school_class_id', $class_id);
        });
    }
    
    //acessor
    public function getStartAtAttribute($value)
    {
        return date('H:i', strtotime($value));
    }
    public function getStartAtTimeAttribute()
    {
        return strtotime($this->getOriginal('start_at'));
    }
    public function getEndAtAttribute($value)
    {
        return date('H:i', strtotime($value));
    }
}
