<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolTeacherSubjects extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'school_teacher_id', 'school_class_id', 'school_subjects_id', 'deleted_at', 'created_at', 'updated_at'
    ];
    protected $dates =['deleted_at'];

    
    public function subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubjects', 'school_subjects_id', 'id');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClass', 'school_class_id', 'id');
    }
    public function teacher()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacher', 'school_teacher_id', 'id');
    }
    public function schedules()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolClassSchedule', 'school_teacher_subjects_id', 'id');
    }
    public function task_classes()
    {
        return $this->HasMany('App\Models\SmartSchools\SchoolTaskClass', 'teacher_subjects_id', 'id');
    }

    public function scopeRole($query)
    {
        if(!auth()->user()->hasPermissionTo('Semua Hak Akses')){
            return $query->whereHas('teacher', function($q){
                $q->where('user_id', auth()->user()->id);
            });
        }
        return null;
    }
    public function scopeSchool($query, $school_id)
    {
        return $query->whereHas('teacher', function($q) use($school_id){
            $q->where('school_id', $school_id);
        });
    }
}
