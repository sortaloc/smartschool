<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $fillable = [
        'school_id', 'class_id', 'subclass_id', 'homeroom_teacher_id', 'created_at', 'updated_at'
    ];
    protected $appends = ['class_name'];
    protected $hidden = ['class', 'sub_class'];
    
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClassRef', 'class_id', 'id');
    }
    public function sub_class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubclassRef', 'subclass_id', 'id');
    }
    public function school()
    {
        return $this->belongsTo('App\Models\SmartSchools\School', 'school_id', 'id');
    }
    public function homeroom()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacher', 'homeroom_teacher_id', 'id');
    }
    public function subjects()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTeacherSubjects', 'school_class_id', 'id');
    }
    public function all_students()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolStudentClass', 'class_id', 'id');
    }
    public function students()
    {
        return $this->all_students()->where('is_active', 1);
    }

    //scope
    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }

    //accesor
    public function getClassNameAttribute()
    {
        return $this->class->name.' '.$this->sub_class->name;
    }
}
