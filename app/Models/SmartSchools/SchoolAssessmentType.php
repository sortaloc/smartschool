<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolAssessmentType extends Model
{
    protected $fillable = [
        'school_id', 'assessment_category_id', 'assessment_type_ref_id', 'weight', 'created_at', 'updated_at'
    ];

    public function tasks()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolTasks');
    }
    public function assessment_category()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolAssessmentCategory', 'assessment_category_id', 'id');
    }
    public function assessment_type_ref()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolAssessmentTypeRef', 'assessment_type_ref_id', 'id');
    }

    public function scopeSchool($query, $school_id)
    {
        return null;
    }
}
