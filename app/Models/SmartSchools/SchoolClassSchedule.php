<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolClassSchedule extends Model
{
    protected $fillable = [
        'school_teacher_subjects_id', 'school_year_id', 'day', 'start_at', 'end_at', 'created_at', 'updated_at'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'school_year_id', 'school_teacher_subjects_id', 'id'];
    
    public function teacher_subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacherSubjects', 'school_teacher_subjects_id', 'id');
    }
    public function school_year()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolYear', 'school_year_id', 'id');
    }
    public function class_journals()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolClassJournal', 'school_class_schedule_id', 'id');
    }

    //scope
    public function scopeRole($query, $school_id )
    {
        if(isset(auth()->user()->school_teacher->id) && !auth()->user()->hasPermissionTo('Semua Hak Akses')){
            $teacher_id = auth()->user()->school_teacher->id;
            return $query->whereHas('teacher_subjects.teacher', function($q) use($school_id, $teacher_id){
                $q->where('school_id', $school_id)->where('id', $teacher_id);
            });
        }elseif(auth()->user()->hasPermissionTo('Semua Hak Akses')){
            return $query->whereHas('teacher_subjects.teacher', function($q) use($school_id){
                $q->where('school_id', $school_id);
            });
        }
    }
    public function scopeThisYear($query){
        $year = mySchoolYear();
        return $query->where('school_year_id', $year->id);
    }

    //acessor
    public function getStartAtAttribute($value)
    {
        return date('H:i', strtotime($value));
    }
    public function getEndAtAttribute($value)
    {
        return date('H:i', strtotime($value));
    }
}
