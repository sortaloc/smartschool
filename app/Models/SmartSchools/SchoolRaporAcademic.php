<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRaporAcademic extends Model
{
    protected $fillable = [
        'rapor_id', 'subjects_id', 'pengetahuan', 'keterampilan', 'final_point', 'predicate', 
         'created_at', 'updated_at'
    ];

    public function subjects()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolSubjects', 'subjects_id', 'id');
    }
}
