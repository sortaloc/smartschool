<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolRapor extends Model
{
    protected $fillable = [
        'school_id', 'student_id', 'year_id', 'class_id', 'semester', 'teacher_id',
        'sakit', 'izin', 'tanpa_keterangan', 'catatan_akademik', 'kenaikan_kelas', 'ke_kelas',
        'catatan_perkembangan_karakter', 'rapor', 'created_at', 'updated_at'
    ];

    public function student()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolStudent', 'student_id', 'id');
    }
    public function homeroom_teacher()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolTeacher', 'teacher_id', 'id');
    }
    public function class()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClass', 'class_id', 'id');
    }
    public function nextClass()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClass', 'ke_kelas', 'id');
    }
    public function academics()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolRaporAcademic', 'rapor_id', 'id');
    }
    public function pkls()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolRaporPkl', 'rapor_id', 'id');
    }
    public function extracurriculars()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolRaporExtracurricular', 'rapor_id', 'id');
    }
    public function characters()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolRaporCharacter', 'rapor_id', 'id');
    }

    //accesor
    public function getRaporAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
}
