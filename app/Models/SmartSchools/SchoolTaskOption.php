<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolTaskOption extends Model
{
    protected $fillable = [
        'school_task_question_id', 'A', 'B', 'C', 'D', 'E', 'created_at', 'updated_at'
    ];
    
    protected $hidden = ['created_at', 'updated_at'];
}
