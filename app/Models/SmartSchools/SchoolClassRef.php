<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolClassRef extends Model
{
    protected $fillable = [
        'school_id', 'name', 'created_at', 'updated_at'
    ];

    //scope
    public function scopeSchoolId($query, $school_id)
    {
        return $query->where('school_id', $school_id);
    }

    public function classes()
    {
        return $this->hasMany('App\Models\SmartSchools\SchoolClass', 'class_id', 'id');
    }
}
