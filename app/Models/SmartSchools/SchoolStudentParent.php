<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolStudentParent extends Model
{
    protected $fillable = [
        'student_id', 'father_name', 'mother_name', 'address', 'district_id', 'village', 'zip_code', 'phone',
        'father_job', 'mother_job', 'created_at', 'updated_at'
    ];

    //relation=
    public function district()
    {
        return $this->belongsTo('App\Models\Konfigurasi\District');
    }
}
