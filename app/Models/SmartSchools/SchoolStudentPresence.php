<?php

namespace App\Models\SmartSchools;

use Illuminate\Database\Eloquent\Model;

class SchoolStudentPresence extends Model
{
    protected $fillable = [
        'school_class_journal_id', 'student_id', 'presence', 'letter', 'joined_at', 'created_at', 'updated_at'
    ];

    protected $dates = ['joined_at'];

    public function class_journal()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolClassJournal', 'school_class_journal_id', 'id');
    }
    public function student()
    {
        return $this->belongsTo('App\Models\SmartSchools\SchoolStudent', 'student_id', 'id');
    }

    //accessor
    public function getLetterAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return $value;
    }
    public function getJoinedAtAttribute($value)
    {
        $value = $value ? \Carbon\Carbon::create($value) : null;
        if($this->presence != 'A'){
            if($value){
                return $value->isoFormat('HH:mm');
            }
            return $this->updated_at->isoFormat('HH:mm');
        }
        return 'belum masuk';
    }
}
