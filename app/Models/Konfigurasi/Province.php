<?php

namespace App\Models\Konfigurasi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Konfigurasi\City;

class Province extends Model
{
    protected $table = 'address_provinces';
    protected $fillable = ['name', 'p_bsni'];
    public $timestamps = false;

    public function city()
    {
        return $this->hasMany(City::class, 'province_id', 'id');
    }
}
