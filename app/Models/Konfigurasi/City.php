<?php

namespace App\Models\Konfigurasi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Konfigurasi\Province;
use App\Models\Konfigurasi\District;

class City extends Model
{
    protected $table = 'address_cities';
    protected $fillable = ['province_id', 'name', 'type', 'ibukota', 'c_bsni'];
    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id', 'id');
    }
    public function district()
    {
        return $this->hasMany(District::class, 'district_id', 'id');
    }
}
