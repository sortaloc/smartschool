<?php

namespace App\Models\Konfigurasi;

use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    protected $fillable = [
        'version', 'description', 'created_at', 'updated_at'
    ];
    public $timestamps = true;
}
