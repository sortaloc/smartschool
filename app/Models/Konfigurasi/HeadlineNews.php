<?php

namespace App\Models\Konfigurasi;

use Illuminate\Database\Eloquent\Model;

class HeadlineNews extends Model
{
    protected $fillable = [
        'title', 'headline_news', 'image', 'created_at', 'updated_at'
    ];

    public function getImageAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return null;
    }
}
