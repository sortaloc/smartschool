<?php

namespace App\Models\Konfigurasi;

use Illuminate\Database\Eloquent\Model;
use App\Models\Konfigurasi\City;

class District extends Model
{
    protected $table = 'address_districts';
    protected $fillable = ['city_id', 'name'];
    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
