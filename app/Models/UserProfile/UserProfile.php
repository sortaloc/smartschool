<?php

namespace App\Models\UserProfile;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = [
        'user_id', 'district_id', 'city_id', 'username', 'name', 'phone_number', 'gender', 'parents_phone_number', 'date_of_birth', 'school_name', 'city_id', 'address', 'kip_number', 'task_location', 'avatar'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function city()
    {
        return $this->belongsTo('App\Models\Konfigurasi\City');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\Konfigurasi\District');
    }
    public function getStatusAttribute()
    {
        $difference = $this->is_active
            ? 'Aktif'
            : 'Non Aktif';
        return $difference;
    }
    public function getAvatarAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return null;
    }
}
