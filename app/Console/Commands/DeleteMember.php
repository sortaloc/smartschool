<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Member;
use Carbon\Carbon;

class DeleteMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'member:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $member = Member::with('paket');
        $member->whereHas('paket', function ($query) use ($now) {
            $query->where('expired_at', '<', $now);
        })->delete();
    }
}
