<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Konfigurasi\Promo;
use Carbon\Carbon;

class DeletePromo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promo:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $promo = Promo::where('expired_at', '<', $now)->delete();
    }
}
