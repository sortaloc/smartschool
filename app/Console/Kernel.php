<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\CheckTransaction',
        'App\Console\Commands\DeleteMember',
        'App\Console\Commands\DeletePaket',
        'App\Console\Commands\DeletePromo'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('App\Console\Commands\CheckTransaction')->hourly();
        $schedule->command('App\Console\Commands\DeleteMember')->dailyAt('00:01');
        $schedule->command('App\Console\Commands\DeletePaket')->dailyAt('00:10');
        $schedule->command('App\Console\Commands\DeletePromo')->dailyAt('00:15');
        $schedule->command('backup:clean')->dailyAt('01:00');
        $schedule->command('backup:run')->dailyAt('01:05');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
