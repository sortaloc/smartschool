<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsTugasAlreadyTaken implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tugas = \App\Models\Guru\RekapNilai::where('tugas_id', $value)->where('siswa_id', auth()->user()->id)->first();
        if ($tugas) { 
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tugas belum diambil';
    }
}
