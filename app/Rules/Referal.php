<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Illuminate\Support\Facades\Hash;

class Referal implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $message;
    public function __construct()
    {
        $this->message = '';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(auth()->user()->username == $value){ 
            $this->message = 'Anda tidak dapat menggunakan kode referal anda sendiri';
            return false;
        }elseif(auth()->user()->referal != null){ 
            if(auth()->user()->referal == $value){
                return true;
            }
            $this->message = 'Anda sudah pernah menggunakan kode referal';
            return false;
        }else{
             return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
