<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsAllSoalFinished implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->no_soal = null;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tugas = null;
        $model = '';
        $entity = '';
        $name_space = '\\App\\Models\\Guru\\';
        $search_tugas = \App\Models\Guru\Tugas::where('id', $value)->first();
        if($search_tugas){
            if($search_tugas->type == 'Pilihan Ganda'){
                $model = $name_space.'TugasPilihan';
            }elseif($search_tugas->type == 'Essay'){
                $model = $name_space.'TugasEssay';
            }elseif($search_tugas->type == 'File'){
                $model = $name_space.'TugasFile';
            }
            $this->no_soal = $model::where('tugas_id', $value)->doesntHave('your_answer')->pluck('number')->toArray();
        }
        if (!$this->no_soal || request('force_done') == 'yes') { 
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Soal Nomor '.implode(", ", $this->no_soal).' Belum Dijawab, Yakin Ingin Selesai Sekarang ?';
    }
}
