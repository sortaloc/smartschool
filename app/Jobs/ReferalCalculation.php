<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\User;
use App\Models\User\UserFinance;
use App\Models\Marketing\Marketing;
use App\Models\Marketing\MarketingWilayah;
use App\Models\Marketing\MarketingArea;
use App\Models\Marketing\RewardMarketing;
use App\Models\Marketing\RewardUser;

class ReferalCalculation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    protected $referal;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $referal)
    {
        $this->id = $id;
        $this->referal = $referal;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $referal =$this->referal;
        $marketing_type = $this->checkReferalType();
        /*
        if($marketing_type == 'user'){
            $marketing_reward = RewardMarketing::where('jabatan', 'user')->first();
            if($marketing_reward){
                $marketing = UserFinance::whereHas('user', function($q) use($referal){
                    $q->where('username', $referal);
                })->first();
                $marketing->balance = $marketing->balance + $marketing_reward->reward;
                $marketing->save();
            }
        }
        */
        $user_reward = RewardUser::where('type', 'uang')->first();
        if($user_reward){
            $user = UserFinance::where('user_id', $id)->first();
            if($user){
                $user->balance = $user->balance + $user_reward->amount;
                $user->save();
            }else{
                UserFinance::create([
                    'user_id' => $id,
                    'balance' =>  $user_reward->amount
                ]);
            }
        }
    }
    private function checkReferalType()
    {
        $id = $this->id;
        $referal =$this->referal;
        $user = User::findOrFail($id);
        $wilayah_id = $user->profile->district_id;
        $area_id = $user->profile->district->city_id;
        $marketing_type = '';
        $marketing = Marketing::whereHas('user', function($q) use($referal){
            $q->where('username', $referal);
        })->first();
        if($marketing){
            /*
            $marketing_area = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) use($area_id) {
                $q->where('jabatan', 'area')->where('wilayah_id', $area_id);
            })->first();
            if($marketing_area){
                $marketing_type = 'area';
            }else{
                $marketing_wilayah = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) use($wilayah_id) {
                    $q->where('jabatan', 'wilayah')->where('wilayah_id', $wilayah_id);
                })->first();
                if($marketing_wilayah){
                    $marketing_type = 'wilayah';
                }else{
                    $marketing_user = Marketing::where('id', $marketing->id)->whereHas('marketing_wilayah', function($q) {
                        $q->where('jabatan', 'user')->where('wilayah_id', null);
                    })->first();
                    if($marketing_user){
                        $marketing_type = 'user';
                    }else{
                        $new = MarketingWilayah::create([
                            'marketing_id'  => $marketing->id,
                            'jabatan'       => 'user'
                        ]);
                        $marketing_type = 'user';
                    }
                }
            }
            */
        }else{
            $marketing = User::where('username', $referal)->first();
            $new_marketing = Marketing::create([
                'user_id' => $marketing->id,
            ]);
            $marketing_wilayah = MarketingWilayah::create([
                'marketing_id'  => $new_marketing->id,
                'jabatan'       => 'user'
            ]);
            $marketing_finance = UserFinance::firstOrcreate([
                'user_id' => $marketing->id
            ]);
            $marketing_type = 'user';
        }
        return $marketing_type;
    }
}
