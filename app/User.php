<?php

namespace App;

use App\Models\UserProfile\UserProfile;
use App\Models\User\UserFinance;
use App\Models\Transaction\Transaction;
use App\Models\Marketing\Marketing;
use App\Models\Mentor\Mentor;
use App\Models\Guru\Guru;
use App\Models\Events\EventTransaction;
use App\Models\Events\EventParticipant;
use App\Models\Events\EventHasCommittee;
use App\User;
use App\Member;
use App\Models\Guru\RekapNilai;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'referal', 'password', 'avatar', 'activation_token', 'is_active', 'last_session'
    ];
    protected $dates =['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'last_session'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    public function active()
    {
        $this->attributes['is_active'] = 1;
        self::save();
    }
    public function nonActive()
    {
        $this->attributes['is_active'] = 0;
        self::save();
    }
    public function profile()
    {
        return $this->hasOne(\App\Models\SmartSchools\SchoolStudent::class, 'user_id', 'id')->withTrashed();
    }
    public function school_admin()
    {
        return $this->hasOne(\App\Models\SmartSchools\SchoolAdmin::class);
    }
    public function school_teacher()
    {
        return $this->hasOne(\App\Models\SmartSchools\SchoolTeacher::class);
    }
    public function school_student()
    {
        return $this->hasOne(\App\Models\SmartSchools\SchoolStudent::class)->withTrashed();
    }
    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }
    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    //accesor
    public function getAvatarAttribute($value)
    {
        if($value){
            return \Storage::disk('s3')->url($value);
        }
        return null;
    }
}
