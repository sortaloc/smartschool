/* globals QiscusSDKCore */
// This service is to bridge QiscusSDK with this sample app

define(['service/emitter'], function(emitter) {
  var Qiscus = QiscusSDKCore
  var qiscus = new QiscusSDKCore()

  var appId = $('#genius_id').val()

  qiscus.init({
    AppId: appId,
    // baseURL: 'https://dragongo.qiscus.com',
    options: {
      loginSuccessCallback: function(authData) {
        emitter.emit('qiscus::login-success', authData)
      },
      newMessagesCallback: function(messages) {
        messages.forEach(function(it) {
          emitter.emit('qiscus::new-message', it)
          const message = it
          showDesktopNotification(message)
        })
      },
      presenceCallback: function(data) {
        var isOnline = data.split(':')[0] === '1'
        var lastOnline = new Date(Number(data.split(':')[1]))
        emitter.emit('qiscus::online-presence', {
          isOnline: isOnline,
          lastOnline: lastOnline,
        })
      },
      commentReadCallback: function(data) {
        emitter.emit('qiscus::comment-read', data)
      },
      commentDeliveredCallback: function(data) {
        emitter.emit('qiscus::comment-delivered', data)
      },
      typingCallback: function(data) {
        emitter.emit('qiscus::typing', data)
      },
    },
  })
  // qiscus.debugMode = true
  // qiscus.debugMQTTMode = true
  function showDesktopNotification (data) {
    if (!window.Notification) {
      console.log('Browser does not support notifications.');
    }else{
      // check if permission is already granted
      if (Notification.permission === 'granted') {
          // show notification here
          notifyMe(data);
      } else {
          // request permission from user
          Notification.requestPermission().then(function (p) {
              if (p === 'granted') {
                  // show notification here
                  notifyMe(data);
              } else {
                  alert('Jangan block notifikasi dari Genius !')
              }
          }).catch(function (err) {
              console.error(err);
          });
      }
    }
  }
  function notifyMe(data) {
    var username = data.username
    var useravatar = data.user_avatar
    var message = data.message
    if(username != document.getElementById("display_name").value){
      var notif = new Notification(username + ' mengirim pesan', {
        icon: useravatar,
        body: $(message).text()
      })
      notif.onclick = function(){
        window.focus();
        notif.close();
      }
    }
  }
  var conv = new showdown.Converter()

  // Here is an implementation of interceptor for semi translate
  qiscus.intercept(Qiscus.Interceptor.MESSAGE_BEFORE_SENT, function(message) {
    return message
  })
  qiscus.intercept(Qiscus.Interceptor.MESSAGE_BEFORE_RECEIVED, async function(
    message
  ) {
    const content = message.message.replace(/(qis)(cus)/im, function(_, $1, $2) {
      return `**${$1.toLowerCase()}**${$2.toLowerCase()}`
    })

    Object.assign(message, {
      message: conv.makeHtml(content),
      extras: Object.assign(message.extras || {}, { before_received: true }),
    })
    return message
  })

  return qiscus
})
