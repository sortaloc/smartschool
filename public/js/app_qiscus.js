define([
  'jquery', 'service/qiscus', 'service/route', 'service/emitter', 'service/content',
  'pages/login', 'pages/chat-list', 'pages/chat',
  'pages/users', 'pages/create-group', 'pages/profile',
  'pages/room-info'
], function (
  $, qiscus, route, emitter, $content,
  LoginPage, ChatListPage, ChatPage, 
  UserPage, CreateGroupPage, Profile,
  RoomInfoPage
) {
  window.route = route
  window.qiscus = qiscus
  var routes = [
    LoginPage,
    ChatListPage,
    ChatPage,
    UserPage,
    CreateGroupPage,
    Profile,
    RoomInfoPage
  ]
  var user_id = document.getElementById("user_id").value
  var user_email = document.getElementById("user_email").value
  var user_key = document.getElementById("user_key").value
  var display_name = document.getElementById("display_name").value
  var avatar = document.getElementById("avatar").value
  var position = document.getElementById("position").value
  if(position == 'Mentor'){
    var extras_data = JSON.parse(document.getElementById("extras").value)
    var extras = extras_data
  }else{
    var extras = {position : position}
  }
  qiscus.setUser(user_email, user_key, display_name, avatar, extras)
  .then(function (authData) {
      // On success
  })
  .catch(function (error) {
      alert('Tidak Bisa Login Karena Password Salah')
  })
  $content.html(LoginPage)
  if (!qiscus.isLogin) {
    route.replace('/login')
  }

  emitter.on('qiscus::login-success', function () {
    route.replace('/chat')
    localStorage.setItem('authdata', JSON.stringify(qiscus.userData))
  })
  emitter.on('route::change', function (location) {
    var content = routes.find(function (page) {
      return page.path === location.pathname
    })
    $content.html(content(location.state))
  })

  $('.widget-container').on('click', 'button.close-btn', function (event) {
    event.preventDefault()
    $('.widget-container').slideUp()
  })
  $('.toggle-widget-btn').on('click', function (event) {
    event.preventDefault()
    $('.widget-container').slideDown()
  })

  if (localStorage['authdata'] != null) {
    var authdata = JSON.parse(localStorage['authdata'])
    qiscus.setUserWithIdentityToken({ user: authdata })
  }
})
