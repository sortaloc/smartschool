// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDBDViKAUkfIcOF38qdG4kvGRuQrPMCQTY",
    authDomain: "genius-983aa.firebaseapp.com",
    databaseURL: "https://genius-983aa.firebaseio.com",
    projectId: "genius-983aa",
    storageBucket: "genius-983aa.appspot.com",
    messagingSenderId: "312401825955",
    appId: "1:312401825955:web:80bed668e9ae424443ef16",
    measurementId: "G-N43285BH1Z"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.requestPermission()
    .then(function () {
        return messaging.getToken()
    })
    .then(function(token) {
        sendTokenToServer(token)

})
.catch(function (err) {
});

messaging.onMessage(function(payload) {
    var notify;
    notify = new Notification(payload.notification.title,{
        body: payload.notification.body,
        icon: payload.notification.icon,
        tag: ""
    });
});

function sendTokenToServer($token){
    var localFCMToken = localStorage.getItem("localFCMToken");
    if(localFCMToken != $token){
        $.ajax({
            method: "POST",
            url: "/fcm-token",
            data: { fcm_token: $token }
        })
        .done(function( msg ) {
            localStorage.setItem("localFCMToken", $token);
        });
    }
}

