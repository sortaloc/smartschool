<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolStudentParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_student_parents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('student_id')->unsigned();
            $table->string('father_name', 100)->nullable();
            $table->string('mother_name', 100)->nullable();
            $table->string('address', 150)->nullable();
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->string('village', 100)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('father_job', 100)->nullable();
            $table->string('mother_job', 100)->nullable();
            $table->timestamps();

            $table->foreign('student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('district_id')->references('id')->on('address_districts')
                  ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_student_parents');
    }
}
