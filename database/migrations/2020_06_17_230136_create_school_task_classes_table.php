<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTaskClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_task_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_task_id')->unsigned();
            $table->bigInteger('teacher_subjects_id')->unsigned();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamps();

            $table->foreign('school_task_id')->references('id')->on('school_tasks')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('teacher_subjects_id')->references('id')->on('school_teacher_subjects')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_task_classes');
    }
}
