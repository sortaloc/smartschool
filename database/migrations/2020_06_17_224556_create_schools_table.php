<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jenjang', 150)->nullable();
            $table->string('name', 150);
            $table->enum('type', ['kemendikbud', 'kemenag'])->default('kemendikbud');
            $table->string('NPSN', 20)->nullable();
            $table->string('NDS', 50)->nullable();
            $table->integer('total_students')->default(0);
            $table->string('address', 150)->nullable();
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->string('village', 150)->nullable();
            $table->string('zip_code', 20)->nullable();
            $table->string('fax', 50)->nullable();
            $table->string('phone', 20);
            $table->string('website', 150)->nullable();
            $table->string('email', 150);
            $table->string('headmaster', 150)->nullable();
            $table->string('headmaster_nip', 150)->nullable();
            $table->string('headmaster_signature', 150)->nullable();
            $table->string('logo', 150)->nullable();
            $table->boolean('rapor_distribution')->default(0);
            $table->date('expired_at')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('address_districts')
                  ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('users')
                  ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('updated_by')->references('id')->on('users')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
