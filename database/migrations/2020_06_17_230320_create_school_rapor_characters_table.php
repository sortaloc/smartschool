<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolRaporCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_rapor_characters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rapor_id')->unsigned();
            $table->bigInteger('assessment_type_ref_id')->unsigned();
            $table->string('description', 200);
            $table->timestamps();

            $table->foreign('rapor_id')->references('id')->on('school_rapors')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assessment_type_ref_id')->references('id')->on('school_assessment_type_refs')
                  ->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_rapor_characters');
    }
}
