<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolRaporsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_rapors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->bigInteger('year_id')->unsigned();
            $table->bigInteger('class_id')->unsigned();
            $table->bigInteger('teacher_id')->unsigned();
            $table->enum('semester', ['Ganjil', 'Genap'])->nullable();
            $table->smallInteger('sakit')->default(0);
            $table->smallInteger('izin')->default(0);
            $table->smallInteger('tanpa_keterangan')->default(0);
            $table->text('catatan_akademik')->nullable();
            $table->enum('kenaikan_kelas', ['Semester', 'Naik Kelas', 'Tidak Naik Kelas'])->nullable();
            $table->bigInteger('ke_kelas')->unsigned()->nullable();
            $table->text('catatan_perkembangan_karakter')->nullable();
            $table->string('rapor', 150)->nullable();
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('year_id')->references('id')->on('school_years')
                  ->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('class_id')->references('id')->on('school_classes')
                  ->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('teacher_id')->references('id')->on('school_teachers')
                  ->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('ke_kelas')->references('id')->on('school_classes')
                  ->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_rapors');
    }
}
