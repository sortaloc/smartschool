<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolRaporExtracurricularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_rapor_extracurriculars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rapor_id')->unsigned();
            $table->string('activity', 100);
            $table->string('description', 150);
            $table->timestamps();

            $table->foreign('rapor_id')->references('id')->on('school_rapors')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_rapor_extracurriculars');
    }
}
