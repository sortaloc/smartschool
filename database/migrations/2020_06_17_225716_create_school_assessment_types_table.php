<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolAssessmentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_assessment_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('assessment_category_id')->unsigned();
            $table->bigInteger('assessment_type_ref_id')->unsigned();
            $table->tinyInteger('weight')->default(1);
            $table->timestamps();

            $table->foreign('assessment_category_id')->references('id')->on('school_assessment_categories')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assessment_type_ref_id')->references('id')->on('school_assessment_type_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_assessment_types');
    }
}
