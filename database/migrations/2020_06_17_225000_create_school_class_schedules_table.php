<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolClassSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_class_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_teacher_subjects_id')->unsigned();
            $table->bigInteger('school_year_id')->unsigned();
            $table->enum('day', ['SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU', 'MINGGU']);
            $table->time('start_at')->nullable();
            $table->time('end_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('school_teacher_subjects_id')->references('id')->on('school_teacher_subjects')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_year_id')->references('id')->on('school_years')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_class_schedules');
    }
}
