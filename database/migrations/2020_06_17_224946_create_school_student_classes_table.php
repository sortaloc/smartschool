<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolStudentClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_student_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_student_id')->unsigned();
            $table->bigInteger('class_id')->unsigned();
            $table->bigInteger('school_year_id')->unsigned();
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('school_student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('class_id')->references('id')->on('school_classes')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_year_id')->references('id')->on('school_years')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_student_classes');
    }
}
