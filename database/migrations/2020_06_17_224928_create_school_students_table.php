<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('school_id')->unsigned();
            $table->string('name', 150);
            $table->string('NIPD', 50)->nullable();
            $table->string('NISN', 50)->nullable();
            $table->enum('religion', ['ISLAM', 'KRISTEN', 'KATHOLIK', 'HINDU', 'BUDHA', 'KHONG HUCU'])->nullable();
            $table->string('place_of_birth', 50)->nullable();
            $table->date('date_of_birth', 50)->nullable();
            $table->enum('gender', ['Laki-laki', 'Perempuan'])->nullable();
            $table->string('status_in_family', 50)->nullable();
            $table->string('child_number', 50)->nullable();
            $table->string('address', 150)->nullable();
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->string('village', 100)->nullable();
            $table->string('zip_code', 10)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('origin_school', 50)->nullable();
            $table->bigInteger('start_class_id')->unsigned()->nullable();
            $table->date('register_date')->nullable();
            $table->bigInteger('year_generation_id')->unsigned()->nullable();
            $table->string('photo', 150)->nullable();
            $table->string('rapor', 150)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('district_id')->references('id')->on('address_districts')
                  ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('year_generation_id')->references('id')->on('school_generations')
                  ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_students');
    }
}
