<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTeacherSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_teacher_subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_teacher_id')->unsigned();
            $table->bigInteger('school_class_id')->unsigned();
            $table->bigInteger('school_subjects_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('school_teacher_id')->references('id')->on('school_teachers')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_class_id')->references('id')->on('school_classes')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_subjects_id')->references('id')->on('school_subjects')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_teacher_subjects');
    }
}
