<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('school_year_id')->unsigned();
            $table->bigInteger('assessment_type_ref_id')->unsigned();
            $table->enum('question_type', ['Pilihan Ganda', 'Essay', 'File']);
            $table->enum('task_type', ['Utama', 'Susulan', 'Perbaikan'])->default('Utama');
            $table->bigInteger('old_task_id')->unsigned()->nullable();
            $table->enum('point_preference', ['Terbaik', 'Perbaikan', 'Maksimal Nilai'])->nullable();
            $table->smallInteger('max_point')->nullable();
            $table->string('name', 200);
            $table->string('description', 200);
            $table->boolean('is_visible')->default(0);
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_year_id')->references('id')->on('school_years')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assessment_type_ref_id')->references('id')->on('school_assessment_type_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('old_task_id')->references('id')->on('school_tasks')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_tasks');
    }
}
