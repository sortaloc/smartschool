<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolStudentPresencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_student_presences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_class_journal_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->enum('presence', ['H', 'I', 'T', 'A'])->default('A');
            $table->string('letter', 150)->nullable();
            $table->time('joined_at')->nullable();
            $table->timestamps();

            $table->foreign('school_class_journal_id')->references('id')->on('school_class_journals')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_student_presences');
    }
}
