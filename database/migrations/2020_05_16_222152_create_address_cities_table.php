<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('province_id')->unsigned();
            $table->string('name', 100)->index();
            $table->enum('type', ['Kota', 'Kabupaten']);
            $table->string('ibukota', 100)->index()->nullable();
            $table->char('c_bsni', 5);
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('address_provinces')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_cities');
    }
}
