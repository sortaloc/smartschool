<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolAssessmentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_assessment_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('school_class_id')->unsigned();
            $table->bigInteger('school_subjects_id')->unsigned();
            $table->tinyInteger('pengetahuan')->default(0);
            $table->tinyInteger('keterampilan')->default(0);
            $table->tinyInteger('sikap')->default(0);
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_class_id')->references('id')->on('school_class_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_subjects_id')->references('id')->on('school_subjects')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_assessment_categories');
    }
}
