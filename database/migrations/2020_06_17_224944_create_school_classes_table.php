<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('class_id')->unsigned();
            $table->bigInteger('subclass_id')->unsigned();
            $table->bigInteger('homeroom_teacher_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('class_id')->references('id')->on('school_class_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subclass_id')->references('id')->on('school_subclass_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('homeroom_teacher_id')->references('id')->on('school_teachers')
                  ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_classes');
    }
}
