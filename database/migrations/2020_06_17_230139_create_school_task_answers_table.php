<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTaskAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_task_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_task_question_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->enum('option_answer', ['A', 'B', 'C', 'D', 'E'])->nullable();
            $table->text('text_answer')->nullable();
            $table->string('file_answer', 150)->nullable();
            $table->smallInteger('point')->nullable()->default(0);
            $table->text('correction')->nullable();
            $table->timestamps();

            $table->foreign('school_task_question_id')->references('id')->on('school_task_questions')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_task_answers');
    }
}
