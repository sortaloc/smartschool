<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTaskOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_task_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_task_question_id')->unsigned();
            $table->text('A');
            $table->text('B');
            $table->text('C');
            $table->text('D');
            $table->text('E')->nullable();
            $table->timestamps();

            $table->foreign('school_task_question_id')->references('id')->on('school_task_questions')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_task_options');
    }
}
