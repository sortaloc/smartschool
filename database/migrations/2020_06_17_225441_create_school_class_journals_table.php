<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolClassJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_class_journals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_class_schedule_id')->unsigned();
            $table->date('date')->nullable();
            $table->time('start_at')->nullable();
            $table->time('end_at')->nullable();
            $table->string('materi', 150)->nullable();
            $table->string('note', 150)->nullable();
            $table->timestamps();
            
            $table->foreign('school_class_schedule_id')->references('id')->on('school_class_schedules')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_class_journals');
    }
}
