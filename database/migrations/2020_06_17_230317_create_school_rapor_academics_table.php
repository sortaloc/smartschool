<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolRaporAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_rapor_academics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rapor_id')->unsigned();
            $table->bigInteger('subjects_id')->unsigned();
            $table->float('pengetahuan', 5, 2);
            $table->float('keterampilan', 5, 2);
            $table->float('final_point', 5, 2);
            $table->string('predicate', 10);
            $table->timestamps();

            $table->foreign('rapor_id')->references('id')->on('school_rapors')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subjects_id')->references('id')->on('school_subjects')
                  ->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_rapor_academics');
    }
}
