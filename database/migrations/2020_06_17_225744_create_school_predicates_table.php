<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolPredicatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_predicates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_id')->unsigned();
            $table->bigInteger('assessment_type_ref_id')->unsigned();
            $table->tinyInteger('lower_limit');
            $table->tinyInteger('upper_limit');
            $table->string('description', 150);
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('schools')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assessment_type_ref_id')->references('id')->on('school_assessment_type_refs')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_predicates');
    }
}
