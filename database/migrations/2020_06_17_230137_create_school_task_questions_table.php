<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTaskQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_task_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_task_id')->unsigned();
            $table->integer('number');
            $table->text('question');
            $table->enum('correct_answer', ['A', 'B', 'C', 'D', 'E'])->nullable();
            $table->text('justification')->nullable();
            $table->timestamps();

            $table->foreign('school_task_id')->references('id')->on('school_tasks')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_task_questions');
    }
}
