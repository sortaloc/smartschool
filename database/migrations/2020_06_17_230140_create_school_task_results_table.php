<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolTaskResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_task_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('school_task_id')->unsigned();
            $table->bigInteger('task_class_id')->unsigned();
            $table->bigInteger('school_student_id')->unsigned();
            $table->float('point', 5,2)->default(0);
            $table->string('explanation', 150)->nullable();
            $table->enum('status', ['Proses', 'Sedang Dikoreksi', 'Selesai'])->default('Proses');
            $table->timestamp('finish')->nullable();
            $table->timestamps();

            $table->foreign('school_task_id')->references('id')->on('school_tasks')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('task_class_id')->references('id')->on('school_task_classes')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('school_student_id')->references('id')->on('school_students')
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_task_results');
    }
}
