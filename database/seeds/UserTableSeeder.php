<?php

use App\Models\UserProfile\UserProfile;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username'  => "admin",
            'email'     => "admin@gmail.com",
            'level'     => "Super Admin",
            'is_active' => 1,
            'password'  => bcrypt("admin"),
        ]);
        $profile = UserProfile::create([
            'name'              => "admin",
            'phone_number'      => "087730705563",
            'user_id'           => $user->id,
        ]);

        $user->assignRole("Super Admin");
    }
}
