<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => "Super Admin"],
            ['name' => "Admin"],
            ['name' => "Super Admin Sekolah"],
            ['name' => "Admin Sekolah"],
            ['name' => "Siswa"]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }

        $can = ['Lihat', 'Tambah', 'Ubah', 'Hapus'];
        $type = "SMARTSCHOOL";
        $guard = 'web';
        $permission = [
            'Info Sekolah', 'Hak Akses', 'Admin', 'Angkatan', 'Tahun Ajaran',
            'Kelas', 'Mata Pelajaran', 'Info Siswa', 'Info Guru', 'Jurnal Kelas',
            'Jadwal Kelas', 'Kategori Penilaian', 'Jenis Penilaian',
            'Predikat', 'Soal', 'Rapor', 'Materi Guru', 'Buku', 'Mading', 'Pengumuman'
        ];

        Permission::create([
            'name' => 'Semua Hak Akses',
            'guard_name' => $guard,
            'type' => $type
        ]);

        foreach ($permission as $p) {
            foreach ($can as $key => $value) {
                Permission::create([
                    'name' => $value.' '.$p,
                    'guard_name' => $guard,
                    'type' => $type
                ]);
            }
        }
        //$permissions = Permission::where('type', 'SMARTSCHOOL')->pluck('name');
        Role::findByName('Super Admin Sekolah')->givePermissionTo('Semua Hak Akses');
    }
}
